package com.scorevine.fb_graph_api;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

/**
 * Created by adrianjez on 23.12.2016.
 */

public class FBAsync extends AsyncTask<Void, Void, Void> {

    FBBase [] fbBase = null;
    View mProgress = null;
    Context context = null;

    public FBAsync(View progress, FBBase ... t){
        fbBase = t;
        this.mProgress = progress;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(mProgress != null) mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if(mProgress != null) mProgress.setVisibility(View.GONE);
    }

    @Override
    protected Void doInBackground(Void... params) {
        if(this.fbBase != null){
            for(FBBase fb : this.fbBase){
                if(fb != null){
                    fb.update();
                }
            }
        }
        return null;
    }
}
