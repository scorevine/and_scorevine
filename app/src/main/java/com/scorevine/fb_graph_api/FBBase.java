package com.scorevine.fb_graph_api;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by adrianjez on 23.12.2016.
 */

public abstract class FBBase {

    protected Context context;
    protected String key;
    private Bundle args = null;

    public FBBase(Context context, String key) {
        this.context = context;
        this.key = key;
    }

    public FBBase(Context context, String key, String next){
        this.context = context;
        this.key = key;
        this.args = new Bundle();
        args.putInt("limit", 25);
        args.putString("next", next);
    }

    public abstract void parseResponse(GraphResponse response);

    public void update(){
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                key, args, HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        parseResponse(response);
                    }
                }
        ).executeAndWait();
    }


    protected String getStr(JSONObject json, String key) {
        if (json.has(key) && !json.isNull(key))
            try {
                return json.getString(key);
            } catch (JSONException e) {
                Log.e("WSBase", e.getMessage());
            }
        return "";
    }

    /** picture": {
     "data": {
     "is_silhouette": false,
     "url": "https:\/\/scontent.xx.fbcdn.net\/v\/t1.0-1\/c206.44.545.545\/s50x50\/552670_36129662393"
*/
     protected String getPicture(JSONObject jsonObject, String key){
         try {
             JSONObject picture = jsonObject.getJSONObject(key);
             if((!picture.isNull("data")) && picture.getJSONObject("data").has("url")){
                 return picture.getJSONObject("data").getString("url");
             }
         } catch (JSONException e) {
             e.printStackTrace();
         }
         return null;
     }
}
