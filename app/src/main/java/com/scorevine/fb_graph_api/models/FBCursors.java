package com.scorevine.fb_graph_api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by adrianjez on 23.12.2016.
 */

public class FBCursors {

    @SerializedName("before")
    @Expose
    String before;

    @SerializedName("after")
    @Expose
    String after;

    public String getBefore() {
        return before;
    }

    public void setBefore(String before) {
        this.before = before;
    }

    public String getAfter() {
        return after;
    }

    public void setAfter(String after) {
        this.after = after;
    }
}
