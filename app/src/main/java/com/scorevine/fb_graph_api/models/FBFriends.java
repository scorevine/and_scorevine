package com.scorevine.fb_graph_api.models;

import java.util.ArrayList;

/**
 * Created by adrianjez on 23.12.2016.
 */

public class FBFriends {

    private ArrayList<FBUser> data;
    private FBPaging paging;

    public ArrayList<FBUser> getData() {
        return data;
    }

    public void setData(ArrayList<FBUser> data) {
        this.data = data;
    }

    public FBPaging getPaging() {
        return paging;
    }

    public void setPaging(FBPaging paging) {
        this.paging = paging;
    }
}
