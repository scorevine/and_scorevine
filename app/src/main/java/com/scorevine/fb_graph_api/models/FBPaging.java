package com.scorevine.fb_graph_api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by adrianjez on 23.12.2016.
 */

public class FBPaging {

    @SerializedName("cursors")
    @Expose
    FBCursors cursors;

    @SerializedName("next")
    @Expose
    String next;

    @SerializedName("previous")
    @Expose
    String previous;

    public FBCursors getCursors() {
        return cursors;
    }

    public void setCursors(FBCursors cursors) {
        this.cursors = cursors;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }
}
