package com.scorevine.fb_graph_api.getfriends;

import android.content.Context;

import com.facebook.GraphResponse;
import com.google.gson.GsonBuilder;
import com.scorevine.fb_graph_api.FBBase;
import com.scorevine.fb_graph_api.models.FBFriends;
import com.scorevine.fb_graph_api.models.FBPaging;
import com.scorevine.fb_graph_api.models.FBUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by adrianjez on 23.12.2016.
 */

public class FBGetFriends extends FBBase{

    public interface OnFBGetFriendsListener {
        void onReceived(FBFriends friends);
        void onError();
    }

    private OnFBGetFriendsListener mCallback;


    public FBGetFriends(Context context, OnFBGetFriendsListener callback){
        super(context, "/" + "1417080614991149" + "/taggable_friends");
        this.mCallback = callback;
    }

    public FBGetFriends(Context context, String next, OnFBGetFriendsListener callback){
        super(context, "/" + "1417080614991149" + "/taggable_friends", next);
        this.mCallback = callback;
    }

    @Override
    public void parseResponse(GraphResponse response) {
        ArrayList<FBUser> users = new ArrayList<>();
        try {
            JSONArray jsonArray = response.getJSONObject().getJSONArray("data");
            for(int i = 0; i<jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                FBUser user = new FBUser();
                user.setId(getStr(jsonObject, "id"));
                user.setName(getStr(jsonObject, "name"));
                user.setPicture(getPicture(jsonObject, "picture"));
                users.add(user);
            }


            FBFriends fbFriends = new FBFriends();
            fbFriends.setData(users);
            fbFriends.setPaging(new GsonBuilder().create().
                    fromJson(response.getJSONObject().getJSONObject("paging").toString(), FBPaging.class));
            if(mCallback != null){
                mCallback.onReceived(fbFriends);
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(mCallback != null) mCallback.onError();
    }

}
