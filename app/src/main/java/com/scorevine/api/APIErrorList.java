package com.scorevine.api;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Yoga on 2016-12-09.
 */

public class APIErrorList {

    @SerializedName("errors")
    @Expose
    private List<APIError> error = null;
    @Nullable
    @SerializedName("suggestedUsername")
    @Expose
    private String suggestedUsername = null;
    @Nullable
    @SerializedName("failLoginMessage")
    @Expose
    private String failLoginMessage;
    @Nullable
    @SerializedName("retreivalMethods")
    @Expose
    private List<String> retreivalMethods;

    /**
     * @return The error
     */
    public List<APIError> getError() {
        return error;
    }

    /**
     * @param error The error
     */
    public void setError(List<APIError> error) {
        this.error = error;
    }

    @Nullable
    public String getSuggestedUsername() {
        return suggestedUsername;
    }

    public void setSuggestedUsername(@Nullable String suggestedUsername) {
        this.suggestedUsername = suggestedUsername;
    }

    @Nullable
    public String getFailLoginMessage() {
        return failLoginMessage;
    }

    @Nullable
    public List<String> getRetreivalMethods() {
        return retreivalMethods;
    }
}
