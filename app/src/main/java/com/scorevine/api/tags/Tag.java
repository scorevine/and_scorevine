package com.scorevine.api.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by adrianjez on 18.12.2016.
 * TODO - further changes...
 */

public class Tag implements Serializable, Parcelable {

    public static final String TAG_ARRAY_BUNDLE_KEY = "TagArrayBundleKey";

    public enum TagType{
        @SerializedName("review")
        review
    }

    @SerializedName("id")
    @Expose
    long id;

    @SerializedName("tag")
    @Expose
    private String tag;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("scoreAvg")
    @Expose
    private String scoreAvg;

    @SerializedName("currentScore")
    @Expose
    private int currentScore;

    @SerializedName("isFollowed")
    @Expose
    private boolean isFollowed;

    @SerializedName("vines")
    @Expose
    private int vines;

    @SerializedName("type")
    @Expose
    private TagType type;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getVines() {
        return vines;
    }

    public void setVines(int vines) {
        this.vines = vines;
    }

    public String getScoreAvg() {
        return scoreAvg;
    }

    public void setScoreAvg(String scoreAvg) {
        this.scoreAvg = scoreAvg;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getFollowed() {
        return isFollowed;
    }

    public void setFollowed(boolean followed) {
        isFollowed = followed;
    }

    public TagType getType() {
        return type;
    }

    public void setType(TagType type) {
        this.type = type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.tag);
        dest.writeString(this.image);
        dest.writeString(this.scoreAvg);
        dest.writeInt(this.currentScore);
        dest.writeByte(this.isFollowed ? (byte) 1 : (byte) 0);
        dest.writeInt(this.vines);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
    }

    public Tag() {
    }

    protected Tag(Parcel in) {
        this.id = in.readLong();
        this.tag = in.readString();
        this.image = in.readString();
        this.scoreAvg = in.readString();
        this.currentScore = in.readInt();
        this.isFollowed = in.readByte() != 0;
        this.vines = in.readInt();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : TagType.values()[tmpType];
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel source) {
            return new Tag(source);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };
}
