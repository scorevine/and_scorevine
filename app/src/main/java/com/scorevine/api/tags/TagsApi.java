package com.scorevine.api.tags;

import com.scorevine.api.ApiConstants;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by adrianjez on 20.12.2016.
 */

public interface TagsApi {

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @GET(ApiConstants.API_TAGS_TRENDING)
    Call<TagsResponse> getTrendingTags(@Header("X-access-token") String encodedToken);

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @POST(ApiConstants.API_TAGS_FOLLOW)
    Call<Void> follow(@Header("X-access-token") String encodedToken, @Path("id") Long tagID, @Body RequestBody body);

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @HTTP(method = "DELETE", path = (ApiConstants.API_TAGS_FOLLOW), hasBody = true)
    Call<Void> unfollow(@Header("X-access-token") String encodedToken, @Path("id") Long tagID, @Body RequestBody body);

}
