package com.scorevine.api.tags;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by adrianjez on 20.12.2016.
 */

public class TagsResponse {

    @SerializedName("results")
    @Expose
    private ArrayList<Tag> results = null;

    @SerializedName("pages")
    @Expose
    private int pages;

    public ArrayList<Tag> getResults() {
        return results;
    }

    public void setResults(ArrayList<Tag> results) {
        this.results = results;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
