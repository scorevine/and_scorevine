package com.scorevine.api.contact;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Yoga on 2016-12-27.
 */

public class Reasons {

    @SerializedName("reasons")
    @Expose
    private List<Reason> reasons = null;

    public List<Reason> getReasons() {
        return reasons;
    }

    public void setReasons(List<Reason> reasons) {
        this.reasons = reasons;
    }

}
