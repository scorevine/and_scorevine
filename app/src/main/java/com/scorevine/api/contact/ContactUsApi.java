package com.scorevine.api.contact;

import android.support.annotation.Nullable;

import com.scorevine.api.ApiConstants;
import com.scorevine.api.StatusResponse;
import com.scorevine.api.SuccessResponse;
import com.scorevine.api.signing.LoginResponse;

import java.security.cert.CertPathValidatorException;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by NIKAK
 * Ready4S
 * on 17.12.2016 18:10
 */
public interface ContactUsApi {

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @POST(ApiConstants.API_CONTACT_SEND_MESSAGE)
    Call<ResponseBody> sendMessage(@Header("X-access-token") String encodedToken, @Query("reason_id") int reasonId, @Query("heading") String heading, @Query("text") String text);


    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @GET(ApiConstants.API_CONTACT_REASONS_TYPE)
    Call<Reasons> getContactReasonByType(@Path("type") String type, @Header("X-access-token") String encodedToken);


    @Headers({ApiConstants.HEADER_ACCEPT})
    @POST(ApiConstants.API_CONTACT_SEND_TAG)
    @Multipart
    Call<StatusResponse> sendTagSuggestion(@Header("X-access-token") String encodedToken,
                                           @Part("tag_short_name") RequestBody tagShortName,
                                           @Part("tag_name") RequestBody tagName,
                                           @Part("description") RequestBody description,
                                           @Nullable @Part("website") RequestBody website,
                                           @Part MultipartBody.Part photo,
                                           @Part("link") RequestBody link,
                                           @Part("reason_id") RequestBody reasonId,
                                           @Part("look_for_description") RequestBody lookForDescription,
                                           @Nullable @Part("is_adult") RequestBody isAdult);


}