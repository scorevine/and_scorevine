package com.scorevine.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Agnieszka Duleba on 2016-12-21.
 */

public class StatusResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;

    public Boolean getSuccess() {
        return status;
    }

    public void setSuccess(Boolean status) {
        this.status = status;
    }
}
