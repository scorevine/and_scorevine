package com.scorevine.api.profile;

import android.support.annotation.Nullable;

import com.scorevine.api.ApiConstants;
import com.scorevine.api.StatusResponse;
import com.scorevine.api.SuccessResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public interface UserProfileInterface {

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @GET(ApiConstants.USER_PROFILE)
    Call<UserProfileResponse> getUserProfile(@Header("X-access-token") String token);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @PUT(ApiConstants.USER_PROFILE)
    @FormUrlEncoded
    Call<SuccessResponse> updateUserProfile(@Header("X-access-token") String token,
                                            @Nullable @Field("birth_date") String birthDate,
                                            @Nullable @Field("gender") String gender,
                                            @Nullable @Field("country") String country,
                                            @Nullable @Field("first_name") String firstName,
                                            @Nullable @Field("surname") String surname,
                                            @Nullable @Field("story") String story);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @PUT(ApiConstants.USER_CHANGE_EMAIL)
    @FormUrlEncoded
    Call<ChangeEmailResponse> changeEmail(@Header("X-access-token") String token, @Field("email") String email, @Nullable @Field("password") String password);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @PUT(ApiConstants.USER_CHANGE_PASSWORD)
    @FormUrlEncoded
    Call<StatusResponse> changePassword(@Header("X-access-token") String token, @Nullable @Field("oldPassword") String oldPassword, @Field("newPassword") String newPassword);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @POST(ApiConstants.USER_CHANGE_MOBILE)
    @FormUrlEncoded
    Call<SuccessResponse> changeMobile(@Header("X-access-token") String token, @Field("mobile") String mobile, @Nullable @Field("password") String password);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @PUT(ApiConstants.USER_CHANGE_MOBILE)
    @FormUrlEncoded
    Call<SuccessResponse> sendCode(@Header("X-access-token") String token, @Field("code") String code );

}
