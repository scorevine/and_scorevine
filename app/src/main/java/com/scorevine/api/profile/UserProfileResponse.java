package com.scorevine.api.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Agnieszka Duleba on 2016-12-19.
 */

public class UserProfileResponse {


    @SerializedName("user")
    @Expose
    private UserProfile user;

    public UserProfile getUser() {
        return user;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }

}
