package com.scorevine.api;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class ApiConstants {

    public static final String API_BASE_URL = "http://35.164.200.28/api/";
    public static final String HEADER_ACCEPT = "Accept: application/json";
    public static final String HEADER_CONTENT_TYPE = "Content-Type: application/x-www-form-urlencoded";

    public static final String API_RESET_PASSWORD = "password/reset";
    public static final String API_UNLOCK = "account/unlock";


    public static final String API_SEND_VERIFICATION = "send-verification";

    public static final String API_LOGIN = "login";
    public static final String API_FACEBOOK_LOGIN = "facebook/login";
    public static final String API_REGISTER = "register";
    public static final String API_CATEGORIES = "categories";
    public static final String API_FOLLOW_CATEGORIES = "categories/follow";

    public static final String API_CONTACT_REASONS_TYPE = "contact/reasons/{type}";
    public static final String API_CONTACT_REASONS_MESSAGE = "message";
    public static final String API_CONTACT_REASONS_TAGS = "tags";
    public static final String API_CONTACT_REASONS_REPORT = "reported";

    public static final String API_CONTACT_SEND_MESSAGE = "contact/message";
    public static final String API_CONTACT_SEND_TAG = "contact/tag";


    public static final String API_DELETE_CUSTOM_CATEGORY = "categories/custom";

    public static final String API_CUSTOM_CATEGORIES = "categories/custom";

    public static final String USER_PROFILE = "user/profile";
    public static final String USER_CHANGE_EMAIL = "user/email/change";
    public static final String USER_CHANGE_PASSWORD = "user/password/change";
    public static final String USER_CHANGE_MOBILE = "user/mobile/change";

    /** TAGS **/
    public static final String API_TAGS_TRENDING = "tags/trending";
    public static final String API_TAGS_FOLLOW = "tags/{id}/follow";

    /** POSTS **/
    public static final String API_POSTS_NEWFEED = "posts/newsfeed";


}
