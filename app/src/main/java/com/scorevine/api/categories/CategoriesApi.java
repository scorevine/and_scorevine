package com.scorevine.api.categories;

import com.scorevine.api.ApiConstants;
import com.scorevine.api.signing.FollowCategoriesResponse;
import com.scorevine.ui.signing.configuration.adapters.CategoriesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by NIKAK
 * Ready4S
 * on 17.12.2016 21:22
 */
public interface CategoriesApi {

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @GET(ApiConstants.API_CATEGORIES)
    Call<CategoriesResponse> getCategories(@Header("X-access-token") String encodedToken);

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @POST(ApiConstants.API_FOLLOW_CATEGORIES)
    Call<FollowCategoriesResponse> followCategories(@Header("X-access-token") String encodedToken, @Query("categories") String subcategories);

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @POST(ApiConstants.API_DELETE_CUSTOM_CATEGORY)
    Call<FollowCategoriesResponse> deleteCategories(@Header("X-access-token") String encodedToken, @Query("categories") String customCategoriesList);

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @GET(ApiConstants.API_CUSTOM_CATEGORIES)
    Call<CustomCategoriesResponse> getCustomCategories(@Header("X-access-token") String encodedToken);


}
