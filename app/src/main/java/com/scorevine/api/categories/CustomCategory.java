package com.scorevine.api.categories;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomCategory implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category")
    @Expose
    private String category;
    @Nullable
    private Boolean isChecked = false;

    protected CustomCategory(Parcel in) {
        this.category = in.readString();
        this.id = in.readInt();
        this.isChecked = in.readInt() == 1;
    }

    public static final Creator<CustomCategory> CREATOR = new Creator<CustomCategory>() {
        @Override
        public CustomCategory createFromParcel(Parcel in) {
            return new CustomCategory(in);
        }

        @Override
        public CustomCategory[] newArray(int size) {
            return new CustomCategory[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Nullable
    public Boolean isChecked() {
        if (isChecked == null) {
            isChecked = false;
        }
        return isChecked;
    }

    public void setChecked(@Nullable Boolean checked) {
        isChecked = checked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(category);
        parcel.writeInt(id);
        parcel.writeInt(isChecked ? 1 : 0);
    }

    @Override
    public String toString() {
        return "CustomCategory{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
