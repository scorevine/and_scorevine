package com.scorevine.api.categories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by weronikapapkouskaya on 19.12.2016.
 */


public class CustomCategoriesResponse {

    @SerializedName("results")
    @Expose
    private List<CustomCategory> customCategories;

    public List<CustomCategory> getCustomCategories() {
        return customCategories;
    }

    public void setCustomCategories(List<CustomCategory> customCategories) {
        this.customCategories = customCategories;
    }

    @Override
    public String toString() {
        return "CustomCategoriesResponse{" +
                "customCategories=" + customCategories +
                '}';
    }
}


