package com.scorevine.api.signing;

import java.io.Serializable;

/**
 * Created by weronikapapkouskaya on 21.12.2016.
 */

public class ResetPasswordSendCodeRequest implements Serializable {

    private String username;
    private String password;
    private String code;


    public ResetPasswordSendCodeRequest(String username, String password, String code) {
        this.username = username;
        this.password = password;
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
