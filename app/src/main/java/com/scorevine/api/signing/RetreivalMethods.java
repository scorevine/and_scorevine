package com.scorevine.api.signing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by weronikapapkouskaya on 21.12.2016.
 */

public class RetreivalMethods {


    @SerializedName("retreivalMethods")
    @Expose
    private List<String> retreivalMethods = null;

    public List<String> getRetreivalMethods() {
        return retreivalMethods;
    }

    public void setRetreivalMethods(List<String> retreivalMethods) {
        this.retreivalMethods = retreivalMethods;
    }

    @Override
    public String toString() {
        return "RetreivalMethods{" +
                "retreivalMethods=" + retreivalMethods +
                '}';
    }
}
