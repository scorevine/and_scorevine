package com.scorevine.api.signing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Yoga on 2016-12-09.
 */

public class BlockedError {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("retreivalMethods")
    @Expose
    private List<String> retreivalMethods = null;

    /**
     *
     * @return
     * The error
     */
    public String getError() {
        return error;
    }

    /**
     *
     * @param error
     * The error
     */
    public void setError(String error) {
        this.error = error;
    }

    /**
     *
     * @return
     * The retreivalMethods
     */
    public List<String> getRetreivalMethods() {
        return retreivalMethods;
    }

    /**
     *
     * @param retreivalMethods
     * The retreivalMethods
     */
    public void setRetreivalMethods(List<String> retreivalMethods) {
        this.retreivalMethods = retreivalMethods;
    }

}