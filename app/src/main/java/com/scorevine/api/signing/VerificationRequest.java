package com.scorevine.api.signing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */
public class VerificationRequest {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("resend")
    @Expose
    private boolean resend;

    public VerificationRequest(String username, String email, String mobile) {

        this.username = username;
        this.email = email;
        this.mobile = mobile;
    }
    public VerificationRequest(String username, String email, String mobile, boolean resend) {

        this.username = username;
        this.email = email;
        this.mobile = mobile;
        this.resend = resend;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isResend() {
        return resend;
    }

    public void setResend(boolean resend) {
        this.resend = resend;
    }

    @Override
    public String toString() {
        return "phone: " + mobile;
    }
}
