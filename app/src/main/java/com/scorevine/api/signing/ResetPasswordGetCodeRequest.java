package com.scorevine.api.signing;

import java.io.Serializable;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class ResetPasswordGetCodeRequest implements Serializable {


    private String username;

    private String retreivalMethod;

    public ResetPasswordGetCodeRequest(String username, String method) {
        this.username = username;
        this.retreivalMethod = method;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String email) {
        this.username = email;
    }

    public String getMethod() {
        return retreivalMethod;
    }

    public void setMethod(String method) {
        this.retreivalMethod = method;
    }
}
