package com.scorevine.api.signing;

import android.support.annotation.Nullable;

import com.scorevine.api.ApiConstants;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public interface SigningApiInterface {

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @POST(ApiConstants.API_LOGIN)
    Call<LoginResponse> login(@Query("username") String username, @Query("password") String password);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @POST(ApiConstants.API_SEND_VERIFICATION)
    Call<VerificationResponse> sendVerification(@Body VerificationRequest verificationRequest);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @GET(ApiConstants.API_RESET_PASSWORD)
    Call<RetreivalMethods> getResetPasswordMethods(@Query("username") String username);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @POST(ApiConstants.API_RESET_PASSWORD)
    Call<ResetPasswordResponse> resetPasswordGetCode(@Body ResetPasswordGetCodeRequest resetPasswordRequest);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @PUT(ApiConstants.API_RESET_PASSWORD)
    Call<ResetPasswordResponse> resetPasswordSendCode(@Body ResetPasswordSendCodeRequest resetPasswordCodeRequest);


    @Headers(ApiConstants.HEADER_ACCEPT)
    @POST(ApiConstants.API_UNLOCK)
    Call<ResetPasswordResponse> unlockGetCode(@Body ResetPasswordGetCodeRequest resetPasswordRequest);

    @Headers(ApiConstants.HEADER_ACCEPT)
    @PUT(ApiConstants.API_UNLOCK)
    Call<ResetPasswordResponse> unlockSendCode(@Body ResetPasswordSendCodeRequest resetPasswordCodeRequest);


    @Headers({ApiConstants.HEADER_ACCEPT})
    @POST(ApiConstants.API_REGISTER)
    @Multipart
    Call<LoginResponse> register(@Part("email") RequestBody email, @Part("mobile") RequestBody mobile, @Part("username") RequestBody username,
                                 @Nullable @Part("password") RequestBody password, @Nullable @Part("facebook_id") RequestBody facebookToken,
                                 @Part("birth_date") RequestBody birthDate, @Part("gender") RequestBody gender,
                                 @Part("country") RequestBody country, @Part("first_name") RequestBody firstName,
                                 @Part("surname") RequestBody surname, @Nullable @Part("referal") RequestBody referal,
                                 @Nullable @Part MultipartBody.Part photo, @Part("code") RequestBody code,
                                 @Nullable @Part("deviceUID") RequestBody deviceUID, @Nullable @Part("push_token") RequestBody push_token);

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @POST(ApiConstants.API_FACEBOOK_LOGIN)
    @FormUrlEncoded
    Call<FbLoginResponse> fbLogin(@Field("facebook_id") String facebookToken, @Nullable @Field("deviceUID") String deviceUID, @Field("push_token") String push_token);



}
