package com.scorevine.api.signing;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */
public class VerificationResponse {

    @SerializedName("status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Nullable
    @SerializedName("code")
    @Expose
    private String code;

    @Nullable
    public String getCode() {
        return code;
    }

    @Nullable
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return status + " " + code;
    }
}
