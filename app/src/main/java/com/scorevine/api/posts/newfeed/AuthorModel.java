package com.scorevine.api.posts.newfeed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by adrianjez on 20.12.2016.
 */

public class AuthorModel implements Serializable, Parcelable {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("profile_image")
    @Expose
    private String profileImage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /** Help methods **/
    public String getDisplayName(){
        return this.getFirstName() + " " + getSurname();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.username);
        dest.writeString(this.firstName);
        dest.writeString(this.surname);
        dest.writeString(this.profileImage);
    }

    public AuthorModel() {
    }

    protected AuthorModel(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.username = in.readString();
        this.firstName = in.readString();
        this.surname = in.readString();
        this.profileImage = in.readString();
    }

    public static final Parcelable.Creator<AuthorModel> CREATOR = new Parcelable.Creator<AuthorModel>() {
        @Override
        public AuthorModel createFromParcel(Parcel source) {
            return new AuthorModel(source);
        }

        @Override
        public AuthorModel[] newArray(int size) {
            return new AuthorModel[size];
        }
    };
}
