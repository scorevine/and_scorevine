package com.scorevine.api.posts.newfeed;

import com.scorevine.api.ApiConstants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by adrianjez on 20.12.2016.
 */

public interface NewfeedApi {

    @Headers({ApiConstants.HEADER_ACCEPT, ApiConstants.HEADER_CONTENT_TYPE})
    @GET(ApiConstants.API_POSTS_NEWFEED)
    Call<NewfeedResponse> getNewfeed(@Header("X-access-token") String encodedToken,
                                     @Query("page") int page,
                                     @Query("results") int results);

}
