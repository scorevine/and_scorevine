package com.scorevine.api.posts.newfeed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.scorevine.api.tags.Tag;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by adrianjez on 18.12.2016.
 */

public class NewsfeedModel implements Serializable, Parcelable {

    public static final String NEWFEED_ARRAY_BUNDLE_KEY = "NewsfeedModelArrayBundleKey";

    public enum CurrentVoteType {
        @SerializedName("up")
        up,
        @SerializedName("down")
        down
    }

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("datetime")
    @Expose
    private long datetime;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("link")
    @Expose
    private String link;

    @SerializedName("votesUp")
    @Expose
    private int votesUp;

    @SerializedName("votesDown")
    @Expose
    private int votesDown;

    @SerializedName("currentVote")
    @Expose
    private CurrentVoteType currentVote;

    @SerializedName("shares")
    @Expose
    private int shares;

    @SerializedName("comments_amount")
    @Expose
    private int commentsAmount;

    @SerializedName("author")
    @Expose
    private AuthorModel author;


    @SerializedName("tags")
    @Expose
    private ArrayList<Tag> tags;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getVotesUp() {
        return votesUp;
    }

    public void setVotesUp(int votesUp) {
        this.votesUp = votesUp;
    }

    public int getVotesDown() {
        return votesDown;
    }

    public void setVotesDown(int votesDown) {
        this.votesDown = votesDown;
    }

    public CurrentVoteType getCurrentVote() {
        return currentVote;
    }

    public void setCurrentVote(CurrentVoteType currentVote) {
        this.currentVote = currentVote;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public int getCommentsAmount() {
        return commentsAmount;
    }

    public void setCommentsAmount(int commentsAmount) {
        this.commentsAmount = commentsAmount;
    }

    public AuthorModel getAuthor() {
        return author;
    }

    public void setAuthor(AuthorModel author) {
        this.author = author;
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }


    public String getTagsHeaderHtml(){
        String htmlText = "";
        if(this.getTags() != null && getTags().size() > 0){
            for(int i = 0; i < this.getTags().size(); i++){
                htmlText += "<font color=#5dc3ae>" + "#" +"</font>" +
                        "<font color=#000000>" + this.getTags().get(i).getTag()  +
                        (i == this.getTags().size() - 1 ? "" : ", ") + "</font>";
            }
        }
        return htmlText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.datetime);
        dest.writeString(this.description);
        dest.writeString(this.link);
        dest.writeInt(this.votesUp);
        dest.writeInt(this.votesDown);
        dest.writeInt(this.currentVote == null ? -1 : this.currentVote.ordinal());
        dest.writeInt(this.shares);
        dest.writeInt(this.commentsAmount);
        dest.writeParcelable(this.author, flags);
        dest.writeTypedList(this.tags);
    }

    public NewsfeedModel() {
    }

    protected NewsfeedModel(Parcel in) {
        this.id = in.readLong();
        this.datetime = in.readLong();
        this.description = in.readString();
        this.link = in.readString();
        this.votesUp = in.readInt();
        this.votesDown = in.readInt();
        int tmpCurrentVote = in.readInt();
        this.currentVote = tmpCurrentVote == -1 ? null : CurrentVoteType.values()[tmpCurrentVote];
        this.shares = in.readInt();
        this.commentsAmount = in.readInt();
        this.author = in.readParcelable(AuthorModel.class.getClassLoader());
        this.tags = in.createTypedArrayList(Tag.CREATOR);
    }

    public static final Creator<NewsfeedModel> CREATOR = new Creator<NewsfeedModel>() {
        @Override
        public NewsfeedModel createFromParcel(Parcel source) {
            return new NewsfeedModel(source);
        }

        @Override
        public NewsfeedModel[] newArray(int size) {
            return new NewsfeedModel[size];
        }
    };
}
