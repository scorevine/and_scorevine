package com.scorevine.api.posts.newfeed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by adrianjez on 20.12.2016.
 */

public class NewfeedResponse {

    @SerializedName("results")
    @Expose
    private ArrayList<NewsfeedModel> results = null;

    @SerializedName("pages")
    @Expose
    private int pages;

    public ArrayList<NewsfeedModel> getResults() {
        return results;
    }

    public void setResults(ArrayList<NewsfeedModel> results) {
        this.results = results;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
