package com.scorevine.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.helpers.TypefaceHelper;

/**
 * Created by adrianjez on 15.12.2016.
 */

public class TypefacedTabLayout extends TabLayout {

    private Typeface mTypeface;

    public TypefacedTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!this.isInEditMode()) {
            this.mTypeface = TypefaceHelper.initTypeFace(this, attrs);
        }
    }

    public TypefacedTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!this.isInEditMode()) {
            this.mTypeface = TypefaceHelper.initTypeFace(this, attrs);
        }
    }

    @Override
    public void addTab(Tab tab) {
        super.addTab(tab);
        initTypeface(tab);
    }

    @Override
    public void addTab(@NonNull Tab tab, boolean setSelected) {
        super.addTab(tab, setSelected);
        initTypeface(tab);
    }

    private void initTypeface(Tab tab){
        ViewGroup mainView = (ViewGroup) getChildAt(0);
        ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());

        int tabChildCount = tabView.getChildCount();
        for (int i = 0; i < tabChildCount; i++) {
            View tabViewChild = tabView.getChildAt(i);
            if (tabViewChild instanceof TextView) {
                ((TextView) tabViewChild).setTypeface(mTypeface);
            }
        }
    }
}