package com.scorevine.helpers;

import android.content.Context;
import android.widget.ImageView;

import com.scorevine.R;
import com.squareup.picasso.Picasso;

/**
 * Created by adrianjez on 22.12.2016.
 */

public class PicassoHelper {

    /** Private main method **/
    private static void setImage(Context context, ImageView destImageView, String url, int placeholderID){
        Picasso.with(context)
                .load(url)
                .placeholder(placeholderID)
                .into(destImageView);
    }

    /** Placeholder for profile **/
    public static void setProfileImage(Context context, ImageView destImageView, String url){
        PicassoHelper
                .setImage(context, destImageView, url, R.mipmap.ic_launcher);
    }

    /** Some default placeholder **/
    public static void setImage(Context context, ImageView destImageView, String url){
        PicassoHelper
                .setImage(context, destImageView, url, R.mipmap.ic_comment);
    }


}
