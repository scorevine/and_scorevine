package com.scorevine.helpers;

import android.support.design.widget.TextInputEditText;

import com.scorevine.R;
import com.scorevine.ui.validation.InputTypeSV;

import java.util.regex.Pattern;

/**
 * Created by 8570p on 2016-12-18.
 */

public class ValidationHelper {

    private static final String PATTERN_UNIQUE_NAME = "^[a-zA-Z0-9,_]{4,16}$";
    private static final String PATTERN_PASSWORD = "(?=.*\\d)(?=.*[a-zA-Z]).*$";
    private static final String PATTERN_CODE = "^[0-9]+$";
    private static final String PATTERN_PHONE_NUMBER = "^[0-9]{6,14}$";
    private static final String PATTERN_EMAIL = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
    private static final String PATTERN_FIRSTNAME = "^[a-zA-Z]+$";
    private static final String PATTERN_URL_LINK = "(https?:\\/\\/(?:www\\.|(?!www))[^\\s\\.]+\\.[^\\s]{2,})"; //^(?i)(https?://)[wW]{3}.[a-zA-Z]{3,}+.[a-z]{2,}.*"
  //  private static final String PATTERN_URL_LINK = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$\n";//(https://)[a-zA-Z]+[.][a-zA-Z]+"; //^(?i)(https?://)[wW]{3}.[a-zA-Z]{3,}+.[a-z]{2,}.*"  ??

    public boolean validateCharacters(String value, InputTypeSV type) {
        Pattern pattern = getPatternForType(type);
        return pattern.matcher(value).matches();
    }

    public boolean validateMinLength(String value, InputTypeSV type) {
        return value.length() >= type.minLength();
    }

    public boolean validateMaxLength(String value, InputTypeSV type) {
        return value.length() <= type.maxLength();
    }

    public Pattern getPatternForType(InputTypeSV type) {
        Pattern pattern = null;
        switch (type) {
            case UNIQUE_NAME:
                pattern = Pattern.compile(PATTERN_UNIQUE_NAME);
                break;
            case PASSWORD:
            case CONFIRM_PASSWORD:
            case CURRENT_PASSWORD:
                pattern = Pattern.compile(PATTERN_PASSWORD);
                break;
            case CODE:
                pattern = Pattern.compile(PATTERN_CODE);
                break;
            case EMAIL:
                pattern = Pattern.compile(PATTERN_EMAIL);
                break;
            case PHONE_NUMBER:
                pattern = Pattern.compile(PATTERN_PHONE_NUMBER);
                break;
            case FIRST_NAME:
                pattern = Pattern.compile(PATTERN_FIRSTNAME);
                break;
            case SURNAME:
                pattern = Pattern.compile(PATTERN_FIRSTNAME);
                break;
            case URI:
                pattern = Pattern.compile(PATTERN_URL_LINK);
                break;
            default:
                break;
        }
        return pattern;
    }

    public int getError(InputTypeSV inputType) {
        int errorId = R.string.unknown_error;

        switch (inputType) {
            case UNIQUE_NAME:
                errorId = R.string.error_unique_name_symbols;
                break;
            case PASSWORD:
            case CONFIRM_PASSWORD:
            case CURRENT_PASSWORD:
                errorId = R.string.error_characters_password;
                break;
            case CODE:
                errorId = R.string.verification_code_characters;
                break;
            case EMAIL:
                errorId = R.string.error_invalid_email;
                break;
            case PHONE_NUMBER:
                errorId = R.string.error_invalid_phone_number;
                break;
            case FIRST_NAME:
            case SURNAME:
                errorId = R.string.error_invalid_fields;
                break;
            default:
                break;
        }
        return errorId;

    }

    public boolean checkNull(TextInputEditText... textInputEditTexts) {
        boolean isNull = false;
        for (TextInputEditText textInputEditText : textInputEditTexts) {
            isNull = (textInputEditText.getText().length() == 0);
            if(isNull)
                return isNull;
        }
        return isNull;
    }

}
