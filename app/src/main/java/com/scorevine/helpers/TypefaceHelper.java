package com.scorevine.helpers;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;

import com.scorevine.R;

import java.util.Hashtable;

/**
 * Created by adrianjez on 15.12.2016.
 */

public class TypefaceHelper {


    public static Typeface initTypeFace(TabLayout tabLayout, AttributeSet attrs) {
        if (attrs == null) return null;
        TypedArray a = tabLayout.getContext().obtainStyledAttributes(attrs, R.styleable.TypefacedTabLayout);
        int typefaceId = a.getInt(R.styleable.TypefacedTabLayout_typeface, -1);
        a.recycle();
        return initTypefaceForTabLayout(tabLayout, typefaceId);
    }


    private static Hashtable<String, Typeface> loadedTypefaces = new Hashtable<>();

    /** Private Methods **/
    private static Typeface initTypefaceForTabLayout(TabLayout tabLayout, int typefaceID) {
        String typefaceName = getTypefaceName(typefaceID);
        return TypefaceHelper.getTypeface(tabLayout.getContext(), typefaceName);
    }

    private static String getTypefaceName(int typefaceID){
        String typefaceName = null;
        switch (typefaceID) {
            case  100: typefaceName = "fonts/SanFranciscoDisplay-Bold.ttf";         break;
            case  101: typefaceName = "fonts/SanFranciscoDisplay-Medium.ttf";       break;
            case  102: typefaceName = "fonts/SanFranciscoDisplay-Regular.ttf";      break;
            case  103: typefaceName = "fonts/SanFranciscoText-Bold.ttf";            break;
            case  104: typefaceName = "fonts/SanFranciscoText-BoldItalic.ttf";      break;
            case  105: typefaceName = "fonts/SanFranciscoText-Medium.ttf";          break;
            case  106: typefaceName = "fonts/SanFranciscoText-MediumItalic.ttf";    break;
            case  107: typefaceName = "fonts/SanFranciscoText-Regular.ttf";         break;
            case  108: typefaceName = "fonts/SanFranciscoText-RegularItalic.ttf";   break;
            case  109: typefaceName = "fonts/SanFranciscoText-Semibold.ttf";        break;
            case  110: typefaceName = "fonts/SanFranciscoText-SemiboldItalic.ttf";  break;
        }
        return typefaceName;
    }

    public static Typeface getTypeface(Context context, String typefaceName) {
        Typeface myTypeface = TypefaceHelper.loadedTypefaces.get(typefaceName);
        if (myTypeface == null) {
            myTypeface = Typeface.createFromAsset(context.getAssets(), typefaceName);
            TypefaceHelper.loadedTypefaces.put(typefaceName, myTypeface);
        }
        return myTypeface;
    }
}
