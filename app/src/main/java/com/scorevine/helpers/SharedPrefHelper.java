package com.scorevine.helpers;

import android.content.Context;

import com.prashantsolanki.secureprefmanager.SecurePrefManager;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class SharedPrefHelper {

    public static final String SP_TERMS_OF_SERVICE = "ToS accepted";
    public static final String SP_EMAIL = "email";
    public static final String SP_CODE = "phone code";
    public static final String SP_MOBILE = "mobile";
    public static final String SP_FIRST_NAME = "first name";
    public static final String SP_SURNAME = "surname";
    public static final String SP_UNIQUENAME = "uniquename";
    public static final String SP_PASSWORD = "password";
    public static final String SP_CONFIRMPASSWORD = "confirm password";
    public static final String SP_BIRTHDATE = "birthdate";
    public static final String SP_GENDER = "gender";
    public static final String SP_COUNTRY = "country";
    public static final String SP_SMSCODE = "smscode";
    public static final String SP_PHOTO = "photo";
    public static final String SP_REFFERAL = "refferal";
    public static final String SP_FB_ID = "facebook id";


    public static final String SP_SAVE_DATA_P1 = "save data part 1";
    public static final String SP_SAVE_DATA_P2 = "save data part 2";
    public static final String SP_SAVE_DATA = "save data";

    public static final String SP_TOKEN = "token";

    public void addSharedPref(Context context, String prefName, String prefValue) {
        SecurePrefManager.with(context)
                .set(prefName)
                .value(prefValue)
                .go();
    }


    public String getSharedPref(Context context, String prefName, String defaultValue) {
        return SecurePrefManager.with(context)
                .get(prefName)
                .defaultValue(defaultValue)
                .go();
    }

    public void clearSharedPref(Context context ) {

        SecurePrefManager.with(context).clear().confirm();

    }

}


