
package com.scorevine.helpers;

import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;

import com.scorevine.R;
import com.tooltip.Tooltip;

/**
 * Created by NIKAK
 * Ready4S
 * on 20.12.2016 13:18
 */
public class TooltipHelper {

    public static final float ARROW_WIDTH = 40f;
    public static final float ARROW_HEIGHT = 50f;
    public static final float ARROW_WIDTH2 = 70f;

    public static Tooltip createToolTipDialogBox(EditText editText, int stringId, int gravity, Context context) {

        return new Tooltip.Builder(editText)
                .setGravity(gravity)
                .setCancelable(true)
                .setText(stringId)
                .setTextSize(R.dimen.text_size_tooltip)
                .setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark))
                .setCornerRadius(R.dimen._4sdp)
                .setTextColor(context.getResources().getColor(R.color.colorText))
                .setPadding(R.dimen._8sdp)
                .setArrowWidth(R.dimen.arrow_width_tooltip)
                .setArrowHeight(R.dimen.arrow_height_tooltip)
                .build();
    }


    public static Tooltip createToolTipDialogBoxCounter(EditText editText, int stringId, int gravity, Context context) {

        return new Tooltip.Builder(editText)
                .setGravity(gravity)
                .setCancelable(true)
                .setText(stringId)
                .setTextSize(R.dimen.text_size_tooltip)
                .setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark))
                .setCornerRadius(R.dimen._4sdp)
                .setTextColor(context.getResources().getColor(R.color.colorText))
                .setPadding(R.dimen._8sdp)
                .setArrowWidth(R.dimen.arrow_width_tooltip)
                .setArrowHeight(R.dimen.arrow_height_tooltip)
                .build();
    }
    public static Tooltip createTooltipTV(TextView textView, int stringId, int gravity, Context context) {

        return new Tooltip.Builder(textView)
                .setGravity(gravity)
                .setCancelable(true)
                .setText(stringId)
                .setTextSize(R.dimen.text_size_tooltip)
                .setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark))
                .setCornerRadius(R.dimen._4sdp)
                .setTextColor(context.getResources().getColor(R.color.colorText))
                .setPadding(R.dimen._8sdp)
                .setArrowWidth(R.dimen.arrow_width_tooltip)
                .setArrowHeight(R.dimen.arrow_height_tooltip)
                .build();
    }

}
