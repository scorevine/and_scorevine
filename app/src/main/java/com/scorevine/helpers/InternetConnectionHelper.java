package com.scorevine.helpers;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by weronikapapkouskaya on 14.12.2016.
 */

public class InternetConnectionHelper {
    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }
}
