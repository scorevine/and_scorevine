package com.scorevine.helpers;

import android.util.Log;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.ApiManager;
import com.scorevine.api.signing.BlockedError;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */
public class ErrorUtils {

    public static APIErrorList parseError(Response<?> response) {
        Converter<ResponseBody, APIErrorList> converter =
                new ApiManager().getRetrofit()
                        .responseBodyConverter(APIErrorList.class, new Annotation[0]);

        APIErrorList error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIErrorList();
        }

        return error;
    }


    public static BlockedError parseBlockedError(Response<?> response) {
        Converter<ResponseBody, BlockedError> converter =
                new ApiManager().getRetrofit()
                        .responseBodyConverter(BlockedError.class, new Annotation[0]);

        BlockedError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new BlockedError();
        }

        return error;
    }

}
