package com.scorevine.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by adrianjez on 18.12.2016.
 */

public class DateFormatHelper {

    public static final String COMMON_DATE_FORMAT = "MM.dd.yyyy";
    public static final String MESSAGE_DATE_FORMAT = "hh:mm a MM.dd.yyyy";

    public static final String API_PROFILE_DATE_FORMAT = "yyyy-MM-dd";
    public static final String APP_PROFILE_DATE_FORMAT= "dd - MM - yyyy";

    public static final String AM_PM_TIME = "hh:mm a";



    /**
     *
     * @param format - expected return format type
     * @param time - timestamp in seconds !
     * @return
     */
    public static String getDateWithFormat(String format, long time){
        return new SimpleDateFormat(format, Locale.getDefault()).format(time /* *1000*/);
    }
    public static String getDateWithFormat(String format, Date date){
        return new SimpleDateFormat(format, Locale.getDefault()).format(date);
    }

    public static String changeDateFormat(String format1, String format2, String date) {

        SimpleDateFormat format = new SimpleDateFormat(format1);
        Date newDate = null;
        try {
            newDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat apiFormat = new SimpleDateFormat(format2);
        return apiFormat.format(newDate);
    }

    /**
     *
     * @param time - timestamp in seconds
     * @return time like: 7th november 2016
     */
    public static String getSuffixedDate(long time){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time * 1000);
        String topPart = c.get(Calendar.DAY_OF_MONTH)  + getSuffix(c.get(Calendar.DAY_OF_MONTH));
        String lastPart = new SimpleDateFormat(" MMMM yyyy", Locale.getDefault()).format(time);
        return topPart + lastPart;
    }

    /** Help methods **/
    private static String getSuffix(final int n) {
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }
}
