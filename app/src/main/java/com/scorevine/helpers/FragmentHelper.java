package com.scorevine.helpers;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.scorevine.R;

import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-02.
 */

public class FragmentHelper {


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void clearBackStack(FragmentManager fragmentManager){
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        /*
        for (int i=0; i<fragmentManager.getBackStackEntryCount()-1; i++){
            fragmentManager.popBackStack();
        }*/

    }

    public static void addFirstFragment(FragmentManager fragmentManager, Fragment fragment, String tag) {

        fragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragment, tag)
                .commit();

    }

    public static void replaceFragment(FragmentManager fragmentManager, Fragment fragment, String tag) {

        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .addToBackStack(null)
                .commit();

    }

    public static void setFragment(FragmentManager fragmentManager, Fragment fragment, String tag) {
        FragmentHelper.clearBackStack(fragmentManager);
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragment, tag)
                .commit();
    }

    public static void putStringBundle(Fragment fragment, String key, String value) {

        Bundle bundle = new Bundle();
        bundle.putString(key, value);
        fragment.setArguments(bundle);
    }

    public static Fragment getVisibleFragment(FragmentManager fragmentManager){

        List<Fragment> fragments = fragmentManager.getFragments();
        if(fragments != null){
            for(Fragment fragment : fragments){
                if(fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }
}
