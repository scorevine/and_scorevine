package com.scorevine.helpers;

import android.app.Activity;

import com.google.gson.Gson;
import com.scorevine.model.CountryCodes;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-02.
 */

public class LoadFileHelper {


    public static List<CountryCodes.CountryCode> getCountryCodesList(Activity activity){

        Gson gson = new Gson();
        String json = null;
        try {
            InputStream is = activity.getAssets().open("country_codes.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        CountryCodes countryCodesObj = gson.fromJson(json, CountryCodes.class);

        return countryCodesObj.getCountryCodesList();

    }


}

