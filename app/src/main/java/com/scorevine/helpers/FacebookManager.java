package com.scorevine.helpers;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

/**
 * Created by 8570p on 2016-12-17.
 */

public class FacebookManager implements FacebookCallback<LoginResult> {

    private static final String FACEBOOK_FIELD_PARAM_NAME = "fields";
    private static final String FACEBOOK_FIELD_PARAMS = "email, id";

    public interface FacebookLoginCallback {
        void onFacebookSuccess(String token);

        void onFacebookCancel();

        void onFacebookFailure();

        void onFacebookData(String email, String firstName, String lastName, String id);
    }

    private FacebookLoginCallback mCallback;
    private Profile mProfile;
    private ProfileTracker mProfileTracker;

    public void registerCallback(FacebookLoginCallback callback) {
        mCallback = callback;
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

        if (Profile.getCurrentProfile() == null) {
            mProfileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                    // profile2 is the new profile
                    mProfile = profile2;
                    Log.v("facebook - profile", mProfile.getFirstName());
                    mProfileTracker.stopTracking();
                }
            };
            // no need to call startTracking() on mProfileTracker
            // because it is called by its constructor, internally.
        } else {
            mProfile = Profile.getCurrentProfile();
        }
        performGraphMeRequest(loginResult.getAccessToken());
    }

    private void performGraphMeRequest(final AccessToken token) {
        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        handleGraphMeResponse(token, response);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString(FACEBOOK_FIELD_PARAM_NAME, FACEBOOK_FIELD_PARAMS);
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void handleGraphMeResponse(AccessToken token, GraphResponse response) {
        if (response.getError() != null) {
            mCallback.onFacebookFailure();
        }
        try {
            String email = response.getJSONObject().getString("email");
            String id = response.getJSONObject().getString("id");
            if (mCallback != null) {
                mCallback.onFacebookSuccess(id);
                mCallback.onFacebookData(email, mProfile.getFirstName(), mProfile.getLastName(), mProfile.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            mCallback.onFacebookFailure();
        }
    }

    @Override
    public void onCancel() {
        mCallback.onFacebookCancel();
    }

    @Override
    public void onError(FacebookException e) {
        mCallback.onFacebookFailure();
        LoginManager.getInstance().logOut();
    }
}
