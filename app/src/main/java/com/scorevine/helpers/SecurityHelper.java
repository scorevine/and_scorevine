package com.scorevine.helpers;

import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by weronikapapkouskaya on 13.12.2016.
 */

public class SecurityHelper {

    public static String generateSalt() {

        final Random r = new SecureRandom();
        char[] salt = new char[64];
        for (int i = 0; i < 64; i++) {

            char character = (char) (r.nextInt(128 - 32) + 32);
            salt[i] = character;

        }
        return new String(salt);

    }

    public static String encodeToken(String token) {

        String salt = generateSalt();
        String saltTokenSalt = salt + token + salt;
        String encodedString = Base64.encodeToString(saltTokenSalt.getBytes(), Base64.NO_WRAP);
        Log.i("security", String.format("encodedString salt %s", salt));
        Log.i("security", String.format("encodedString saltTokenSalt %s", saltTokenSalt));
        Log.i("security", String.format("encodedString token %s", token));
        Log.i("security", String.format("encodedString %s", encodedString));
        return encodedString;

    }

    public static String decodeToken(String token) {

        byte[] decoded = Base64.decode(token, Base64.DEFAULT);
        String decodedString = null;
        try {
            decodedString = new String(decoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Log.i("security", String.format("decodedString %s", decodedString));
        return decodedString;

    }

}
