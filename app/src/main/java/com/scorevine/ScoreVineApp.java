package com.scorevine;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by NIKAK
 * Ready4S
 * on 30.11.2016 18:15
 */
public class ScoreVineApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SanFranciscoText-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
    public static Application getAppContext(){
        return getAppContext();
    }
}
