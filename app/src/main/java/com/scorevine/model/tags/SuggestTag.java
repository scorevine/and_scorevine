package com.scorevine.model.tags;

import java.io.Serializable;

/**
 * Created by Yoga on 2016-12-28.
 */

public class SuggestTag implements Serializable {

    private String shortName;
    private String name;

    private String description;
    private String website;
    private String photo;
    private String evidenceLink;
    private String lookFor;
    private int tagSort;
    private int over18;


    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEvidenceLink() {
        return evidenceLink;
    }

    public void setEvidenceLink(String evidenceLink) {
        this.evidenceLink = evidenceLink;
    }

    public String getLookFor() {
        return lookFor;
    }

    public void setLookFor(String lookFor) {
        this.lookFor = lookFor;
    }

    public int getTagSort() {
        return tagSort;
    }

    public void setTagSort(int tagSort) {
        this.tagSort = tagSort;
    }

    public int getOver18() {
        return over18;
    }

    public void setOver18(int over18) {
        this.over18 = over18;
    }

}
