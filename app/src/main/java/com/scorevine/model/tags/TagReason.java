package com.scorevine.model.tags;

/**
 * Created by Yoga on 2016-12-27.
 */

public class TagReason {

    private String reasonName;
    private int reasonId;
    private boolean isPerson;


    public TagReason(String name, int id, int person){
        if(person==1)
            isPerson = true;
        else
            isPerson = false;

        reasonName = name;
        reasonId = id;
    }
    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public int getReasonId() {
        return reasonId;
    }

    public void setReasonId(int reasonId) {
        this.reasonId = reasonId;
    }


    public boolean isPerson() {
        return isPerson;
    }

    public void setPerson(boolean person) {
        isPerson = person;
    }
}
