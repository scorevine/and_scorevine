package com.scorevine.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-02.
 */

public class CountryCodes {
    @SerializedName("countries")
    @Expose
    public List<CountryCode> countryCodesList;

    public List<CountryCode> getCountryCodesList() {
        return countryCodesList;
    }

    public void setCountryCodesList(List<CountryCode> countryCodesList) {
        this.countryCodesList = countryCodesList;
    }

    public class CountryCode {
        @SerializedName("code")
        @Expose
        private String countryCode;

        @SerializedName("dial_code")
        @Expose
        private String dialCode;

        @SerializedName("name")
        @Expose
        private String countryName;


        public String getDialCode() {
            return dialCode;
        }

        public void setDialCode(String dialCode) {
            this.dialCode = dialCode;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getCountryName() {
            return countryName;
        }

        public void setCountryName(String countryName) {
            this.countryName = countryName;
        }
    }

}