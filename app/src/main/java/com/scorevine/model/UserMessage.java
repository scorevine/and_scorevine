package com.scorevine.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by adrianjez on 17.12.2016.
 *
 * Test model for further changes
 */

public class UserMessage implements Serializable, Parcelable {

    private String display_name;
    private String login;
    private String message;
    private long date;

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.display_name);
        dest.writeString(this.login);
        dest.writeString(this.message);
        dest.writeLong(this.date);
    }

    public UserMessage() {
    }

    protected UserMessage(Parcel in) {
        this.display_name = in.readString();
        this.login = in.readString();
        this.message = in.readString();
        this.date = in.readLong();
    }

    public static final Creator<UserMessage> CREATOR = new Creator<UserMessage>() {
        @Override
        public UserMessage createFromParcel(Parcel source) {
            return new UserMessage(source);
        }

        @Override
        public UserMessage[] newArray(int size) {
            return new UserMessage[size];
        }
    };
}
