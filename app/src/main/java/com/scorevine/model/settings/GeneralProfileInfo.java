package com.scorevine.model.settings;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Agnieszka Duleba on 2016-12-19.
 */
public class GeneralProfileInfo implements Parcelable {
    public GeneralProfileInfo() {
        super();
    }

    protected GeneralProfileInfo(Parcel in) {
        firstName = in.readString();
        surname = in.readString();
        uniquename = in.readString();
        birthDate = in.readString();
        gender = in.readString();
        country = in.readString();
        city = in.readString();
        story = in.readString();
    }

    public static final Creator<GeneralProfileInfo> CREATOR = new Creator<GeneralProfileInfo>() {
        @Override
        public GeneralProfileInfo createFromParcel(Parcel in) {
            return new GeneralProfileInfo(in);
        }

        @Override
        public GeneralProfileInfo[] newArray(int size) {
            return new GeneralProfileInfo[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUniquename() {
        return uniquename;
    }

    public void setUniquename(String uniquename) {
        this.uniquename = uniquename;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    private String firstName;

    private String surname;

    private String uniquename;

    private String birthDate;
    private String gender;
    private String country;
    private String city;
    private String story;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(firstName);
        parcel.writeString(surname);
        parcel.writeString(uniquename);
        parcel.writeString(birthDate);
        parcel.writeString(gender);
        parcel.writeString(country);
        parcel.writeString(city);
        parcel.writeString(story);
    }

    @Override
    public String toString() {
        return "GeneralProfileInfo{" +
                "firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", uniquename='" + uniquename + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", gender='" + gender + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", story='" + story + '\'' +
                '}';
    }
}
