package com.scorevine.model.settings;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Agnieszka Duleba on 2016-12-19.
 */
public class AccountSettings implements Parcelable {

    private String email;
    private boolean hasPassword;
    private String countryCode;
    private String mobile;
    private boolean appSounds = false;
    private boolean notificationsSounds = false;

    public AccountSettings() {
        super();
    }


    protected AccountSettings(Parcel in) {
        email = in.readString();
        hasPassword = in.readByte() != 0;
        countryCode = in.readString();
        mobile = in.readString();
        appSounds = in.readByte() != 0;
        notificationsSounds = in.readByte() != 0;
    }

    public static final Creator<AccountSettings> CREATOR = new Creator<AccountSettings>() {
        @Override
        public AccountSettings createFromParcel(Parcel in) {
            return new AccountSettings(in);
        }

        @Override
        public AccountSettings[] newArray(int size) {
            return new AccountSettings[size];
        }
    };

    public boolean isAppSounds() {
        return appSounds;
    }

    public void setAppSounds(boolean appSounds) {
        this.appSounds = appSounds;
    }

    public boolean isNotificationsSounds() {
        return notificationsSounds;
    }

    public void setNotificationsSounds(boolean notificationsSounds) {
        this.notificationsSounds = notificationsSounds;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(email);
        parcel.writeByte((byte) (hasPassword ? 1 : 0));
        parcel.writeString(countryCode);
        parcel.writeString(mobile);
        parcel.writeByte((byte) (appSounds ? 1 : 0));
        parcel.writeByte((byte) (notificationsSounds ? 1 : 0));
    }
}
