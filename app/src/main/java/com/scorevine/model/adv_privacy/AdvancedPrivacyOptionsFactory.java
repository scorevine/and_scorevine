package com.scorevine.model.adv_privacy;

import android.content.Context;

import com.scorevine.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 8570p on 2016-12-17.
 */

public class AdvancedPrivacyOptionsFactory {

    private Context mContext;

    public AdvancedPrivacyOptionsFactory(Context context) {
        mContext = context;
    }

    public List<AdvancedPrivacyTrichoiceModel> generateTrichoiceModels() {
        List<AdvancedPrivacyTrichoiceModel> models = new ArrayList<>();
        models.add(new AdvancedPrivacyTrichoiceModel(mContext.getString(R.string.adv_privacy_follow_me_title),
                mContext.getString(R.string.adv_privacy_choice_anyone),
                mContext.getString(R.string.adv_privacy_choice_followers),
                mContext.getString(R.string.adv_privacy_choice_nobody)));
        models.add(new AdvancedPrivacyTrichoiceModel(mContext.getString(R.string.adv_privacy_find_me_title),
                mContext.getString(R.string.adv_privacy_choice_anyone),
                mContext.getString(R.string.adv_privacy_choice_followers),
                mContext.getString(R.string.adv_privacy_choice_nobody)));
        models.add(new AdvancedPrivacyTrichoiceModel(mContext.getString(R.string.adv_privacy_see_posts_title),
                mContext.getString(R.string.adv_privacy_choice_anyone),
                mContext.getString(R.string.adv_privacy_choice_followers),
                mContext.getString(R.string.adv_privacy_choice_nobody)));

        return models;
    }

    public List<AdvancedPrivacyBichoiceModel> generateBichoiceModels() {
        List<AdvancedPrivacyBichoiceModel> models = new ArrayList<>();
        models.add(new AdvancedPrivacyBichoiceModel(mContext.getString(R.string.adv_pricacy_general_posts_notification_title),
                mContext.getString(R.string.adv_privacy_choice_followers),
                mContext.getString(R.string.adv_privacy_choice_no_one)));
        models.add(new AdvancedPrivacyBichoiceModel(mContext.getString(R.string.adv_privacy_group_posts_notification_title),
                mContext.getString(R.string.adv_privacy_choice_followers),
                mContext.getString(R.string.adv_privacy_choice_group_members)));
        models.add(new AdvancedPrivacyBichoiceModel(mContext.getString(R.string.adv_privacy_comments_notification_title),
                mContext.getString(R.string.adv_privacy_choice_followers),
                mContext.getString(R.string.adv_privacy_choice_no_one)));
        models.add(new AdvancedPrivacyBichoiceModel(mContext.getString(R.string.adv_privacy_votes_notification_title),
                mContext.getString(R.string.adv_privacy_choice_followers),
                mContext.getString(R.string.adv_privacy_choice_no_one)));

        return models;
    }
}

