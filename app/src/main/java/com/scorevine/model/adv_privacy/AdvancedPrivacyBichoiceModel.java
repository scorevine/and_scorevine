package com.scorevine.model.adv_privacy;

import com.scorevine.ui.my_profile.advanced_privacy.AdvancedPrivacyContract;

/**
 * Created by 8570p on 2016-12-17.
 */

public class AdvancedPrivacyBichoiceModel extends AdvancedPrivacyChoiceModel {

    private AdvancedPrivacyContract.Bichoice mSelection;

    public AdvancedPrivacyBichoiceModel(String title, String firstChoice, String secondChoice) {
        super(title);
        addChoices(firstChoice, secondChoice);
        mSelection = AdvancedPrivacyContract.Bichoice.FIRST;
    }

    public AdvancedPrivacyContract.Bichoice getSelection() {
        return mSelection;
    }

    public String getChoiceName(AdvancedPrivacyContract.Bichoice choice) {
        return getChoiceName(choice.value());
    }

    public void setSelection(AdvancedPrivacyContract.Bichoice selection) {
        mSelection = selection;
    }
}
