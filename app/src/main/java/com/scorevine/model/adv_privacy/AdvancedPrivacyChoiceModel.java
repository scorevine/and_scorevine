package com.scorevine.model.adv_privacy;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 8570p on 2016-12-17.
 */

class AdvancedPrivacyChoiceModel {

    private String mTitle;

    protected List<String> mChoicesNames;

    public AdvancedPrivacyChoiceModel(String title) {
        mTitle = title;
        mChoicesNames = new ArrayList<>();
    }

    protected void addChoices(String... choices) {
        for (String choice : choices) {
            mChoicesNames.add(choice);
        }
    }

    protected String getChoiceName(int choice) {
        if (choice < mChoicesNames.size()) {
            return mChoicesNames.get(choice);
        }
        return "Not a valid choice.";
    }

    public String getTitle() {
        return mTitle;
    }
}
