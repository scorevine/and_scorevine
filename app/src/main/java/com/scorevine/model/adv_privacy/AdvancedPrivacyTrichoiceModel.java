package com.scorevine.model.adv_privacy;

import com.scorevine.ui.my_profile.advanced_privacy.AdvancedPrivacyContract;

/**
 * Created by 8570p on 2016-12-17.
 */

public class AdvancedPrivacyTrichoiceModel extends AdvancedPrivacyChoiceModel {

    private AdvancedPrivacyContract.Trichoice mSelection;

    public AdvancedPrivacyTrichoiceModel(String title, String firstChoice, String secondChoice, String thirdChoice) {
        super(title);
        addChoices(firstChoice, secondChoice, thirdChoice);
        mSelection = AdvancedPrivacyContract.Trichoice.FIRST;
    }

    public AdvancedPrivacyContract.Trichoice getSelection() {
        return mSelection;
    }

    public String getChoiceName(AdvancedPrivacyContract.Trichoice choice) {
        return getChoiceName(choice.value());
    }

    public void setSelection(AdvancedPrivacyContract.Trichoice selection) {
        mSelection = selection;
    }
}
