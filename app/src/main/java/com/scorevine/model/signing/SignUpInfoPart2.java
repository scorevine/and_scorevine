package com.scorevine.model.signing;

import java.io.Serializable;

/**
 * Created by Yoga on 2016-12-07.
 */
public class SignUpInfoPart2 implements Serializable {

    public String birthdate;
    public String gender;
    public String country;
    public String smsCode;
    public String refferal;
    public String photoUri;
}
