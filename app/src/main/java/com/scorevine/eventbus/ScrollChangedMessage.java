package com.scorevine.eventbus;

/**
 * Created by adrianjez on 16.12.2016.
 */
public class ScrollChangedMessage {
    private int mScrollY;
    private boolean mFirstScroll;
    private boolean mDragging;
    private int mCurrentScrollY;

    private ScrollChangedMessage(Builder builder){
        this.mScrollY = builder.scrollY;
        this.mFirstScroll = builder.firstScroll;
        this.mDragging = builder.dragging;
        this.mCurrentScrollY = builder.currentScrollY;
    }

    public int getScrollY() {
        return mScrollY;
    }

    public boolean isFirstScroll() {
        return mFirstScroll;
    }

    public boolean isDragging() {
        return mDragging;
    }

    public int getmCurrentScrollY() {
        return mCurrentScrollY;
    }

    public static class Builder {
        private int scrollY;
        private boolean firstScroll;
        private boolean dragging;
        private int currentScrollY;


        public Builder setScrollY(int scrollY){
            this.scrollY = scrollY;
            return this;
        }

        public Builder setFirstScroll(boolean firstScroll){
            this.firstScroll = firstScroll;
            return this;
        }

        public Builder setDragging(boolean dragging){
            this.dragging = dragging;
            return this;
        }

        public Builder setCurrentScrollY(int currentScrollY){
            this.currentScrollY = currentScrollY;
            return this;
        }

        public ScrollChangedMessage build(){
            return new ScrollChangedMessage(this);
        }
    }
}