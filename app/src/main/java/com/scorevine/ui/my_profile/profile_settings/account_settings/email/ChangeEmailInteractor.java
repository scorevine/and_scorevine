package com.scorevine.ui.my_profile.profile_settings.account_settings.email;

import android.support.annotation.Nullable;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.profile.ChangeEmailResponse;
import com.scorevine.api.profile.UserProfileInterface;
import com.scorevine.helpers.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-21.
 */

public class ChangeEmailInteractor implements ChangeEmailContract.Interactor {


    UserProfileInterface userProfileInterface = new ApiManager().getRetrofit().create(UserProfileInterface.class);

    @Override
    public void changeEmail(String token, String email, @Nullable String password, final ChangeEmailContract.Interactor.OnFinishedListener listener) {

        userProfileInterface.changeEmail(token, email, password).enqueue(new Callback<ChangeEmailResponse>() {
            @Override
            public void onResponse(Call<ChangeEmailResponse> call, Response<ChangeEmailResponse> response) {
                switch (response.code()) {
                    case 200:
                        listener.onResponse(response.body());
                        break;
                    case 401:
                    case 406:
                    case 410:
                    case 422:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<ChangeEmailResponse> call, Throwable t) {
                listener.onFailure(R.string.check_your_internet_connection);

            }
        });
    }

}
