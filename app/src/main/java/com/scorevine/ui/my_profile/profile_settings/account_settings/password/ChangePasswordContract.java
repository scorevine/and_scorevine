package com.scorevine.ui.my_profile.profile_settings.account_settings.password;


import com.scorevine.api.APIErrorList;
import com.scorevine.api.StatusResponse;
import com.scorevine.ui.validation.ValidateInputPresenterInterface;
import com.scorevine.ui.validation.ValidateInputView;

public interface ChangePasswordContract {

    interface View extends ValidateInputView {
        String getOldPassword();

        String getNewPassword();

        void showTextMessage(String message, boolean isLong);

        void showMessage(int messageId, boolean isLong);

    }

    interface Presenter extends ValidateInputPresenterInterface {
        void subscribe();

        void unsubscribe();

    }

    interface Interactor {

        interface OnFinishedListener {
            void onResponse(StatusResponse response);

            void onFailure(int responseId);

            void onError(APIErrorList errorsList);

        }

        void changePassword(String token, String password, String newPassword, final OnFinishedListener listener);
    }

}
