package com.scorevine.ui.my_profile.profile_settings.account_settings.password;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.StatusResponse;
import com.scorevine.api.profile.UserProfileInterface;
import com.scorevine.helpers.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-21.
 */
public class ChangePasswordInteractor implements ChangePasswordContract.Interactor {


    UserProfileInterface userProfileInterface = new ApiManager().getRetrofit().create(UserProfileInterface.class);

    @Override
    public void changePassword(String token, String password, String newPassword, final OnFinishedListener listener) {
        userProfileInterface.changePassword(token, password, newPassword).enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                switch (response.code()) {
                    case 200:
                        listener.onResponse(response.body());
                        break;
                    case 401:
                    case 406:
                    case 410:
                    case 422:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                listener.onFailure(R.string.check_your_internet_connection);

            }
        });
    }
}
