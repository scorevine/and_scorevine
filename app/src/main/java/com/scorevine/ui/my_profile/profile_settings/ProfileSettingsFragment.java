package com.scorevine.ui.my_profile.profile_settings;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;

import com.scorevine.R;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.model.settings.AccountSettings;
import com.scorevine.model.settings.GeneralProfileInfo;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.my_profile.advanced_privacy.AdvancedPrivacyActivity;
import com.scorevine.ui.my_profile.profile_settings.account_settings.email.ChangeEmailFragment;
import com.scorevine.ui.my_profile.profile_settings.account_settings.mobile.ChangeMobileFragment;
import com.scorevine.ui.my_profile.profile_settings.account_settings.password.ChangePasswordFragment;
import com.scorevine.ui.my_profile.profile_settings.adapter.ProfileSettingsAdapter;
import com.scorevine.ui.my_profile.profile_settings.adapter.ProfileSettingsAdapterActions;
import com.scorevine.ui.my_profile.profile_settings.adapter.SettingsSection;
import com.scorevine.ui.my_profile.profile_settings.adapter.SubSetting;
import com.scorevine.ui.todo.ToDoFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Agnieszka Duleba on 2016-12-16.
 */

public class ProfileSettingsFragment extends Fragment implements ProfileSettingsContract.View, ProfileSettingsAdapterActions {

    public static final String TAG_FRAGMENT_PROFILE_SETTINGS = ProfileSettingsFragment.class.getSimpleName();
    private static final String GENERAL_INFO = "general_user_info";
    private static final String NOTIFICATIONS_SETTINGS = "notifications_settings";
    private static final String ACCOUNT_SETTINGS = "account_settings";


    @BindView(R.id.settings_rv)
    RecyclerView mSettingsRV;

    @BindArray(R.array.settings_sections)
    String[] settingsSectionsLabels;

    @BindArray(R.array.genders)
    String[] mGenders;

    ListPopupWindow listPopupWindow;
    Calendar myCalendar = Calendar.getInstance();

    ProfileSettingsAdapter mAdapter;
    ProfileSettingsContract.Presenter mPresenter;
    ArrayList<SettingsSection> settingsSections;
    AccountSettings mAccountSettings = new AccountSettings();
    GeneralProfileInfo generalInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProfileSettingsAdapter.sToggledCategory = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_settings, null, false);
        ButterKnife.bind(this, view);
        mPresenter = new ProfileSettingsPresenter(this, this.getActivity());
        if (savedInstanceState == null) {
            mPresenter.initAdapter();
        } else {
            initAdapterData();
        }
        ((ProfileSettingsActivity) getActivity()).setUpView(getString(R.string.settings_title), getString(R.string.settings_bottom_button_label));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();

    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }


    public void initAdapterData() {
        settingsSections = new ArrayList<>();
        for (int i = 0; i < settingsSectionsLabels.length; i++) {
            ArrayList<SubSetting> subSetting = new ArrayList<>();
            subSetting.add(new SubSetting("single_subsetting", 1));
            settingsSections.add(new SettingsSection(settingsSectionsLabels[i], subSetting));
        }

        mAdapter = new ProfileSettingsAdapter(this.getActivity(), settingsSections, mAccountSettings, generalInfo, this);
        mSettingsRV.setAdapter(mAdapter);
        mSettingsRV.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mAdapter.toggleGroup(ProfileSettingsAdapter.sToggledCategory);
        mAdapter.setStaticData(mGenders, mPresenter.getCountriesList(this.getActivity()), mPresenter.getCitiesList());
    }

    @Override
    public void setAccountSettings(String email, boolean hasPassword, String countryCode, String mobile) {
        if (mAccountSettings == null) {
            mAccountSettings = new AccountSettings();
        }
        mAccountSettings.setEmail(email);
        mAccountSettings.setHasPassword(hasPassword);
        mAccountSettings.setCountryCode(countryCode);
        mAccountSettings.setMobile(mobile);

    }

    @Override
    public void setGeneralInfo(String firstName, String surname, String username, String birthDate, String gender, String country, String city, String story) {
        if (generalInfo == null) {
            generalInfo = new GeneralProfileInfo();
        }
        generalInfo.setFirstName(firstName);
        generalInfo.setSurname(surname);
        generalInfo.setUniquename(username);
        generalInfo.setBirthDate(birthDate);
        generalInfo.setGender(gender);
        generalInfo.setCountry(country);
        generalInfo.setCity(city);
        generalInfo.setStory(story);

    }

    @Override
    public void showMessage(int messageId, boolean isLong) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        if (getActivity() != null)
            ((BaseActivity) getActivity()).showToast(message, isLong);
    }

    @Override
    public boolean getGeneralInfoChanged() {
        //TODO: if required - check for changed in general info??
        return true;
    }

    @Override
    public GeneralProfileInfo getGeneralInfo() {
        if (mAdapter != null)
            return mAdapter.returnGeneralInfoChanges();
        else
            return null;
    }

    @Override
    public void notifyDataChanged() {
        mAdapter.setDownloadedData(mAccountSettings, generalInfo);
    }

    @Override
    public void onChangeMobileClicked(String code, String mobile, boolean hasPassword) {
        Fragment nextFragment = ChangeMobileFragment.newInstance(code, mobile, hasPassword);
        FragmentHelper.replaceFragment(getFragmentManager(), nextFragment, ChangeMobileFragment.TAG_FRAGMENT_CHANGE_MOBILE);

    }

    @Override
    public void onChangePasswordClicked(boolean hasPassword) {
        if (hasPassword) {
            Fragment nextFragment = new ChangePasswordFragment();
            FragmentHelper.replaceFragment(getFragmentManager(), nextFragment, ChangePasswordFragment.TAG_FRAGMENT_CHANGE_PASSWORD);
        } else {
            showTextMessage(getString(R.string.account_password_reset_locked_fb_user), true); //TODO: change text
        }
    }

    @Override
    public void onChangeEmailClicked(String email, boolean hasPassword) {
        Fragment nextFragment = ChangeEmailFragment.newInstance(email, hasPassword);
        FragmentHelper.replaceFragment(getFragmentManager(), nextFragment, ChangeEmailFragment.TAG_FRAGMENT_CHANGE_EMAIL);


    }

    @Override
    public void onCalendarClicked(final EditText textInputEditText) {
        KeyboardHelper.hideKeyboard(getView(), getActivity());
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                textInputEditText.setText(DateFormatHelper.getDateWithFormat(DateFormatHelper.APP_PROFILE_DATE_FORMAT, myCalendar.getTime()));
                generalInfo.setBirthDate(textInputEditText.getText().toString());

            }
        };

        Date date1 = new Date();
        Calendar calendar21 = Calendar.getInstance();
        calendar21.setTime(date1);
        calendar21.add(Calendar.YEAR, -21);
        long time = calendar21.getTime().getTime();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(time);
        datePickerDialog.show();
    }


    @Override
    public void onSpinnerClicked(final EditText textInputEditText, final List<String> listItems) {
        KeyboardHelper.hideKeyboard(getView(), getActivity());

        listPopupWindow = new ListPopupWindow(getActivity());
        listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.dropdown_item, listItems));
        listPopupWindow.setAnchorView(textInputEditText);
        listPopupWindow.setModal(true);
        listPopupWindow.setWidth(getResources().getDimensionPixelSize(R.dimen.code_spinner_width));
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    textInputEditText.setText(listItems.get(position));
                    listPopupWindow.dismiss();
                }
            }
        });

        listPopupWindow.show();
    }

    @Override
    public void onHelpClicked() {
        KeyboardHelper.hideKeyboard(getView(), getActivity());

    }


    @Override
    public void onAdvancedPrivacyClicked() {
        startActivity(new Intent(this.getActivity(), AdvancedPrivacyActivity.class));

    }

    @Override
    public void onBlockListClicked() {
        FragmentHelper.replaceFragment(getFragmentManager(), ToDoFragment.newInstance("Block list"), ToDoFragment.TAG_FRAGMENT_TODO);
        ((ProfileSettingsActivity) getActivity()).setUpView(getString(R.string.title_block_list), getString(R.string.settings_bottom_button_label));

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(GENERAL_INFO,
                mAdapter.returnGeneralInfoChanges() == null ? generalInfo : mAdapter.returnGeneralInfoChanges());
        outState.putParcelable(ACCOUNT_SETTINGS,
                mAdapter.getAccountSettings() == null ? mAccountSettings : mAdapter.getAccountSettings());
        mAdapter.onSaveInstanceState(outState);

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            mAdapter.onRestoreInstanceState(savedInstanceState);
            generalInfo = savedInstanceState.getParcelable(GENERAL_INFO);
            mAccountSettings = savedInstanceState.getParcelable(ACCOUNT_SETTINGS);
            if (generalInfo != null && mAccountSettings != null) {
                setGeneralInfo(generalInfo.getFirstName(), generalInfo.getSurname(),
                        generalInfo.getUniquename(),   //change to city?
                        generalInfo.getBirthDate(), generalInfo.getGender(), generalInfo.getCountry(),
                        generalInfo.getStory(), generalInfo.getStory());

                setAccountSettings(mAccountSettings.getEmail(), mAccountSettings.isHasPassword(),
                        mAccountSettings.getCountryCode(), mAccountSettings.getMobile());
                notifyDataChanged();
            } else {
                mPresenter.initAdapter();
            }

        }
    }
}
