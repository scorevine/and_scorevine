package com.scorevine.ui.my_profile.profile_settings.account_settings.mobile;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.SuccessResponse;
import com.scorevine.api.profile.UserProfileInterface;
import com.scorevine.helpers.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-21.
 */
public class ChangeMobileInteractor implements ChangeMobileContract.Interactor {
    UserProfileInterface userProfileInterface = new ApiManager().getRetrofit().create(UserProfileInterface.class);


    @Override
    public void changeMobile(String token, String mobile, String password, final ChangeMobileContract.Interactor.OnFinishedListenerMobile listener) {
        userProfileInterface.changeMobile(token, mobile, password).enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        listener.onResponseMobile(response.body());
                        break;
                    case 401:
                    case 406:
                    case 409:
                    case 410:
                    case 422:
                        listener.onErrorMobile(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                listener.onFailureMobile(R.string.check_your_internet_connection);

            }
        });
    }

    @Override
    public void sendCode(String token, String code, final ChangeMobileContract.Interactor.OnFinishedListenerCode listener) {
        userProfileInterface.sendCode(token, code).enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                switch (response.code()) {
                    case 200:
                        listener.onResponseCode(response.body());
                        break;
                    case 401:
                    case 406:
                    case 410:
                    case 422:
                        listener.onErrorCode(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                listener.onFailureCode(R.string.check_your_internet_connection);

            }
        });
    }

}
