package com.scorevine.ui.my_profile.profile_settings.account_settings.mobile;

import android.support.annotation.Nullable;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.SuccessResponse;
import com.scorevine.ui.validation.ValidateInputPresenterInterface;
import com.scorevine.ui.validation.ValidateInputView;

/**
 * Created by 8570p on 2016-12-17.
 */

public interface ChangeMobileContract {

    interface View extends ValidateInputView {
        String getMobile();

        String getPassword();

        void showTextMessage(String message, boolean isLong);

        void showMessage(int messageId, boolean isLong);

        void onResendClicked();

        void onConfirmClicked(String code);

        void showSmsCodeDialog();
    }

    interface Presenter extends ValidateInputPresenterInterface {
        void subscribe();

        void unsubscribe();

        void sendCode(String code);

        void sendCodeRequest();

    }

    interface Interactor {

        interface OnFinishedListenerMobile {
            void onResponseMobile(SuccessResponse response);

            void onFailureMobile(int responseId);

            void onErrorMobile(APIErrorList errorsList);

        }

        interface OnFinishedListenerCode {
            void onResponseCode(SuccessResponse response);

            void onFailureCode(int responseId);

            void onErrorCode(APIErrorList errorsList);

        }

        void changeMobile(String token, String mobile, @Nullable String password, final ChangeMobileContract.Interactor.OnFinishedListenerMobile listener);

        void sendCode(String token, String code, final ChangeMobileContract.Interactor.OnFinishedListenerCode listener);

    }

}
