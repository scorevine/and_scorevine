package com.scorevine.ui.my_profile.profile_settings.account_settings.password;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.R;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.my_profile.profile_settings.ProfileSettingsActivity;
import com.scorevine.ui.validation.InputTypeSV;
import com.scorevine.ui.validation.ValidateInputFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Agnieszka Duleba on 2016-12-20.
 */
public class ChangePasswordFragment extends ValidateInputFragment implements ChangePasswordContract.View {
    public static final String TAG_FRAGMENT_CHANGE_PASSWORD = ChangePasswordFragment.class.getSimpleName();


    @BindView(R.id.current_password_ti)
    TextInputLayout mCurrentPassword;
    @BindView(R.id.new_password_ti)
    TextInputLayout mNewPassword;
    @BindView(R.id.confirm_password_ti)
    TextInputLayout mConfirmNewPassword;

    ChangePasswordPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, null, false);
        ButterKnife.bind(this, view);
        mPresenter = new ChangePasswordPresenter(this, this.getActivity());
        setPasswordTextChangedListener(InputTypeSV.CURRENT_PASSWORD);
        setPasswordTextChangedListener();
        setConfirmPasswordTextChangedListener();
        ((ProfileSettingsActivity) getActivity()).setUpView(getString(R.string.title_password_change), getString(R.string.settings_bottom_button_label));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();

    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @Override
    public String getOldPassword() {
        return mCurrentPassword.getEditText().getText().toString();
    }

    @Override
    public String getNewPassword() {
        return mNewPassword.getEditText().getText().toString();
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        if ((BaseActivity) getActivity() != null)
            ((BaseActivity) getActivity()).showToast(message, isLong);
    }


    @Override
    public void showMessage(int messageId, boolean isLong) {
        if ((BaseActivity) getActivity() != null)
            ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
    }

    @Override
    protected TextInputLayout getTextInputLayout(InputTypeSV inputType) {
        TextInputLayout textInputLayout = null;
        switch (inputType) {
            case PASSWORD:
                textInputLayout = mNewPassword;
                break;
            case CONFIRM_PASSWORD:
                textInputLayout = mConfirmNewPassword;
                break;
            case CURRENT_PASSWORD:
                textInputLayout = mCurrentPassword;
                break;
            default:
                break;
        }
        return textInputLayout;
    }
}
