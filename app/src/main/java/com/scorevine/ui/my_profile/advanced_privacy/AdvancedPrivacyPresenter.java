package com.scorevine.ui.my_profile.advanced_privacy;

import android.content.Context;

import com.scorevine.model.adv_privacy.AdvancedPrivacyOptionsFactory;

/**
 * Created by 8570p on 2016-12-17.
 */

public class AdvancedPrivacyPresenter {

    private AdvancedPrivacyContract.OuterView mOuterView;
    private AdvancedPrivacyContract.InnerView mInnerView;
    private Context mContext;

    private AdvancedPrivacyOptionsFactory mOptionsFactory;

    public AdvancedPrivacyPresenter(Context context, AdvancedPrivacyContract.OuterView outerView, AdvancedPrivacyContract.InnerView innerView) {
        mOuterView = outerView;
        mInnerView = innerView;
        mContext = context;
        mOptionsFactory = new AdvancedPrivacyOptionsFactory(mContext);
    }

    public void generateChoices() {
        mInnerView.presentChoices(mOptionsFactory.generateBichoiceModels(),
                mOptionsFactory.generateTrichoiceModels());
    }
}
