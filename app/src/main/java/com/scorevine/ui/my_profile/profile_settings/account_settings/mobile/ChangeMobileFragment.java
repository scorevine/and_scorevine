package com.scorevine.ui.my_profile.profile_settings.account_settings.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.ListPopupWindow;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.scorevine.R;
import com.scorevine.ui.dialogs.SmsCodeDialogFragment;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.my_profile.profile_settings.ProfileSettingsActivity;
import com.scorevine.ui.validation.InputTypeSV;
import com.scorevine.ui.validation.ValidateInputFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Agnieszka Duleba on 2016-12-20.
 */
public class ChangeMobileFragment extends ValidateInputFragment implements ChangeMobileContract.View {
    public static final String TAG_FRAGMENT_CHANGE_MOBILE = ChangeMobileFragment.class.getSimpleName();
    private static final String MOBILE_BUNDLE_KEY = "mobile";
    private static final String MOBILE_CODE_BUNDLE_KEY = "code";
    private static final String HAS_PASSWORD_BUNDLE_KEY = "password";

    ChangeMobilePresenter mPresenter;

    @BindView(R.id.current_code_ti)
    TextInputLayout mCurrentCodeTI;
    @BindView(R.id.current_mobile_ti)
    TextInputLayout mCurrentNumberTI;

    @BindView(R.id.confirm_password_ti)
    TextInputLayout mPasswordTI;

    @BindView(R.id.phone_code_ti)
    TextInputLayout mNewCodeTI;
    @BindView(R.id.new_mobile_ti)
    TextInputLayout mNewNumberTI;


    public static ChangeMobileFragment newInstance(String code, String mobile, boolean hasPassword) {
        Bundle args = new Bundle();
        args.putString(MOBILE_CODE_BUNDLE_KEY, code);
        args.putString(MOBILE_BUNDLE_KEY, mobile);
        args.putBoolean(HAS_PASSWORD_BUNDLE_KEY, hasPassword);
        ChangeMobileFragment f = new ChangeMobileFragment();
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_mobile, null, false);
        ButterKnife.bind(this, view);
        mPresenter = new ChangeMobilePresenter(this, this.getActivity());
        mCurrentCodeTI.getEditText().setText(getArguments().getString(MOBILE_CODE_BUNDLE_KEY));
        mCurrentNumberTI.getEditText().setText(getArguments().getString(MOBILE_BUNDLE_KEY));
        if (getArguments().getBoolean(HAS_PASSWORD_BUNDLE_KEY)) {
            mPasswordTI.setVisibility(View.VISIBLE);
            setPasswordTextChangedListener(InputTypeSV.PASSWORD);
        } else {
            mPasswordTI.setVisibility(View.GONE);
        }
        setPhoneNumberTextChangedListener();
        ((ProfileSettingsActivity) getActivity()).setUpView(getString(R.string.title_mobile_change), getString(R.string.settings_bottom_button_label));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.unsubscribe();
    }

    @Override
    public String getMobile() {

        return mNewCodeTI.getEditText().getText().toString() + "-" + mNewNumberTI.getEditText().getText().toString();
    }


    @Nullable
    @Override
    public String getPassword() {
        if (getArguments().getBoolean(HAS_PASSWORD_BUNDLE_KEY))
            return mPasswordTI.getEditText().getText().toString();
        else
            return null;
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        if ((BaseActivity) getActivity() != null)
            ((BaseActivity) getActivity()).showToast(message, isLong);
    }


    @Override
    public void showMessage(int messageId, boolean isLong) {
        if ((BaseActivity) getActivity() != null)
            ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
    }

    protected SmsCodeDialogFragment smsCodeDialogFragment;

    @Override
    public void showSmsCodeDialog() {
        smsCodeDialogFragment = SmsCodeDialogFragment.newInstance(new Bundle(),
                getString(R.string.toast_smscode_send_to_mobile),
                getString(R.string.sms_dialog_left_btn),
                getString(R.string.sms_dialog_right_btn), true);
        smsCodeDialogFragment.show(getFragmentManager(), "sms_dialog");
    }

    @Override
    public void onResendClicked() {
        mPresenter.sendCodeRequest();
    }

    public void onConfirmClicked(String code) {
        if (code.equals(""))
            showMessage(R.string.change_mobile_enter_sms_code, true);
        else {
            mPresenter.sendCode(code);
            smsCodeDialogFragment.dismiss();
        }

    }

    @OnClick(R.id.phone_code_et)
    public void onCountryCodesClicked() {
        final List<String> listItems = mPresenter.getCountryCodesList(this.getActivity());

        final ListPopupWindow listPopupWindow = new ListPopupWindow(getActivity());
        listPopupWindow.setAdapter(new ArrayAdapter(getActivity(), R.layout.dropdown_item, listItems));
        listPopupWindow.setAnchorView(mNewCodeTI.getEditText());
        listPopupWindow.setModal(true);
        listPopupWindow.setWidth(getResources().getDimensionPixelSize(R.dimen.code_spinner_width));
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    mNewCodeTI.getEditText().setText(mPresenter.getCodeForPos(position));
                    listPopupWindow.dismiss();
                }
            }
        });

        listPopupWindow.show();
    }


    @Override
    protected TextInputLayout getTextInputLayout(InputTypeSV inputType) {
        TextInputLayout textInputLayout = null;
        switch (inputType) {
            case PASSWORD:
                textInputLayout = mPasswordTI;
                break;
            case PHONE_NUMBER:
                textInputLayout = mNewNumberTI;
                break;
            default:
                break;
        }
        return textInputLayout;
    }
}
