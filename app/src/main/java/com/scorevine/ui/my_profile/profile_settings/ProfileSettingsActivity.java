package com.scorevine.ui.my_profile.profile_settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.eventbus.UpdateProfileDataEvent;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.ui.BaseActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Agnieszka Duleba on 2016-12-14.
 */

public class ProfileSettingsActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitleTV;

    @BindView(R.id.save_bnt)
    TextView mSaveBtnTV;

    private FragmentManager mFragmentManager;
    private Fragment mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);
        ButterKnife.bind(this);

        mFragmentManager = getSupportFragmentManager();
        Fragment firstFragment = mFragmentManager.findFragmentById(R.id.fragment_container);

        if (firstFragment == null) {
            firstFragment = new ProfileSettingsFragment();
            FragmentHelper.addFirstFragment(mFragmentManager, firstFragment, ProfileSettingsFragment.TAG_FRAGMENT_PROFILE_SETTINGS);
            mContent = firstFragment;
        }

        if (savedInstanceState != null) {
            //Restore the fragment's instance
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        } else {
            mContent = FragmentHelper.getVisibleFragment(mFragmentManager);
        }
    }

    @OnClick(R.id.save_bnt)
    public void onSaveClicked() {
        EventBus.getDefault().post(new UpdateProfileDataEvent());
    }

    @OnClick(R.id.backArrowButton)
    public void onBackClicked() {
        KeyboardHelper.hideKeyboard(getCurrentFocus(), this);
        onBackPressed();
    }

    public void setUpView(String toolbarTitle, String bottomButton) {
        mToolbarTitleTV.setText(toolbarTitle);
        mSaveBtnTV.setText(bottomButton);
    }

}
