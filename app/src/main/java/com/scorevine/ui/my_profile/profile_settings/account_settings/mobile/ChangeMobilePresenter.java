package com.scorevine.ui.my_profile.profile_settings.account_settings.mobile;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.SuccessResponse;
import com.scorevine.eventbus.UpdateProfileDataEvent;
import com.scorevine.helpers.LoadFileHelper;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.model.CountryCodes;
import com.scorevine.ui.validation.ValidateInputPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-20.
 */
public class ChangeMobilePresenter extends ValidateInputPresenter implements ChangeMobileContract.Presenter, ChangeMobileContract.Interactor.OnFinishedListenerMobile, ChangeMobileContract.Interactor.OnFinishedListenerCode {

    ChangeMobileContract.View mView;
    ChangeMobileContract.Interactor mInteractor;
    FragmentActivity mActivity;
    String mToken;
    private List<CountryCodes.CountryCode> countryCodesList;

    public ChangeMobilePresenter(ChangeMobileFragment view, FragmentActivity activity) {
        super(view);
        mView = view;
        mActivity = activity;
        mInteractor = new ChangeMobileInteractor();
        mToken = SecurityHelper.encodeToken(new SharedPrefHelper().getSharedPref(mActivity, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN));

    }

    @Override
    public void subscribe() {
        EventBus.getDefault().register(this);

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }
    @Override
    public void sendCode(String code) {
        mInteractor.sendCode(mToken, code, this);
    }

    @Override
    public void sendCodeRequest(){
        mInteractor.changeMobile(mToken, mView.getMobile(), mView.getPassword(), this);

    }

    @Subscribe
    public void onEvent(UpdateProfileDataEvent event) {
        sendCodeRequest();
    }


    public ArrayList<String> getCountryCodesList(Activity activity) {
        countryCodesList = LoadFileHelper.getCountryCodesList(activity);
        final ArrayList<String> countriesList = new ArrayList<>();

        for (CountryCodes.CountryCode element : countryCodesList) {
            countriesList.add(element.getCountryName());
        }

        return countriesList;
    }
    public String getCodeForPos(int pos){

        return countryCodesList.get(pos).getDialCode();
    }

    @Override
    public void onResponseCode(SuccessResponse response) {
        mView.showMessage(R.string.account_phone_number_changed, true);
        mActivity.onBackPressed();
    }

    @Override
    public void onFailureCode(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onErrorCode(APIErrorList errorsList) {
        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

    }

    @Override
    public void onResponseMobile(SuccessResponse response) {
        mView.showSmsCodeDialog();

    }

    @Override
    public void onFailureMobile(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onErrorMobile(APIErrorList errorsList) {
        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

    }
}
