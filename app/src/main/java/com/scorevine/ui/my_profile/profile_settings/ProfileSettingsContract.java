package com.scorevine.ui.my_profile.profile_settings;

import android.app.Activity;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.SuccessResponse;
import com.scorevine.api.profile.UserProfile;
import com.scorevine.model.settings.GeneralProfileInfo;

import java.util.ArrayList;

/**
 * Created by Agnieszka Duleba on 2016-12-16.
 */

public interface ProfileSettingsContract {

    interface View {
        void initAdapterData();

        void setAccountSettings(String email, boolean hasPassword, String countryCode, String mobile);

        void setGeneralInfo(String firstName, String surname, String username, String birthDate, String gender, String country, String city, String story);

        void showMessage(int messageId, boolean isLong);
        void showTextMessage(String message, boolean isLong);

        boolean getGeneralInfoChanged();

        GeneralProfileInfo getGeneralInfo();

        void notifyDataChanged();

    }

    interface Presenter {
        void subscribe();
        void unsubscribe();

        void initAdapter();

        ArrayList<String> getCountriesList(Activity activity);

        ArrayList<String> getCitiesList();
    }

    interface Interactor {

        interface OnGetFinishedListener {
            void onGetResponse(UserProfile response);
            void onGetFailure(int responseId);
            void onGetError(APIErrorList errorsList);
        }
        interface OnPutFinishedListener {
            void onPutResponse(SuccessResponse response);
            void onPutFailure(int responseId);
            void onPutError(APIErrorList errorsList);
        }


        void getUserProfile(String token, final OnGetFinishedListener listener);
        void saveUserProfile(String token, String birthDate, String gender, String country,
                             String firstName, String surname, String story,
                             final OnPutFinishedListener listener);

    }

}
