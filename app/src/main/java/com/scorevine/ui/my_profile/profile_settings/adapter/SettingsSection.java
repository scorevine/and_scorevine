package com.scorevine.ui.my_profile.profile_settings.adapter;

import android.support.annotation.Nullable;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-15.
 */

public class SettingsSection extends ExpandableGroup<SubSetting> {

    private Integer id;
    private String category;
    private ArrayList<SubSetting> subCategories;

    public SettingsSection(String title, List<SubSetting> items) {
        super(title, items);
        if (id == null) {
            id = 800;
        }
        this.subCategories = (ArrayList<SubSetting>) items;
        category = title;
    }

    public String getCategoryName() {
        return category;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public List<SubSetting> getSubCategories() {
        return subCategories;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", subCategories=" + subCategories +
                '}';
    }

    @Override
    public List<SubSetting> getItems() {
        return getSubCategories();
    }

    @Override
    public String getTitle() {
        return getCategoryName();
    }
}
