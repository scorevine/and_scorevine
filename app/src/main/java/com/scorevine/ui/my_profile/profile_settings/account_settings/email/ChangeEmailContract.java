package com.scorevine.ui.my_profile.profile_settings.account_settings.email;

import android.support.annotation.Nullable;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.profile.ChangeEmailResponse;
import com.scorevine.ui.validation.InputTypeSV;
import com.scorevine.ui.validation.ValidateInputPresenterInterface;


public interface ChangeEmailContract {

    interface View {
        String getEmail();

        void showInputError(String errorMessage, InputTypeSV inputType);

        void hideInputError(InputTypeSV inputType);

        String getPassword();

        void showTextMessage(String message, boolean isLong);

        void showMessage(int messageId, boolean isLong);

    }

    interface Presenter extends ValidateInputPresenterInterface {
        void subscribe();

        void unsubscribe();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onResponse(ChangeEmailResponse response);

            void onFailure(int responseId);

            void onError(APIErrorList errorsList);

        }

        void changeEmail(String token, String email, @Nullable String password, final OnFinishedListener listener);
    }

}
