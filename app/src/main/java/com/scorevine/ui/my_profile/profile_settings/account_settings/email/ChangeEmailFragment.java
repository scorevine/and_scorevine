package com.scorevine.ui.my_profile.profile_settings.account_settings.email;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.R;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.my_profile.profile_settings.ProfileSettingsActivity;
import com.scorevine.ui.validation.InputTypeSV;
import com.scorevine.ui.validation.ValidateInputFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Agnieszka Duleba on 2016-12-20.
 */
public class ChangeEmailFragment extends ValidateInputFragment implements ChangeEmailContract.View {
    public static final String TAG_FRAGMENT_CHANGE_EMAIL = ChangeEmailFragment.class.getSimpleName();
    private static final String EMAIL_BUNDLE_KEY = "email";
    private static final String HAS_PASSWORD_BUNDLE_KEY = "has_password";

    ChangeEmailPresenter mPresenter;

    @BindView(R.id.current_email_ti)
    TextInputLayout mCurrentEmail;
    @BindView(R.id.new_email_ti)
    TextInputLayout mNewEmail;
    @BindView(R.id.confirm_password_ti)
    TextInputLayout mPassword;


    public static ChangeEmailFragment newInstance(String email, boolean hasPassword) {
        Bundle args = new Bundle();
        args.putString(EMAIL_BUNDLE_KEY, email);
        args.putBoolean(HAS_PASSWORD_BUNDLE_KEY, hasPassword);
        ChangeEmailFragment f = new ChangeEmailFragment();
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_email, null, false);
        ButterKnife.bind(this, view);
        mPresenter = new ChangeEmailPresenter(this, this.getActivity());
        mCurrentEmail.getEditText().setText(getArguments().getString(EMAIL_BUNDLE_KEY));
        if (getArguments().getBoolean(HAS_PASSWORD_BUNDLE_KEY))
            mPassword.setVisibility(View.VISIBLE);
        else
            mPassword.setVisibility(View.GONE);

        setPasswordTextChangedListener(InputTypeSV.PASSWORD);
        setEmailTextChangedListener();
        ((ProfileSettingsActivity) getActivity()).setUpView(getString(R.string.title_email_change), getString(R.string.settings_bottom_button_label));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();

    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @Override
    public String getEmail() {
        return mNewEmail.getEditText().getText().toString();
    }

    @Nullable
    @Override
    public String getPassword() {
        if (getArguments().getBoolean(HAS_PASSWORD_BUNDLE_KEY))
            return mPassword.getEditText().getText().toString();
        else
            return null;
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        if ((BaseActivity) getActivity() != null)
            ((BaseActivity) getActivity()).showToast(message, isLong);
    }


    @Override
    public void showMessage(int messageId, boolean isLong) {
        if ((BaseActivity) getActivity() != null)
            ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
    }

    @Override
    public TextInputLayout getTextInputLayout(InputTypeSV inputType) {
        TextInputLayout textInputLayout = null;
        switch (inputType) {
            case PASSWORD:
                textInputLayout = mPassword;
                break;
            case EMAIL:
                textInputLayout = mNewEmail;
                break;
            default:
                break;
        }
        return textInputLayout;
    }
}
