package com.scorevine.ui.my_profile.profile_settings.adapter;

import android.widget.EditText;

import java.util.List;

/**
 * Created by Yoga on 2016-12-20.
 */

public interface ProfileSettingsAdapterActions {

    //account settings
    void onChangeMobileClicked(String code, String mobile, boolean hasPassword);
    void onChangePasswordClicked(boolean hasPassword);
    void onChangeEmailClicked(String email, boolean hasPassword);

    //general info
    void onSpinnerClicked(final EditText textInputEditText, final List<String> listItems );
    void onCalendarClicked(final EditText textInputEditText);

    void onAdvancedPrivacyClicked();
    void onBlockListClicked();

    void onHelpClicked();
}
