package com.scorevine.ui.my_profile.profile_settings.adapter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Agnieszka Duleba on 2016-12-15.
 */
public class SubSetting implements Parcelable {


    private int id;
    private String setting;

    public SubSetting(String setting, int id) {
        this.setting = setting;
    }

    protected SubSetting(Parcel in) {
        id = in.readInt();
        setting = in.readString();
    }


    public static final Creator<SubSetting> CREATOR = new Creator<SubSetting>() {
        @Override
        public SubSetting createFromParcel(Parcel in) {
            return new SubSetting(in);
        }

        @Override
        public SubSetting[] newArray(int size) {
            return new SubSetting[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getSetting() {
        return setting;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "id=" + id +
                ", setting='" + setting + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(setting);
    }
}
