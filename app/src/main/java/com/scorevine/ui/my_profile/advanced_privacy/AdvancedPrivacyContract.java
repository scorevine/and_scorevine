package com.scorevine.ui.my_profile.advanced_privacy;

import com.scorevine.model.adv_privacy.AdvancedPrivacyBichoiceModel;
import com.scorevine.model.adv_privacy.AdvancedPrivacyTrichoiceModel;

import java.util.List;

/**
 * Created by 8570p on 2016-12-17.
 */

public interface AdvancedPrivacyContract {

    enum AdvancedPrivacyTrichoiceType {
        FOLLOW_ME(0),
        FIND_ME(1),
        SEE_POSTS(2);

        private int mValue;
        AdvancedPrivacyTrichoiceType(int value) {
            mValue = value;
        }

        public int value() {
            return mValue;
        }
    }

    enum AdvancedPrivacyBichoiceType {
        GENERAL_POSTS_NOTIFICATION(0),
        GROUP_POSTS_NOTIFICATION(1),
        COMMENTS_NOTIFICATION(2),
        VOTES_NOTIFICATION(3);

        private int mValue;
        AdvancedPrivacyBichoiceType(int value) {
            mValue = value;
        }

        public int value() {
            return mValue;
        }
    }

    // Unfortunately, enums cannot extend another enums. :'(
    enum Trichoice {
        FIRST(0),
        SECOND(1),
        THIRD(2);

        private int mValue;
        Trichoice(int value) {
            mValue = value;
        }

        public int value() {
            return mValue;
        }
    }

    enum Bichoice {
        FIRST(0),
        SECOND(1);

        private int mValue;
        Bichoice(int value) {
            mValue = value;
        }

        public int value() {
            return mValue;
        }
    }

    interface InnerView {
        void presentChoices(List<AdvancedPrivacyBichoiceModel>bichoices, List<AdvancedPrivacyTrichoiceModel> trichoices);
    }

    interface OuterView {
        void back();
        void attachPresenter(AdvancedPrivacyPresenter presenter);
    }
}
