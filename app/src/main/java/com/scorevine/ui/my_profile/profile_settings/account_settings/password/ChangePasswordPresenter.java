package com.scorevine.ui.my_profile.profile_settings.account_settings.password;

import android.support.v4.app.FragmentActivity;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.StatusResponse;
import com.scorevine.eventbus.UpdateProfileDataEvent;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.validation.ValidateInputPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Agnieszka Duleba on 2016-12-20.
 */
public class ChangePasswordPresenter extends ValidateInputPresenter implements ChangePasswordContract.Presenter, ChangePasswordContract.Interactor.OnFinishedListener {

    ChangePasswordContract.View mView;
    ChangePasswordContract.Interactor mInteractor;
    FragmentActivity mActivity;
    String mToken;

    public ChangePasswordPresenter(ChangePasswordFragment view, FragmentActivity activity) {
        super(view);
        mView = view;
        mActivity = activity;
        mInteractor = new ChangePasswordInteractor();
        mToken = SecurityHelper.encodeToken(new SharedPrefHelper().getSharedPref(mActivity, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN));
    }

    @Override
    public void subscribe() {
        EventBus.getDefault().register(this);

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }

    @Subscribe
    public void onEvent(UpdateProfileDataEvent event) {

        mInteractor.changePassword(mToken, mView.getOldPassword(), mView.getNewPassword(), this);
    }

    @Override
    public void onResponse(StatusResponse response) {
        mView.showMessage(R.string.account_password_changed, true);
        mActivity.onBackPressed();
    }

    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onError(APIErrorList errorsList) {
        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

    }
}