package com.scorevine.ui.my_profile.profile_settings.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.scorevine.R;

import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.model.settings.AccountSettings;
import com.scorevine.model.settings.GeneralProfileInfo;

import com.scorevine.helpers.TooltipHelper;
import com.scorevine.helpers.ValidationHelper;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.validation.InputTypeSV;

import com.thoughtbot.expandablerecyclerview.MultiTypeExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.tooltip.Tooltip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Agnieszka Duleba on 2016-12-14.
 */

public class ProfileSettingsAdapter extends MultiTypeExpandableRecyclerViewAdapter<GroupViewHolder, ChildViewHolder> {
    private static final int SETTING_VIEW_TYPE = 0;

    private static final int SETTINGS_SOUNDS_VIEW_TYPE = 1;
    private static final int SETTINGS_ACCOUNT_VIEW_TYPE = 2;
    private static final int SETTINGS_GENERAL_VIEW_TYPE = 3;
    private static final int SETTINGS_PRIVACY_VIEW_TYPE = 4;

    public static int sToggledCategory = 0;

    private final Context mContext;
    private LayoutInflater mInflater;

    private SoundsViewHolder soundsViewHolder;
    private AccountViewHolder accountViewHolder;
    private GeneralViewHolder generalViewHolder;
    private PrivacyViewHolder privacyViewHolder;

    AccountSettings mAccountSettings;
    GeneralProfileInfo mGeneralInfo = new GeneralProfileInfo();
    ProfileSettingsAdapterActions mCallback;

    List<String> mGenderList;
    List<String> mCountries;
    List<String> mCities;


    public ProfileSettingsAdapter(Context context, @NonNull List<SettingsSection> categories,
                                  AccountSettings accountSettings, GeneralProfileInfo generalInfo,
                                  ProfileSettingsAdapterActions callback) {
        super(categories);
        this.mContext = context;
        this.mAccountSettings = accountSettings;
        this.mGeneralInfo = generalInfo;
        this.mCallback = callback;
        mInflater = LayoutInflater.from(context);
    }

    public void setStaticData(String[] mGenders, ArrayList<String> countries, ArrayList<String> cities) {
        mGenderList = new ArrayList<>(Arrays.asList(mGenders));
        mCountries = countries;
        mCities = cities;
    }

    public void setDownloadedData(AccountSettings accountSettings, GeneralProfileInfo generalInfo) {
        mAccountSettings = accountSettings;
        mGeneralInfo = generalInfo;
        notifyDataSetChanged();
    }

    @Override
    public GroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.list_item_parent_configuration, parent, false);
        return new ProfileSettingsAdapter.CategoryViewHolder(view);
    }


    @Override
    public void onBindGroupViewHolder(GroupViewHolder holder, final int flatPosition, ExpandableGroup group) {
        ((ProfileSettingsAdapter.CategoryViewHolder) holder).bind(group.getTitle());
    }


    @NonNull
    @Override
    public ChildViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {

        View view;
        switch (viewType) {
            case SETTINGS_SOUNDS_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_settings_sound, childViewGroup, false);
                soundsViewHolder = new SoundsViewHolder(view);
                return soundsViewHolder;
            case SETTINGS_ACCOUNT_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_settings_account, childViewGroup, false);
                accountViewHolder = new AccountViewHolder(view);
                return accountViewHolder;
            case SETTINGS_GENERAL_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_settings_general, childViewGroup, false);
                generalViewHolder = new GeneralViewHolder(view);
                return generalViewHolder;
            case SETTINGS_PRIVACY_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_settings_privacy, childViewGroup, false);
                privacyViewHolder = new PrivacyViewHolder(view);
                return privacyViewHolder;
            default:
                view = mInflater.inflate(R.layout.list_item_settings_sound, childViewGroup, false);
                soundsViewHolder = new SoundsViewHolder(view);
                return soundsViewHolder;
        }
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {

        switch (flatPosition) {
            case SETTINGS_SOUNDS_VIEW_TYPE:
                soundsViewHolder.mNotificationTV.setText(mContext.getString(R.string.settings_notifications_app_label));
                soundsViewHolder.mSoundsTV.setText(mContext.getString(R.string.settings_notifications_sound_label));
                soundsViewHolder.mNotificationTB.setChecked(mAccountSettings.isNotificationsSounds());
                soundsViewHolder.mSoundsTB.setChecked(mAccountSettings.isAppSounds());
                break;
            case SETTINGS_ACCOUNT_VIEW_TYPE:
                accountViewHolder.mEmailTV.setText(mContext.getString(R.string.settings_account_email_label));
                accountViewHolder.mMobileTV.setText(mContext.getString(R.string.settings_account_mobile_label));
                accountViewHolder.mPasswordTV.setText(mContext.getString(R.string.settings_account_password_label));
                break;
            case SETTINGS_GENERAL_VIEW_TYPE:
                setGeneralInfo();
                break;
            case SETTINGS_PRIVACY_VIEW_TYPE:
                privacyViewHolder.mAdvSettingsTV.setText(mContext.getString(R.string.settings_privacy_advanced_label));
                privacyViewHolder.mBlockListTV.setText(mContext.getString(R.string.settings_privacy_blocklist_label));
                break;
            default:
                break;
        }
    }

    public int getFlattenedGroupPosition(int groupIndex) {
        int runningTotal = 0;
        for (int i = 0; i < groupIndex; i++) {
            runningTotal += numberOfVisibleItemsInGroup(i);
        }
        return runningTotal;
    }

    private int numberOfVisibleItemsInGroup(int group) {
        if (expandableList.expandedGroupIndexes.get(group)) {
            return expandableList.groups.get(group).getItemCount() + 1;
        } else {
            return 1;
        }
    }


    @Override
    public boolean isChild(int viewType) {
        return viewType == SETTINGS_SOUNDS_VIEW_TYPE || viewType == SETTINGS_ACCOUNT_VIEW_TYPE ||
                viewType == SETTINGS_GENERAL_VIEW_TYPE || viewType == SETTINGS_PRIVACY_VIEW_TYPE;
    }

    @Override
    public boolean isGroup(int viewType) {
        return viewType == SETTING_VIEW_TYPE;
    }

    @Override
    public int getGroupViewType(int position, ExpandableGroup group) {
        return SETTING_VIEW_TYPE;
    }

    @Override
    public int getChildViewType(int position, ExpandableGroup group, int childIndex) {

        switch (position) {
            case 1:
                return SETTINGS_SOUNDS_VIEW_TYPE;
            case 2:
                return SETTINGS_ACCOUNT_VIEW_TYPE;
            case 3:
                return SETTINGS_GENERAL_VIEW_TYPE;
            case 4:
                return SETTINGS_PRIVACY_VIEW_TYPE;
            default:
                return SETTINGS_SOUNDS_VIEW_TYPE;
        }

    }

    @Nullable
    public GeneralProfileInfo returnGeneralInfoChanges() {
        if (generalViewHolder != null) {
            mGeneralInfo.setFirstName(generalViewHolder.mFirstNameTI.getEditText().getText().toString());
            mGeneralInfo.setSurname(generalViewHolder.mSurnameTI.getEditText().getText().toString());
            mGeneralInfo.setBirthDate(generalViewHolder.mBirthDateTI.getEditText().getText().toString());
            mGeneralInfo.setCountry(generalViewHolder.mCountryTI.getEditText().getText().toString());
            mGeneralInfo.setGender(generalViewHolder.mGenderTI.getEditText().getText().toString());
            mGeneralInfo.setStory(generalViewHolder.mStoryTI.getEditText().getText().toString());
            Log.i("generalinfo", mGeneralInfo.toString());
        }
        return mGeneralInfo;
    }

    public AccountSettings getAccountSettings() {
        return mAccountSettings;
    }

    public void setGeneralInfo() {
        if (mGeneralInfo != null) {
            generalViewHolder.mFirstNameTI.getEditText().setText(mGeneralInfo.getFirstName());
            generalViewHolder.mSurnameTI.getEditText().setText(mGeneralInfo.getSurname());
            generalViewHolder.mUniquenameTI.getEditText().setText("@" + mGeneralInfo.getUniquename());
            generalViewHolder.mBirthDateTI.getEditText().setText(mGeneralInfo.getBirthDate());
            generalViewHolder.mGenderTI.getEditText().setText(mGeneralInfo.getGender());
            generalViewHolder.mCountryTI.getEditText().setText(mGeneralInfo.getCountry());
            generalViewHolder.mStoryTI.getEditText().setText(mGeneralInfo.getStory());
            Log.i("generalinfo", mGeneralInfo.toString());
        }
    }

    class CategoryViewHolder extends GroupViewHolder {
        @BindView(R.id.parentNameTV)
        TextView mParentNameTV;
        @BindView(R.id.rightArrow)
        ImageView mRightArrow;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (getLayoutPosition() == 0) expand();
        }

        @Override
        public void onClick(View v) {
            if (isGroupExpanded(getAdapterPosition())) return;
            super.onClick(v);
            KeyboardHelper.hideKeyboard(v, mContext);

        }

        @Override
        public void expand() {
            animateArrow(0, 180);

            List<? extends ExpandableGroup> groups = getGroups();
            for (int i = 0; i < groups.size(); i++) {
                int flatPos = getFlattenedGroupPosition(i);
                if (i != getAdapterPosition() && isGroupExpanded(flatPos)) {
                    toggleGroup(flatPos);
                    notifyItemChanged(flatPos);
                }
            }
            sToggledCategory = getAdapterPosition();

        }

        @Override
        public void collapse() {
            animateArrow(180, 0);
        }

        public void bind(String name) {
            mParentNameTV.setText(name);
        }

        private void animateArrow(int fromDegrees, int toDegrees) {
            RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setInterpolator(new LinearInterpolator());
            rotate.setFillAfter(true);
            mRightArrow.startAnimation(rotate);
        }


    }

    class SoundsViewHolder extends ChildViewHolder {
        @BindView(R.id.notification_tv)
        TextView mNotificationTV;
        @BindView(R.id.sound_tv)
        TextView mSoundsTV;
        @BindView(R.id.notification_toggle)
        SwitchCompat mNotificationTB;
        @BindView(R.id.sound_toggle)
        SwitchCompat mSoundsTB;

        public SoundsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            setOnCheckedListeners();
        }

        private void setOnCheckedListeners() {
            mNotificationTB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    mAccountSettings.setNotificationsSounds(b);
                }
            });
            mSoundsTB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    mAccountSettings.setAppSounds(b);
                }
            });
        }
    }

    class AccountViewHolder extends ChildViewHolder {
        @BindView(R.id.email_tv)
        TextView mEmailTV;
        @BindView(R.id.password_tv)
        TextView mPasswordTV;
        @BindView(R.id.mobile_tv)
        TextView mMobileTV;

        public AccountViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        @OnClick(R.id.email_tv)
        public void onChangeEmailClicked() {
            mCallback.onChangeEmailClicked(mAccountSettings.getEmail(), mAccountSettings.isHasPassword());

        }

        @OnClick(R.id.password_tv)
        public void onChangePasswordClicked() {
            mCallback.onChangePasswordClicked(mAccountSettings.isHasPassword());

        }

        @OnClick(R.id.mobile_tv)
        public void onChangeMobileClicked() {
            mCallback.onChangeMobileClicked(mAccountSettings.getCountryCode(), mAccountSettings.getMobile(), mAccountSettings.isHasPassword());

        }

    }

    class GeneralViewHolder extends ChildViewHolder {
        @BindView(R.id.firstNameTI)
        TextInputLayout mFirstNameTI;
        @BindView(R.id.uniqueNameTI)
        TextInputLayout mUniquenameTI;
        @BindView(R.id.lastNameTI)
        TextInputLayout mSurnameTI;
        @BindView(R.id.date_of_birth_ti)
        TextInputLayout mBirthDateTI;
        @BindView(R.id.gender_ti)
        TextInputLayout mGenderTI;
        @BindView(R.id.country_ti)
        TextInputLayout mCountryTI;
        @BindView(R.id.city_ti)
        TextInputLayout mCityTI;
        @BindView(R.id.story_ti)
        TextInputLayout mStoryTI;
        Tooltip mUniqueNameTooltip;
        Tooltip mStoryTooltip;

        ValidationHelper mValidator = new ValidationHelper();


        public GeneralViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mUniqueNameTooltip = TooltipHelper.createToolTipDialogBox(mUniquenameTI.getEditText(),
                    R.string.settings_uniquename_help, Gravity.BOTTOM, itemView.getContext());

            mStoryTooltip = TooltipHelper.createToolTipDialogBox(mStoryTI.getEditText(),
                    R.string.settings_story_help, Gravity.BOTTOM, itemView.getContext());

            mFirstNameTI.getEditText().addTextChangedListener(new CustomTextWatcher() {
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.toString().isEmpty()) {
                        return;
                    }
                    validateInput(charSequence.toString(), InputTypeSV.FIRST_NAME);
                }
            });
            mSurnameTI.getEditText().addTextChangedListener(new CustomTextWatcher() {
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.toString().isEmpty()) {
                        return;
                    }
                    validateInput(charSequence.toString(), InputTypeSV.SURNAME);
                }
            });
        }

        public boolean validateInput(String value, InputTypeSV inputType) {
            boolean isValid = true;
            if (!mValidator.validateMinLength(value, inputType)) {
                showInputError(String.format(itemView.getContext().getString(R.string.error_min_characters_count), inputType.minLength()), inputType);
                return !isValid;
            }
            if (!mValidator.validateMaxLength(value, inputType)) {
                showInputError(String.format(itemView.getContext().getString(R.string.error_max_characters_count), inputType.maxLength()), inputType);
                return !isValid;
            }
            if (!mValidator.validateCharacters(value, inputType)) {
                showInputError(itemView.getContext().getString(mValidator.getError(inputType)), inputType);
                return !isValid;
            }
            hideInputError(inputType);
            return isValid;

        }

        public void showInputError(String errorMessage, InputTypeSV inputType) {
            TextInputLayout textInputLayout = getTextInputLayout(inputType);
            if (textInputLayout != null) {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(errorMessage);
            }

        }

        private TextInputLayout getTextInputLayout(InputTypeSV inputType) {
            TextInputLayout textInputLayout = null;
            switch (inputType) {
                case FIRST_NAME:
                    textInputLayout = mFirstNameTI;
                    break;
                case SURNAME:
                    textInputLayout = mSurnameTI;
                    break;
                default:
                    break;
            }
            return textInputLayout;
        }

        public void hideInputError(InputTypeSV inputType) {
            TextInputLayout textInputLayout = getTextInputLayout(inputType);
            if (textInputLayout != null) {
                textInputLayout.setErrorEnabled(false);
            }
        }

        @OnClick(R.id.date_of_birth_et)
        public void onDateOfBirthClicked() {
            mCallback.onCalendarClicked(mBirthDateTI.getEditText());

        }

        @OnClick(R.id.gender_et)
        public void onGenderClicked() {
            mCallback.onSpinnerClicked(mGenderTI.getEditText(), mGenderList);
        }

        @OnClick(R.id.country_et)
        public void onCountryClicked() {
            mCallback.onSpinnerClicked(mCountryTI.getEditText(), mCountries);
        }

        @OnClick(R.id.city_et)
        public void onCityClicked() {
            //      mCallback.onSpinnerClicked(mCityTI.getEditText(), mCities);
        }

        @OnClick(R.id.uniquename_iv)
        public void onUniqueNameHelpClicked() {
            mCallback.onHelpClicked();
            mUniqueNameTooltip.show();
        }

        @OnClick(R.id.story_iv)
        public void onStoryHelpClicked() {

            mCallback.onHelpClicked();
            mStoryTooltip.show();
        }

    }

    class PrivacyViewHolder extends ChildViewHolder {
        @BindView(R.id.adv_settings_tv)
        TextView mAdvSettingsTV;
        @BindView(R.id.block_list_tv)
        TextView mBlockListTV;


        public PrivacyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.adv_settings_tv)
        public void onAdvSettingsClicked() {
            mCallback.onAdvancedPrivacyClicked();

        }

        @OnClick(R.id.block_list_tv)
        public void onBlockListClicked() {
            mCallback.onBlockListClicked();

        }
    }
}