package com.scorevine.ui.my_profile.profile_settings.account_settings.email;

import android.support.v4.app.FragmentActivity;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.profile.ChangeEmailResponse;
import com.scorevine.eventbus.UpdateProfileDataEvent;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.validation.ValidateInputPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Agnieszka Duleba on 2016-12-20.
 */
public class ChangeEmailPresenter extends ValidateInputPresenter implements ChangeEmailContract.Presenter, ChangeEmailContract.Interactor.OnFinishedListener {

    ChangeEmailContract.View mView;
    ChangeEmailContract.Interactor mInteractor;
    FragmentActivity mActivity;
    String mToken;

    public ChangeEmailPresenter(ChangeEmailFragment view, FragmentActivity activity) {
        super(view);
        mView = view;
        mActivity = activity;
        mInteractor = new ChangeEmailInteractor();
        mToken = SecurityHelper.encodeToken(new SharedPrefHelper().getSharedPref(mActivity, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN));

    }

    @Override
    public void subscribe() {
        EventBus.getDefault().register(this);

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }


    @Subscribe
    public void onEvent(UpdateProfileDataEvent event) {

        mInteractor.changeEmail(mToken, mView.getEmail(), mView.getPassword(), this);
    }

    @Override
    public void onResponse(ChangeEmailResponse response) {
        mView.showMessage(R.string.account_email_changed, true);
        mActivity.onBackPressed(); //?
    }

    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onError(APIErrorList errorsList) {
        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

    }

}
