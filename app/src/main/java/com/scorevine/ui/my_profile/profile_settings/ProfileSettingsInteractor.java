package com.scorevine.ui.my_profile.profile_settings;

import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.profile.UserProfileInterface;
import com.scorevine.api.profile.UserProfileResponse;
import com.scorevine.api.SuccessResponse;
import com.scorevine.helpers.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-14.
 */

public class ProfileSettingsInteractor implements ProfileSettingsContract.Interactor{

    UserProfileInterface userProfileInterface = new ApiManager().getRetrofit().create(UserProfileInterface.class);

    @Override
    public void getUserProfile(String token, final OnGetFinishedListener listener) {

        userProfileInterface.getUserProfile(token).enqueue(new Callback<UserProfileResponse>() {
            @Override
            public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                switch (response.code()) {
                    case 200:
                        Log.e(ProfileSettingsFragment.TAG_FRAGMENT_PROFILE_SETTINGS, "onGetResponse: " + response.body().toString());
                        listener.onGetResponse(response.body().getUser());
                        break;
                    case 401:
                    case 410:
                        listener.onGetError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<UserProfileResponse> call, Throwable t) {
                listener.onGetFailure(R.string.check_your_internet_connection);

            }
        });
    }

    @Override
    public void saveUserProfile(String token, String birthDate, String gender, String country, String firstName, String surname, String story, final OnPutFinishedListener listener) {

        userProfileInterface.updateUserProfile(token, birthDate, gender, country, firstName, surname, story).enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                switch (response.code()) {
                    case 200:
                        Log.e(ProfileSettingsFragment.TAG_FRAGMENT_PROFILE_SETTINGS, "onGetResponse: " + response.body().toString());
                        listener.onPutResponse(response.body());
                        break;
                    case 401:
                    case 410:
                    case 422:
                        listener.onPutError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                listener.onPutFailure(R.string.check_your_internet_connection);

            }
        });
    }
}
