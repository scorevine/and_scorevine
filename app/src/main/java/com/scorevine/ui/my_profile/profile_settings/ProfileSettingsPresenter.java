package com.scorevine.ui.my_profile.profile_settings;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.profile.UserProfile;
import com.scorevine.api.SuccessResponse;
import com.scorevine.eventbus.UpdateProfileDataEvent;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.LoadFileHelper;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.model.CountryCodes;
import com.scorevine.model.settings.GeneralProfileInfo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-14.
 */

public class ProfileSettingsPresenter implements ProfileSettingsContract.Presenter, ProfileSettingsContract.Interactor.OnGetFinishedListener, ProfileSettingsContract.Interactor.OnPutFinishedListener {


    ProfileSettingsContract.View mView;
    ProfileSettingsContract.Interactor mInteractor;
    FragmentActivity mFragmentActivity;
    String mToken;

    public ProfileSettingsPresenter(ProfileSettingsFragment view, FragmentActivity fragmentActivity) {
        mView = view;
        mFragmentActivity = fragmentActivity;
        mInteractor = new ProfileSettingsInteractor();
        mToken = SecurityHelper.encodeToken(new SharedPrefHelper().getSharedPref(mFragmentActivity, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN));

    }

    @Override
    public void subscribe() {
        EventBus.getDefault().register(this);

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void initAdapter() {
        mView.initAdapterData();

        mInteractor.getUserProfile(mToken, this);

    }

    @Override
    public ArrayList<String> getCountriesList(Activity activity) {
        List<CountryCodes.CountryCode> countryCodesList = LoadFileHelper.getCountryCodesList(activity);
        final ArrayList<String> countriesList = new ArrayList<>();

        for (CountryCodes.CountryCode element : countryCodesList) {
            countriesList.add(element.getCountryName());
        }

        return countriesList;
    }

    @Override
    public ArrayList<String> getCitiesList() {
        return null;
    }

    @Override
    public void onGetResponse(UserProfile response) {
        String birthDateFormattedStr =
                DateFormatHelper.changeDateFormat(DateFormatHelper.API_PROFILE_DATE_FORMAT,
                        DateFormatHelper.APP_PROFILE_DATE_FORMAT, response.getBirthDate());

        mView.setAccountSettings(response.getEmail(), response.getHasPassword(), response.getCountryCode(), response.getMobile());
        mView.setGeneralInfo(response.getFirstName(), response.getSurname(), response.getUsername(),   //change to city!
                birthDateFormattedStr, response.getGender(), response.getCountry(),
                response.getStory(), response.getStory());
        mView.notifyDataChanged();
    }

    @Override
    public void onGetFailure(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onGetError(APIErrorList errorsList) {

        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

    }


    @Subscribe
    public void onEvent(UpdateProfileDataEvent event) {
        if(mView.getGeneralInfoChanged()) {
            GeneralProfileInfo generalProfileInfo = mView.getGeneralInfo();
            String birthDateFormattedStr =
                    DateFormatHelper.changeDateFormat(DateFormatHelper.APP_PROFILE_DATE_FORMAT,
                            DateFormatHelper.API_PROFILE_DATE_FORMAT, generalProfileInfo.getBirthDate());

            if (generalProfileInfo!=null)
            mInteractor.saveUserProfile(mToken,
                    birthDateFormattedStr,
                    generalProfileInfo.getGender(), generalProfileInfo.getCountry(),
                    generalProfileInfo.getFirstName(), generalProfileInfo.getSurname(),
                    generalProfileInfo.getStory(), this);

        }
    }

    @Override
    public void onPutResponse(SuccessResponse response) {
        mView.showTextMessage("Success", true);
    }

    @Override
    public void onPutFailure(int responseId) {
        mView.showMessage(responseId, true);
    }

    @Override
    public void onPutError(APIErrorList errorsList) {
        if(errorsList.getError()!=null)
                mView.showMessage(R.string.unknown_error, false);

        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

    }
}
