package com.scorevine.ui.my_profile.advanced_privacy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 8570p on 2016-12-17.
 */

public class AdvancedPrivacyActivity extends AppCompatActivity implements AdvancedPrivacyContract.OuterView {

    @BindView(R.id.toolbar_title)
    TextView mToolbarTitleTV;

    @BindView(R.id.save_bnt)
    TextView mSaveBtnTV;

    private AdvancedPrivacyPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);
        ButterKnife.bind(this);
        setUpView(getString(R.string.adv_privacy_title), getString(R.string.settings_bottom_button_label) );

        FragmentManager fm = getSupportFragmentManager();
        Fragment firstFragment = fm.findFragmentById(R.id.fragment_container);
        if (firstFragment == null) {
            firstFragment = new AdvancedPrivacyFragment();
            mPresenter = new AdvancedPrivacyPresenter(this, this, (AdvancedPrivacyContract.InnerView) firstFragment);
            ((AdvancedPrivacyFragment) firstFragment).setPresenter(mPresenter);
            FragmentHelper.addFirstFragment(fm, firstFragment, AdvancedPrivacyFragment.TAG);
        }
    }

    @Override
    public void attachPresenter(AdvancedPrivacyPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void back() {
        super.onBackPressed();
    }


    @OnClick(R.id.backArrowButton)
    public void onBackArrowClicked(){
        back();
    }

    @OnClick(R.id.save_bnt)
    public void onSaveClicked(){
        back();
    }

    public void setUpView(String toolbarTitle, String bottomButton) {
        mToolbarTitleTV.setText(toolbarTitle);
        mSaveBtnTV.setText(bottomButton);
    }
}
