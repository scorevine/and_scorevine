package com.scorevine.ui.my_profile.advanced_privacy;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.model.adv_privacy.AdvancedPrivacyBichoiceModel;
import com.scorevine.model.adv_privacy.AdvancedPrivacyTrichoiceModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 8570p on 2016-12-17.
 */

public class AdvancedPrivacyAdapter extends RecyclerView.Adapter {

    private List<AdvancedPrivacyBichoiceModel> mBichoices;
    private List<AdvancedPrivacyTrichoiceModel> mTrichoices;

    private static final int TYPE_TRICHOICE = 1;
    private static final int TYPE_BICHOICE = 2;

    public AdvancedPrivacyAdapter(List<AdvancedPrivacyBichoiceModel> bichoices, List<AdvancedPrivacyTrichoiceModel> trichoices) {
        mBichoices = bichoices;
        mTrichoices = trichoices;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_BICHOICE:
                holder = new BichoiceViewHolder(inflater.inflate(R.layout.list_item_advanced_privacy_bichoice, parent, false));
                break;
            case TYPE_TRICHOICE:
                holder = new TrichoiceViewHolder(inflater.inflate(R.layout.list_item_advanced_privacy_trichoice, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_BICHOICE:
                setupBichoice((BichoiceViewHolder) holder, mBichoices.get(position - mTrichoices.size()));
                break;
            case TYPE_TRICHOICE:
                setupTrichoice((TrichoiceViewHolder) holder, mTrichoices.get(position));
                break;
        }
    }

    private void setupTrichoice(TrichoiceViewHolder holder, AdvancedPrivacyTrichoiceModel model) {
        holder.assignModel(model);
    }

    private void setupBichoice(BichoiceViewHolder holder, AdvancedPrivacyBichoiceModel model) {
        holder.assignModel(model);
    }

    @Override
    public int getItemViewType(int position) {
        return position < mTrichoices.size() ? TYPE_TRICHOICE : TYPE_BICHOICE;
    }

    @Override
    public int getItemCount() {
        return mTrichoices.size() + mBichoices.size();
    }

    class TrichoiceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adv_privacy_trichoice_title)
        TextView mTitleTV;
        @BindView(R.id.adv_privacy_trichoice_first_choice)
        TextView mFirstChoiceButton;
        @BindView(R.id.adv_privacy_trichoice_second_choice)
        TextView mSecondChoiceButton;
        @BindView(R.id.adv_privacy_trichoice_third_choice)
        TextView mThirdChoiceButton;

        private AdvancedPrivacyTrichoiceModel mModel;

        public TrichoiceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void assignModel(AdvancedPrivacyTrichoiceModel model) {
            mModel = model;
            mTitleTV.setText(model.getTitle());
            setSelectedButton(model.getSelection());
            mFirstChoiceButton.setText(model.getChoiceName(AdvancedPrivacyContract.Trichoice.FIRST));
            mSecondChoiceButton.setText(model.getChoiceName(AdvancedPrivacyContract.Trichoice.SECOND));
            mThirdChoiceButton.setText(model.getChoiceName(AdvancedPrivacyContract.Trichoice.THIRD));
        }

        private void setSelectedButton(AdvancedPrivacyContract.Trichoice selection) {
            TextView button = mFirstChoiceButton;
            switch (selection) {
                case FIRST:
                    button = mFirstChoiceButton;
                    break;
                case SECOND:
                    button = mSecondChoiceButton;
                    break;
                case THIRD:
                    button = mThirdChoiceButton;
                    break;
            }
            button.setTextColor(button.getContext().getResources().getColor(R.color.colorAccent));
            mModel.setSelection(selection);
        }

        private void deselectButtons(TextView... buttons) {
            for (TextView button : buttons) {
                button.setTextColor(button.getContext().getResources().getColor(R.color.colorPrimaryDark));
            }
        }

        @OnClick(R.id.adv_privacy_trichoice_first_choice)
        void onFirstChoiceClick() {
            deselectButtons(mSecondChoiceButton, mThirdChoiceButton);
            setSelectedButton(AdvancedPrivacyContract.Trichoice.FIRST);
        }

        @OnClick(R.id.adv_privacy_trichoice_second_choice)
        void onSecondChoiceClick() {
            deselectButtons(mFirstChoiceButton, mThirdChoiceButton);
            setSelectedButton(AdvancedPrivacyContract.Trichoice.SECOND);
        }

        @OnClick(R.id.adv_privacy_trichoice_third_choice)
        void onThirdChoiceClick() {
            deselectButtons(mSecondChoiceButton, mFirstChoiceButton);
            setSelectedButton(AdvancedPrivacyContract.Trichoice.THIRD);
        }
    }

    class BichoiceViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.adv_privacy_bichoice_title)
        TextView mTitleTV;
        @BindView(R.id.adv_privacy_bichoice_first_choice)
        TextView mFirstChoiceButton;
        @BindView(R.id.adv_privacy_bichoice_second_choice)
        TextView mSecondChoiceButton;

        private AdvancedPrivacyBichoiceModel mModel;

        public BichoiceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void assignModel(AdvancedPrivacyBichoiceModel model) {
            mModel = model;
            mTitleTV.setText(model.getTitle());
            setSelectedButton(model.getSelection());
            mFirstChoiceButton.setText(model.getChoiceName(AdvancedPrivacyContract.Bichoice.FIRST));
            mSecondChoiceButton.setText(model.getChoiceName(AdvancedPrivacyContract.Bichoice.SECOND));
        }

        private void setSelectedButton(AdvancedPrivacyContract.Bichoice selection) {
            TextView button = mFirstChoiceButton;
            switch (selection) {
                case FIRST:
                    button = mFirstChoiceButton;
                    break;
                case SECOND:
                    button = mSecondChoiceButton;
                    break;
            }
            button.setTextColor(button.getContext().getResources().getColor(R.color.colorAccent));
            mModel.setSelection(selection);
        }

        private void deselectButtons(TextView... buttons) {
            for (TextView button : buttons) {
                button.setTextColor(button.getContext().getResources().getColor(R.color.colorPrimaryDark));
            }
        }

        @OnClick(R.id.adv_privacy_bichoice_first_choice)
        void onFirstChoiceClick() {
            deselectButtons(mSecondChoiceButton);
            setSelectedButton(AdvancedPrivacyContract.Bichoice.FIRST);
        }

        @OnClick(R.id.adv_privacy_bichoice_second_choice)
        void onSecondChoiceClick() {
            deselectButtons(mFirstChoiceButton);
            setSelectedButton(AdvancedPrivacyContract.Bichoice.SECOND);
        }
    }
}
