package com.scorevine.ui.my_profile.advanced_privacy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.R;
import com.scorevine.model.adv_privacy.AdvancedPrivacyBichoiceModel;
import com.scorevine.model.adv_privacy.AdvancedPrivacyTrichoiceModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 8570p on 2016-12-17.
 */

public class AdvancedPrivacyFragment extends Fragment implements AdvancedPrivacyContract.InnerView{

    public static final String TAG = AdvancedPrivacyFragment.class.getSimpleName();

    @BindView(R.id.generic_recycler_view)
    RecyclerView mRecyclerView;

    private AdvancedPrivacyPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, null, false);
        ButterKnife.bind(this, view);

        if (mPresenter == null) {
            mPresenter = new AdvancedPrivacyPresenter(getActivity(), (AdvancedPrivacyContract.OuterView) getActivity(), this);
        }
        mPresenter.generateChoices();
        return view;
    }

    public void setPresenter(AdvancedPrivacyPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void presentChoices(List<AdvancedPrivacyBichoiceModel> bichoices, List<AdvancedPrivacyTrichoiceModel> trichoices) {
        mRecyclerView.setAdapter(new AdvancedPrivacyAdapter(bichoices, trichoices));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
    }
}
