package com.scorevine.ui.my_vine;

import android.util.Log;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.ApiManager;
import com.scorevine.api.contact.ContactUsApi;
import com.scorevine.api.contact.Reason;
import com.scorevine.api.contact.Reasons;
import com.scorevine.helpers.ErrorUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoga on 2016-12-27.
 */

public class InteractorReasons {

    public interface OnFinishedListener {
        void onResponseReasonsList(List<Reason> reasons);
        void onErrorReasonsList(APIErrorList errorList);
        void onFailureReasonsList();
    }

    ContactUsApi contactUsApi = new ApiManager().getRetrofit().create(ContactUsApi.class);

    public void getReasonsList(String type, String encodedToken, final OnFinishedListener listener) {
        contactUsApi.getContactReasonByType(type, encodedToken).enqueue(new Callback<Reasons>() {
            @Override
            public void onResponse(Call<Reasons> call, Response<Reasons> response) {
                switch (response.code()) {
                    case 200:
                        listener.onResponseReasonsList(response.body().getReasons());
                        break;
                    case 401:
                    case 410:
                        listener.onErrorReasonsList(ErrorUtils.parseError(response));
                        break;
                    default:
                        listener.onFailureReasonsList();
                }
            }

            @Override
            public void onFailure(Call<Reasons> call, Throwable t) {

                if (t.getMessage() != null) {
                    Log.i("ContactUsApi failure: ", t.getMessage());

                }
                listener.onFailureReasonsList();

            }
        });
    }
}
