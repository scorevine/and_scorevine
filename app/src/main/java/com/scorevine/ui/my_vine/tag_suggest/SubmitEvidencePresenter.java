package com.scorevine.ui.my_vine.tag_suggest;

import android.content.Context;
import android.net.Uri;
import android.support.design.widget.TextInputEditText;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.ApiConstants;
import com.scorevine.api.contact.Reason;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.helpers.TooltipHelper;
import com.scorevine.helpers.ValidationHelper;
import com.scorevine.model.tags.SuggestTag;
import com.scorevine.ui.my_vine.InteractorReasons;
import com.scorevine.model.tags.TagReason;
import com.scorevine.ui.validation.InputTypeSV;
import com.tooltip.Tooltip;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Yoga on 2016-12-23.
 */
public class SubmitEvidencePresenter implements SuggestTagContract.PresenterNext, InteractorReasons.OnFinishedListener, SuggestTagContract.Interactor.OnFinishedListener{

    SuggestTagContract.ViewNext mView;
    SuggestTagContract.Interactor mInteractor;
    Context mContext;
    String mToken;
    InteractorReasons mInteractorReasons;
    ValidationHelper mValidator;
    private final HashMap<String, Integer> reasonsMap = new HashMap<>();


    public SubmitEvidencePresenter(SubmitEvidenceFragment view, Context context) {
        mView = view;
        mContext = context;
        mInteractor = new TagInteractor();
        mInteractorReasons = new InteractorReasons();
        mValidator = new ValidationHelper();
        mToken = SecurityHelper.encodeToken(new SharedPrefHelper().getSharedPref(mContext, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN));

    }

    @Override
    public void subscribe() {
        EventBus.getDefault().register(this);
        mView.setListener();

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }


    public Tooltip setTextTooltip(final TextView textView, int helpTextId) {

        final Tooltip finalTooltip = TooltipHelper.createTooltipTV(textView, helpTextId, Gravity.BOTTOM, mContext);

        return finalTooltip;
    }

    public Tooltip setTextTooltip(final TextInputEditText textInputEditText, int helpTextId, final int fieldLengthId) {

        final Tooltip finalTooltip = TooltipHelper.createToolTipDialogBoxCounter(textInputEditText, helpTextId, Gravity.BOTTOM, mContext);

        textInputEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (b) {
                        if (textInputEditText.getText().toString().isEmpty()) {
                            finalTooltip.show();

                        }
                    } else {
                        finalTooltip.dismiss();
                    }
                }
            });

        return finalTooltip;
    }

    @Override
    public void submitTagSuggestion(SuggestTag suggestTag){

        RequestBody tagShortName = RequestBody.create(MediaType.parse("text/plain"), suggestTag.getShortName());
        RequestBody tagName = RequestBody.create(MediaType.parse("text/plain"), suggestTag.getName());
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), suggestTag.getDescription());
        RequestBody website = RequestBody.create(MediaType.parse("text/plain"), suggestTag.getWebsite());

        MultipartBody.Part filePart = null;
        String imagePath = suggestTag.getPhoto();
        if(!imagePath.equals("")) {
            Uri imageUri = Uri.parse(imagePath.substring(7, imagePath.length()));
            File file = new File(imageUri.getPath());
            if(file!=null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                filePart = MultipartBody.Part.createFormData("photo", file.getName(), requestFile);
            }
        }

        RequestBody link = RequestBody.create(MediaType.parse("text/plain"), suggestTag.getEvidenceLink());
        RequestBody reasonId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(suggestTag.getTagSort()));
        RequestBody lookForDescription = RequestBody.create(MediaType.parse("text/plain"), suggestTag.getLookFor());
        RequestBody isAdult;
        if(suggestTag.getOver18() == -2){
            isAdult = null;
        }else {
            isAdult = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(suggestTag.getOver18()));

        }
        mInteractor.submitEvidence(mToken, tagShortName, tagName, description, website, filePart, link,
                reasonId, lookForDescription, isAdult, this);

    }

    @Subscribe
    public void onEvent(NextButtonEvent event){
        if(!mView.checkFieldsNull()) {
            mView.setData();
            mView.showConfirmationDialog();
        }else {
            mView.showToast(R.string.suggest_tag_names_cannot_be_empty);
        }
    }

    public void getReasonsTags() {
        mInteractorReasons.getReasonsList(ApiConstants.API_CONTACT_REASONS_TAGS, mToken, this);
    }


    @Override
    public void onResponseReasonsList(List<Reason> reasons) {
        List<TagReason> reasonList = new ArrayList<>();

        if (reasons != null && reasons.size() > 0) {
            for (Reason reason : reasons) {
                reasonList.add(new TagReason(reason.getReason(), reason.getId(), reason.getPerson()));
            }

            mView.setReasons(reasonList);
        }
    }

    @Override
    public void onErrorReasonsList(APIErrorList errorList) {
        if (errorList.getError() != null)
            mView.showTextMessage(errorList.getError().get(0).getMessage(), false);

    }


    @Override
    public void onFailureReasonsList() {
        mView.showToast(R.string.check_your_internet_connection);
    }


    @Override
    public void onResponse() {
        mView.showToast(R.string.suggest_a_tag_toast_success);
        mView.onBackPressed();
    }

    @Override
    public void onError(APIErrorList apiError) {
        if (apiError.getError() != null)
            mView.showTextMessage(apiError.getError().get(0).getMessage(), false);

    }

    @Override
    public void onFailure() {
        mView.showToast(R.string.check_your_internet_connection);
    }

    @Override
    public void validateInput(String value, InputTypeSV inputType) {

        if (!mValidator.validateMaxLength(value, (InputTypeSV) inputType)) {
            mView.showInputError(inputType, R.string.error_max_characters_count_100);
            return;
        }
        if (!mValidator.validateCharacters(value, inputType)) {
            mView.showInputError(inputType, R.string.suggest_a_tag_error_invalid_url);
            return;
        }
        mView.hideInputError(inputType);
    }
}
