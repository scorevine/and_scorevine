package com.scorevine.ui.my_vine.tag_suggest;


import com.scorevine.api.ApiManager;
import com.scorevine.api.StatusResponse;
import com.scorevine.api.contact.ContactUsApi;
import com.scorevine.helpers.ErrorUtils;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoga on 2016-12-23.
 */
public class TagInteractor implements SuggestTagContract.Interactor {

    ContactUsApi contactUsApi = new ApiManager().getRetrofit().create(ContactUsApi.class);

    @Override
    public void submitEvidence(String token, RequestBody tagShortName, RequestBody tagName,
                               RequestBody description, RequestBody website, MultipartBody.Part photo,
                               RequestBody link, RequestBody reasonId, RequestBody lookForDescription,
                               RequestBody isAdult, final SuggestTagContract.Interactor.OnFinishedListener listener){

        contactUsApi.sendTagSuggestion(token, tagShortName, tagName, description, website, photo,
                link, reasonId, lookForDescription, isAdult).enqueue(new Callback<StatusResponse>() {
            @Override
            public void onResponse(Call<StatusResponse> call, Response<StatusResponse> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        listener.onResponse();
                        break;
                    case 401:
                    case 410:
                    case 422:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                    default:
                        listener.onFailure();
                }
            }

            @Override
            public void onFailure(Call<StatusResponse> call, Throwable t) {
                listener.onFailure();
            }
        });
    }
}
