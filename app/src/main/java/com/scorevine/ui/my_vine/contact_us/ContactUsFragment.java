package com.scorevine.ui.my_vine.contact_us;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.ListPopupWindow;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.scorevine.R;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.helpers.TooltipHelper;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.tooltip.Tooltip;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NIKAK
 * Ready4S
 * on 16.12.2016 11:19
 */
public class ContactUsFragment extends Fragment implements ContactUsContract.View {


    @BindView(R.id.reasonET)
    EditText mReasonEditText;
    @BindView(R.id.headingTI)
    TextInputLayout mHeadingTI;
    @BindView(R.id.headingET)
    EditText mHeadingEditText;
    @BindView(R.id.textET)
    EditText mTextEditText;
    @BindView(R.id.counterHeadingTV)
    TextView counterHeadingTV;
    @BindView(R.id.counterTextTV)
    TextView counterTextTV;

    ContactUsContract.Presenter mPresenter;
    private ListPopupWindow listPopupWindow;
    private ArrayList<String> mReasons;
    private Tooltip mTextTooltip;
    private Tooltip mHeadingTooltip;


    public static ContactUsFragment getInstance() {
        return new ContactUsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new ContactUsPresenter();
        mPresenter.getReasonsList(getContext());

        setTextTooltip();
        setHeadingTooltip();
        return view;
    }

    private void setHeadingTooltip() {
        mHeadingTooltip = TooltipHelper.createToolTipDialogBoxCounter(mHeadingEditText, R.string.contact_us_help_info, Gravity.BOTTOM, getContext());
        mHeadingEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (mHeadingEditText.getText().toString().isEmpty()) {
                        mHeadingTooltip.show();
                    }
                } else {
                    mHeadingTooltip.dismiss();
                }
            }
        });
        counterHeadingTV.setText(String.valueOf(getResources().getInteger(R.integer.contact_us_heading_max_length)));
        mHeadingEditText.addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int charactersLeft = getResources().getInteger(R.integer.contact_us_heading_max_length) - mHeadingEditText.length();
                counterHeadingTV.setText(String.valueOf(charactersLeft));
            }
        });
    }

    private void setTextTooltip() {
        mTextTooltip = TooltipHelper.createToolTipDialogBoxCounter(mTextEditText, R.string.contact_us_text_help, Gravity.BOTTOM, getContext());
        mTextEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (mTextEditText.getText().toString().isEmpty()) {
                        mTextTooltip.show();

                    }
                } else {
                    mTextTooltip.dismiss();
                }
            }
        });
        counterTextTV.setText(String.valueOf(getResources().getInteger(R.integer.contact_us_text_max_length)));
        mTextEditText.addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int charactersLeft = getResources().getInteger(R.integer.contact_us_text_max_length) - mTextEditText.length();
                counterTextTV.setText(String.valueOf(charactersLeft));
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    public void setReasons(ArrayList<String> reasons) {
        listPopupWindow = new ListPopupWindow(getActivity());
        mReasons = reasons;
        listPopupWindow.setAdapter(new ArrayAdapter<>(getContext(), R.layout.dropdown_item_light, mReasons));
        listPopupWindow.setAnchorView(mReasonEditText);
        listPopupWindow.setModal(true);

        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    mReasonEditText.setText(mReasons.get(position));
                    listPopupWindow.dismiss();
                }
            }
        });
    }

    @Override
    public void onSendButtonClick() {
        mPresenter.sendMessage(mReasonEditText.getText().toString(),
                mHeadingEditText.getText().toString(),
                mTextEditText.getText().toString(),
                getContext());
    }

    @Override
    public void showToast(int messageId) {
        Toast.makeText(getContext(), getString(messageId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTextMessage(String message, boolean b) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.reasonET)
    public void onReasonEditTextClick() {
        KeyboardHelper.hideKeyboard(getView(), getActivity());
        if (listPopupWindow != null) {
            listPopupWindow.show();
        }
    }

    @OnClick(R.id.headingHelpIV)
    public void onHeadingHelpClick() {

        if (!mHeadingTooltip.isShowing()) {
            mHeadingTooltip.show();
        }
    }

    @OnClick(R.id.textHelpIV)
    public void onTextHelpClick() {
        if (!mTextTooltip.isShowing()) {
            mTextTooltip.show();
        }

    }
}
