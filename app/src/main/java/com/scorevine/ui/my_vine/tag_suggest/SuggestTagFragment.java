package com.scorevine.ui.my_vine.tag_suggest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.ValidationHelper;
import com.scorevine.model.tags.SuggestTag;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.dialogs.GalleryDialogFragment;
import com.scorevine.ui.signing.CropPhotoActivity;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.validation.InputTypeSV;
import com.tooltip.Tooltip;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.scorevine.ui.signing.CropPhotoActivity.CAMERA_REQUEST_CODE;
import static com.scorevine.ui.signing.CropPhotoActivity.GALLERY_REQUEST_CODE;

/**
 * Created by Yoga on 2016-12-23.
 */
public class SuggestTagFragment extends Fragment implements SuggestTagContract.View {


    public static final String SUGGEST_TAG_FRAGMENT = SuggestTagFragment.class.getSimpleName();
    public static final String BUNDLE_SAVED_STATE_SHORTNAME = "shortName";
    public static final String BUNDLE_SAVED_STATE_NAME = "name";
    public static final String BUNDLE_SAVED_STATE_DESCRIPTION = "description";
    public static final String BUNDLE_SAVED_STATE_WEBSITE = "website";
    public static final String BUNDLE_SAVED_STATE_PHOTO = "photo";

    public static String photoUri;

    SuggestTagPresenter mPresenter;

    @BindView(R.id.tag_name_et)
    TextInputEditText mTagNameET;
    @BindView(R.id.tag_name_ti)
    TextInputLayout mTagNameTI;

    @BindView(R.id.tag_short_name_et)
    TextInputEditText mTagShortNameET;

    @BindView(R.id.tag_description_et)
    TextInputEditText mTagDescriptionET;

    @BindView(R.id.tag_website_et)
    TextInputEditText mWebsiteET;
    @BindView(R.id.tag_website_ti)
    TextInputLayout mWebsiteTI;

    @BindView(R.id.counter_tag_short_name_tv)
    TextView mTagShortNameCounterTV;

    @BindView(R.id.counter_tag_name_tv)
    TextView mTagNameCounterTV;

    @BindView(R.id.tag_counter_description_tv)
    TextView mTagDescriptionCounterTV;

    @BindView(R.id.tag_add_photo_tv)
    AppCompatTextView mAddTagPhotoTV;


    public Tooltip mTooltipShortName;
    public Tooltip mTooltipName;
    public Tooltip mTooltipDescription;

    SuggestTag mSuggestTag;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tag_suggest, container, false);
        ButterKnife.bind(this, view);

        mPresenter = new SuggestTagPresenter(this, getContext());
        mSuggestTag = new SuggestTag();

        mTooltipShortName = mPresenter.setTextTooltip(mTagShortNameET, mTagShortNameCounterTV, R.string.tag_suggest_help_short_name, R.integer.suggest_tag_short_name_length);
        mTooltipName = mPresenter.setTextTooltip(mTagNameET, mTagNameCounterTV, R.string.tag_suggest_help_name, R.integer.suggest_tag_name_length);
        mTooltipDescription = mPresenter.setTextTooltip(mTagDescriptionET, mTagDescriptionCounterTV, R.string.tag_suggest_help_description, R.integer.suggest_tag_description_length);
        ((SuggestTagActivity) getActivity()).setUpView(R.string.tag_title, R.string.tag_bottom_button_label);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(BUNDLE_SAVED_STATE_SHORTNAME, mTagShortNameET.getText().toString());
        outState.putString(BUNDLE_SAVED_STATE_NAME, mTagNameET.getText().toString());
        outState.putString(BUNDLE_SAVED_STATE_DESCRIPTION, mTagDescriptionET.getText().toString());
        outState.putString(BUNDLE_SAVED_STATE_WEBSITE, mWebsiteET.getText().toString());

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            mTagShortNameET.setText(savedInstanceState.getString(BUNDLE_SAVED_STATE_SHORTNAME));
            mTagNameET.setText(savedInstanceState.getString(BUNDLE_SAVED_STATE_NAME));
            mTagDescriptionET.setText(savedInstanceState.getString(BUNDLE_SAVED_STATE_DESCRIPTION));
            mWebsiteET.setText(savedInstanceState.getString(BUNDLE_SAVED_STATE_WEBSITE));
            mAddTagPhotoTV.setText(savedInstanceState.getString(BUNDLE_SAVED_STATE_PHOTO));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
        if (photoUri == null)
            return;

        if (!photoUri.equals("")) {
            mSuggestTag.setPhoto(photoUri);
            mAddTagPhotoTV.setText(getString(R.string.tag_suggest_added_tag_photo));
        }

    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }


    @OnClick(R.id.tag_short_name_iv)
    public void onShortNameHelpClick() {
        if (mTooltipShortName == null)
            return;
        if (!mTooltipShortName.isShowing()) {
            mTooltipShortName.show();
        }
    }

    @OnClick(R.id.tag_name_iv)
    public void onNameHelpClick() {
        if (mTooltipName == null)
            return;

        if (!mTooltipName.isShowing()) {
            mTooltipName.show();
        }
    }

    @OnClick(R.id.tag_description_iv)
    public void onDescriptionHelpClick() {
        if (mTooltipDescription == null)
            return;
        if (!mTooltipDescription.isShowing()) {
            mTooltipDescription.show();
        }
    }

    GalleryDialogFragment galleryDialogFragment;

    @OnClick(R.id.tag_add_photo_tv)
    public void onAddTagPhotoClicked() {
        galleryDialogFragment = GalleryDialogFragment.newInstance(
                new Bundle(),
                getString(R.string.choose_image_label),
                getString(R.string.camera_btn),
                getString(R.string.gallery_btn), false);

        galleryDialogFragment.show(getChildFragmentManager(), "gallery_dialog");

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) && resultCode == RESULT_OK) {
            photoUri = data.getExtras().getString(CropPhotoActivity.BUNDLE_EXTRA_PHOTO_URI);
            mSuggestTag.setPhoto(photoUri);
            mAddTagPhotoTV.setText(getString(R.string.tag_suggest_added_tag_photo));
        }

    }

    @Override
    public void goNext() {

        mSuggestTag.setShortName(mTagShortNameET.getText().toString());
        mSuggestTag.setName(mTagNameET.getText().toString().substring(1));
        mSuggestTag.setDescription(mTagDescriptionET.getText().toString());
        mSuggestTag.setWebsite(mWebsiteET.getText().toString());

        SubmitEvidenceFragment fragment = SubmitEvidenceFragment.newInstance(mSuggestTag);

        FragmentHelper.replaceFragment(getFragmentManager(), fragment, SubmitEvidenceFragment.SUBMIT_EVIDENCE_FRAGMENT);
    }

    @Override
    public boolean checkFieldsNull() {
        return new ValidationHelper().checkNull(mTagShortNameET, mTagNameET, mTagDescriptionET) || mSuggestTag.getPhoto() == null || mWebsiteTI.isErrorEnabled();
    }

    @Override
    public void showToast(int messageId) {
        ((BaseActivity) getActivity()).showToast(getString(messageId), true);
    }

    @Override
    public void setListener() {

        for (final InputTypeSV inputType : InputTypeSV.values()) {
            TextInputLayout tv = getTextViewForInputType(inputType);
            if (tv == null)
                continue;
            tv.getEditText().addTextChangedListener(new CustomTextWatcher() {
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    mPresenter.validateInput(charSequence.toString(), inputType, i1);
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (inputType == InputTypeSV.TAG_NAME)
                        mPresenter.leaveNameTagPrefix(editable);

                }
            });


        }

    }


    @Override
    public void showInputError(InputTypeSV type, int errorMessageId) {
        TextInputLayout tv = getTextViewForInputType(type);

        tv.setErrorEnabled(true);
        tv.setError(getString(errorMessageId));

    }

    private TextInputLayout getTextViewForInputType(InputTypeSV type) {
        TextInputLayout tv = null;
        switch (type) {
            case URI:
                tv = mWebsiteTI;
                break;
            case TAG_NAME:
                tv = mTagNameTI;
        }
        return tv;
    }

    @Override
    public void hideInputError(InputTypeSV type) {
        TextInputLayout tv = getTextViewForInputType(type);
        tv.setErrorEnabled(false);
        tv.setError(null);
        tv.setEnabled(true);
    }

    @Override
    public void setNameText(Spannable text, int start) {

        mTagNameET.setText(text);
        mTagNameET.setSelection(start);
    }

    @Override
    public void clearNameTV() {
        mTagNameET.setText("");
    }


}
