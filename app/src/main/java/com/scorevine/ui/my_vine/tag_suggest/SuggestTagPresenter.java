package com.scorevine.ui.my_vine.tag_suggest;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.Spannable;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.prashantsolanki.secureprefmanager.SecurePrefManagerInit;
import com.scorevine.R;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.helpers.TooltipHelper;
import com.scorevine.helpers.ValidationHelper;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.validation.InputTypeSV;
import com.tooltip.Tooltip;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Yoga on 2016-12-23.
 */
public class SuggestTagPresenter implements SuggestTagContract.Presenter {


    SuggestTagContract.View mView;
    SuggestTagContract.Interactor mInteractor;
    Context mContext;
    String mToken;
    private ValidationHelper mValidator;

    public SuggestTagPresenter(SuggestTagFragment view, Context context) {
        mView = view;
        mContext = context;
        mInteractor = new TagInteractor();

        mToken = SecurityHelper.encodeToken(new SharedPrefHelper().getSharedPref(mContext, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN));
        mValidator = new ValidationHelper();
    }

    @Override
    public void subscribe() {
        EventBus.getDefault().register(this);
        mView.setListener();
    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }


    public Tooltip setTextTooltip(final TextInputEditText textInputEditText, final TextView mCounterTV, int helpTextId, final int fieldLengthId) {

        final Tooltip finalTooltip = TooltipHelper.createToolTipDialogBoxCounter(textInputEditText, helpTextId, Gravity.BOTTOM, mContext);

        textInputEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean onFocusChangeListener) {
                    if (onFocusChangeListener) {
                        if (textInputEditText.getText().toString().isEmpty()) {
                            finalTooltip.show();
                        }
                    } else {
                        finalTooltip.dismiss();
                    }
                }
            });
            mCounterTV.setText(String.valueOf(mContext.getResources().getInteger(fieldLengthId)));
            textInputEditText.addTextChangedListener(new CustomTextWatcher() {
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    int charactersLeft = mContext.getResources().getInteger(fieldLengthId) - textInputEditText.length();
                    mCounterTV.setText(String.valueOf(charactersLeft));
                }
            });

        return finalTooltip;
    }


    @Subscribe
    public void onEvent(NextButtonEvent event){
        if(!mView.checkFieldsNull())
            mView.goNext();
        else
            mView.showToast(R.string.suggest_tag_names_cannot_be_empty);
    }

    @Override
    public void validateInput(String charSequence, InputTypeSV inputType, int i1) {
        switch (inputType){
            case URI:
                if (!mValidator.validateMaxLength(charSequence, inputType)) {
                    mView.showInputError(inputType, R.string.error_max_characters_count_100);
                    return;
                }
                if (!mValidator.validateCharacters(charSequence, inputType)) {
                    mView.showInputError(inputType, R.string.suggest_a_tag_error_invalid_url);
                    return;
                }
                if(charSequence.length()==0){
                    mView.hideInputError(inputType);
                    return;
                }
                mView.hideInputError(inputType);
                break;
            case TAG_NAME:
                if (charSequence.length() == 1 && i1 >= 1) {
                    mView.clearNameTV();
                } else if (charSequence.length() == 1) {
                    Spannable text = new CustomTextWatcher().getSpannable(String.format("@%s", charSequence), mContext);
                    mView.setNameText(text, 2);
                }
                break;
        }

    }

    public void leaveNameTagPrefix(Editable editable) {

        if (editable.length() > 0 && editable.toString().charAt(0) != '@') {
            Spannable text = new CustomTextWatcher().getSpannable(String.format("@%s", editable.toString()), mContext);
            mView.setNameText(text, editable.length()+1);

        }
    }
}
