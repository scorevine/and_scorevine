package com.scorevine.ui.my_vine.contact_us;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ScrollView;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContactUsActivity extends AppCompatActivity {

    @BindView(R.id.signInScrollView)
    ScrollView mScrollView;
    @BindView(R.id.toolbar_title)
    TextView mTitleTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        mTitleTextView.setText(R.string.contact_us);

        if(getSupportFragmentManager().findFragmentByTag("Contactus")==null) {
            FragmentHelper.addFirstFragment(getSupportFragmentManager(), ContactUsFragment.getInstance(), "Contactus");
        }

    }

    @OnClick(R.id.bottom_button)
    public void onBottomButtonClick() {
        EventBus.getDefault().post(new NextButtonEvent());
    }

    @OnClick(R.id.backArrowButton)
    public void onBackArrowClick() {
        KeyboardHelper.hideKeyboard(mScrollView, this);
        onBackPressed();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
