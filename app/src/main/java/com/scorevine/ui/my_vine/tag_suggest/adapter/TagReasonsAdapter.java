package com.scorevine.ui.my_vine.tag_suggest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.model.tags.TagReason;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yoga on 2016-12-27.
 */
public class TagReasonsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnSelectionChangeListener {

        void onSelectionChange();
    }

    private OnSelectionChangeListener mOnSelectionChangeListener;
    private List<TagReason> mReasonsList;
    public int mSelectedPosition = -1;

    public TagReasonsAdapter(List<TagReason> reasonList, OnSelectionChangeListener onSelectionChangeListener) {

        this.mReasonsList = reasonList;
        this.mOnSelectionChangeListener = onSelectionChangeListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        holder = new TagsViewHolder(inflater.inflate(R.layout.list_item_submit_evidence_reasons, parent, false));

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((TagsViewHolder)holder).mReasonTV.setText(String.valueOf(position + 1) + ". " + mReasonsList.get(position).getReasonName());

        if (position == mSelectedPosition) {
            ((TagsViewHolder)holder).mReasonTV.setTextColor(
                    ((TagsViewHolder)holder).mReasonTV.getContext().getResources().getColor(R.color.colorTextAccent));
        } else {
            ((TagsViewHolder)holder).mReasonTV.setTextColor(
                    ((TagsViewHolder)holder).mReasonTV.getContext().getResources().getColor(R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return mReasonsList.size();
    }

    public void setReasonSelected(int reasonSelected) {

        mSelectedPosition = reasonSelected;

    }

    public int getSelectedItemId(){
        if(mSelectedPosition==-1){
            return mSelectedPosition;
        }else {
            return mReasonsList.get(mSelectedPosition).getReasonId();
        }
    }

    public boolean isPersonSelected(){
        if(mSelectedPosition==-1){
            return false;
        }else {
            return mReasonsList.get(mSelectedPosition).isPerson();
        }
    }



    public class TagsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        @BindView(R.id.reason_tv)
        TextView mReasonTV;

        public TagsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mSelectedPosition == getLayoutPosition())
                mSelectedPosition = -1;
            else {
                mSelectedPosition = getLayoutPosition();
            }

            mOnSelectionChangeListener.onSelectionChange();
            notifyDataSetChanged();
        }

    }

}
