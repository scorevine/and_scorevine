package com.scorevine.ui.my_vine.tag_suggest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.helpers.ValidationHelper;
import com.scorevine.model.tags.SuggestTag;
import com.scorevine.model.tags.TagReason;
import com.scorevine.ui.dialogs.ConfirmDialogFragment;
import com.scorevine.ui.my_vine.tag_suggest.adapter.TagReasonsAdapter;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.validation.InputTypeSV;
import com.tooltip.Tooltip;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Yoga on 2016-12-23.
 */

public class SubmitEvidenceFragment extends Fragment implements SuggestTagContract.ViewNext, TagReasonsAdapter.OnSelectionChangeListener{


    public static final String SUBMIT_EVIDENCE_FRAGMENT = SubmitEvidenceFragment.class.getSimpleName();
    public static final String BUNDLE_SAVED_STATE_LINK = "evidenceLink";
    public static final String BUNDLE_SAVED_STATE_WHAT_LOOK = "whatlook";
    public static final String BUNDLE_SAVED_STATE_REASON_ID = "reasonid";
    public static final String BUNDLE_SAVED_STATE_OVER_18 = "over18";
    private static final String BUNDLE_EXTRA_SUGGEST_TAG_KEY = "suggest tag obj";


    @BindView(R.id.tag_link_et)
    TextInputEditText mTagLinkET;
    @BindView(R.id.tag_link_ti)
    TextInputLayout mTagLinkTI;

    @BindView(R.id.tag_look_for_et)
    TextInputEditText mTagLookForET;

    @BindView(R.id.help_sorts_iv)
    ImageView mSortsHelpBtn;

    @BindView(R.id.sorts_rv)
    RecyclerView mTagSortsRV;

    @BindView(R.id.sorts_title)
    TextView mTagSortsTV;

    @BindView(R.id.over_18_layout)
    LinearLayout mOverLayout;

    @BindView(R.id.over_18_first_choice)
    TextView mOverFirstChoiceTV;
    @BindView(R.id.over_18_second_choice)
    TextView mOverSecondChoiceTV;
    @BindView(R.id.over_18_third_choice)
    TextView mOverThirdChoiceTV;

    public int mSelectedOver18 = -2;
    public int mSelectedReason = -1;

    public Tooltip mTooltipLink;
    public Tooltip mTooltipLookFor;
    public Tooltip mTooltipTagSorts;

    SubmitEvidencePresenter mPresenter;
    TagReasonsAdapter adapter;
    SuggestTag mSuggestTag;

    public static SubmitEvidenceFragment newInstance(SuggestTag suggestTag){
        Bundle bundle = new Bundle();
        bundle.putSerializable(BUNDLE_EXTRA_SUGGEST_TAG_KEY, suggestTag);

        SubmitEvidenceFragment fragment = new SubmitEvidenceFragment();
        fragment.setArguments(bundle);

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tag_submit_evidence, container, false);
        ButterKnife.bind(this, view);

        mPresenter = new SubmitEvidencePresenter(this, getContext());
        if (getArguments()!=null)
            mSuggestTag = (SuggestTag) getArguments().getSerializable(BUNDLE_EXTRA_SUGGEST_TAG_KEY);
        else
            mSuggestTag = new SuggestTag();


        mTooltipLink = mPresenter.setTextTooltip(mTagLinkET, R.string.tag_evidence_help_link, R.integer.submit_evidence_link);
        mTooltipLookFor = mPresenter.setTextTooltip(mTagLookForET, R.string.tag_evidence_help_what_to_look_for, R.integer.submit_what_to_look);
        mTooltipTagSorts = mPresenter.setTextTooltip(mTagSortsTV, R.string.tag_suggest_help_description);

        mPresenter.getReasonsTags();

        ((SuggestTagActivity)getActivity()).setUpView(R.string.tag_evidence_title, R.string.tag_evidence_bottom_button);

        return view;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(BUNDLE_SAVED_STATE_LINK, mTagLinkET.getText().toString());
        outState.putString(BUNDLE_SAVED_STATE_WHAT_LOOK,  mTagLookForET.getText().toString());
        outState.putInt(BUNDLE_SAVED_STATE_REASON_ID, mSelectedReason);
        outState.putInt(BUNDLE_SAVED_STATE_OVER_18, mSelectedOver18);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            mTagLinkET.setText(savedInstanceState.getString(BUNDLE_SAVED_STATE_LINK));
            mTagLookForET.setText(savedInstanceState.getString(BUNDLE_SAVED_STATE_WHAT_LOOK));

            mSelectedReason = savedInstanceState.getInt(BUNDLE_SAVED_STATE_REASON_ID);
            selectOver18(savedInstanceState.getInt(BUNDLE_SAVED_STATE_OVER_18));
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();

    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @OnClick(R.id.tag_link_iv)
    public void onLinkHelpClick() {
        if(mTooltipLink==null)
            return;
        if (!mTooltipLink.isShowing()) {
            mTooltipLink.show();
        }
    }
    @OnClick(R.id.tag_look_for_iv)
    public void onLookForHelpClick() {
        if(mTooltipLookFor==null)
            return;

        if (!mTooltipLookFor.isShowing()) {
            mTooltipLookFor.show();
        }
    }
    @OnClick(R.id.help_sorts_iv)
    public void onSortsHelpClick() {
        if(mTooltipTagSorts==null)
            return;
        if (!mTooltipTagSorts.isShowing()) {
            mTooltipTagSorts.show();
        }
    }

    @OnClick(R.id.over_18_first_choice)
    public void onYesClick() {
        clearSelection(mOverFirstChoiceTV, mOverSecondChoiceTV, mOverThirdChoiceTV);
        setSelection(mOverFirstChoiceTV);
        mSelectedOver18 = 1;
    }

    @OnClick(R.id.over_18_second_choice)
    public void onNoClick() {
        clearSelection(mOverSecondChoiceTV, mOverFirstChoiceTV, mOverThirdChoiceTV);
        setSelection(mOverSecondChoiceTV);

        mSelectedOver18 = 0;
    }

    @OnClick(R.id.over_18_third_choice)
    public void onDontKnowClick() {
        clearSelection(mOverThirdChoiceTV, mOverSecondChoiceTV, mOverFirstChoiceTV);
        setSelection(mOverThirdChoiceTV);

        mSelectedOver18 = -1;
    }

    public void selectOver18(int selectedOver18) {
        switch (selectedOver18){
            case -1:
                onDontKnowClick();
                break;
            case 0:
                onNoClick();
                break;
            case 1:
                onYesClick();
                break;
        }

    }

    public void clearSelection(TextView ... textViews){

        for (TextView textView:
                textViews) {
            textView.setTextColor(textView.getContext().getResources().getColor(R.color.colorPrimary));
        }

        mSelectedOver18 = -2;
    }
    public void setSelection(TextView textView2){
        textView2.setTextColor(textView2.getContext().getResources().getColor(R.color.colorTextAccent));
    }

    @Override
    public void setReasons(List<TagReason> reasons) {

        adapter = new TagReasonsAdapter(reasons, this);
        adapter.setReasonSelected(mSelectedReason);
        if (adapter.isPersonSelected())
            mOverLayout.setVisibility(View.VISIBLE);
        else{
            mOverLayout.setVisibility(View.INVISIBLE);
        }
        mTagSortsRV.setAdapter(adapter);
        mTagSortsRV.setLayoutManager(new LinearLayoutManager(this.getActivity()));
    }

    @Override
    public void showToast(int messageId) {
        ((SuggestTagActivity)getActivity()).showToast(getString(messageId), true);
    }

    @Override
    public void setData() {
        mSuggestTag.setEvidenceLink(mTagLinkET.getText().toString());
        mSuggestTag.setLookFor(mTagLookForET.getText().toString());
        mSuggestTag.setTagSort(adapter.getSelectedItemId());
        mSuggestTag.setOver18(mSelectedOver18);
    }

    @Override
    public void showTextMessage(String message, boolean b) {
        ((SuggestTagActivity)getActivity()).showToast(message, true);

    }

    @Override
    public void onBackPressed() {
        getActivity().finish();
    }


    @Override
    public void onSelectionChange() {
        KeyboardHelper.hideKeyboard(mTagSortsRV, this.getActivity());
        clearSelection(mOverThirdChoiceTV, mOverSecondChoiceTV, mOverFirstChoiceTV);
        mSelectedReason = adapter.mSelectedPosition;
        if (adapter.isPersonSelected())
            mOverLayout.setVisibility(View.VISIBLE);
        else{
            mOverLayout.setVisibility(View.INVISIBLE);
        }
    }


    ConfirmDialogFragment confirmDialogFragment;

    public void showConfirmationDialog() {
        confirmDialogFragment = ConfirmDialogFragment.newInstance(
                new Bundle(),
                getString(R.string.suggest_a_tag_dialog_message),
                getString(R.string.suggest_a_tag_dialog_confirm),
                null, false);

        confirmDialogFragment.show(getChildFragmentManager(), "confirm_tag_dialog");

    }

    @Override
    public boolean checkFieldsNull() {
        return new ValidationHelper().checkNull(mTagLinkET, mTagLookForET) || mSelectedReason==-1 || (adapter.isPersonSelected() && mSelectedOver18 == -2);
    }

    @Override
    public void onConfirmClicked() {

        mPresenter.submitTagSuggestion(mSuggestTag);
        confirmDialogFragment.dismiss();
    }

    @Override
    public void setListener() {

        TextInputLayout tv = getTextViewForInputType(InputTypeSV.URI);
        tv.getEditText().addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPresenter.validateInput(charSequence.toString(), InputTypeSV.URI);
            }
        });
    }

    @Override
    public void showInputError(InputTypeSV type, int errorMessageId) {
        TextInputLayout tv = getTextViewForInputType(type);

        tv.setErrorEnabled(true);
        tv.setError(getString(errorMessageId));

    }
    private TextInputLayout getTextViewForInputType(InputTypeSV type) {
        TextInputLayout tv = null;
        switch (type) {
            case URI:
                tv = mTagLinkTI;
                break;
        }
        return tv;
    }

    @Override
    public void hideInputError(InputTypeSV type) {
        TextInputLayout tv = getTextViewForInputType(type);
        tv.setErrorEnabled(false);
        tv.setError(null);
        tv.setEnabled(true);
    }
}
