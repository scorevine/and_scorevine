package com.scorevine.ui.my_vine.contact_us;

import android.content.Context;
import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.ApiConstants;
import com.scorevine.api.contact.Reason;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.InternetConnectionHelper;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.my_vine.InteractorReasons;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by NIKAK
 * Ready4S
 * on 16.12.2016 11:23
 */
public class ContactUsPresenter implements ContactUsContract.Presenter, ContactUsContract.Interactor.OnFinishedListener, InteractorReasons.OnFinishedListener
{

    ContactUsContract.Interactor mInteractor = new ContactUsInteractor();
    InteractorReasons mInteractorReasons = new InteractorReasons();
    ContactUsContract.View mView;
    private final HashMap<String, Integer> reasonsMap = new HashMap<>();

    @Override
    public void subscribe(ContactUsContract.View view) {
        EventBus.getDefault().register(this);
        mView = view;
    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }


    private String getEncodedToken(Context context) {
        String token = new SharedPrefHelper().getSharedPref(context, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN);
        Log.i("token", token);
        return SecurityHelper.encodeToken(token);
    }

    public void getReasonsList(Context context) {
        String encodedToken = getEncodedToken(context);
        mInteractorReasons.getReasonsList(ApiConstants.API_CONTACT_REASONS_MESSAGE, encodedToken, this);

    }

    @Override
    public void sendMessage(String reasonTitle, String heading, String text, Context context) {
        if (InternetConnectionHelper.isConnected(context)) {
            String encodedToken = getEncodedToken(context);
            if (getReasonId(reasonTitle) >= 0 && !heading.isEmpty() && !text.isEmpty()) {
                mInteractor.sendMessage(encodedToken, getReasonId(reasonTitle), heading, text, this);
            } else {
                mView.showToast(R.string.error_invalid_fields);
            }
        } else {
            mView.showToast(R.string.connection_error);
        }
    }

    int getReasonId(String title) {
        int id = -1;
        if (reasonsMap.containsKey(title))
            id = reasonsMap.get(title);

        return id;

    }

    @Override
    public void onResponseSendMessage() {

        mView.showToast(R.string.contact_us_message_sent);

    }

    @Override
    public void onFailureSendMessage() {
        mView.showToast(R.string.check_your_internet_connection);

    }

    @Subscribe
    public void onBottomButtonPressedEvent(NextButtonEvent event) {
        mView.onSendButtonClick();
    }

    @Override
    public void onResponseReasonsList(List<Reason> reasons){

        if (reasons != null && reasons.size() > 0) {
            ArrayList<String> reasonsTitles = new ArrayList<>();
            for (Reason reason : reasons) {
                reasonsTitles.add(reason.getReason());
                reasonsMap.put(reason.getReason(), reason.getId());
            }
            mView.setReasons(reasonsTitles);
        }
    }

    @Override
    public void onErrorReasonsList(APIErrorList errorList) {
        if (errorList.getError() != null)
                mView.showTextMessage(errorList.getError().get(0).getMessage(), false);

        }

    @Override
    public void onFailureReasonsList() {
        mView.showToast(R.string.check_your_internet_connection);
    }
}
