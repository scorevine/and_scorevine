package com.scorevine.ui.my_vine.contact_us;

import android.util.Log;

import com.scorevine.api.ApiManager;
import com.scorevine.api.contact.ContactUsApi;
import com.scorevine.api.contact.Reason;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NIKAK
 * Ready4S
 * on 17.12.2016 18:41
 */
public class ContactUsInteractor implements ContactUsContract.Interactor {

    ContactUsApi contactUsApi = new ApiManager().getRetrofit().create(ContactUsApi.class);


    @Override
    public void sendMessage(String encodedToken,int reasonId, String heading, String text, final OnFinishedListener listener) {
        contactUsApi.sendMessage(encodedToken, reasonId, heading, text).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("api", "response");
                Log.i("api", String.valueOf(response.code()));
                listener.onResponseSendMessage();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("api", "failure");
                listener.onFailureSendMessage();
            }
        });
    }
}
