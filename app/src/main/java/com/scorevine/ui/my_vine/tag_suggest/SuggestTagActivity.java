package com.scorevine.ui.my_vine.tag_suggest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.ScrollView;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.model.tags.SuggestTag;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.signing.login.LoginFragment;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SuggestTagActivity extends BaseActivity {

    @BindView(R.id.scrollView)
    ScrollView mScrollView;
    @BindView(R.id.toolbar_title)
    TextView mTitleTextView;
    @BindView(R.id.bottom_button)
    TextView mBottomButton;

    private Fragment mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_scroll);
        ButterKnife.bind(this);

        Fragment firstFragment;
        if(getSupportFragmentManager().findFragmentByTag(SuggestTagFragment.SUGGEST_TAG_FRAGMENT)==null) {
            firstFragment = new SuggestTagFragment();
            FragmentHelper.addFirstFragment(getSupportFragmentManager(), firstFragment, SuggestTagFragment.SUGGEST_TAG_FRAGMENT);
            mContent = firstFragment;

        }
        if (savedInstanceState != null) {
            //Restore the fragment's instance
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        } else {
            mContent = FragmentHelper.getVisibleFragment(getSupportFragmentManager());
        }

    }

    @OnClick(R.id.bottom_button)
    public void onBottomButtonClick() {
        KeyboardHelper.hideKeyboard(mScrollView, this);
        EventBus.getDefault().post(new NextButtonEvent());
    }

    @OnClick(R.id.backArrowButton)
    public void onBackArrowClick() {
        KeyboardHelper.hideKeyboard(mScrollView, this);
        onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Save the fragment's instance
        if (mContent != null)
            getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FragmentHelper.getVisibleFragment(getSupportFragmentManager()) instanceof SuggestTagFragment)
            FragmentHelper.getVisibleFragment(getSupportFragmentManager()).onActivityResult(requestCode, resultCode, data);
    }

    public void setUpView(int title, int bottomButton) {
        mTitleTextView.setText(title);
        mBottomButton.setText(bottomButton);

    }
}
