package com.scorevine.ui.my_vine.tag_suggest;

import android.support.design.widget.TextInputEditText;
import android.text.Spannable;
import android.text.SpannableString;
import android.widget.TextView;

import com.scorevine.api.APIErrorList;
import com.scorevine.model.tags.SuggestTag;
import com.scorevine.model.tags.TagReason;
import com.scorevine.ui.validation.InputTypeSV;
import com.tooltip.Tooltip;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Yoga on 2016-12-23.
 */

public interface SuggestTagContract {

    interface View {

        void goNext();
        boolean checkFieldsNull();

        void showToast(int messageId);

        void setListener();

        void showInputError(InputTypeSV type, int errorMessageId);
        void hideInputError(InputTypeSV type);

        void setNameText(Spannable text, int start);

        void clearNameTV();

    }

    interface ViewNext{

        void showConfirmationDialog();

        void onConfirmClicked();

        void setReasons(List<TagReason> reasons);
        void showToast(int messageId);

        void setData();

        void showTextMessage(String message, boolean b);

        void onBackPressed();

        boolean checkFieldsNull();

        void showInputError(InputTypeSV type, int errorMessageId);
        void hideInputError(InputTypeSV type);

        void setListener();
    }

    interface Presenter {

        void subscribe();

        void unsubscribe();

        Tooltip setTextTooltip(final TextInputEditText textInputEditText, final TextView mCounterTV, int helpTextId, final int fieldLengthId);
        void validateInput(String charSequence, InputTypeSV inputType, int i1);

    }
    interface PresenterNext {

        void subscribe();

        void unsubscribe();

        Tooltip setTextTooltip(final TextInputEditText textInputEditText, int helpTextId, final int fieldLengthId);
        void submitTagSuggestion(SuggestTag suggestTag);
        void validateInput(String value, InputTypeSV inputType);


    }


    interface Interactor {

        interface OnFinishedListener {
            void onResponse();
            void onError(APIErrorList apiError);
            void onFailure();
        }

        void submitEvidence(String token, RequestBody tagShortName, RequestBody tagName,
                                   RequestBody description, RequestBody website, MultipartBody.Part photo,
                                   RequestBody link, RequestBody reasonId, RequestBody lookForDescription,
                            RequestBody isAdult, final SuggestTagContract.Interactor.OnFinishedListener listener);
    }

}
