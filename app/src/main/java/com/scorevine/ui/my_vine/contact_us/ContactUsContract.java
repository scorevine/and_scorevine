package com.scorevine.ui.my_vine.contact_us;

import android.content.Context;

import com.scorevine.api.contact.Reason;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NIKAK
 * Ready4S
 * on 16.12.2016 11:21
 */
public class ContactUsContract {

    public interface View {

        void setReasons(ArrayList<String> mReasons);

        void onSendButtonClick();

        void showToast(int messageId);

        void showTextMessage(String message, boolean b);
    }

    public interface Presenter {

        void subscribe(View view);

        void unsubscribe();

        void getReasonsList(Context context);

        void sendMessage(String reasonTitle, String heading, String text, Context context);
    }

    public interface Interactor {

        interface OnFinishedListener {

            void onResponseSendMessage();

            void onFailureSendMessage();
        }

        void sendMessage(String encodedToken, int reasonId, String heading, String text, OnFinishedListener listener);

    }

}
