package com.scorevine.ui.todo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adrianjez on 14.12.2016.
 *
 * Help Class To Remove
 */


public class ToDoFragment extends Fragment{

    public static final String TAG_FRAGMENT_TODO = "TagFragmentToDo";
    private static final String TITLE_BUNDLE_KEY = "TitleBundleKey";

    public static ToDoFragment newInstance(String title){
        Bundle args = new Bundle();
        args.putString(TITLE_BUNDLE_KEY, title);
        ToDoFragment f = new ToDoFragment();
        f.setArguments(args);
        return f;
    }

    @BindView(R.id.title)
    TextView mTitleTextView;

    private String mTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null && getArguments().containsKey(TITLE_BUNDLE_KEY))
            mTitle = getArguments().getString(TITLE_BUNDLE_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        NestedScrollView rootView = (NestedScrollView) inflater.inflate(R.layout.fragment_to_do, container, false);
        ButterKnife.bind(this, rootView);
        mTitleTextView.setText("TODO: \n" + mTitle);
        return rootView;
    }

}
