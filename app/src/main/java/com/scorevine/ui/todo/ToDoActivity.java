package com.scorevine.ui.todo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by adrianjez on 20.12.2016.
 */

public class ToDoActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;
    private Fragment mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);
        ButterKnife.bind(this, this);

        String token = new SharedPrefHelper().getSharedPref(this, SharedPrefHelper.SP_TOKEN, "");
        String encodedToken = SecurityHelper.encodeToken(token);

        /** Fragment init **/
        mFragmentManager = getSupportFragmentManager();
        Fragment firstFragment = mFragmentManager.findFragmentById(R.id.fragment_container);
        if (firstFragment == null) {
            firstFragment = ToDoFragment.newInstance("To Do");
            FragmentHelper.addFirstFragment(mFragmentManager, firstFragment, ToDoFragment.TAG_FRAGMENT_TODO);
            mContent = firstFragment;

        }
        if (savedInstanceState != null) {
            //Restore the fragment's instance
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        } else {
            mContent = FragmentHelper.getVisibleFragment(mFragmentManager);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's instance
        if (mContent != null)
            mFragmentManager.putFragment(outState, "mContent", mContent);
    }

    @OnClick(R.id.backArrowButton)
    public void onBackClicked(){
        super.onBackPressed();
    }
}
