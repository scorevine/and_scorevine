package com.scorevine.ui.todo.my_categories.delete_category;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.FragmentHelper;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DeleteCategoryActivity extends AppCompatActivity {


    @BindView(R.id.toolbar_title)
    TextView mTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_category);
        ButterKnife.bind(this);

        mTitleTextView.setText(R.string.delete_a_category);

        if (getSupportFragmentManager().findFragmentByTag("Deleteacategory") == null) {

            FragmentHelper.addFirstFragment(getSupportFragmentManager(), DeleteCategoryFragment.getInstance(), "Deleteacategory");
        }

    }

    @OnClick(R.id.bottom_button)
    public void onBottomButtonClick() {
        EventBus.getDefault().post(new NextButtonEvent());
    }

    @OnClick(R.id.backArrowButton)
    public void onBackArrowClick() {
        onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
