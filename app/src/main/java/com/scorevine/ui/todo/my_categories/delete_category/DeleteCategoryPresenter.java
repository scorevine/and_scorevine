package com.scorevine.ui.todo.my_categories.delete_category;

import android.content.Context;
import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.categories.CustomCategory;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.InternetConnectionHelper;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by weronikapapkouskaya on 19.12.2016.
 */

public class DeleteCategoryPresenter implements DeleteCategoryContract.Presenter, DeleteCategoryContract.Interactor.OnFinishedListener {
    private DeleteCategoryContract.View mView;
    private DeleteCategoryContract.Interactor mInteractor = new DeleteCategoryInteractor();

    public DeleteCategoryPresenter(DeleteCategoryContract.View mView) {
        this.mView = mView;
    }

    @Override
    public void subscribe(DeleteCategoryContract.View view) {

        EventBus.getDefault().register(this);
        mView = view;

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void getFollowedCategories(Context context) {
        if (InternetConnectionHelper.isConnected(context)) {
            mInteractor.getFollowedCategories(getEncodedToken(context), this);
            Log.i("delete categories", "get followed categories");

        } else mView.showToast(R.string.connection_error);

    }

    private String getEncodedToken(Context context) {
        String token = new SharedPrefHelper().getSharedPref(context, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN);
        Log.i("token", token);
        return SecurityHelper.encodeToken(token);
    }

    @Override
    public void deleteCategoriesFromFollowed(ArrayList<Integer> categoriesIdList, Context context) {
        if (InternetConnectionHelper.isConnected(context)) {
            if (categoriesIdList.size() > 0) {
                JSONArray jsonArray = new JSONArray(categoriesIdList);
                mInteractor.deleteCategoriesFromFollowed(getEncodedToken(context), jsonArray.toString(), this);
            } else {
                mView.showToast(R.string.connection_error);
            }

        } else mView.showToast(R.string.connection_error);

    }

    @Override
    public void onResponseDeleteCategories() {

    }

    @Override
    public void onFailureDeleteCategories() {

    }

    @Override
    public void onResponseGetCategories(List<CustomCategory> customCategories) {

        if (customCategories != null) {
            mView.setAdapter(new ArrayList<>(customCategories));
        }

    }

    @Override
    public void onFailureGetCategories() {

    }

    @Subscribe
    public void onBottomButtonPressedEvent(NextButtonEvent event) {
        mView.onSaveButtonClick();
    }
}
