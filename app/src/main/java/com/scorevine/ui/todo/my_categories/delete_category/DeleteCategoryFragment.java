package com.scorevine.ui.todo.my_categories.delete_category;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.scorevine.R;
import com.scorevine.api.categories.CustomCategory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DeleteCategoryFragment extends Fragment implements DeleteCategoryContract.View {

    private static final String LIST_STATE_KEY = "deletecategoryadapter.state";
    @BindView(R.id.delete_cat_rv)
    RecyclerView mDeleteCategoryRV;
    private DeleteCategoryAdapter mAdapter;
    private DeleteCategoryContract.Presenter mPresenter;


    public static DeleteCategoryFragment getInstance() {
        return new DeleteCategoryFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete_category, null, false);

        ButterKnife.bind(this, view);
        mPresenter = new DeleteCategoryPresenter(this);
        if (savedInstanceState == null) {
            mPresenter.getFollowedCategories(getContext());
        } else {
            mAdapter = new DeleteCategoryAdapter(new ArrayList<CustomCategory>(), this.getActivity());
            mDeleteCategoryRV.setAdapter(mAdapter);
            mDeleteCategoryRV.setLayoutManager(new LinearLayoutManager(this.getActivity()));
            Log.i("delete", "setAdapter");
        }


        //setAdapter(customCategories);

        return view;

    }


    @Override
    public void onSaveButtonClick() {
        mPresenter.deleteCategoriesFromFollowed(mAdapter.getCheckedCategories(), getContext());
    }

    @Override
    public void setAdapter(ArrayList<CustomCategory> customCategories) {

        mAdapter = new DeleteCategoryAdapter(customCategories, this.getActivity());

        mDeleteCategoryRV.setAdapter(mAdapter);
        mDeleteCategoryRV.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        Log.i("adapter", "setAdapter");
    }

    @Override
    public void showToast(int message) {
        Toast.makeText(getActivity(), getString(message), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe(this);
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(LIST_STATE_KEY, mDeleteCategoryRV.getLayoutManager().onSaveInstanceState());
        outState.putParcelableArrayList(LIST_STATE_KEY + LIST_STATE_KEY, mAdapter.getCategories());

    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            Parcelable savedRecyclerLayoutState = savedInstanceState.getParcelable(LIST_STATE_KEY);
            ArrayList<CustomCategory> categories = savedInstanceState.getParcelableArrayList(LIST_STATE_KEY + LIST_STATE_KEY);
            mDeleteCategoryRV.getLayoutManager().onRestoreInstanceState(savedRecyclerLayoutState);
            mAdapter.mCategories = categories;
            mAdapter.notifyDataSetChanged();
        }
    }
}
