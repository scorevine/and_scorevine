package com.scorevine.ui.todo.my_categories.delete_category;

import android.content.Context;

import com.scorevine.api.categories.CustomCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NIKAK
 * Ready4S
 * on 17.12.2016 21:33
 */
public class DeleteCategoryContract {
    public interface View {
        void onSaveButtonClick();

        void setAdapter(ArrayList<CustomCategory> customCategories);

        void showToast(int message);
    }

    public interface Presenter {

        void subscribe(DeleteCategoryContract.View view);

        void unsubscribe();

        void getFollowedCategories(Context context);

        void deleteCategoriesFromFollowed(ArrayList<Integer> categoriesIdList, Context context);
    }

    public interface Interactor {
        interface OnFinishedListener {
            void onResponseDeleteCategories();

            void onFailureDeleteCategories();

            void onResponseGetCategories(List<CustomCategory> customCategories);

            void onFailureGetCategories();
        }

        void deleteCategoriesFromFollowed(String encodedToken, String categoriesIdList, DeleteCategoryContract.Interactor.OnFinishedListener listener);

        void getFollowedCategories(String encodedToken, DeleteCategoryContract.Interactor.OnFinishedListener listener);


    }
}
