package com.scorevine.ui.todo.my_categories.delete_category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.Toast;

import com.scorevine.R;
import com.scorevine.api.categories.CustomCategory;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NIKAK
 * Ready4S
 * on 16.12.2016 12:38
 */
public class DeleteCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<CustomCategory> mCategories;
    Context mContext;
    public static final int HEADER_VIEWTYPE = 1;
    public static final int CATEGORY_VIEWTYPE = 2;


    public DeleteCategoryAdapter(ArrayList<CustomCategory> categories, Context context) {
        mCategories = categories;
        mContext = context;
    }

    public ArrayList<CustomCategory> getCategories() {
        return mCategories;
    }

    @Override
    public int getItemViewType(int position) {
        int itemViewType = CATEGORY_VIEWTYPE;
        if (position == 0) {
            itemViewType = HEADER_VIEWTYPE;
        }
        return itemViewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view;
        switch (viewType) {
            case HEADER_VIEWTYPE:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_item_header_delete_category, parent, false);
                viewHolder = new HeaderViewHolder(view);
                break;
            case CATEGORY_VIEWTYPE:
                view = LayoutInflater.from(mContext).inflate(R.layout.list_item_delete_category, parent, false);
                viewHolder = new CategoryViewHolder(view);
                break;
            default:

        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof CategoryViewHolder) {
            ((CategoryViewHolder) holder).bind(mCategories.get(position - 1));
        }

    }

    @Override
    public int getItemCount() {
        return mCategories.size() + 1;
    }


    public ArrayList<Integer> getCheckedCategories() {
        ArrayList<Integer> checkedCategories = new ArrayList<>();
        for (CustomCategory category :
                mCategories) {
            if (category.isChecked()) {
                checkedCategories.add(category.getId());
            }
        }
        return checkedCategories;
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.categoryTitleCTV)
        CheckedTextView mCategoryTitle;
        @BindView(R.id.categoryCB)
        ImageView mCategoryCB;
        CustomCategory mCategory;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(CustomCategory category) {
            mCategory = category;
            mCategoryTitle.setText(category.getCategory());
            boolean isChecked = category.isChecked();
            mCategoryTitle.setChecked(isChecked);
            mCategoryCB.setSelected(isChecked);
        }

        @OnClick({R.id.categoryTitleCTV, R.id.categoryCB})
        public void onClick(View view) {
            if (!mCategoryTitle.isChecked()) {
                Toast.makeText(mContext, "Category will be deleted when you click save", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, "Delete cancelled", Toast.LENGTH_SHORT).show();

            }
            boolean isChecked = !mCategoryTitle.isChecked();
            mCategoryTitle.setChecked(isChecked);
            mCategoryCB.setSelected(isChecked);
            mCategory.setChecked(isChecked);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

}
