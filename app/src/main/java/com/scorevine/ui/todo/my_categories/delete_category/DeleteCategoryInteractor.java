package com.scorevine.ui.todo.my_categories.delete_category;

import android.util.Log;

import com.scorevine.api.ApiManager;
import com.scorevine.api.categories.CategoriesApi;
import com.scorevine.api.categories.CustomCategoriesResponse;
import com.scorevine.api.signing.FollowCategoriesResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NIKAK
 * Ready4S
 * on 20.12.2016 19:52
 */
public class DeleteCategoryInteractor implements DeleteCategoryContract.Interactor {
    CategoriesApi categoriesApi = new ApiManager().getRetrofit().create(CategoriesApi.class);

    @Override
    public void deleteCategoriesFromFollowed(String encodedToken, String categoriesIdList, final DeleteCategoryContract.Interactor.OnFinishedListener listener) {
        categoriesApi.deleteCategories(encodedToken, categoriesIdList).enqueue(new Callback<FollowCategoriesResponse>() {
            @Override
            public void onResponse(Call<FollowCategoriesResponse> call, Response<FollowCategoriesResponse> response) {

                listener.onResponseDeleteCategories();

            }

            @Override
            public void onFailure(Call<FollowCategoriesResponse> call, Throwable t) {

                listener.onFailureDeleteCategories();
            }
        });

    }

    @Override
    public void getFollowedCategories(String encodedToken, final DeleteCategoryContract.Interactor.OnFinishedListener listener) {
        categoriesApi.getCustomCategories(encodedToken).enqueue(new Callback<CustomCategoriesResponse>() {
            @Override
            public void onResponse(Call<CustomCategoriesResponse> call, Response<CustomCategoriesResponse> response) {
                if (response != null && response.body() != null) {
                    listener.onResponseGetCategories(response.body().getCustomCategories());
                    Log.i("delete categories", String.valueOf(response.body().toString()));

                }
                Log.i("delete categories", String.valueOf(response.code()));

            }

            @Override
            public void onFailure(Call<CustomCategoriesResponse> call, Throwable t) {
                listener.onFailureGetCategories();

            }
        });
    }
}
