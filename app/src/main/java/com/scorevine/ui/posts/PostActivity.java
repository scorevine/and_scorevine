package com.scorevine.ui.posts;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.posts.newfeed.NewsfeedModel;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.ui.BaseActivity;

import butterknife.ButterKnife;


/**
 * Created by NIKAK
 * Ready4S
 * on 28.12.2016 14:28
 */
public class PostActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        ButterKnife.bind(this);

        Fragment firstFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);


        if (getIntent().getExtras() != null && firstFragment == null) {
            NewsfeedModel post = getIntent().getExtras().getParcelable(PostFragment.ARG_POST);
            FragmentHelper.addFirstFragment(getSupportFragmentManager(),
                    PostFragment.getInstance(post), PostFragment.TAG_POST_FRAGMENT);
            Log.i("post", post.toString());

        } else FragmentHelper.getVisibleFragment(getSupportFragmentManager());


    }
}
