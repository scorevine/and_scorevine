package com.scorevine.ui.posts;

/**
 * Created by NIKAK
 * Ready4S
 * on 28.12.2016 14:22
 */
public interface PostContract {
    interface View {
    }

    interface Presenter {
    }

    interface Interactor {
    }
}
