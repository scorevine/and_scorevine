package com.scorevine.ui.posts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.R;
import com.scorevine.api.posts.newfeed.NewsfeedModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NIKAK
 * Ready4S
 * on 28.12.2016 14:22
 */
public class PostFragment extends Fragment {
    public static String TAG_POST_FRAGMENT = PostFragment.class.getSimpleName();
    public static String ARG_POST = "post";


    @BindView(R.id.rv_post)
    RecyclerView mPostRecyclerView;
    private PostAdapter mAdapter;
    private NewsfeedModel mPost;


    public static PostFragment getInstance(NewsfeedModel post) {

        PostFragment postFragment = new PostFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_POST, post);
        postFragment.setArguments(bundle);

        return postFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, view);

        mPost = getArguments().getParcelable(ARG_POST);

        mAdapter = new PostAdapter(mPost);
        mPostRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mPostRecyclerView.setAdapter(mAdapter);


        return view;
    }
}
