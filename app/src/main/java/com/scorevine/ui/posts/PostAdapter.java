package com.scorevine.ui.posts;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.leocardz.link.preview.library.LinkPreviewCallback;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.scorevine.R;
import com.scorevine.api.posts.newfeed.NewsfeedModel;
import com.scorevine.api.tags.Tag;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.PicassoHelper;
import com.scorevine.ui.home.newsfeed.myfeed.adapter.MyfeedAdapterActions;

import java.util.HashMap;
import java.util.WeakHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NIKAK
 * Ready4S
 * on 28.12.2016 14:24
 */
public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int POST_HEADER_VIEW_TYPE = 0;
    public static final int POST_COMMENT_VIEW_TYPE = 1;
    public static final int POST_COMMENT_REPLY_VIEW_TYPE = 2;

    private WeakHashMap<Long, Boolean> expandState = new WeakHashMap<>();
    private HashMap<String, SourceContent> mUrlPreviewCache;
    private MyfeedAdapterActions mCallback;

    NewsfeedModel mPost;

    public PostAdapter(NewsfeedModel postDetails) {
        mPost = postDetails;
        this.mUrlPreviewCache = new HashMap<>();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view;

        switch (viewType) {
            case POST_HEADER_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_post_header, parent, false);
                viewHolder = new PostHeaderViewHolder(view);
                break;
            case POST_COMMENT_VIEW_TYPE:
            case POST_COMMENT_REPLY_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_post_comment, parent, false);
                viewHolder = new CommentViewHolder(view, viewType);
                break;
            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Log.i("post", String.valueOf(position));

        if (holder instanceof PostHeaderViewHolder) {
            ((PostHeaderViewHolder) holder).bind(mPost);
        }
        if (holder instanceof CommentViewHolder) {
            ((CommentViewHolder) holder).bind();
        }

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {
        int itemViewType;
        if (position == 0) {
            itemViewType = POST_HEADER_VIEW_TYPE;
        } else if (position % 2 != 0) {
            itemViewType = POST_COMMENT_VIEW_TYPE;
        } else {
            itemViewType = POST_COMMENT_REPLY_VIEW_TYPE;
        }
        return itemViewType;
    }

    public class PostHeaderViewHolder extends RecyclerView.ViewHolder implements LinkPreviewCallback {

        /**
         * list_item_my_feed_new_part1.xml
         **/
        @BindView(R.id.avatar_container)
        ImageView mAvatarContainer;
        @BindView(R.id.author_username)
        TextView mAuthorUserName;
        @BindView(R.id.author_name)
        TextView mAuthorName;
        @BindView(R.id.time_container)
        TextView mTimeContainer;
        @BindView(R.id.date_container)
        TextView mDateContainer;
        /**
         * First tag properties
         **/
        @BindView(R.id.rating_bar)
        RatingBar mFirstTagRatingBar;
        @BindView(R.id.first_tag_name)
        TextView mFirstTagName;
        @BindView(R.id.first_tag_follow_button)
        ImageView mFirstTagFollowButton;
        /**
         * Description
         **/
        @BindView(R.id.feed_description)
        TextView mFeedDescriptionContainer;
        /**
         * URL PREVIEW @layout/list_item_my_feed_new_main_content
         */
        @BindView(R.id.preview_container)
        View mPreviewContainer;
        @BindView(R.id.progress_view)
        View mProgressView;
        @BindView(R.id.link_title)
        TextView mLinkTitle;
        @BindView(R.id.link_canonical_url)
        TextView mLinkCanonicalUrl;
        @BindView(R.id.link_image)
        ImageView mLinkImage;
        /**
         * Drop down Tags
         *
         * @layout/list_item_my_feed_tags_container
         **/
        @BindView(R.id.drop_down_container)
        View mDropDownMainContentView;
        @BindView(R.id.tags_header_text_container)
        TextView mTagsHeaderTextContainer;
        @BindView(R.id.expandableLayout)
        ExpandableRelativeLayout mLayoutToExpandCollapse;
        @BindView(R.id.tags_container)
        LinearLayout mDroppedDownContainer;
        /**
         * Bottom text information
         **/
        @BindView(R.id.bottom_information)
        TextView mBottomInformation;
        private TextCrawler mTextCrawler;
        private Long mCurrentId;

        @OnClick(R.id.tags_header)
        void onDropTagsClicked() {
            if (mLayoutToExpandCollapse.isExpanded()) mLayoutToExpandCollapse.collapse();
            else mLayoutToExpandCollapse.expand();
            expandState.put(mCurrentId, mLayoutToExpandCollapse.isExpanded());
        }

        /**
         * Bound Actions
         **/
        @OnClick(R.id.share_button)
        void onShareClickded() {
            if (mCallback != null) mCallback.onFeedShareClicked(getAdapterPosition() - 1);
        }

        @OnClick(R.id.more_button)
        void onMoreButtonClicked() {
            //if (mCallback != null) mCallback.onMoreClicked(getAdapterPosition() - 1);
        }

        /**
         * Bottom Navigation
         **/
        @OnClick(R.id.influ_button)
        void onInfluButtonClicked() {
            //TODO
        }

        @OnClick(R.id.vote_up_button)
        void onVoteUpClicked() {
            //TODO
        }

        @OnClick(R.id.vote_down_button)
        void onVoteDownClicked() {
            //TODO
        }

        @OnClick(R.id.comment_button)
        void onCommentButtonClicked() {
            //TODO
        }

        public PostHeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mTextCrawler = new TextCrawler();
        }

        private void bind(NewsfeedModel newfeedModel) {
            this.mCurrentId = newfeedModel.getId();
            mLayoutToExpandCollapse.setExpanded(expandState.containsKey(this.mCurrentId) && expandState.get(this.mCurrentId));
            if (newfeedModel != null) {
                if (newfeedModel.getAuthor() != null) {
                    // Fullfillment of list_item_my_feed_new_part1
                    mAuthorName.setText(newfeedModel.getAuthor().getDisplayName());
                    mAuthorUserName.setText(("@" + newfeedModel.getAuthor().getUsername()));
                    mDateContainer.setText(DateFormatHelper.getSuffixedDate(newfeedModel.getDatetime()));
                    mTimeContainer.setText(DateFormatHelper.getDateWithFormat(DateFormatHelper.AM_PM_TIME, newfeedModel.getDatetime()));
                    PicassoHelper.setImage(itemView.getContext(), mAvatarContainer, newfeedModel.getAuthor().getProfileImage());
                }
                /** fill or hide first tag content **/
                if (newfeedModel.getTags() != null && newfeedModel.getTags().size() > 0) {
                    mFirstTagRatingBar.setVisibility(View.VISIBLE);
                    mFirstTagName.setVisibility(View.VISIBLE);
                    mFirstTagFollowButton.setVisibility(View.VISIBLE);
                    Tag tag = newfeedModel.getTags().get(0);
                    mFirstTagRatingBar.setRating(tag.getCurrentScore());
                    mFirstTagName.setText(tag.getTag());
                    mFirstTagFollowButton.setImageResource(tag.getFollowed() ? R.mipmap.ic_minus : R.mipmap.ic_plus);
                } else {
                    mFirstTagRatingBar.setVisibility(View.GONE);
                    mFirstTagName.setVisibility(View.INVISIBLE);
                    mFirstTagFollowButton.setVisibility(View.INVISIBLE);
                }

                if (newfeedModel.getTags().size() > 0) {
                    mTagsHeaderTextContainer.setText(Html.fromHtml(newfeedModel.getTagsHeaderHtml()));
                    mDropDownMainContentView.setVisibility(View.VISIBLE);
                    mDroppedDownContainer.removeAllViews();
                    for (Tag tag : newfeedModel.getTags()) {
                        View tagView = LayoutInflater.from(itemView.getContext()).inflate(R.layout.list_item_my_feed_tags_container_single_tag, mDroppedDownContainer, false);
                        ((TextView) tagView.findViewById(R.id.tag_name)).setText(tag.getTag());
                        ((ImageView) tagView.findViewById(R.id.tag_follow_button))
                                .setImageResource(tag.getFollowed() ? R.mipmap.ic_minus : R.mipmap.ic_plus);
                        ((RatingBar) tagView.findViewById(R.id.tag_rating_bar))
                                .setRating(tag.getCurrentScore());
                        mDroppedDownContainer.addView(tagView, -1, -1);
                    }
                } else {
                    mDropDownMainContentView.setVisibility(View.GONE);
                }


                mFeedDescriptionContainer.setText(newfeedModel.getDescription());
                if (newfeedModel.getLink() != null)
                    if (mUrlPreviewCache.containsKey(newfeedModel.getLink()) && mUrlPreviewCache.get(newfeedModel.getLink()) != null)
                        fillWithData(mUrlPreviewCache.get(newfeedModel.getLink()));
                    else mTextCrawler.makePreview(this, newfeedModel.getLink());
                else mPreviewContainer.setVisibility(View.GONE);

                mBottomInformation.setText(
                        itemView.getContext().getString(R.string.up_) + " " + String.valueOf(newfeedModel.getVotesUp()) +
                                " " + itemView.getContext().getString(R.string.down_) + " " + String.valueOf(newfeedModel.getVotesDown()) +
                                " " + itemView.getContext().getString(R.string.comments_) + " " + String.valueOf(newfeedModel.getCommentsAmount()) +
                                " " + itemView.getContext().getString(R.string.shares_) + " " + "TODO");
                //TODO SHARES AMOUNT
            }
        }

        /**
         * Implementation of LinkPreviewCallback
         **/
        @Override
        public void onPre() {
            if (mPreviewContainer.getVisibility() == View.GONE)
                mPreviewContainer.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPos(SourceContent sourceContent, boolean isNull) {
            mProgressView.setVisibility(View.GONE);
            //No content received means
            if (isNull || (sourceContent != null && sourceContent.getFinalUrl().equals(""))) {
                mPreviewContainer.setVisibility(View.GONE);
            } else {
                mUrlPreviewCache.put(sourceContent.getUrl(), sourceContent);
                fillWithData(sourceContent);
            }
        }

        private void fillWithData(SourceContent sourceContent) {
            mLinkTitle.setText(sourceContent.getTitle());
            mLinkCanonicalUrl.setText(sourceContent.getCannonicalUrl());
            if (sourceContent.getImages().size() > 0)
                PicassoHelper.setImage(itemView.getContext(), mLinkImage, sourceContent.getImages().get(0));
            else //TODO PLACEHOLDER FOR NO THUMBNAIL RECEIVED
                mLinkImage.setImageResource(R.mipmap.ic_comment_black_oval_bubble_shape);
        }
    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_post_comment)
        LinearLayout mCommentLL;
        /**
         * list_item_my_feed_new_part1.xml
         **/
        @BindView(R.id.avatar_container)
        ImageView mAvatarContainer;
        @BindView(R.id.author_username)
        TextView mAuthorUserName;
        @BindView(R.id.author_name)
        TextView mAuthorName;
        @BindView(R.id.time_container)
        TextView mTimeContainer;
        @BindView(R.id.date_container)
        TextView mDateContainer;
        /**
         * Bottom text information
         **/
        @BindView(R.id.bottom_information)
        TextView mBottomInformation;
        int type;

        public CommentViewHolder(View itemView, int type) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.type = type;
        }

        public void bind() {
            int paddingLeft = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.reply_comment_padding_left);
            int padding = itemView.getContext().getResources().getDimensionPixelSize(R.dimen.myfeed_li_item_padding);

            if (type == POST_COMMENT_REPLY_VIEW_TYPE) {
                mCommentLL.setPadding(paddingLeft, padding, padding, padding);
            } else {
                mCommentLL.setPadding(padding, padding, padding, padding);
            }
        }

        @OnClick(R.id.button_upvote)
        public void onUpVoteBtnClick() {
            Toast.makeText(itemView.getContext(), "onUpVoteBtnClick", Toast.LENGTH_SHORT).show();
        }

        @OnClick(R.id.button_downvote)
        public void onDownVoteBtnClick() {
            Toast.makeText(itemView.getContext(), "onDownVoteBtnClick", Toast.LENGTH_SHORT).show();
        }

        @OnClick(R.id.button_reply)
        public void onReplyBtnClick() {
            Toast.makeText(itemView.getContext(), "onReplyBtnClick", Toast.LENGTH_SHORT).show();

        }

        @OnClick(R.id.button_share)
        public void onShareBtnClick() {
            Toast.makeText(itemView.getContext(), "onShareBtnClick", Toast.LENGTH_SHORT).show();

        }

        @OnClick(R.id.button_more)
        public void onMoreBtnClick() {
            Toast.makeText(itemView.getContext(), "onMoreBtnClick", Toast.LENGTH_SHORT).show();

        }

    }

  /*  public class CommentReplyViewHolder extends RecyclerView.ViewHolder {

        public CommentReplyViewHolder(View itemView) {
            super(itemView);
        }

        public void bind() {

        }
    }*/
}
