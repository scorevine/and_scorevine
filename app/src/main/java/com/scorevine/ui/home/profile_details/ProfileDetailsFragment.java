package com.scorevine.ui.home.profile_details;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.scorevine.R;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.home.profile_details.adapter.ProfileDetailsAdapter;
import com.scorevine.ui.home.profile_details.adapter.ProfileDetailsAdapterActions;
import com.scorevine.ui.home.profile_details.adapter.ProfileDetailsSection;
import com.scorevine.ui.home.profile_details.adapter.ProfileDetailsSubsection;
import com.scorevine.ui.my_profile.profile_settings.ProfileSettingsActivity;
import com.scorevine.ui.my_vine.contact_us.ContactUsActivity;
import com.scorevine.ui.my_vine.tag_suggest.SuggestTagActivity;
import com.scorevine.ui.signing.SignInActivity;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adrianjez on 17.12.2016.
 */

public class ProfileDetailsFragment extends Fragment implements ProfileDetailsAdapterActions {

    public static final String TAG_FRAGMENT_PROFILE_DETAILS = ProfileDetailsFragment.class.getSimpleName();

    public static ProfileDetailsFragment newInstance() {
        return new ProfileDetailsFragment();
    }

    @BindView(R.id.my_vine_rv)
    RecyclerView mMyVineRV;

    @BindArray(R.array.my_vine_sections)
    String[] mMyVineSectionsLabels;

    ProfileDetailsAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_details, container, false);
        ButterKnife.bind(this, rootView);

        ArrayList<ExpandableGroup> profileDetailsSections = new ArrayList<>();
        //TODO INBOX
        //profileDetailsSections.add(new ProfileDetailsInboxSection(getString(R.string.inbox), makeMessages()));
        for (int i = 0; i < mMyVineSectionsLabels.length; i++) {
            ArrayList<ProfileDetailsSubsection> subSetting = new ArrayList<>();
            subSetting.add(new ProfileDetailsSubsection("subsetting", 1));
            profileDetailsSections.add(new ProfileDetailsSection(mMyVineSectionsLabels[i], subSetting));
        }

        mMyVineRV.setAdapter(mAdapter = new ProfileDetailsAdapter(this.getActivity(), profileDetailsSections, this));
        mMyVineRV.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mAdapter.toggleGroup(2);

        return rootView;
    }

    /**
     * ProfileDetailsAdapterActions
     */
    @Override
    public void onWriteNewClicked() {
        Toast.makeText(getActivity(), "On write new clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSeeAllMessagesClicked() {
        Toast.makeText(getActivity(), "see all clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuggestATagClicked() {
        Intent intent = new Intent(getActivity(), SuggestTagActivity.class);
        startActivity(intent);
    }

    @Override
    public void onImproveATagClicked() {
        Toast.makeText(getActivity(), "improve a tag clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onContactUsClicked() {

        Intent intent = new Intent(getActivity(), ContactUsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCreateGroupClicked() {
        Toast.makeText(getActivity(), "create group clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJoinGroupClicked() {
        Toast.makeText(getActivity(), "join group clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLeaveGroupClicked() {
        Toast.makeText(getActivity(), "leave group clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMyProfileSettingsClicked() {
        startActivity(new Intent(this.getActivity(), ProfileSettingsActivity.class));
    }

    @Override
    public void onMyProfileFollowingClicked() {
        Toast.makeText(getActivity(), "onMyProfileFollowingClicked clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMyProfileFollowersClicked() {
        Toast.makeText(getActivity(), "onMyProfileFollowersClicked clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoInternetMessage() {
        Toast.makeText(getActivity(), getString(R.string.connection_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMyProfileLogoutClicked() {
        new SharedPrefHelper().clearSharedPref(getContext());
        LoginManager.getInstance().logOut();
        getActivity().finish();
        startActivity(new Intent(getActivity(), SignInActivity.class));
    }
}
