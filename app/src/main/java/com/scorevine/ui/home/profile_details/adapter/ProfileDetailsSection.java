package com.scorevine.ui.home.profile_details.adapter;

import android.support.annotation.Nullable;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adrianjez on 17.12.2016.
 */

public class ProfileDetailsSection extends ExpandableGroup<ProfileDetailsSubsection> {

    private Integer id;
    private String category;
    private ArrayList<ProfileDetailsSubsection> subCategories;

    public ProfileDetailsSection(String title, List<ProfileDetailsSubsection> items) {
        super(title, items);
        if (id == null) {
            id = 800;
        }
        this.subCategories = (ArrayList<ProfileDetailsSubsection>) items;
        category = title;
    }

    public String getCategoryName() {
        return category;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public List<ProfileDetailsSubsection> getSubCategories() {
        return subCategories;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", subCategories=" + subCategories +
                '}';
    }

    @Override
    public List<ProfileDetailsSubsection> getItems() {
        return getSubCategories();
    }

    @Override
    public String getTitle() {
        return getCategoryName();
    }
}