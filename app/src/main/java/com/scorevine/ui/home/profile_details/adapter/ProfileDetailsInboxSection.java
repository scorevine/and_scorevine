package com.scorevine.ui.home.profile_details.adapter;

import com.scorevine.model.UserMessage;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by adrianjez on 17.12.2016.
 */

public class ProfileDetailsInboxSection extends ExpandableGroup<UserMessage> {

    public ProfileDetailsInboxSection(String title, List<UserMessage> items) {
        super(title, items);
    }

    /** For this section is defined footer **/
    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }
}
