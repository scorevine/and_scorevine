package com.scorevine.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.scorevine.R;
import com.scorevine.eventbus.ScrollChangedMessage;
import com.scorevine.ui.todo.ToDoFragment;
import com.scorevine.ui.home.newsfeed.NewsfeedFragment;
import com.scorevine.ui.home.profile_details.ProfileDetailsFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by adrianjez on 19.12.2016.
 */

public class HomeMainPresenter implements HomeMainContract.Presenter, AHBottomNavigation.OnTabSelectedListener {

    private HomeMainContract.View mView;
    private Bundle mArgs;
    private boolean mIsUserLoggedIn;
    private Context context;

    public HomeMainPresenter(@NonNull HomeMainContract.View view, Bundle args, boolean isUserLoggedIn, Context context){
        this.mView = view;
        this.mArgs = args;
        this.mIsUserLoggedIn = isUserLoggedIn;
        this.context = context;
    }

    /** HomeMainContract.Presenter **/
    @Override
    public void subscribe(HomeMainActivity activity) {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void insertNestedPage(int position){
        switch (position){
            case 0:
                mView.setNestedPage(ToDoFragment.newInstance("NESTED TEST 1"), "NESTED 1");
                break;
            case 1:
                mView.setNestedPage(NewsfeedFragment.newInstance(mArgs, mIsUserLoggedIn), NewsfeedFragment.TAG_FRAGMENT_NEWSFEED);
                break;
            case 2:
                mView.setNestedPage(ToDoFragment.newInstance("NESTED TEST 3"), "NESTED 3");
                break;
            case 3:
                mView.setNestedPage(ToDoFragment.newInstance("NESTED TEST 4"), "NESTED 4");
                break;
            case 4:
                if(mIsUserLoggedIn)
                    mView.setNestedPage(ProfileDetailsFragment.newInstance(), ProfileDetailsFragment.TAG_FRAGMENT_PROFILE_DETAILS);
                else
                    Toast.makeText(context, context.getString(R.string.user_without_account), Toast.LENGTH_SHORT);
                break;
        }
    }

    /** AHBottomNavigation.OnTabSelectedListener **/
    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        insertNestedPage(position);
        return true;
    }

    private int mBaseTranslationY;

    /** EventBus Subscriptions **/
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScrollNotifyMessage(ScrollChangedMessage scrollMessageType){
        if (scrollMessageType.isDragging()) {
            int toolbarHeight = mView.getBottomMenuHeight();
            if (scrollMessageType.isFirstScroll()) {
                float currentHeaderTranslationY = mView.getYTranslationOfBottomBar();
                if (-toolbarHeight < currentHeaderTranslationY) {
                    mBaseTranslationY = scrollMessageType.getScrollY();
                }
            }
            float headerTranslationY = ScrollUtils.getFloat(-(scrollMessageType.getmCurrentScrollY() - mBaseTranslationY), -toolbarHeight, 0);
            mView.moveBottomMenu(-headerTranslationY);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onScrollStateMessage(ScrollState scrollState){
        if (scrollState == ScrollState.DOWN) {
            mView.showBottomMenu();
        } else if (scrollState == ScrollState.UP) {
            mView.hideBottomMenu();
            /*
            int toolbarHeight = mToolbarView.getHeight();
            int scrollY = mRecyclerView.getCurrentScrollY();
            if (toolbarHeight <= scrollY) {
                hideToolbar();
            } else {
                showToolbar();
            }*/
        } else {
            /*
            // Even if onScrollChanged occurs without scrollY changing, toolbar should be adjusted
            if (!toolbarIsShown() && !toolbarIsHidden()) {
                // Toolbar is moving but doesn't know which to move:
                // you can change this to hideToolbar()
                showToolbar();
            }*/
        }
    }
}
