package com.scorevine.ui.home.newsfeed;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.scorevine.R;
import com.scorevine.ui.home.HomeMainActivity;
import com.scorevine.ui.home.newsfeed.adapter.NewsfeedAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by adrianjez on 14.12.2016.
 */

public class NewsfeedFragment extends Fragment implements NewsfeedContractor.View {

    public static final String TAG_FRAGMENT_NEWSFEED = NewsfeedFragment.class.getSimpleName();
    private static final String VIEW_PAGER_STATE_KEY = "NewsfeedFragmentViewPagerState";
    private static final String NEWSFEED_FRAGMENT_STATE_KEY = "NewsfeedFragmentState";
    private static final String IS_USER_LOGGED_IN_BUNDLE_KEY = "IsLoggedInBundleKey";


    public static final NewsfeedFragment newInstance(Bundle commonNestedArgs, boolean isUserLoggedIn) {
        NewsfeedFragment f = new NewsfeedFragment();
        commonNestedArgs.putBoolean(IS_USER_LOGGED_IN_BUNDLE_KEY, isUserLoggedIn);
        f.setArguments(commonNestedArgs);
        return f;
    }

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    private Bundle mFragmentState;
    private NewsfeedAdapter mAdapter;
    private NewsfeedPresenter mPresenter;
    private boolean mIsUserLoggedIn;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        restoreStateFromArgs();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            if (getArguments().containsKey(IS_USER_LOGGED_IN_BUNDLE_KEY))
                mIsUserLoggedIn = getArguments().getBoolean(IS_USER_LOGGED_IN_BUNDLE_KEY);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_newsfeed, container, false);
        ButterKnife.bind(this, rootView);
        this.mPresenter = new NewsfeedPresenter(this);
        this.mViewPager.setAdapter(mAdapter = new NewsfeedAdapter(getArguments(), getContext(), getChildFragmentManager(), mIsUserLoggedIn));
        this.mViewPager.setOffscreenPageLimit(4);
        this.mTabLayout.setupWithViewPager(this.mViewPager);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe((HomeMainActivity) getActivity());
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveStateToArgs();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveStateToArgs();
    }

    /** Bound Actions **/
    @OnClick(R.id.new_post_toolbar)
    void onNewPostToolbarIconClicked(){
        Toast.makeText(getActivity(), "New post icon clicked", Toast.LENGTH_SHORT).show();
    }

    /** NewsfeedContractor.View **/


    /**
     * Private Methods
     **/
    private boolean restoreStateFromArgs() {
        if (getArguments() != null && getArguments().containsKey(NEWSFEED_FRAGMENT_STATE_KEY)) {
            mFragmentState = getArguments().getBundle(NEWSFEED_FRAGMENT_STATE_KEY);
            if (mFragmentState != null) restoreOwnState();
        }
        return mFragmentState != null;
    }

    private void saveStateToArgs() {
        mFragmentState = saveOwnState();
        if (mFragmentState != null)
            getArguments().putBundle(NEWSFEED_FRAGMENT_STATE_KEY, mFragmentState);
    }

    private void restoreOwnState() {
        if (mViewPager != null)
            mViewPager.setCurrentItem(mFragmentState.getInt(VIEW_PAGER_STATE_KEY));
    }

    private Bundle saveOwnState() {
        Bundle state = new Bundle();
        if (mViewPager != null)
            state.putInt(VIEW_PAGER_STATE_KEY, mViewPager.getCurrentItem());
        return state;
    }
}
