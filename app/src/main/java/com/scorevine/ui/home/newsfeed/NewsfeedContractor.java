package com.scorevine.ui.home.newsfeed;

import com.scorevine.ui.home.HomeMainActivity;

/**
 * Created by adrianjez on 16.12.2016.
 */

public interface NewsfeedContractor {

    interface View {

    }

    interface Presenter {
        void subscribe(HomeMainActivity activity);
        void unsubscribe();
    }

    interface Interactor {

    }
}
