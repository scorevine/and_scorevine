package com.scorevine.ui.home.newsfeed.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.scorevine.R;
import com.scorevine.ui.home.newsfeed.myfeed.MyfeedFragment;
import com.scorevine.ui.todo.ToDoFragment;

/**
 * Created by adrianjez on 15.12.2016.
 */

public class NewsfeedAdapter extends FragmentPagerAdapter {

    private boolean mIsMyFeedAvailable = false;
    private Context mContext;
    private Bundle args;

    public NewsfeedAdapter(Bundle args, Context context, FragmentManager fragmentManager, boolean isMyFeedAvailable){
        super(fragmentManager);
        this.mContext = context;
        this.mIsMyFeedAvailable = isMyFeedAvailable;
        this.args = args;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            default:
            case 0:
                return mIsMyFeedAvailable ? MyfeedFragment.newInstance(args) : ToDoFragment.newInstance("Hot");
            case 1:
                return mIsMyFeedAvailable ? ToDoFragment.newInstance("Hot") : ToDoFragment.newInstance("Trending");
            case 2:
                return mIsMyFeedAvailable ? ToDoFragment.newInstance("Trending") : ToDoFragment.newInstance("Fresh");
            case 3:
                return ToDoFragment.newInstance("Fresh");
        }
    }

    @Override
    public int getCount() {
        return mIsMyFeedAvailable ? 4 : 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            default:
            case 0:
                return mIsMyFeedAvailable ? mContext.getString(R.string.Myfeed) : mContext.getString(R.string.Hot);
            case 1:
                return mIsMyFeedAvailable ? mContext.getString(R.string.Hot) : mContext.getString(R.string.Trending);
            case 2:
                return mIsMyFeedAvailable ? mContext.getString(R.string.Trending): mContext.getString(R.string.Fresh);
            case 3:
                return mContext.getString(R.string.Fresh);
        }
    }
}
