package com.scorevine.ui.home.newsfeed.myfeed;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.posts.newfeed.NewfeedApi;
import com.scorevine.api.posts.newfeed.NewfeedResponse;
import com.scorevine.api.tags.Tag;
import com.scorevine.api.tags.TagsApi;
import com.scorevine.api.tags.TagsResponse;
import com.scorevine.helpers.ErrorUtils;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by adrianjez on 20.12.2016.
 */

public class MyFeedInteractor implements MyFeedContractor.Interactor {

    TagsApi tagsApiInterface = new ApiManager().getRetrofit().create(TagsApi.class);

    @Override
    public void getTrendingTags(String token, final OnGetTrendingTagsListener listener) {
        tagsApiInterface.getTrendingTags(token).enqueue(new Callback<TagsResponse>() {
            @Override
            public void onResponse(Call<TagsResponse> call, Response<TagsResponse> response) {
                switch (response.code()) {
                    case 200:
                        listener.onTagsResponse(response.body());
                        break;
                    case 401:
                    case 410:
                        listener.onGetError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<TagsResponse> call, Throwable t) {
                listener.onGetFailure(R.string.check_your_internet_connection);
            }
        });
    }

    NewfeedApi newfeedInterface = new ApiManager().getRetrofit().create(NewfeedApi.class);

    @Override
    public void getNewfeeds(String token, int page, final OnGetNewsfeedListener callback) {
        newfeedInterface.getNewfeed(token, page, 2).enqueue(new Callback<NewfeedResponse>() {
            @Override
            public void onResponse(Call<NewfeedResponse> call, Response<NewfeedResponse> response) {
                switch (response.code()) {
                    case 200:
                        callback.onNewsfeedsResponse(response.body());
                        break;
                    case 401:
                    case 410:
                        callback.onGetError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<NewfeedResponse> call, Throwable t) {
                callback.onGetFailure(R.string.check_your_internet_connection);
            }
        });
    }


    @Override
    public void changeFollowState(String token, final Tag tag, final OnFollowChangeListener callback) {
        RequestBody body =
                RequestBody.create(MediaType.parse("text/plain"), "type=" + tag.getType().toString());
        if(tag.getFollowed()){
            tagsApiInterface.unfollow(token, tag.getId(), body).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    switch (response.code()) {
                        case 200:
                            //Revert state and notify ui
                            tag.setFollowed(!tag.getFollowed());
                            callback.onTagFollowChanged(tag);
                            break;
                        case 401:
                        case 410:
                            callback.onGetError(ErrorUtils.parseError(response));
                            break;
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    callback.onGetFailure(R.string.check_your_internet_connection);
                }
            });
        } else {
            tagsApiInterface.follow(token, tag.getId(), body).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    switch (response.code()) {
                        case 201:
                            //Revert state and notify ui
                            tag.setFollowed(!tag.getFollowed());
                            callback.onTagFollowChanged(tag);
                            break;
                        case 401:
                        case 410:
                            callback.onGetError(ErrorUtils.parseError(response));
                            break;
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    callback.onGetFailure(R.string.check_your_internet_connection);
                }
            });
        }
    }
}
