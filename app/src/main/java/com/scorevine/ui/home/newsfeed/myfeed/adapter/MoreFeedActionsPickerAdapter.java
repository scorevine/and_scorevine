package com.scorevine.ui.home.newsfeed.myfeed.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.api.posts.newfeed.NewsfeedModel;

/**
 * Created by adrianjez on 29.12.2016.
 */

public class MoreFeedActionsPickerAdapter extends BaseAdapter {

    private NewsfeedModel mNewfeedModel;
    private LayoutInflater mInflater;
    private Context mContext;

    public enum ActionType {
        REPORT_THIS_POST,
        REPORT_THIS_VINER,
        BLOCK_THIS_VINER,
        SEE_NO_MORE_POSTS_ON_FIRST_TAG,
        WRITE_SIMILAR_POST
    }

    public static ActionType getTypeOnSelectedRow(int position){
        switch (position){
            default:
            case 0: return ActionType.REPORT_THIS_POST;
            case 1: return ActionType.REPORT_THIS_VINER;
            case 2: return ActionType.BLOCK_THIS_VINER;
            case 3: return ActionType.SEE_NO_MORE_POSTS_ON_FIRST_TAG;
            case 4: return ActionType.WRITE_SIMILAR_POST;
        }
    }

    public MoreFeedActionsPickerAdapter(Context context, NewsfeedModel model){
        this.mInflater = LayoutInflater.from(context);
        this.mNewfeedModel = model;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) convertView = mInflater.inflate(R.layout.dropdown_newsfeed_more_adapter, parent, false);
        switch (getTypeOnSelectedRow(position)){
            default:
            case REPORT_THIS_POST:
                ((TextView) convertView.findViewById(R.id.text_content))
                        .setText(mContext.getString(R.string.report_this_post));
                break;
            case REPORT_THIS_VINER:
                ((TextView) convertView.findViewById(R.id.text_content))
                        .setText(mContext.getString(R.string.report_this_Viner));
                break;
            case BLOCK_THIS_VINER:
                ((TextView) convertView.findViewById(R.id.text_content))
                        .setText(mContext.getString(R.string.block_this_Viner));
                break;
            case SEE_NO_MORE_POSTS_ON_FIRST_TAG:
                ((TextView) convertView.findViewById(R.id.text_content))
                        .setText(mContext.getString(R.string.see_no_more_posts_on));
                //TODO SEE_NO_MORE_POSTS_ON_FIRST_TAG
                break;
            case WRITE_SIMILAR_POST:
                ((TextView) convertView.findViewById(R.id.text_content))
                        .setText(mContext.getString(R.string.write_similar_post));
                break;
        }


        return convertView;
    }
}
