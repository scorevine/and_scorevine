package com.scorevine.ui.home.newsfeed.myfeed;

import android.content.Context;
import android.support.v7.widget.ListPopupWindow;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.posts.newfeed.NewfeedResponse;
import com.scorevine.api.posts.newfeed.NewsfeedModel;
import com.scorevine.api.tags.Tag;
import com.scorevine.api.tags.TagsResponse;
import com.scorevine.eventbus.ScrollChangedMessage;
import com.scorevine.fb_graph_api.getfriends.FBGetFriends;
import com.scorevine.fb_graph_api.models.FBFriends;
import com.scorevine.ui.home.newsfeed.myfeed.adapter.MoreFeedActionsPickerAdapter;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by adrianjez on 19.12.2016.
 */

public class MyFeedPresenter implements MyFeedContractor.Presenter, ObservableScrollViewCallbacks,
        MyFeedContractor.Interactor.OnGetNewsfeedListener,
        MyFeedContractor.Interactor.OnGetTrendingTagsListener,
        MyFeedContractor.Interactor.OnFollowChangeListener,
        FBGetFriends.OnFBGetFriendsListener {

    private MyFeedContractor.View mView;
    private MyFeedContractor.Interactor mInteractor;

    public MyFeedPresenter(MyFeedContractor.View view){
        this.mView = view;
        this.mInteractor = new MyFeedInteractor();
    }

    /** MyFeedContractor.Presenter **/
    @Override
    public void loadTrendingTags(String token) {
        this.mInteractor.getTrendingTags(token, this);
    }

    @Override
    public void loadNewfeeds(String token, int page) {
        this.mInteractor.getNewfeeds(token, page, this);
    }

    @Override
    @Deprecated
    public void loadFacebookFriends(Context context, View progress) {
        //new FBAsync(progress, new FBGetFriends(context, this)).execute();
    }

    @Override
    public void changeFollowState(String token, Tag tag) {
        this.mInteractor.changeFollowState(token, tag, this);
    }

    @Override
    public void showMorePicker(NewsfeedModel model, View anchorView, final Context context) {
        final ListPopupWindow popup = new ListPopupWindow(context);
        popup.setAdapter(new MoreFeedActionsPickerAdapter(context, model));
        popup.setAnchorView(anchorView);
        popup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popup.dismiss();
                Toast.makeText(context, "selected position: " + position, Toast.LENGTH_SHORT).show();
            }
        });
        popup.setModal(true);
        popup.show();
    }

    /** ObservableScrollViewCallbacks **/
    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        ScrollChangedMessage message = new ScrollChangedMessage.Builder()
                .setDragging(dragging)
                .setFirstScroll(firstScroll)
                .setScrollY(scrollY)
                .setCurrentScrollY(mView.getCurrentScrollY())
                .build();
        EventBus.getDefault().post(message);
    }

    @Override
    public void onDownMotionEvent() {
        //Not used
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        EventBus.getDefault().post(scrollState);
    }

    /** OnGetFinishedListener **/
    @Override
    public void onGetFailure(int responseId) {
        mView.showMessage(responseId, true);
    }

    @Override
    public void onGetError(APIErrorList errorsList) {
        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);
    }

    @Override
    public void onNewsfeedsResponse(NewfeedResponse response) {
        mView.initNewfeeds(response);
    }

    @Override
    public void onTagsResponse(TagsResponse response) {
        mView.initTrendingTags(response);
    }

    @Override
    public void onTagFollowChanged(Tag tag) {
        mView.refreshTag(tag);
    }

    /** OnFBGetFriendsListener **/
    @Deprecated
    @Override
    public void onReceived(FBFriends friends) {
        mView.initWithFacebookFriends(friends);
    }

    @Deprecated
    @Override
    public void onError() {

    }

}
