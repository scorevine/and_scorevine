package com.scorevine.ui.home.profile_details.adapter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by adrianjez on 17.12.2016.
 */

public class ProfileDetailsSubsection implements Parcelable {


    private int id;
    private String setting;

    public ProfileDetailsSubsection(String setting, int id) {
        this.setting = setting;
    }

    protected ProfileDetailsSubsection(Parcel in) {
        id = in.readInt();
        setting = in.readString();
    }


    public static final Creator<ProfileDetailsSubsection> CREATOR = new Creator<ProfileDetailsSubsection>() {
        @Override
        public ProfileDetailsSubsection createFromParcel(Parcel in) {
            return new ProfileDetailsSubsection(in);
        }

        @Override
        public ProfileDetailsSubsection[] newArray(int size) {
            return new ProfileDetailsSubsection[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getSetting() {
        return setting;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "id=" + id +
                ", setting='" + setting + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(setting);
    }
}