package com.scorevine.ui.home.newsfeed.myfeed.adapter;

import android.view.View;

/**
 * Created by adrianjez on 20.12.2016.
 */

public interface MyfeedAdapterActions {

    void onSeeAllClicked();
    void onFollowUnfollowClicked(int position);
    void onFeedShareClicked(int position);
    void onMoreClicked(int position, View anchorView);
    void onVoteUpClicked(int position);
    void onVoteDownClicked(int position);
    void onShowCommentsClicked(int positon);
    void onInfluButtonClicked(int position);
}
