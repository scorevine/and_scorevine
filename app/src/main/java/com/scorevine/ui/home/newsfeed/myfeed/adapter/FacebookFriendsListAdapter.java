package com.scorevine.ui.home.newsfeed.myfeed.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.fb_graph_api.models.FBFriends;
import com.scorevine.fb_graph_api.models.FBUser;
import com.scorevine.helpers.PicassoHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by adrianjez on 23.12.2016.
 */

public class FacebookFriendsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<FBUser> fbUsers;
    private Context mContext;
    private LayoutInflater mInflater;

    public FacebookFriendsListAdapter(Context context){
        mContext = context;
        mInflater = LayoutInflater.from(context);
        fbUsers = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cellView = mInflater.inflate(R.layout.list_item_my_feed_facebook_single_friend, parent, false);
        return new FBSingleUserViewHolder(cellView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof FBSingleUserViewHolder)
            ((FBSingleUserViewHolder) holder).refresh(fbUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return fbUsers != null ? fbUsers.size() : 0;
    }

    public void addUsers(FBFriends friends){
        this.fbUsers.addAll(friends.getData());
        notifyDataSetChanged();
    }

    class FBSingleUserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.fb_user_avatar)
        CircleImageView mFBUserAvatar;

        @BindView(R.id.name_container)
        TextView mUserNameContainer;

        @OnClick(R.id.fb_user_avatar)
        void onFbUserClick(){
            Uri uri = Uri.parse("fb-messenger://user/");
            //uri = ContentUris.withAppendedId(uri, fbUsers.get(getAdapterPosition()).getId());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            mContext.startActivity(intent);
        }

        public FBSingleUserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void refresh(FBUser user){
            PicassoHelper.setProfileImage(mContext, mFBUserAvatar, user.getPicture());
            mUserNameContainer.setText(user.getName());
        }
    }
}
