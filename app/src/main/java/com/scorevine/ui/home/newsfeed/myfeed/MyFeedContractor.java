package com.scorevine.ui.home.newsfeed.myfeed;

import android.content.Context;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.posts.newfeed.NewfeedResponse;
import com.scorevine.api.posts.newfeed.NewsfeedModel;
import com.scorevine.api.tags.Tag;
import com.scorevine.api.tags.TagsResponse;
import com.scorevine.fb_graph_api.models.FBFriends;

/**
 * Created by adrianjez on 19.12.2016.
 */

public interface MyFeedContractor {

    interface View {
        int getCurrentScrollY();
        void showMessage(int messageId, boolean isLong);
        void showTextMessage(String message, boolean isLong);
        void initTrendingTags(TagsResponse response);
        void initNewfeeds(NewfeedResponse response);
        void initWithFacebookFriends(FBFriends friends);
        void refreshTag(Tag tag);
    }

    interface Presenter {
        void loadTrendingTags(String token);
        void loadNewfeeds(String token, int page);
        void loadFacebookFriends(Context context, android.view.View progress);
        void changeFollowState(String token, Tag tag);
        void showMorePicker(NewsfeedModel model, android.view.View anchorView, final Context context);
    }

    interface Interactor {

        void getTrendingTags(String token, OnGetTrendingTagsListener callback);
        void getNewfeeds(String token, int page, OnGetNewsfeedListener callback);
        void changeFollowState(String token, Tag tag, OnFollowChangeListener callback);

        interface BaseInterface {
            void onGetFailure(int responseId);
            void onGetError(APIErrorList errorsList);
        }

        interface OnGetTrendingTagsListener extends BaseInterface {
            void onTagsResponse(TagsResponse response);
        }

        interface OnGetNewsfeedListener extends BaseInterface {
            void onNewsfeedsResponse(NewfeedResponse response);
        }

        interface OnFollowChangeListener extends BaseInterface {
            void onTagFollowChanged(Tag tag);
        }
    }
}
