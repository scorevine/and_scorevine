package com.scorevine.ui.home.newsfeed.myfeed.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.leocardz.link.preview.library.LinkPreviewCallback;
import com.leocardz.link.preview.library.SourceContent;
import com.leocardz.link.preview.library.TextCrawler;
import com.scorevine.R;
import com.scorevine.api.posts.newfeed.NewsfeedModel;
import com.scorevine.api.tags.Tag;
import com.scorevine.fb_graph_api.models.FBFriends;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.PicassoHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.WeakHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by adrianjez on 18.12.2016.
 */

public class MyFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int MYFEED_FACEBOOK_FRIENDS_HEADER_VIEW_TYPE = 1;
    private static final int MYFEED_TRENDING_TAGS_HEADER_VIEW_TYPE = 2;
    private static final int MYFEED_TRENDING_TAGS_LIST_ROW_VIEW_TYPE = 3;
    private static final int MYFEED_TRENDING_TAGS_FOOTER_VIEW_TYPE = 4;

    private static final int MYFEED_NEWFEED_ROW_VIEW_TYPE = 5;

    public static final int MYFEED_RV_FOOTER_VIEW_TYPE = 6;

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<Tag> mTrendingTags;
    private ArrayList<NewsfeedModel> mNews;
    private MyfeedAdapterActions mCallback;
    private HashMap<String, SourceContent> mUrlPreviewCache;
    private WeakHashMap<Long, Boolean> expandState = new WeakHashMap<>();
    private FacebookFriendsListAdapter mFacebookListAdapter;
    private boolean allFeedsHasBeenLoaded = false;
    private boolean isFacebookVisible = false;

    public MyFeedAdapter(Context context, ArrayList<Tag> trendingTags, ArrayList<NewsfeedModel> newFeed) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mTrendingTags = trendingTags;
        this.mNews = newFeed;
        this.mUrlPreviewCache = new HashMap<>();
        this.isFacebookVisible = false; //TODO ?
    }

    /**
     * RecyclerView Methods
     **/
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cellView;
        switch (viewType) {
            case MYFEED_FACEBOOK_FRIENDS_HEADER_VIEW_TYPE:
                cellView = mInflater.inflate(R.layout.list_item_my_feed_facebook_friends, parent, false);
                return new FacebookFiendsViewHolder(cellView);
            default:
            case MYFEED_TRENDING_TAGS_HEADER_VIEW_TYPE:
                cellView = mInflater.inflate(R.layout.list_item_header_my_feed_trending_tag, parent, false);
                return new TrendingTagsHeaderViewHolder(cellView);
            case MYFEED_TRENDING_TAGS_FOOTER_VIEW_TYPE:
                cellView = mInflater.inflate(R.layout.list_item_footer_my_feed_trending_tag, parent, false);
                return new TrendingTagsFooterViewHolder(cellView);
            case MYFEED_TRENDING_TAGS_LIST_ROW_VIEW_TYPE:
                cellView = mInflater.inflate(R.layout.list_item_my_feed_trending_tag, parent, false);
                return new TrendingTagsRowViewHolder(cellView);
            case MYFEED_NEWFEED_ROW_VIEW_TYPE:
                cellView = mInflater.inflate(R.layout.list_item_my_feed_new_root_view, parent, false);
                return new NewFeedViewHolder(cellView);
            case MYFEED_RV_FOOTER_VIEW_TYPE:
                cellView = mInflater.inflate(R.layout.list_item_my_feed_footer_view, parent, false);
                return new NewsfeedFooterViewHolder(cellView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case MYFEED_TRENDING_TAGS_LIST_ROW_VIEW_TYPE:
                TrendingTagsRowViewHolder tagsRowViewHolder = (TrendingTagsRowViewHolder) holder;
                tagsRowViewHolder.refresh(getTagEntity(position));
                break;
            case MYFEED_NEWFEED_ROW_VIEW_TYPE:
                NewFeedViewHolder newFeedViewHolder = (NewFeedViewHolder) holder;
                newFeedViewHolder.refresh(getNewEntity(position));
                break;
            case MYFEED_RV_FOOTER_VIEW_TYPE:
                NewsfeedFooterViewHolder footerViewHolder = (NewsfeedFooterViewHolder) holder;
                footerViewHolder.mFooterProgressView.setVisibility(allFeedsHasBeenLoaded ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return getNewsSize() +             //news size
                getTrendingTagsSize() +     //trending tags size
                getTopExtraRowsSizeCount()  //top header and fb is is
                + 2;                        //footer for trending tags section and footer main
    }

    @Override
    public int getItemViewType(int position) {
        if (isFacebookVisible) {
            if (position == 0) return MYFEED_FACEBOOK_FRIENDS_HEADER_VIEW_TYPE;
            if (position == 1) return MYFEED_TRENDING_TAGS_HEADER_VIEW_TYPE;
        } else if (position == 0) return MYFEED_TRENDING_TAGS_HEADER_VIEW_TYPE;

        if (position < getTrendingTagsSize() + getTopExtraRowsSizeCount())
            return MYFEED_TRENDING_TAGS_LIST_ROW_VIEW_TYPE;
        if (position == getTrendingTagsSize() + getTopExtraRowsSizeCount())
            return MYFEED_TRENDING_TAGS_FOOTER_VIEW_TYPE;

        if (position < getTrendingTagsSize() + getNewsSize() + getTopExtraRowsSizeCount() + 1)
            return MYFEED_NEWFEED_ROW_VIEW_TYPE;
        if (position == getTrendingTagsSize() + getNewsSize() + getTopExtraRowsSizeCount() + 1) {
            return MYFEED_RV_FOOTER_VIEW_TYPE;
        }
        Log.e("Unknown type", "AT: " + position);
        throw new RuntimeException("Unknown type");
    }

    //Public Methods
    public void setCallback(MyfeedAdapterActions callback) {
        mCallback = callback;
    }

    public void addTrendingTags(List<Tag> tag) {
        int lastOneIndex = getTopExtraRowsSizeCount() + getTrendingTagsSize(); // 1 + trending tags header
        if (mTrendingTags == null) mTrendingTags = new ArrayList<>();
        for (int i = 0; i < tag.size() && i < 3; i++) {
            mTrendingTags.add(tag.get(i));
        }
        notifyItemRangeInserted(lastOneIndex, tag.size());
    }

    public void addNewfeeds(ArrayList<NewsfeedModel> models) {
        int lastOneIndex = getTopExtraRowsSizeCount() + 1 + getTrendingTagsSize() + getNewsSize();
        if (mNews == null) mNews = new ArrayList<>();
        this.mNews.addAll(models);
        notifyItemRangeInserted(lastOneIndex, models.size());
    }

    public void addFriends(FBFriends friends) {
        if (mFacebookListAdapter != null) mFacebookListAdapter.addUsers(friends);
    }

    public void refreshTag(Tag tag) {
        //notifyDataSetChanged();
        for (int i = 0; i < mTrendingTags.size(); i++) {
            if (mTrendingTags.get(i).getId() == tag.getId())
                notifyItemChanged(i + getTopExtraRowsSizeCount());
        }
    }

    public void saveStateToArgs(Bundle arg) {
        if (arg != null) {
            //arg.putSerializable("PREVIEW_MAP", mUrlPreviewCache);
            arg.putBoolean("ALL_FEEDS_HAS_BEEN_LOADED", allFeedsHasBeenLoaded);
        }
    }

    public void restoreStateFromArgs(Bundle arg) {
        if (arg != null) {
            //mUrlPreviewCache = (HashMap<String, SourceContent>) arg.getSerializable("PREVIEW_MAP");
            allFeedsHasBeenLoaded = arg.getBoolean("ALL_FEEDS_HAS_BEEN_LOADED");
        }
    }

    public void setAllFeedsHasBeenLoaded(){
        this.allFeedsHasBeenLoaded = true;
    }

    // Private Methods
    private int getTopExtraRowsSizeCount() {
        if (isFacebookVisible) return 2;
        else return 1;
    }

    private int getAbsoluteNewfeedTopSectionSize() {
        return getTopExtraRowsSizeCount() + getTrendingTagsSize() + 1;
    }

    private int getNewsSize() {
        return (mNews != null ? mNews.size() : 0);
    }

    private int getTrendingTagsSize() {
        return (mTrendingTags != null ? mTrendingTags.size() : 0);
    }

    private Tag getTagEntity(int position) {
        try {
            if (position - getTopExtraRowsSizeCount() < getTrendingTagsSize()) // -1 for header
                return mTrendingTags.get(position - getTopExtraRowsSizeCount());
        } catch (Exception err) {
            err.printStackTrace();
        }
        return null;
    }

    private NewsfeedModel getNewEntity(int position) {
        try {
            if (position - (getTrendingTagsSize() + getTopExtraRowsSizeCount() + 1) < getNewsSize()) // - for tag contents
                return mNews.get(position - (getTrendingTagsSize() + getTopExtraRowsSizeCount() + 1));
        } catch (Exception err) {
            err.printStackTrace();
        }
        return null;
    }

    // ViewHolder Classes
    class TrendingTagsHeaderViewHolder extends RecyclerView.ViewHolder {

        public TrendingTagsHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    class TrendingTagsFooterViewHolder extends RecyclerView.ViewHolder {

        @OnClick(R.id.see_all_button)
        void onSeeAllClicked() {
            if (mCallback != null) mCallback.onSeeAllClicked();
        }

        public TrendingTagsFooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class TrendingTagsRowViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_container)
        ImageView mImageContainer;

        @BindView(R.id.plus_minus_container)
        ImageView mPlusMinusContainer;

        @BindView(R.id.rate_text_container)
        TextView mRateContainer;

        @BindView(R.id.display_name)
        TextView mDisplayName;

        @BindView(R.id.vines_container)
        TextView mVinesContainer;

        @OnClick(R.id.plus_minus_container)
        void onFollowUnfollowClicked() {
            if (mCallback != null)
                mCallback.onFollowUnfollowClicked(getAdapterPosition() - getTopExtraRowsSizeCount());
        }

        private TrendingTagsRowViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void refresh(Tag tag) {
            if (tag != null) {
                mDisplayName.setText(tag.getTag());
                mRateContainer.setText(String.valueOf(tag.getScoreAvg()));
                mVinesContainer.setText(mContext.getString(R.string.vines_) + " " + String.valueOf(tag.getVines()));
                mPlusMinusContainer.setImageResource(tag.getFollowed() ? R.mipmap.ic_minus : R.mipmap.ic_plus);
                PicassoHelper.setProfileImage(mContext, mImageContainer, tag.getImage());
            }
        }
    }

    class NewFeedViewHolder extends RecyclerView.ViewHolder implements LinkPreviewCallback {

        /**
         * list_item_my_feed_new_part1.xml
         **/
        @BindView(R.id.avatar_container)
        ImageView mAvatarConainer;

        @BindView(R.id.author_username)
        TextView mAuthorUserName;

        @BindView(R.id.author_name)
        TextView mAuthorName;

        @BindView(R.id.time_container)
        TextView mTimeContainer;

        @BindView(R.id.date_container)
        TextView mDateContainer;

        /**
         * First tag properties
         **/
        @BindView(R.id.drop_down_anchor_view)
        View mDropDownAnchorView;

        @BindView(R.id.rating_bar)
        RatingBar mFirstTagRatingBar;

        @BindView(R.id.first_tag_name)
        TextView mFirstTagName;

        @BindView(R.id.first_tag_follow_button)
        ImageView mFirstTagFollowButton;


        /**
         * Description
         **/
        @BindView(R.id.feed_description)
        TextView mFeedDescriptionContainer;

        /**
         * URL PREVIEW @layout/list_item_my_feed_new_main_content
         */
        @BindView(R.id.preview_container)
        View mPreviewContainer;

        @BindView(R.id.progress_view)
        View mProgressView;

        @BindView(R.id.link_title)
        TextView mLinkTitle;

        @BindView(R.id.link_canonical_url)
        TextView mLinkCanonicalUrl;

        @BindView(R.id.link_image)
        ImageView mLinkImage;

        /**
         * Drop down Tags
         *
         * @layout/list_item_my_feed_tags_container
         **/
        @BindView(R.id.drop_down_container)
        View mDropDownMainContentView;

        @BindView(R.id.tags_header_text_container)
        TextView mTagsHeaderTextContainer;

        @BindView(R.id.expandableLayout)
        ExpandableRelativeLayout mLayoutToExpandCollapse;

        @BindView(R.id.tags_container)
        LinearLayout mDropedDownContainer;

        @OnClick(R.id.tags_header)
        void onDropTagsClicked() {
            if (mLayoutToExpandCollapse.isExpanded()) mLayoutToExpandCollapse.collapse();
            else mLayoutToExpandCollapse.expand();
            expandState.put(mCurrentId, mLayoutToExpandCollapse.isExpanded());
        }

        /**
         * Bottom text information
         **/
        @BindView(R.id.bottom_information)
        TextView mBottomInformation;

        /**
         * Bound Actions
         **/
        @OnClick(R.id.share_button)
        void onShareClickded() {
            if (mCallback != null)
                mCallback.onFeedShareClicked(getAdapterPosition() - getAbsoluteNewfeedTopSectionSize());
        }

        @OnClick(R.id.more_button)
        void onMoreButtonClicked() {
            if (mCallback != null)
                mCallback.onMoreClicked(getAdapterPosition() - getAbsoluteNewfeedTopSectionSize(), mDropDownAnchorView);
        }

        /**
         * Bottom Navigation
         **/
        @OnClick(R.id.influ_button)
        void onInfluButtonClicked() {
            if (mCallback != null)
                mCallback.onInfluButtonClicked(getAdapterPosition() - getAbsoluteNewfeedTopSectionSize());
        }

        @OnClick(R.id.vote_up_button)
        void onVoteUpClicked() {
            if (mCallback != null)
                mCallback.onVoteUpClicked(getAdapterPosition() - getAbsoluteNewfeedTopSectionSize());
        }

        @OnClick(R.id.vote_down_button)
        void onVoteDownClicked() {
            if (mCallback != null)
                mCallback.onVoteDownClicked(getAdapterPosition() - getAbsoluteNewfeedTopSectionSize());
        }

        @OnClick(R.id.comment_button)
        void onCommentButtonClicked() {
            if (mCallback != null)
                mCallback.onShowCommentsClicked(getAdapterPosition() - getAbsoluteNewfeedTopSectionSize());
        }


        private TextCrawler mTextCrawler;
        private Long mCurrentId;

        public NewFeedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mTextCrawler = new TextCrawler();
        }

        private void refresh(final NewsfeedModel newfeedModel) {
            this.mCurrentId = newfeedModel.getId();
            if (newfeedModel != null) {
                if (newfeedModel.getAuthor() != null) {
                    // Fullfillment of list_item_my_feed_new_part1
                    mAuthorName.setText(newfeedModel.getAuthor().getDisplayName());
                    mAuthorUserName.setText(("@" + newfeedModel.getAuthor().getUsername()));
                    mDateContainer.setText(DateFormatHelper.getSuffixedDate(newfeedModel.getDatetime()));
                    mTimeContainer.setText(DateFormatHelper.getDateWithFormat(DateFormatHelper.AM_PM_TIME, newfeedModel.getDatetime()));
                    PicassoHelper.setProfileImage(mContext, mAvatarConainer, newfeedModel.getAuthor().getProfileImage());
                }
                /** fill or hide first tag content **/
                if (newfeedModel.getTags() != null && newfeedModel.getTags().size() > 0) {
                    mFirstTagRatingBar.setVisibility(View.VISIBLE);
                    mFirstTagName.setVisibility(View.VISIBLE);
                    mFirstTagFollowButton.setVisibility(View.VISIBLE);
                    Tag tag = newfeedModel.getTags().get(0);
                    mFirstTagRatingBar.setRating(tag.getCurrentScore());
                    mFirstTagName.setText(tag.getTag());
                    mFirstTagFollowButton.setImageResource(tag.getFollowed() ? R.mipmap.ic_minus : R.mipmap.ic_plus);
                } else {
                    mFirstTagRatingBar.setVisibility(View.GONE);
                    mFirstTagName.setVisibility(View.INVISIBLE);
                    mFirstTagFollowButton.setVisibility(View.INVISIBLE);
                }

                if (newfeedModel.getTags().size() > 1) {
                    mTagsHeaderTextContainer.setText(Html.fromHtml(newfeedModel.getTagsHeaderHtml()));
                    mDropDownMainContentView.setVisibility(View.VISIBLE);
                    mDropedDownContainer.removeAllViews();
                    for (int i = 1; i < newfeedModel.getTags().size(); i++) {
                        Tag tag = newfeedModel.getTags().get(i);
                        View tagView = mInflater.inflate(R.layout.list_item_my_feed_tags_container_single_tag, mDropedDownContainer, false);
                        ((TextView) tagView.findViewById(R.id.tag_name)).setText(tag.getTag());
                        ((ImageView) tagView.findViewById(R.id.tag_follow_button))
                                .setImageResource(tag.getFollowed() ? R.mipmap.ic_minus : R.mipmap.ic_plus);
                        ((RatingBar) tagView.findViewById(R.id.tag_rating_bar)).setRating(tag.getCurrentScore());
                        mDropedDownContainer.addView(tagView, -1, -1);
                    }
                } else {
                    mDropDownMainContentView.setVisibility(View.GONE);
                }


                mFeedDescriptionContainer.setText(newfeedModel.getDescription());
                if (newfeedModel.getLink() != null)
                    if (mUrlPreviewCache.containsKey(newfeedModel.getLink()) && mUrlPreviewCache.get(newfeedModel.getLink()) != null)
                        fillWithData(mUrlPreviewCache.get(newfeedModel.getLink()));
                    else mTextCrawler.makePreview(this, newfeedModel.getLink());
                else mPreviewContainer.setVisibility(View.GONE);

                mBottomInformation.setText(
                        mContext.getString(R.string.up_) + " " + String.valueOf(newfeedModel.getVotesUp()) +
                                " " + mContext.getString(R.string.down_) + " " + String.valueOf(newfeedModel.getVotesDown()) +
                                " " + mContext.getString(R.string.comments_) + " " + String.valueOf(newfeedModel.getCommentsAmount()) +
                                " " + mContext.getString(R.string.shares_) + " " + String.valueOf(newfeedModel.getShares()));
                mLayoutToExpandCollapse.post(new Runnable() {
                    @Override
                    public void run() {
                        mLayoutToExpandCollapse
                                .setExpanded(expandState.containsKey(newfeedModel.getId()) &&
                                        expandState.get(newfeedModel.getId()));
                    }
                });
            }
        }

        /**
         * Implementation of LinkPreviewCallback
         **/
        @Override
        public void onPre() {
            if (mPreviewContainer.getVisibility() == View.GONE)
                mPreviewContainer.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPos(SourceContent sourceContent, boolean isNull) {
            mProgressView.setVisibility(View.GONE);
            //No content received means
            if (isNull || (sourceContent != null && sourceContent.getFinalUrl().equals(""))) {
                mPreviewContainer.setVisibility(View.GONE);
            } else {
                mUrlPreviewCache.put(sourceContent.getUrl(), sourceContent);
                fillWithData(sourceContent);
            }
        }

        private void fillWithData(SourceContent sourceContent) {
            mLinkTitle.setText(sourceContent.getTitle());
            mLinkCanonicalUrl.setText(sourceContent.getCannonicalUrl());
            if (sourceContent.getImages().size() > 0)
                PicassoHelper.setImage(mContext, mLinkImage, sourceContent.getImages().get(0));
            else //TODO PLACEHOLDER FOR NO THUMBNAIL RECEIVED
                mLinkImage.setImageResource(R.mipmap.ic_comment_black_oval_bubble_shape);
        }
    }

    @Deprecated
    class FacebookFiendsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.facebook_friends_list)
        RecyclerView mFacebookFriendsList;

        public FacebookFiendsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mFacebookFriendsList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            mFacebookFriendsList.setAdapter(mFacebookListAdapter = new FacebookFriendsListAdapter(mContext));
        }
    }

    /** R.layout.list_item_my_feed_footer_view **/
    class NewsfeedFooterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.footer_progress_view)
        View mFooterProgressView;

        public NewsfeedFooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}