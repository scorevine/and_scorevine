package com.scorevine.ui.home;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.prashantsolanki.secureprefmanager.SecurePrefManagerInit;
import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;

import butterknife.ButterKnife;

/**
 * Created by Yoga on 2016-12-09.
 */

public class HomeMainActivity extends AppCompatActivity {


    private FragmentManager mFragmentManager;
    private Fragment mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signed_in);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        ButterKnife.bind(this, this);

        new SecurePrefManagerInit.Initializer(getApplicationContext())
                .useEncryption(true)
                .initialize();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        String token = new SharedPrefHelper().getSharedPref(this, SharedPrefHelper.SP_TOKEN, "");
        String encodedToken = SecurityHelper.encodeToken(token);

        /** Fragment init **/
        mFragmentManager = getSupportFragmentManager();
        Fragment firstFragment = mFragmentManager.findFragmentById(R.id.fragment_container);
        if (firstFragment == null) {
            firstFragment = HomeMainFragment.newInstance(token.length() != 0);
            FragmentHelper.addFirstFragment(mFragmentManager, firstFragment, HomeMainFragment.TAG_FRAGMENT_HOME_MAIN);
            mContent = firstFragment;

        }
        if (savedInstanceState != null) {
            //Restore the fragment's instance
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        } else {
            mContent = FragmentHelper.getVisibleFragment(mFragmentManager);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's instance
        if (mContent != null)
            mFragmentManager.putFragment(outState, "mContent", mContent);
    }
}
