package com.scorevine.ui.home.newsfeed.myfeed;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.scorevine.R;
import com.scorevine.api.posts.newfeed.NewfeedResponse;
import com.scorevine.api.posts.newfeed.NewsfeedModel;
import com.scorevine.api.tags.Tag;
import com.scorevine.api.tags.TagsResponse;
import com.scorevine.fb_graph_api.models.FBFriends;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.home.newsfeed.myfeed.adapter.EndlessRecyclerViewScrollListener;
import com.scorevine.ui.home.newsfeed.myfeed.adapter.MyFeedAdapter;
import com.scorevine.ui.home.newsfeed.myfeed.adapter.MyfeedAdapterActions;
import com.scorevine.ui.posts.PostActivity;
import com.scorevine.ui.posts.PostFragment;
import com.scorevine.ui.todo.ToDoActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adrianjez on 15.12.2016.
 */

public class MyfeedFragment extends Fragment implements MyFeedContractor.View, MyfeedAdapterActions {

    private static final String RECYCLER_VIEW_STATE_KEY = "MyfeedFragmentRecyclerViewState";
    private static final String MY_FEED_FRAGMENT_STATE = "MyFeedFragmentState";

    public static final MyfeedFragment newInstance(Bundle nestedArgs) {
        MyfeedFragment f = new MyfeedFragment();
        f.setArguments(nestedArgs);
        return f;
    }

    @BindView(R.id.my_feed_rv)
    ObservableRecyclerView mRecyclerView;

    @BindView(R.id.progress_view)
    View progress;

    private MyFeedPresenter mPresenter;
    private Bundle mFragmentState;
    private String encodedToken;
    private MyFeedAdapter mMyFeedAdapter;
    private ArrayList<Tag> mTrendingTags;
    private ArrayList<NewsfeedModel> mNewfeeds;
    private String fb;
    private EndlessRecyclerViewScrollListener mScrollListener;
    private int maxPagesCount = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        restoreStateFromArgs();

        String token = new SharedPrefHelper().getSharedPref(getContext(), SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN);
        this.encodedToken = SecurityHelper.encodeToken(token);
        this.fb = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_FB_ID, "");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_myfeed, container, false);
        ButterKnife.bind(this, rootView);
        mPresenter = new MyFeedPresenter(this);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setScrollViewCallbacks(mPresenter);
        mRecyclerView.setAdapter(mMyFeedAdapter = new MyFeedAdapter(getContext(), mTrendingTags, mNewfeeds));
        mScrollListener = new EndlessRecyclerViewScrollListener(llm) {
            @Override
            public int getFooterViewType(int defaultNoFooterViewType) {
                return -1;
            }

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if(page <= maxPagesCount) mPresenter.loadNewfeeds(encodedToken, page);
                if(page == maxPagesCount) mMyFeedAdapter.setAllFeedsHasBeenLoaded();
            }
        };
        mMyFeedAdapter.restoreStateFromArgs(mFragmentState);
        mScrollListener.onRestoreInstanceState(mFragmentState);
        mRecyclerView.addOnScrollListener(mScrollListener);
        mMyFeedAdapter.setCallback(this);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mTrendingTags == null) mPresenter.loadTrendingTags(encodedToken);
        if (mNewfeeds == null) mPresenter.loadNewfeeds(encodedToken, 1);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveStateToArgs();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveStateToArgs();
    }

    /* MyFeedContractor.View */
    @Override
    public int getCurrentScrollY() {
        return mRecyclerView.getCurrentScrollY();
    }

    @Override
    public void showMessage(int messageId, boolean isLong) {
        Toast.makeText(getActivity(), String.valueOf(messageId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initTrendingTags(TagsResponse response) {
        mTrendingTags = response.getResults();
        mMyFeedAdapter.addTrendingTags(response.getResults());
    }

    @Override
    public void initNewfeeds(NewfeedResponse response) {
        maxPagesCount = response.getPages();
        if(mNewfeeds == null) mNewfeeds = new ArrayList<>();
        mNewfeeds.addAll(response.getResults());
        mMyFeedAdapter.addNewfeeds(response.getResults());
    }

    @Override
    public void initWithFacebookFriends(final FBFriends friends) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMyFeedAdapter.addFriends(friends);
                }
            });
        }
    }

    @Override
    public void refreshTag(Tag tag) {
        mMyFeedAdapter.refreshTag(tag);
    }

    /**
     * MyFeedAdapterActions
     **/
    @Override
    public void onSeeAllClicked() {
        Intent intent = new Intent(getActivity(), ToDoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onFollowUnfollowClicked(int position) {
        mPresenter.changeFollowState(encodedToken, mTrendingTags.get(position));
    }

    @Override
    public void onFeedShareClicked(int position) {
        Toast.makeText(getActivity(), "Feed share icon clicked at: " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMoreClicked(int position, View anchorView) {
        mPresenter.showMorePicker(mNewfeeds.get(position), anchorView, getContext());
    }

    @Override
    public void onVoteUpClicked(int position) {
        Toast.makeText(getContext(), "Vote Up clicked at: " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVoteDownClicked(int position) {
        Toast.makeText(getContext(), "Vote Down clicked at: " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowCommentsClicked(int position) {
        Toast.makeText(getContext(), "Show comments  clicked at: " + position, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getContext(), PostActivity.class);
        intent.putExtra(PostFragment.ARG_POST, (Parcelable) mNewfeeds.get(position));
        Log.i("post", mNewfeeds.get(position).toString());
        startActivity(intent);
    }

    @Override
    public void onInfluButtonClicked(int position) {
        Toast.makeText(getContext(), "Influ Button  clicked at: " + position, Toast.LENGTH_SHORT).show();
    }

    /* Private Methods */
    private boolean restoreStateFromArgs() {
        if (getArguments() != null && getArguments().containsKey(MY_FEED_FRAGMENT_STATE)) {
            mFragmentState = getArguments().getBundle(MY_FEED_FRAGMENT_STATE);
            if (mFragmentState != null) restoreOwnState();
        }
        return mFragmentState != null;
    }

    private void saveStateToArgs() {
        mFragmentState = saveOwnState();
        if (mFragmentState != null)
            getArguments().putBundle(MY_FEED_FRAGMENT_STATE, mFragmentState);
    }

    private void restoreOwnState() {
        if (mRecyclerView != null)
            mRecyclerView.getLayoutManager()
                    .onRestoreInstanceState(mFragmentState.getParcelable(RECYCLER_VIEW_STATE_KEY));
        mTrendingTags = mFragmentState.getParcelableArrayList(Tag.TAG_ARRAY_BUNDLE_KEY);
        mNewfeeds = mFragmentState.getParcelableArrayList(NewsfeedModel.NEWFEED_ARRAY_BUNDLE_KEY);
        if(mScrollListener != null) mScrollListener.onRestoreInstanceState(mFragmentState);
    }

    private Bundle saveOwnState() {
        Bundle state = new Bundle();
        if (mRecyclerView != null)
            state.putParcelable(RECYCLER_VIEW_STATE_KEY, mRecyclerView.getLayoutManager().onSaveInstanceState());
        state.putParcelableArrayList(Tag.TAG_ARRAY_BUNDLE_KEY, mTrendingTags);
        state.putParcelableArrayList(NewsfeedModel.NEWFEED_ARRAY_BUNDLE_KEY, mNewfeeds);
        mMyFeedAdapter.saveStateToArgs(state);
        if(mScrollListener != null) mScrollListener.onSaveInstanceState(state);
        return state;
    }
}
