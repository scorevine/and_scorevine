package com.scorevine.ui.home;

import android.support.v4.app.Fragment;

/**
 * Created by adrianjez on 19.12.2016.
 */

public interface HomeMainContract {

    interface View {
        void showBottomMenu();
        void hideBottomMenu();
        void setNestedPage(Fragment f, String tag);
        int getBottomMenuHeight();
        float getYTranslationOfBottomBar();
        void moveBottomMenu(float distance);
    }

    interface Presenter  {
        void subscribe(HomeMainActivity activity);
        void unsubscribe();
        void insertNestedPage(int position);
    }
}
