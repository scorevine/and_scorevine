package com.scorevine.ui.home.profile_details.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.InternetConnectionHelper;
import com.scorevine.model.UserMessage;
import com.thoughtbot.expandablerecyclerview.MultiTypeExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by adrianjez on 17.12.2016.
 */

public class ProfileDetailsAdapter extends MultiTypeExpandableRecyclerViewAdapter<GroupViewHolder, ChildViewHolder> {

    private static final int MYPROFILE_GROUP_VIEW_TYPE = 0;

    private static final int MYPROFILE_INBOX_VIEW_TYPE = 1;
    private static final int MYPROFILE_MY_VINE_VIEW_TYPE = 2;
    private static final int MYPROFILE_GROUPS_VIEW_TYPE = 3;
    private static final int MYPROFILE_MY_PROFILE_VIEW_TYPE = 4;

    private static final int MYPROFILE_INBOX_FOOTER_VIEW_TYPE = 6;

    private LayoutInflater mInflater;
    private Context mContext;
    private ProfileDetailsAdapterActions mCallback;
    private InboxViewHolder mInboxViewHolder;

    public ProfileDetailsAdapter(Context context, @NonNull List<ExpandableGroup> sections,
                                 ProfileDetailsAdapterActions callback) {
        super(sections);
        this.mCallback = callback;
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Recycler View Methods
     **/
    @Override
    public GroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_parent_configuration_dark, parent, false);
        return new ProfileDetailsAdapter.CategoryViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            default:
            case MYPROFILE_INBOX_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_profile_details_inbox, parent, false);
                return mInboxViewHolder = new InboxViewHolder(view);
            case MYPROFILE_INBOX_FOOTER_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_footer_profile_details_inbox, parent, false);
                return new InboxFooterViewHolder(view);
            case MYPROFILE_MY_VINE_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_profile_details_my_vine, parent, false);
                return new MyVineViewHolder(view);
            case MYPROFILE_GROUPS_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_profile_details_groups, parent, false);
                return new GroupsViewHolder(view);
            case MYPROFILE_MY_PROFILE_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_profile_details_my_profile, parent, false);
        }
        return new MyProfileViewHolder(view);

    }


    @Override
    public void onBindChildViewHolder(ChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        if (group instanceof ProfileDetailsInboxSection && group.getItems().size() > childIndex) {
            UserMessage m = (UserMessage) group.getItems().get(childIndex);
            mInboxViewHolder.mDisplayName.setText(m.getDisplay_name());
            mInboxViewHolder.mTextMessageContainer.setText(m.getMessage());
            mInboxViewHolder.mUserLogin.setText(m.getLogin());
            mInboxViewHolder.mTimeContainer.setText(DateFormatHelper.getDateWithFormat(DateFormatHelper.MESSAGE_DATE_FORMAT, m.getDate()));
            //TODO
        }
    }

    @Override
    public void onBindGroupViewHolder(GroupViewHolder holder, int flatPosition, ExpandableGroup group) {
        ((ProfileDetailsAdapter.CategoryViewHolder) holder).bind(group.getTitle());
    }

    @Override
    public boolean isChild(int viewType) {
        return viewType == MYPROFILE_INBOX_VIEW_TYPE
                || viewType == MYPROFILE_GROUPS_VIEW_TYPE
                || viewType == MYPROFILE_MY_VINE_VIEW_TYPE
                || viewType == MYPROFILE_MY_PROFILE_VIEW_TYPE
                || viewType == MYPROFILE_INBOX_FOOTER_VIEW_TYPE;

    }

    @Override
    public boolean isGroup(int viewType) {
        return viewType == MYPROFILE_GROUP_VIEW_TYPE;
    }

    @Override
    public int getGroupViewType(int position, ExpandableGroup group) {
        return MYPROFILE_GROUP_VIEW_TYPE;
    }

    @Override
    public int getChildViewType(int position, ExpandableGroup group, int childIndex) {
        if (group instanceof ProfileDetailsInboxSection) {
            //This group type has already overridden getItemCount Method - return one more for footer
            if (group.getItemCount() == childIndex + 1) return MYPROFILE_INBOX_FOOTER_VIEW_TYPE;
            return MYPROFILE_INBOX_VIEW_TYPE;
        }
        switch (position) {
            case 1:
                return MYPROFILE_MY_VINE_VIEW_TYPE;
            case 2:
                return MYPROFILE_GROUPS_VIEW_TYPE;
            case 3:
                return MYPROFILE_MY_PROFILE_VIEW_TYPE;
        }
        return super.getChildViewType(position, group, childIndex);
    }

    /**
     * Private Methods
     **/
    private int getFlattenedGroupPosition(int groupIndex) {
        int runningTotal = 0;
        for (int i = 0; i < groupIndex; i++) {
            runningTotal += numberOfVisibleItemsInGroup(i);
        }
        return runningTotal;
    }

    private int numberOfVisibleItemsInGroup(int group) {
        if (expandableList.expandedGroupIndexes.get(group)) {
            return expandableList.groups.get(group).getItemCount() + 1;
        } else {
            return 1;
        }
    }

    /**
     * Cell View Holders
     **/
    class CategoryViewHolder extends GroupViewHolder {

        @BindView(R.id.parentNameTV)
        TextView mParentNameTV;
        @BindView(R.id.rightArrow)
        ImageView mRightArrow;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (getLayoutPosition() == 0) {
                expand();
            }
        }

        @Override
        public void onClick(View v) {
            if (isGroupExpanded(getAdapterPosition())) return;
            super.onClick(v);
        }


        @Override
        public void expand() {
            animateArrow(0, 180);


            List<? extends ExpandableGroup> groups = getGroups();
            for (int i = 0; i < groups.size(); i++) {
                int flatPos = getFlattenedGroupPosition(i);
                if (i != getAdapterPosition() && isGroupExpanded(flatPos)) {
                    toggleGroup(flatPos);
                    notifyItemChanged(flatPos);
                }
            }
        }

        @Override
        public void collapse() {
            animateArrow(180, 0);
        }

        public void bind(String name) {
            mParentNameTV.setText(name);
        }

        private void animateArrow(int fromDegrees, int toDegrees) {
            RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setInterpolator(new LinearInterpolator());
            rotate.setFillAfter(true);
            mRightArrow.startAnimation(rotate);
        }
    }


    class InboxViewHolder extends ChildViewHolder {

        @BindView(R.id.time_container)
        TextView mTimeContainer;

        @BindView(R.id.user_avatar)
        ImageView mUserAvatar;

        @BindView(R.id.text_message_container)
        TextView mTextMessageContainer;

        @BindView(R.id.display_name)
        TextView mDisplayName;

        @BindView(R.id.user_login)
        TextView mUserLogin;

        public InboxViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class InboxFooterViewHolder extends ChildViewHolder {

        @OnClick(R.id.write_new)
        void onNewClicked(){
        if (mCallback != null) {
            if (InternetConnectionHelper.isConnected(mContext)) {
                mCallback.onWriteNewClicked();
            } else {
                mCallback.showNoInternetMessage();
            }
        }
        }

        @OnClick(R.id.see_all_messages)
        void onSeeAllMessagesClicked() {
            if (mCallback != null) {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onSeeAllMessagesClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        public InboxFooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class MyVineViewHolder extends ChildViewHolder {

        @OnClick(R.id.suggest_a_tag)
        void onSuggestTagClicked() {
            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onSuggestATagClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        @OnClick(R.id.improve_a_tag)
        void onImproveATagClicked() {
            if (mCallback != null) {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onImproveATagClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        @OnClick(R.id.contact_us)
        void onContactUsClicked() {
            if (mCallback != null) {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onContactUsClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        public MyVineViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class GroupsViewHolder extends ChildViewHolder {

        @OnClick(R.id.create_group)
        void onCreateGroupClicked() {

            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onCreateGroupClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        @OnClick(R.id.join_group)
        void onJoinGroupClicked() {

            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onJoinGroupClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        @OnClick(R.id.leave_group)
        void onLeaveGroupClicked() {
            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onLeaveGroupClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        public GroupsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class MyProfileViewHolder extends ChildViewHolder {

        @OnClick(R.id.my_profile_settings)
        void onMyProfileSettingsClicked() {
            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onMyProfileSettingsClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }

        }

        @OnClick(R.id.my_profile_following)
        void onMyProfileFollowingClicked() {
            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onMyProfileFollowingClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        @OnClick(R.id.my_profile_followers)
        void onMyProfileFollowersClicked() {
            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onMyProfileFollowersClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        @OnClick(R.id.my_profile_logout)
        void onMyProfileLogoutClicked(){
            if (mCallback != null)
            {
                if (InternetConnectionHelper.isConnected(mContext)) {
                    mCallback.onMyProfileLogoutClicked();
                } else {
                    mCallback.showNoInternetMessage();
                }
            }
        }

        public MyProfileViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
