package com.scorevine.ui.home.newsfeed;

import com.scorevine.ui.home.HomeMainActivity;

/**
 * Created by adrianjez on 16.12.2016.
 */

public class NewsfeedPresenter implements NewsfeedContractor.Presenter {

    private final NewsfeedContractor.View mView;

    public NewsfeedPresenter(NewsfeedContractor.View view){
        this.mView = view;
    }

    /** NewsfeedContractor.Presenter*/
    @Override
    public void subscribe(HomeMainActivity activity) {

    }

    @Override
    public void unsubscribe() {

    }

}
