package com.scorevine.ui.home.profile_details.adapter;

/**
 * Created by adrianjez on 18.12.2016.
 */

public interface ProfileDetailsAdapterActions {

    void onWriteNewClicked();
    void onSeeAllMessagesClicked();

    void onSuggestATagClicked();
    void onImproveATagClicked();
    void onContactUsClicked();

    void onCreateGroupClicked();
    void onJoinGroupClicked();
    void onLeaveGroupClicked();

    void onMyProfileSettingsClicked();
    void onMyProfileFollowingClicked();
    void onMyProfileFollowersClicked();
    void onMyProfileLogoutClicked();

    void showNoInternetMessage();

    }
