package com.scorevine.ui.home;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.scorevine.R;
import com.scorevine.helpers.ViewHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by adrianjez on 15.12.2016.
 */

public class HomeMainFragment extends Fragment implements HomeMainContract.View {

    public static final String TAG_FRAGMENT_HOME_MAIN = HomeMainFragment.class.getSimpleName();
    private static final String COMMON_NESTED_FRAGMENTS_STATE_KEY = "CommonNestedFragmentState";
    private static final String HOME_MAIN_FRAGMENT_STATE_KEY = "HomeMainFragmentState";
    private static final String WAS_ALREADY_ADDED_NESTED_CONTENT_STATE_KEY = "WasAlreadyAddedNestedContent";
    private static final String IS_USER_LOGGED_IN_BUNDLE_KEY = "IsLoggedInBundleKey";
    private static final int DEFAULT_SELECTED_BOTTOM_OPTION = 0;

    public static HomeMainFragment newInstance(boolean isCurrentlyLoggedIn){
        Bundle args = new Bundle();
        args.putBoolean(IS_USER_LOGGED_IN_BUNDLE_KEY, isCurrentlyLoggedIn);
        HomeMainFragment f = new HomeMainFragment();
        f.setArguments(args);
        return f;
    }

    @BindView(R.id.bottom_navigation)
    AHBottomNavigation mAhBottomNavigation;

    private boolean mIsUserLoggedIn;
    private HomeMainPresenter mPresenter;
    private Bundle mCommonNestedBundleArgs;
    private boolean mWasAddedContent;

    private Bundle mFragmentState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null)
            if(getArguments().containsKey(IS_USER_LOGGED_IN_BUNDLE_KEY))
                mIsUserLoggedIn = getArguments().getBoolean(IS_USER_LOGGED_IN_BUNDLE_KEY);
        if(!restoreStateFromArgs()) mCommonNestedBundleArgs = new Bundle();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_main, container, false);
        ButterKnife.bind(this, rootView);
        mPresenter = new HomeMainPresenter(this, mCommonNestedBundleArgs, mIsUserLoggedIn, getContext());

        //Init of bottom menu
        mAhBottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));
        mAhBottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
        mAhBottomNavigation.setOnTabSelectedListener(mPresenter);
        mAhBottomNavigation.setAccentColor(ContextCompat.getColor(getContext(), R.color.colorIconPrimary));
        mAhBottomNavigation.setInactiveColor(ContextCompat.getColor(getContext(), R.color.colorIconAccent));
        mAhBottomNavigation.setCurrentItem(DEFAULT_SELECTED_BOTTOM_OPTION);
        AHBottomNavigationAdapter navigationAdapter = new AHBottomNavigationAdapter(getActivity(), R.menu.newsfeed_bottom_menu);
        navigationAdapter.setupWithBottomNavigation(mAhBottomNavigation);
        /** initially will be first option **/
        if(!mWasAddedContent) {
            mPresenter.insertNestedPage(DEFAULT_SELECTED_BOTTOM_OPTION);
            mWasAddedContent = true;
        }
        return rootView;
    }

    public void onResume() {
        super.onResume();
        mPresenter.subscribe((HomeMainActivity) getActivity());
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveStateToArgs();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveStateToArgs();
    }

    /** HomeMainContract.View **/
    @Override
    public void showBottomMenu() {
        float headerTranslationY = ViewHelper.getTranslationY(mAhBottomNavigation);
        if (headerTranslationY != 0) {
            ViewPropertyAnimator.animate(mAhBottomNavigation).cancel();
            ViewPropertyAnimator.animate(mAhBottomNavigation).translationY(0).setDuration(200).start();
        }
    }

    @Override
    public void hideBottomMenu() {
        float headerTranslationY = ViewHelper.getTranslationY(mAhBottomNavigation);
        int toolbarHeight = mAhBottomNavigation.getHeight();
        if (headerTranslationY != -toolbarHeight) {
            ViewPropertyAnimator.animate(mAhBottomNavigation).cancel();
            ViewPropertyAnimator.animate(mAhBottomNavigation).translationY(toolbarHeight).setDuration(200).start();
        }
    }

    @Override
    public void setNestedPage(Fragment fragment, String tag){
        getChildFragmentManager().
                beginTransaction()
                .replace(R.id.nestedContainer, fragment, tag)
                .commit();
    }

    @Override
    public int getBottomMenuHeight() {
        return mAhBottomNavigation.getHeight();
    }

    @Override
    public float getYTranslationOfBottomBar() {
        return ViewHelper.getTranslationY(mAhBottomNavigation);
    }

    @Override
    public void moveBottomMenu(float distance) {
        ViewPropertyAnimator.animate(mAhBottomNavigation).cancel();
        ViewHelper.setTranslationY(mAhBottomNavigation, distance);
    }

    /** Private Methods **/
    private boolean restoreStateFromArgs(){
        if (getArguments() != null && getArguments().containsKey(HOME_MAIN_FRAGMENT_STATE_KEY)) {
            mFragmentState = getArguments().getBundle(HOME_MAIN_FRAGMENT_STATE_KEY);
            if(mFragmentState != null) restoreOwnState();
        }
        return mFragmentState != null;
    }

    private void saveStateToArgs(){
        mFragmentState = saveOwnState();
        if(mFragmentState != null)
            getArguments().putBundle(HOME_MAIN_FRAGMENT_STATE_KEY, mFragmentState);
    }

    private void restoreOwnState(){
        mCommonNestedBundleArgs = mFragmentState.getBundle(COMMON_NESTED_FRAGMENTS_STATE_KEY);
        mWasAddedContent= mFragmentState.getBoolean(WAS_ALREADY_ADDED_NESTED_CONTENT_STATE_KEY);
    }

    private Bundle saveOwnState(){
        Bundle state = new Bundle();
        state.putBundle(COMMON_NESTED_FRAGMENTS_STATE_KEY, mCommonNestedBundleArgs);
        state.putBoolean(WAS_ALREADY_ADDED_NESTED_CONTENT_STATE_KEY, true);
        return state;
    }

}
