package com.scorevine.ui.signing.login;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.CallbackManager;
import com.scorevine.R;
import com.scorevine.ui.dialogs.ResetPasswordMethodsDialogFragment;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.InternetConnectionHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.model.signing.SignUpInfoPart1;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.home.HomeMainActivity;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.resetpassword.step1.ResetPasswordStep1Fragment;
import com.scorevine.ui.signing.signup1.SignUpStep1Fragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by Agnieszka Duleba on 2016-12-02.
 */
public class LoginFragment extends Fragment implements LoginContract.View {

    @BindView(R.id.uniqueNameTI)
    TextInputLayout mUniqueNameTI;
    @BindView(R.id.passwordTI)
    TextInputLayout mPasswordTI;

    SignUpInfoPart1 mLoginInfo;
    public static final String TAG_FRAGMENT_LOGIN = LoginFragment.class.getSimpleName();
    public static final int UNIQUE_NAME = 1;
    public static final int PASSWORD = 2;

    LoginContract.Presenter mPresenter;

    CallbackManager callbackManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, null, false);
        ButterKnife.bind(this, view);
        mPresenter = new LoginPresenter(this);

        callbackManager = CallbackManager.Factory.create();
        setUniqueNameTextChangedListener();
        setPasswordTextChangedListener();

        Typeface typeface = mPasswordTI.getEditText().getTypeface();
        mPasswordTI.getEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mPasswordTI.getEditText().setTransformationMethod(PasswordTransformationMethod.getInstance());
        mPasswordTI.getEditText().setTypeface(typeface);

        // mUniqueNameTI.getEditText().setText("testnikak");
        //    mPasswordTI.getEditText().setText("00000000p");

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            mLoginInfo = (SignUpInfoPart1) savedInstanceState.getSerializable(TAG_FRAGMENT_LOGIN);
            if (mLoginInfo != null) {
                mUniqueNameTI.getEditText().setText(mLoginInfo.uniqueName);
                if (mLoginInfo.password != null && !mLoginInfo.password.isEmpty()) {
                    mPasswordTI.getEditText().setText(mLoginInfo.password);
                }
            }
        }
    }

    private void setPasswordTextChangedListener() {
        mPasswordTI.getEditText().addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() >= 8) {
                    mPresenter.validateInput(getString(R.string.regex_password), charSequence, R.string.error_characters_password, PASSWORD);
                } else {
                    showInputError(R.string.error_min_characters_count_8, PASSWORD);
                }

            }
        });
    }

    private void setUniqueNameTextChangedListener() {
        mUniqueNameTI.getEditText().addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() == 1 && i1 >= 1) {
                    mUniqueNameTI.getEditText().setText("");
                } else if (charSequence.length() == 1) {

                    Spannable text = getSpannable(String.format("@%s", charSequence), getContext());

                    mUniqueNameTI.getEditText().setText(text);
                    mUniqueNameTI.getEditText().setSelection(2);
                }
                if (charSequence.length() < 5) {
                    showInputError(R.string.error_min_characters_count_4, UNIQUE_NAME);
                }
                if (charSequence.length() == 16) {
                    showInputError(R.string.error_max_characters_count_15, UNIQUE_NAME);
                } else if (charSequence.length() > 4) {
                    mPresenter.validateInput(getString(R.string.regex_unique_name), charSequence.toString().substring(1), R.string.error_unique_name_symbols, UNIQUE_NAME);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (mUniqueNameTI.getEditText().getText().length() > 0 && mUniqueNameTI.getEditText().getText().toString().charAt(0) != '@') {
                    Spannable text = getSpannable(String.format("@%s", editable.toString()), getContext());
                    mUniqueNameTI.getEditText().setText(text);
                    mUniqueNameTI.getEditText().setSelection(mUniqueNameTI.getEditText().getText().length());

                }

            }

        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mLoginInfo = new SignUpInfoPart1();
        if (mUniqueNameTI != null) {
            String uniqueName = mUniqueNameTI.getEditText().getText().toString();
            mLoginInfo.setUniqueName(!uniqueName.isEmpty() ? uniqueName.substring(1) : "");
            if (!mPasswordTI.getEditText().getText().toString().isEmpty()) {
                mLoginInfo.setPassword(mPasswordTI.getEditText().getText().toString());
            }
            outState.putSerializable(TAG_FRAGMENT_LOGIN, mLoginInfo);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe((SignInActivity) getActivity());
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();

    }

    @OnClick(R.id.forgot_password)
    public void onForgotPasswordClick() {
        KeyboardHelper.hideKeyboard(getView(), getActivity());
        FragmentHelper.replaceFragment(getFragmentManager(), new ResetPasswordStep1Fragment(), ResetPasswordStep1Fragment.TAG_RESETPASSWORD);
    }

    @Override
    public void showInputError(int errorMessageId, int inputType) {
        TextInputLayout textInputLayout = getTextInputLayout(inputType);
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(wrapInCustomfont(getString(errorMessageId)));
        }

    }


    private Spannable wrapInCustomfont(String myText) {
        final Typeface typeface = TypefaceUtils.load(getActivity().getAssets(), "fonts/SanFranciscoText-Regular.ttf");
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(typeface);
        SpannableString spannable = new SpannableString(myText);
        spannable.setSpan(typefaceSpan, 0, myText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    @Override
    public void hideInputError(int inputType) {
        TextInputLayout textInputLayout = getTextInputLayout(inputType);
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(false);
        }
    }

    private TextInputLayout getTextInputLayout(int inputType) {
        TextInputLayout textInputLayout = null;
        switch (inputType) {
            case UNIQUE_NAME:
                textInputLayout = mUniqueNameTI;
                break;
            case PASSWORD:
                textInputLayout = mPasswordTI;
                break;
            default:
                break;
        }
        return textInputLayout;
    }

    @Override
    public void goNext() {

        if (!new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_SAVE_DATA, "false").equals("true"))
            new SharedPrefHelper().clearSharedPref(getActivity());
        else
            new SharedPrefHelper().addSharedPref(getActivity(), SharedPrefHelper.SP_SAVE_DATA, "false");

        FragmentHelper.replaceFragment(getFragmentManager(), new SignUpStep1Fragment(), SignUpStep1Fragment.TAG_FRAGMENT_REGISTER1);

    }

    public void putFacebookData(String id, String lastName, String firstName, String email) {
        SignUpStep1Fragment signUpStep1Fragment = new SignUpStep1Fragment();

        Bundle args = new Bundle();
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_ID, id);
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_LAST_NAME, lastName);
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_FIRST_NAME, firstName);
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_EMAIL, email);
        signUpStep1Fragment.setArguments(args);

        new SharedPrefHelper().clearSharedPref(getActivity());
        FragmentHelper.replaceFragment(getFragmentManager(), signUpStep1Fragment, SignUpStep1Fragment.TAG_FRAGMENT_REGISTER1);

    }

    @Override
    public void showResetPasswordMethodsDialog(String username, List<String> retrievalMethods) {
        ResetPasswordMethodsDialogFragment dialogFragment = null;
        if (retrievalMethods.size() == 2) {
            dialogFragment = ResetPasswordMethodsDialogFragment.newInstance(getString(R.string.choose_how_to_reset_password),
                    retrievalMethods.get(0), retrievalMethods.get(1), username);
            Log.i("reset", retrievalMethods.toString());
        } else if (retrievalMethods.size() == 1) {
            dialogFragment = ResetPasswordMethodsDialogFragment.newInstance(getString(R.string.choose_how_to_reset_password),
                    retrievalMethods.get(0), username);
            Log.i("reset", retrievalMethods.toString());
        }
        dialogFragment.show(getChildFragmentManager(), "reset password");
    }

    @Override
    public void showMessage(int messageId, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(getString(messageId).toString(), isLong);
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(message, isLong);
    }

    @Override
    public void goToMyFeed(String token) {
        new SharedPrefHelper().addSharedPref(getActivity(), SharedPrefHelper.SP_TOKEN, token);

        getActivity().finish();
        startActivity(new Intent(getActivity(), HomeMainActivity.class));

    }


    @Override
    public void setUpView(SignInActivity activity) {
        activity.setBottomButtonVisibility(true);
        SpannableString signupLabel = new SpannableString(getString(R.string.signin_signup_btn));
        signupLabel.setSpan(new CalligraphyTypefaceSpan(TypefaceUtils.load(getActivity().getAssets(), "fonts/SanFranciscoText-Regular.ttf")), 0, signupLabel.length() - 8, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        signupLabel.setSpan(new CalligraphyTypefaceSpan(TypefaceUtils.load(getActivity().getAssets(), "fonts/SanFranciscoText-Bold.ttf")), signupLabel.length() - 8, signupLabel.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        activity.setBottomButtonText(signupLabel);
        activity.setToolbarVisibility(false);

        setOnFocusedChangedListeners();

    }

    private void setOnFocusedChangedListeners() {
        TextInputOnFocusChangedListener focusChangedListener = new TextInputOnFocusChangedListener();
        mUniqueNameTI.getEditText().setOnFocusChangeListener(focusChangedListener);
        mPasswordTI.getEditText().setOnFocusChangeListener(focusChangedListener);
    }

    @OnClick(R.id.login_btn)
    public void onLoginButtonClick() {

        KeyboardHelper.hideKeyboard(getView(), getActivity());

        //todo check internet connection

        if (mUniqueNameTI.getEditText().getText().toString().isEmpty() || mPasswordTI.getEditText().getText().toString().isEmpty()) {

            showTextMessage(getString(R.string.error_invalid_fields), false);

        } else if (mUniqueNameTI.getError() == null && mPasswordTI.getError() == null) {

            String username = mUniqueNameTI.getEditText().getText().toString();
            mPresenter.sendLoginRequest(username.substring(1), mPasswordTI.getEditText().getText().toString());

        } else {

            showTextMessage(getString(R.string.error_invalid_fields), false);

        }
    }

    @OnClick(R.id.skip_login)
    public void onSkipLoginClick() {

        startActivity(new Intent(getActivity(), HomeMainActivity.class));
        getActivity().finish();

    }


    public class TextInputOnFocusChangedListener implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {
                ((SignInActivity) getActivity()).scrollBy(100);
            }
        }
    }

    @OnClick({R.id.facebook_login_btn, R.id.facebook_login_label})
    public void onFbLoginClick() {

        if (InternetConnectionHelper.isConnected(getActivity())) {
            mPresenter.registerCallback(callbackManager, this);
        } else {
            showMessage(R.string.connection_error, false);
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
