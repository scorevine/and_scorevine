package com.scorevine.ui.signing;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.prashantsolanki.secureprefmanager.SecurePrefManagerInit;
import com.scorevine.R;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.home.HomeMainActivity;
import com.scorevine.ui.my_vine.tag_suggest.SuggestTagFragment;
import com.scorevine.ui.signing.login.LoginFragment;
import com.scorevine.ui.signing.registerfailed.RegistrationFailedFragment;
import com.scorevine.ui.signing.signup2.SignUpStep2Fragment;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity {


    @BindView(R.id.bottom_button)
    TextView mBottomButtonTV;

    @BindView(R.id.toolbar)
    ConstraintLayout mToolbar;

    @BindView(R.id.signInScrollView)
    ScrollView mScrollView;

    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;

    private FragmentManager mFragmentManager;
    private Fragment mContent;
    private boolean isBottomButtonVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        new SecurePrefManagerInit.Initializer(getApplicationContext())
                .useEncryption(true)
                .initialize();

        if(new SharedPrefHelper().getSharedPref(this, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN).equals(SharedPrefHelper.SP_TOKEN)) {

            mFragmentManager = getSupportFragmentManager();

            Fragment firstFragment = mFragmentManager.findFragmentById(R.id.fragment_container);
            if (firstFragment == null) {
                firstFragment = new LoginFragment();
                FragmentHelper.addFirstFragment(mFragmentManager, firstFragment, LoginFragment.TAG_FRAGMENT_LOGIN);
                mContent = firstFragment;

            }
            if (savedInstanceState != null) {
                //Restore the fragment's instance
                mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
            } else {
                mContent = FragmentHelper.getVisibleFragment(mFragmentManager);
            }

            setScrollViewGlobalLayoutListener();
        }else {
            startActivity(new Intent(this, HomeMainActivity.class));
            finish();

        }
    }

    private void setScrollViewGlobalLayoutListener() {
        mScrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                mScrollView.getWindowVisibleDisplayFrame(r);
                int screenHeight = mScrollView.getRootView().getRootView().getRootView().getHeight();
                int keypadHeight = screenHeight - r.bottom;

                Log.d("keyboard", "keypadHeight = " + keypadHeight);

                if (isBottomButtonVisible) {
                    if (keypadHeight > screenHeight * 0.15) {
                        // keyboard is opened
                        if (mBottomButtonTV.getVisibility() == View.VISIBLE) {
                            mBottomButtonTV.setVisibility(View.GONE);
                        }

                    } else {
                        mBottomButtonTV.setVisibility(View.VISIBLE);

                    }
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Save the fragment's instance
        if (mContent != null)
            mFragmentManager.putFragment(outState, "mContent", mContent);
    }

    public void scrollBy(int scrollBy) {
        mScrollView.scrollBy(0, scrollBy);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void setBottomButtonText(String buttonLabel) {
        mBottomButtonTV.setText(buttonLabel);
    }

    public void setBottomButtonText(SpannableString buttonLabel) {
        mBottomButtonTV.setText(buttonLabel);
    }


    public void setBottomButtonVisibility(boolean visible) {
        isBottomButtonVisible = visible;
        if (visible) {
            mBottomButtonTV.setVisibility(View.VISIBLE);
        } else {
            mBottomButtonTV.setVisibility(View.GONE);
        }
    }

    public void setToolbarVisibility(boolean visible) {
        if (visible) {
            mToolbar.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @OnClick(R.id.bottom_button)
    public void onBottomButtonClick() {
        EventBus.getDefault().post(new NextButtonEvent());
    }

    @OnClick(R.id.backArrowButton)
    public void onBackArrowClick() {
        KeyboardHelper.hideKeyboard(mScrollView, this);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {

        if (FragmentHelper.getVisibleFragment(getSupportFragmentManager()) instanceof RegistrationFailedFragment
                || FragmentHelper.getVisibleFragment(getSupportFragmentManager()) instanceof LoadingFragment) {
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FragmentHelper.getVisibleFragment(getSupportFragmentManager()) instanceof LoginFragment)
            FragmentHelper.getVisibleFragment(getSupportFragmentManager()).onActivityResult(requestCode, resultCode, data);

        if (FragmentHelper.getVisibleFragment(getSupportFragmentManager()) instanceof SignUpStep2Fragment)
            FragmentHelper.getVisibleFragment(getSupportFragmentManager()).onActivityResult(requestCode, resultCode, data);
    }


}
