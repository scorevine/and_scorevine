package com.scorevine.ui.signing.signup2;

import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.signing.SigningApiInterface;
import com.scorevine.api.signing.VerificationRequest;
import com.scorevine.api.signing.VerificationResponse;
import com.scorevine.helpers.ErrorUtils;
import com.scorevine.ui.signing.signup1.SignUpStep1Fragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public class SignUpStep2Interactor implements SignUpStep2Contract.Interactor{


    //TODO: remove and use SignUpStep1Interactor!
    @Override
    public void sendVerification(VerificationRequest verificationRequest, final OnFinishedListener listener) {

        SigningApiInterface signingApiInterface = new ApiManager().getRetrofit().create(SigningApiInterface.class);

        signingApiInterface.sendVerification(verificationRequest).enqueue(new Callback<VerificationResponse>() {
            @Override
            public void onResponse(Call<VerificationResponse> call, Response<VerificationResponse> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        Log.e(SignUpStep2Fragment.TAG_FRAGMENT_REGISTER2, "onGetResponse: " + response.body().toString());
                        listener.onResponse(response.body().toString());
                        break;
                    case 422:
                    case 406:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<VerificationResponse> call, Throwable t) {
                if (t.getMessage() != null) {
                    listener.onFailure(R.string.connection_error);
                    Log.e(SignUpStep1Fragment.TAG_FRAGMENT_REGISTER1, t.getMessage());
                }
            }
        });
    }

}
