package com.scorevine.ui.signing.configuration;

import android.util.Log;

import com.scorevine.api.ApiManager;
import com.scorevine.api.categories.CategoriesApi;
import com.scorevine.api.signing.FollowCategoriesResponse;
import com.scorevine.ui.signing.configuration.adapters.CategoriesResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by weronikapapkouskaya on 12.12.2016.
 */

public class ConfigurationInteractor implements ConfigurationContract.Interactor {

    private CategoriesApi signingApiInterface = new ApiManager().getRetrofit().create(CategoriesApi.class);


    @Override
    public void getCategories(String encodedToken, final OnFinishedListener listener) {
        signingApiInterface.getCategories(encodedToken).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                if (response != null && response.body() != null && response.body().getCategories() != null) {
                    Log.i("categories", response.body().getCategories().toString());
                    listener.onGetCategoriesResponse(response.body().getCategories());
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                listener.onGetCategoriesFailure(0);
            }
        });

    }

    @Override
    public void followCategories(String token, String subcategoriesIdList, final OnFinishedListener listener) {

        signingApiInterface.followCategories(token, subcategoriesIdList).enqueue(new Callback<FollowCategoriesResponse>() {
            @Override
            public void onResponse(Call<FollowCategoriesResponse> call, Response<FollowCategoriesResponse> response) {
                Log.i("subcategories", String.valueOf(response.code()));
                Log.i("subcategories", String.valueOf(response.code()));
                Log.i("subcategories", response.body().toString());
                listener.onFollowCategoriesResponse();
            }

            @Override
            public void onFailure(Call<FollowCategoriesResponse> call, Throwable t) {
                Log.i("subcategories", t.getMessage());

                listener.onFollowCategoriesFailure();

            }
        });

    }

}
