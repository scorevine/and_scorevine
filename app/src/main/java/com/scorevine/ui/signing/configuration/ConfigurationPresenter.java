package com.scorevine.ui.signing.configuration;

import android.content.Context;
import android.util.Log;

import com.scorevine.api.APIErrorList;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.signing.configuration.adapters.Category;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by weronikapapkouskaya on 12.12.2016.
 */

public class ConfigurationPresenter implements ConfigurationContract.Presenter, ConfigurationContract.Interactor.OnFinishedListener {


    private final ConfigurationContract.View mView;
    private final ConfigurationContract.Interactor mInteractor;

    public ConfigurationPresenter(ConfigurationContract.View mView) {
        this.mView = mView;
        mInteractor = new ConfigurationInteractor();
    }


    @Override
    public void getCategories(Context context) {

        String encodedToken = getEncodedToken(context);

        mInteractor.getCategories(encodedToken, this);

    }

    private String getEncodedToken(Context context) {
        String token = new SharedPrefHelper().getSharedPref(context, SharedPrefHelper.SP_TOKEN, SharedPrefHelper.SP_TOKEN);
        Log.i("token", token);
        return SecurityHelper.encodeToken(token);
    }

    @Override
    public void followCategories(List<Integer> categoriesIdList, Context context) {

        if (categoriesIdList.size() > 0) {
            JSONArray jsonArray = new JSONArray(categoriesIdList);
            mInteractor.followCategories(getEncodedToken(context), jsonArray.toString(), this);
        }

    }

    @Override
    public void onGetCategoriesResponse(List<Category> categories) {
        if (categories != null) {
            mView.setCategories(categories);
            Log.i("categories", "onGetCategoriesResponse OK");
        } else {
            Log.i("categories", "onGetCategoriesResponse NOT OK");
        }

    }

    @Override
    public void onGetCategoriesFailure(int responseId) {
        Log.i("categories", "failure");

    }

    @Override
    public void onFollowCategoriesResponse() {
        mView.goNext();
        //todo go to after login activity

    }

    @Override
    public void onFollowCategoriesFailure() {

    }

    @Override
    public void onError(APIErrorList errorsList) {

    }
}
