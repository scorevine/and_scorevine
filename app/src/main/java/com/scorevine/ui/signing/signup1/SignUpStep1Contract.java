package com.scorevine.ui.signing.signup1;

import android.support.v4.app.FragmentActivity;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.VerificationRequest;
import com.scorevine.api.signing.VerificationResponse;
import com.scorevine.model.CountryCodes;
import com.scorevine.ui.signing.BaseViewSigning;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.validation.InputTypeSV;

import java.util.List;


/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public interface SignUpStep1Contract {

    interface View extends BaseViewSigning {

        void showMessage(int messageId, boolean isLong);

        void showTextMessage(String message, boolean isLong);

        void setTextWatchers();

        void setCodeSpinner(List<CountryCodes.CountryCode> countryCodesList);

        void sendVerification();

        void goNext();

        void showSavedData();

        void saveToSharePref();

        boolean checkNotNull();

        void showInputError(int errorMessageId, InputTypeSV inputType);

        void hideInputError(InputTypeSV inputType);

        void setNewUsername(String suggestedUsername);

        void suggestUniqueName();
    }

    interface Presenter {
        void sendVerificationRequest(String username, String email, String code, String mobile);

        void subscribe(SignInActivity activity);

        boolean savedToSP(FragmentActivity activity);

        void unsubscribe();

        boolean validateInput(String patternStr, CharSequence editable, int errorTextId, InputTypeSV inputType);

        void initSPData();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onResponse(VerificationResponse verificationResponse);

            void onResponseCodeSend(VerificationResponse verificationResponse);

            void onFailure(int responseId);

            void onError(APIErrorList errorsList);

            void onError(String error);
        }

        void sendVerification(VerificationRequest verificationRequest, final SignUpStep1Contract.Interactor.OnFinishedListener listener);

    }
}
