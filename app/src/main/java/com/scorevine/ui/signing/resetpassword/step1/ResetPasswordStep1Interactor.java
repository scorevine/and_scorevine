package com.scorevine.ui.signing.resetpassword.step1;

import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.signing.RetreivalMethods;
import com.scorevine.api.signing.SigningApiInterface;
import com.scorevine.helpers.ErrorUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public class ResetPasswordStep1Interactor implements ResetPasswordStep1Contract.Interactor {

    @Override
    public void getResetPasswordMethods(String username, final ResetPasswordStep1Contract.Interactor.OnFinishedListener listener) {

        SigningApiInterface signingApiInterface = new ApiManager().getRetrofit().create(SigningApiInterface.class);

        signingApiInterface.getResetPasswordMethods(username).enqueue(new Callback<RetreivalMethods>() {
            @Override
            public void onResponse(Call<RetreivalMethods> call, Response<RetreivalMethods> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        Log.e(ResetPasswordStep1Fragment.TAG_RESETPASSWORD, "onGetResponse: " + response.body().toString());
                        listener.onResponse(response.body());
                        break;
                    case 422:
                    case 404:
                    case 403:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<RetreivalMethods> call, Throwable t) {
                if (t.getMessage() != null) {
                    listener.onFailure(R.string.connection_error);

                    Log.e(ResetPasswordStep1Fragment.TAG_RESETPASSWORD, t.getMessage());
                }
            }
        });
    }

}
