package com.scorevine.ui.signing.signup3;

import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.signing.LoginResponse;
import com.scorevine.api.signing.SigningApiInterface;
import com.scorevine.helpers.ErrorUtils;
import com.scorevine.ui.signing.signup1.SignUpStep1Fragment;
import com.scorevine.ui.signing.signup2.SignUpStep2Fragment;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public class SignUpStep3Interactor implements SignUpStep3Contract.Interactor{

    @Override
    public void sendRegister(RequestBody email, RequestBody mobile, RequestBody username, RequestBody password, RequestBody facebookToken,
                             RequestBody birthDate, RequestBody gender, RequestBody country, RequestBody firstName,
                             RequestBody surname, RequestBody referal, MultipartBody.Part profile_image, RequestBody code,
                             RequestBody deviceUID, RequestBody push_token, final OnFinishedListener listener) {



        SigningApiInterface signingApiInterface = new ApiManager().getRetrofit().create(SigningApiInterface.class);

        signingApiInterface.register(email, mobile, username, password, facebookToken, birthDate,
                gender, country, firstName, surname, referal, profile_image, code, deviceUID, push_token)
                .enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        Log.e(SignUpStep2Fragment.TAG_FRAGMENT_REGISTER2, "onGetResponse: " +  response.body().toString());
                        listener.onResponse(response.body());
                        break;
                    case 422:
                    case 406:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (t.getMessage() != null) {
                    listener.onFailure(R.string.connection_error);
                    Log.e(SignUpStep1Fragment.TAG_FRAGMENT_REGISTER1, t.getMessage());
                }
            }
        });
    }

}
