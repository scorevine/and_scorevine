package com.scorevine.ui.signing.signup3;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.CheckedTextView;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.signing.LoadingFragment;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.configuration.ConfigurationActivity;
import com.scorevine.ui.signing.registerfailed.RegistrationFailedFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Agnieszka Duleba on 2016-11-29.
 */

public class SignUpStep3Fragment extends Fragment implements SignUpStep3Contract.View {

    public static final String TAG_FRAGMENT_REGISTER3 = SignUpStep3Fragment.class.getSimpleName();
    public static final String TAG_FRAGMENT_REGISTER3_BUNDLE = "ToS accepted";

    @BindView(R.id.termsAgreementWebView)
    WebView mAgreementWebView;

    @BindView(R.id.acceptCheckedTextView)
    CheckedTextView mAccepptButton;
    boolean isChecked;

    SignUpStep3Contract.Presenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register_s3, null, false);
        ButterKnife.bind(this, view);
        mPresenter = new SignUpStep3Presenter(this, this.getActivity());

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            isChecked = savedInstanceState.getBoolean(TAG_FRAGMENT_REGISTER3_BUNDLE);
            mAccepptButton.setChecked(isChecked);
            //Restore the fragment's state here
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(TAG_FRAGMENT_REGISTER3_BUNDLE, isChecked);
        //Save the fragment's state here
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe((SignInActivity) getActivity());
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();

    }

    public void setUpWebview() {

        mAgreementWebView.setBackgroundColor(Color.argb(1, 0, 0, 0));
        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: SanFrancisco;src: url(\"file:///android_asset/fonts/SanFranciscoText-Regular.ttf\")}body {background-color: #32383F; color:white; font-family: SanFrancisco;font-size: small;text-align: justify;}</style></head><body>";
        String pas = "</body></html>";
        String myHtmlString = pish + getString(R.string.registration_privacy_agreement) + pas;
        mAgreementWebView.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);
    }


    @OnClick(R.id.acceptCheckedTextView)
    public void onAcceptButtonClick() {
        isChecked = !mAccepptButton.isChecked();
        mAccepptButton.setChecked(isChecked);
    }

    @Override
    public boolean isBtnChecked() {
        return mAccepptButton.isChecked();
    }


    @Override
    public void goToLoading() {

        FragmentHelper.replaceFragment(getFragmentManager(), new LoadingFragment(), LoadingFragment.TAG_FRAGMENT_LOADING);
    }

    @Override
    public void goToRegisterFailed() {
        // FragmentHelper.clearBackStack(getFragmentManager());

        FragmentHelper.replaceFragment(getFragmentManager(), new RegistrationFailedFragment(), RegistrationFailedFragment.TAG_FRAGMENT_REGISTER_FAILED);
    }

    @Override
    public void goNext(String token) {

        getActivity().finish();
        Intent intent = new Intent(this.getActivity(), ConfigurationActivity.class);
        startActivity(intent);
    }

    @Override
    public void setUpView(SignInActivity activity) {

        activity.setBottomButtonVisibility(true);
        activity.setBottomButtonText(getString(R.string.sign_up_confirm));
        activity.setToolbarVisibility(true);
    }

    @Override
    public void showMessage(int messageId, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(message, isLong);
    }

    public void goBack() {
        getActivity().getFragmentManager().popBackStack();
    }

}