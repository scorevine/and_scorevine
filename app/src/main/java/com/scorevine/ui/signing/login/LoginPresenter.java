package com.scorevine.ui.signing.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.scorevine.api.APIErrorList;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.FacebookManager;
import com.scorevine.helpers.SecurityHelper;
import com.scorevine.ui.signing.SignInActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class LoginPresenter implements LoginContract.Presenter, LoginContract.Interactor.OnFinishedListener, LoginContract.Interactor.OnFinishedFbListener {

    private final LoginContract.View mView;
    private final LoginContract.Interactor mInteractor;

    private final FacebookManager mFacebookManager;

    private String mFacebookEmail;
    private String mFacebookId;
    private String mFacebookFirstName;
    private String mFacebookLastName;
    private String mUsername = "";

    @Override
    public void registerCallback(CallbackManager callbackManager, LoginFragment fragment) {
        LoginManager.getInstance().registerCallback(callbackManager, mFacebookManager);
        LoginManager.getInstance().logInWithReadPermissions(fragment, Arrays.asList("public_profile", "email", "user_friends"));
    }

    public LoginPresenter(@NonNull LoginContract.View view) {
        mView = view;
        mInteractor = new LoginInteractor();
        mFacebookManager = new FacebookManager();
        mFacebookManager.registerCallback(mFacebookLoginCallback);
    }

    @Override
    public void sendLoginRequest(String username, String password) {
        mUsername = username;
        mInteractor.login(username, password, this);

    }

    @Override
    public void subscribe(SignInActivity activity) {
        mView.setUpView(activity);
        EventBus.getDefault().register(this);

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }

    @Override
    public boolean validateInput(String patternStr, CharSequence editable, int errorTextId, int inputType) {
        Pattern pattern = Pattern.compile(patternStr);
        Log.i("validateInput", MessageFormat.format("{0} {1} {2}", pattern.matcher(editable).matches(), patternStr, editable));

        if (!pattern.matcher(editable.toString()).matches()) {
            mView.showInputError(errorTextId, inputType);

        } else {
            mView.hideInputError(inputType);
        }

        return pattern.matcher(editable).matches();

    }

    @Subscribe
    public void onEvent(NextButtonEvent event) {
        mView.goNext();
    }

    @Override
    public void onResponse(String token) {
        String encodeToken = SecurityHelper.encodeToken(token);

        mView.goToMyFeed(token);
    }

    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, true);
    }


    @Override
    public void onError(APIErrorList error) {
        if (error.getError().get(0) != null) {
            String message = error.getError().get(0).getMessage();

            if (error.getRetreivalMethods() != null) {
                mView.showResetPasswordMethodsDialog(mUsername, error.getRetreivalMethods());
                return;
            } else if (error.getFailLoginMessage() != null) {
                message = error.getFailLoginMessage();
            }
            mView.showTextMessage(message, true);
        }

    }

    private void checkFbUser(String token) {
        mInteractor.fbLogin(token, this);
    }

    @Override
    public void onResponseFb(String token) {
        mView.goToMyFeed(token);
    }

    @Override
    public void onFailureFb(int responseId) {
        mView.showMessage(responseId, true);
    }

    @Override
    public void onErrorFb(APIErrorList error) {
        if (error.getError().get(0) != null)
            mView.showTextMessage(error.getError().get(0).getMessage(), true);

    }

    @Override
    public void onUserNotFound() {
        mView.putFacebookData(mFacebookId, mFacebookLastName, mFacebookFirstName, mFacebookEmail);
    }

    private FacebookManager.FacebookLoginCallback mFacebookLoginCallback = new FacebookManager.FacebookLoginCallback() {
        @Override
        public void onFacebookSuccess(String token) {
            checkFbUser(token);
        }

        @Override
        public void onFacebookCancel() {

        }

        @Override
        public void onFacebookFailure() {
        }

        @Override
        public void onFacebookData(String email, String firstName, String lastName, String id) {
            mFacebookEmail = email;
            mFacebookFirstName = firstName;
            mFacebookLastName = lastName;
            mFacebookId = id;
        }
    };
}
