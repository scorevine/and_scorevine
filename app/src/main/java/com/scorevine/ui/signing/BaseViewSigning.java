package com.scorevine.ui.signing;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */
public interface BaseViewSigning {

    void setUpView(SignInActivity activity);

}
