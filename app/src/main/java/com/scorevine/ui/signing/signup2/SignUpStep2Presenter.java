package com.scorevine.ui.signing.signup2;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.VerificationRequest;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.LoadFileHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.model.CountryCodes;
import com.scorevine.ui.signing.SignInActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class SignUpStep2Presenter implements SignUpStep2Contract.Presenter, SignUpStep2Contract.Interactor.OnFinishedListener{

    private final SignUpStep2Contract.View mView;
    private final SignUpStep2Contract.Interactor mInteractor;

    public SignUpStep2Presenter(@NonNull SignUpStep2Contract.View view) {
        mView = view;
        mInteractor = new SignUpStep2Interactor();

    }

    @Override
    public void subscribe(SignInActivity activity) {
        mView.setUpView(activity);
        mView.setCountrySpinner(LoadFileHelper.getCountryCodesList(activity));
        mView.setGenderSpinner();

        mView.setTextWatchers();

        EventBus.getDefault().register(this);

    }
    @Override
    public boolean savedToSP(FragmentActivity activity){
        return new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SAVE_DATA_P2, "false").equals("true")
                || new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SAVE_DATA, "false").equals("true");
    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void resendSmsCode(String username, String email, String code, String mobile){
        String phoneNumber = code + "-" + mobile;
        VerificationRequest verificationRequest = new VerificationRequest(username, email, phoneNumber, true);

        mInteractor.sendVerification(verificationRequest, this);

    }
    public void showToast() {
        mView.showTextMessage("dialog message", true);
    }

    @Override
    public void initSPData() {
        mView.showSavedData();
    }


    @Subscribe
    public void onEvent(NextButtonEvent event){
        if(mView.checkNotNull()) {
            //   mView.sendVerification();
            mView.goNext();
        }
        else
            mView.showMessage(R.string.error_invalid_fields, true);
    }

    @Override
    public void onResponse(String response) {
        if (response.contains("true")) {
            mView.showMessage(R.string.toast_new_sms_code_sent, true);
            Log.i(SignUpStep2Fragment.TAG_FRAGMENT_REGISTER2, "smsCode: " + response);
        }
    }

    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onError(APIErrorList errorsList) {

        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

    }
    @Override
    public void onError(String error) {
        mView.showTextMessage(error, true);
    }

}
