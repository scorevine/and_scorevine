package com.scorevine.ui.signing.resetpassword.step1;

import android.content.Context;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.RetreivalMethods;
import com.scorevine.ui.signing.BaseViewSigning;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.validation.InputTypeSV;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public interface ResetPasswordStep1Contract {

    interface View extends BaseViewSigning {

        void showInputError(int errorMessageId);

        void hideInputError();

        void showMessage(int messageId, boolean isLong);

        void setListeners();

        void goToStep3(int resetType);

        void goToStep2();

        void showTextMessage(String message, boolean isLong);

    }

    interface Presenter {
        void validateInput(String username, InputTypeSV inputType);

        void sendResetPasswordRequest(String email, Context context);

        void subscribe(SignInActivity activity);

        void unsubscribe();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onResponse(RetreivalMethods response);

            void onFailure(int responseId);

            void onError(APIErrorList errorsList);
        }

        void getResetPasswordMethods(String username, final OnFinishedListener listener);
    }
}
