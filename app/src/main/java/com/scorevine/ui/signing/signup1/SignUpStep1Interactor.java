package com.scorevine.ui.signing.signup1;

import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.signing.SigningApiInterface;
import com.scorevine.api.signing.VerificationRequest;
import com.scorevine.api.signing.VerificationResponse;
import com.scorevine.helpers.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public class SignUpStep1Interactor implements SignUpStep1Contract.Interactor {

    @Override
    public void sendVerification(VerificationRequest verificationRequest, final SignUpStep1Contract.Interactor.OnFinishedListener listener) {

        SigningApiInterface signingApiInterface = new ApiManager().getRetrofit().create(SigningApiInterface.class);

        signingApiInterface.sendVerification(verificationRequest).enqueue(new Callback<VerificationResponse>() {
            @Override
            public void onResponse(Call<VerificationResponse> call, Response<VerificationResponse> response) {
                if(response==null)
                    return;
                switch (response.code()) {
                    case 200:
                        listener.onResponse(response.body());
                        break;
                    case 201:
                        listener.onResponseCodeSend(response.body());
                        break;
                    case 422:
                    case 406:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                    case 500:
                        listener.onFailure(R.string.connection_error);
                        break;
                }
            }

            @Override
            public void onFailure(Call<VerificationResponse> call, Throwable t) {
                if (t.getMessage() != null) {
                    listener.onFailure(R.string.connection_error);
                    Log.e(SignUpStep1Fragment.TAG_FRAGMENT_REGISTER1, t.getMessage());
                }
            }
        });
    }

}
