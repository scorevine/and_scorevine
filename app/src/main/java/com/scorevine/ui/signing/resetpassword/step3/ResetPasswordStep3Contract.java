package com.scorevine.ui.signing.resetpassword.step3;

import android.content.Context;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.ResetPasswordGetCodeRequest;
import com.scorevine.api.signing.ResetPasswordSendCodeRequest;
import com.scorevine.ui.validation.ValidateInputPresenterInterface;
import com.scorevine.ui.validation.ValidateInputView;

/**
 * Created by weronikapapkouskaya on 21.12.2016.
 */

public class ResetPasswordStep3Contract {
    public interface View extends ValidateInputView {
        void showToast(int messageId);

        void showToast(String message);

        void backToLogin();

    }

    public interface Presenter extends ValidateInputPresenterInterface {

        void getCode(String username, String method, Context context);

        void sendCode(String username, String code, String password, Context context);

    }

    public interface Interactor {

        interface OnFinishedListener {
            void onError(APIErrorList error);

            void onResponseGetResetCode();

            void onFailureGetResetCode();

            void onResponseResetPassword();

            void onFailureResetPassword();
        }

        void getResetCode(ResetPasswordGetCodeRequest request, OnFinishedListener listener);

        void resetPassword(ResetPasswordSendCodeRequest request, OnFinishedListener listener);

        void unlockGetCode(ResetPasswordGetCodeRequest request, OnFinishedListener listener);

        void unlockSendCode(ResetPasswordSendCodeRequest request, OnFinishedListener listener);

    }
}
