package com.scorevine.ui.signing.registerfailed;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import com.scorevine.ui.signing.BaseViewSigning;
import com.scorevine.ui.signing.SignInActivity;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public interface RegistrationFailedContract {

    interface View extends BaseViewSigning {

        void showMessage(int messageId, boolean isLong);

        void showTextMessage(String message, boolean isLong);
        void goToLogin();
        void goBack();
        void goToLoading();
        void goToRegisterFailed();
        void goNext();
    }
    interface Presenter  {
        void saveData(FragmentActivity fragmentActivity);
        void clearData(FragmentActivity fragmentActivity);
        void subscribe(SignInActivity activity);
        void unsubscribe();

        void registerAgain(Activity activity);
    }
}
