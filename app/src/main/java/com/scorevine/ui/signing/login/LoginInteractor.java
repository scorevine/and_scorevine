package com.scorevine.ui.signing.login;

import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.ApiManager;
import com.scorevine.api.signing.FbLoginResponse;
import com.scorevine.api.signing.LoginResponse;
import com.scorevine.api.signing.SigningApiInterface;
import com.scorevine.helpers.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public class LoginInteractor implements LoginContract.Interactor {
    SigningApiInterface signingApiInterface = new ApiManager().getRetrofit().create(SigningApiInterface.class);

    @Override
    public void fbLogin(String token, final OnFinishedFbListener listener) {

        Log.i(LoginFragment.TAG_FRAGMENT_LOGIN, "fb token: " + token);
        signingApiInterface.fbLogin(token, "", "").enqueue(new Callback<FbLoginResponse>() {
            @Override
            public void onResponse(Call<FbLoginResponse> call, Response<FbLoginResponse> response) {
                switch (response.code()) {
                    case 200:
                        listener.onResponseFb(response.body().getToken());
                        break;
                    case 404:
                        listener.onUserNotFound();
                        break;
                    case 401:
                    case 403:
                    case 422:
                        listener.onErrorFb(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<FbLoginResponse> call, Throwable t) {
                listener.onFailureFb(R.string.connection_error);

            }
        });

    }

    @Override
    public void login(String username, String password, final OnFinishedListener listener) {

        signingApiInterface.login(username, password).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                switch (response.code()) {
                    case 200:
                        listener.onResponse(response.body().getToken());
                        break;
                    case 422:
                    case 401:
                    case 403:
                    case 406:
                        listener.onError(ErrorUtils.parseError(response));
                        break;
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (t.getMessage() != null) {
                    listener.onFailure(R.string.connection_error);
                }
            }
        });

    }


}
