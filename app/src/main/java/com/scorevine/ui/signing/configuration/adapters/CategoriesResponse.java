package com.scorevine.ui.signing.configuration.adapters;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by weronikapapkouskaya on 12.12.2016.
 */

public class CategoriesResponse {

    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories;

    /**
     * @return The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * @param categories The categories
     */
    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }


}
