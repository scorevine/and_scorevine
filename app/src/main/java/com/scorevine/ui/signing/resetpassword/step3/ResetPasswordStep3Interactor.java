package com.scorevine.ui.signing.resetpassword.step3;

import com.scorevine.api.ApiManager;
import com.scorevine.api.signing.ResetPasswordGetCodeRequest;
import com.scorevine.api.signing.ResetPasswordResponse;
import com.scorevine.api.signing.ResetPasswordSendCodeRequest;
import com.scorevine.api.signing.SigningApiInterface;
import com.scorevine.helpers.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by weronikapapkouskaya on 21.12.2016.
 */

public class ResetPasswordStep3Interactor implements ResetPasswordStep3Contract.Interactor {

    SigningApiInterface signingApiInterface = new ApiManager().getRetrofit().create(SigningApiInterface.class);

    @Override
    public void getResetCode(final ResetPasswordGetCodeRequest request, final OnFinishedListener listener) {

        signingApiInterface.resetPasswordGetCode(request).enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        listener.onResponseGetResetCode();
                        break;
                    default:
                        listener.onError(ErrorUtils.parseError(response));
                        break;

                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                listener.onFailureGetResetCode();
            }


        });
    }

    @Override
    public void resetPassword(ResetPasswordSendCodeRequest request,
                              final OnFinishedListener listener) {
        signingApiInterface.resetPasswordSendCode(request).enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {

                switch (response.code()) {
                    case 200:
                    case 201:
                        listener.onResponseResetPassword();
                        break;
                    default:
                        listener.onError(ErrorUtils.parseError(response));
                        break;

                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {

                listener.onFailureResetPassword();
            }
        });

    }

    @Override
    public void unlockGetCode(ResetPasswordGetCodeRequest request,
                              final OnFinishedListener listener) {
        signingApiInterface.unlockGetCode(request).enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                switch (response.code()) {
                    case 200:
                    case 201:
                        listener.onResponseGetResetCode();
                        break;
                    default:
                        listener.onError(ErrorUtils.parseError(response));
                }

            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                listener.onFailureGetResetCode();

            }
        });
    }

    @Override
    public void unlockSendCode(ResetPasswordSendCodeRequest request,
                               final OnFinishedListener listener) {
        signingApiInterface.unlockSendCode(request).enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {

                switch (response.code()) {
                    case 200:
                    case 201:
                        listener.onResponseResetPassword();
                        break;
                    default:
                        listener.onError(ErrorUtils.parseError(response));
                        break;

                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                listener.onFailureGetResetCode();

            }
        });
    }
}
