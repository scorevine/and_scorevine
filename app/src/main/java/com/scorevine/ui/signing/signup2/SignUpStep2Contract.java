package com.scorevine.ui.signing.signup2;

import android.support.v4.app.FragmentActivity;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.VerificationRequest;
import com.scorevine.model.CountryCodes;
import com.scorevine.ui.signing.BaseViewSigning;
import com.scorevine.ui.signing.SignInActivity;

import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public interface SignUpStep2Contract {

    interface View extends BaseViewSigning {

        void showMessage(int messageId, boolean isLong);
        void showTextMessage(String message, boolean isLong);
        void setTextWatchers();
        void setCountrySpinner(List<CountryCodes.CountryCode> countryCodesList);
        void setGenderSpinner();
        void goNext();
        void showSavedData();
        void saveToSharePref();
        boolean checkNotNull();
    }

    interface Presenter  {
        void resendSmsCode(String username, String email, String code, String mobile);
        boolean savedToSP(FragmentActivity activity);
        void initSPData();

        void subscribe(SignInActivity activity);
        void unsubscribe();
    }

    interface Interactor{

        interface OnFinishedListener {
            void onResponse(String response);

            void onFailure(int responseId);
            void onError(APIErrorList errorsList);

            void onError(String error);
        }

        void sendVerification(VerificationRequest verificationRequest, final OnFinishedListener listener);
    }
}
