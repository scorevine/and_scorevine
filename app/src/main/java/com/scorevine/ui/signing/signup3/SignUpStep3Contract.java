package com.scorevine.ui.signing.signup3;

import android.app.Activity;

import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.LoginResponse;
import com.scorevine.ui.signing.BaseViewSigning;
import com.scorevine.ui.signing.SignInActivity;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public interface SignUpStep3Contract {

    interface View extends BaseViewSigning {

        boolean isBtnChecked();
        void setUpWebview();
        void showMessage(int messageId, boolean isLong);
        void showTextMessage(String message, boolean isLong);
        void goNext(String token);
        void goBack();
        void goToLoading();
        void goToRegisterFailed();
    }

    interface Presenter  {
        void subscribe(SignInActivity activity);
        void unsubscribe();
        void register(Activity activity);

    }

    interface Interactor{

        interface OnFinishedListener {
            void onResponse(LoginResponse response);
            void onFailure(int responseId);
            void onError(String error);
            void onError(APIErrorList errorsList);

        }

        void sendRegister(RequestBody email, RequestBody mobile, RequestBody username, RequestBody password, RequestBody facebookToken,
                              RequestBody birthDate, RequestBody gender, RequestBody country, RequestBody firstName,
                              RequestBody surname, RequestBody referal, MultipartBody.Part profile_image, RequestBody code,
                              RequestBody deviceUID, RequestBody push_token, final OnFinishedListener listener);

    }
}
