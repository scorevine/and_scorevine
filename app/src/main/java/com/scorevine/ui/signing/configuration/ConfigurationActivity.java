package com.scorevine.ui.signing.configuration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.scorevine.ui.home.HomeMainActivity;
import com.scorevine.R;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.configuration.adapters.AccountConfigurationAdapter;
import com.scorevine.ui.signing.configuration.adapters.Category;
import com.scorevine.ui.signing.configuration.adapters.SubCategory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConfigurationActivity extends AppCompatActivity implements ConfigurationContract.View {


    @BindView(R.id.configurationRecyclerView)
    RecyclerView mConfigurationRV;
    AccountConfigurationAdapter mAdapter;
    ConfigurationContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        ButterKnife.bind(this);
        mPresenter = new ConfigurationPresenter(this);
        mPresenter.getCategories(this);
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this, HomeMainActivity.class));
    }


    @OnClick(R.id.nextButton)
    public void onNextClicked() {

        //TODO: set chosen configurations for account!
        Log.i("subcategories", mAdapter.getCheckedSubcategories().toString());

        mPresenter.followCategories(mAdapter.getCheckedSubcategories(), this);


    }

    @Override
    public void goNext() {

        finish();
        startActivity(new Intent(this, HomeMainActivity.class));

    }


    @Override
    public void setCategories(List<Category> categories) {

        ArrayList<Category> parentList = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            ArrayList<SubCategory> childItems = new ArrayList<>();
            childItems.addAll(categories.get(i).getSubCategories());
            parentList.add(new Category(categories.get(i).getCategoryName(), childItems));
        }
        parentList.add(new Category("skip", new ArrayList<SubCategory>()));

        mAdapter = new AccountConfigurationAdapter(this, parentList);
        mConfigurationRV.setAdapter(mAdapter);
        mConfigurationRV.setLayoutManager(new LinearLayoutManager(this));

        mAdapter.toggleGroup(0);
    }


    @Override
    public void showMessage(int messageId, boolean isLong) {

    }


    @Override
    public void setUpView(SignInActivity activity) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
