package com.scorevine.ui.signing.resetpassword.step3;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.validation.ValidateInputFragment;
import com.scorevine.ui.validation.InputTypeSV;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by weronikapapkouskaya on 21.12.2016.
 */

public class ResetPasswordStep3Fragment extends ValidateInputFragment implements ResetPasswordStep3Contract.View {

    public static final int RESET_WITH_MOBILE = 0;
    public static final int RESET_WITH_EMAIL = 1;

    public static final int UNLOCK = 12;
    public static final int RESET_PASSWORD = 13;

    public static final String RESET_PASSWORD_STEP3_TAG = ResetPasswordStep3Fragment.class.getCanonicalName();

    public static final String USERNAME_ARG = "username";
    public static final String METHOD_TYPE_ARG = "method_type";
    private static final String RESET_TYPE_ARG = "reset_type";

    private ResetPasswordStep3Contract.Presenter mPresenter;
    private String mUsername;
    private int mMethodType;


    @BindView(R.id.codeTI)
    TextInputLayout mCodeTI;
    @BindView(R.id.passwordTI)
    TextInputLayout mPasswordTI;
    @BindView(R.id.confirmPasswordTI)
    TextInputLayout mConfirmPasswordTI;


    public static ValidateInputFragment getInstance(int methodType, int resetType, String username) {
        Bundle bundle = new Bundle();
        bundle.putInt(METHOD_TYPE_ARG, methodType);
        bundle.putInt(RESET_TYPE_ARG, resetType);
        bundle.putString(USERNAME_ARG, username);
        ValidateInputFragment fragment = new ResetPasswordStep3Fragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password_step3, container, false);
        ButterKnife.bind(this, view);

        mUsername = getArguments().getString(USERNAME_ARG);
        mMethodType = getArguments().getInt(METHOD_TYPE_ARG);
        int resetType = getArguments().getInt(RESET_TYPE_ARG);

        mPresenter = new ResetPasswordStep3Presenter(this, resetType, mMethodType);
        if (savedInstanceState == null) {
            String method = "email";
            if (mMethodType == 0) method = "mobile";
            mPresenter.getCode(mUsername, method, getContext());
        }
        setPasswordTI();
        setOnTextChangedListeners();
        setOnFocusedChangedListeners();

        ((SignInActivity) getActivity()).setBottomButtonVisibility(false);
        ((SignInActivity) getActivity()).setToolbarVisibility(true);

        return view;
    }

    private void setOnTextChangedListeners() {
        setConfirmPasswordTextChangedListener();
        setPasswordTextChangedListener();
        setCodeTextChangedListener();

    }

    private void setPasswordTI() {
        Typeface typeface = mPasswordTI.getEditText().getTypeface();
        mPasswordTI.getEditText().setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mPasswordTI.getEditText().setTransformationMethod(PasswordTransformationMethod.getInstance());
        mPasswordTI.getEditText().setTypeface(typeface);

        typeface = mConfirmPasswordTI.getEditText().getTypeface();
        mConfirmPasswordTI.getEditText().setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mConfirmPasswordTI.getEditText().setTransformationMethod(PasswordTransformationMethod.getInstance());
        mConfirmPasswordTI.getEditText().setTypeface(typeface);
    }


    private void setOnFocusedChangedListeners() {
        TextInputOnFocusChangedListener focusChangedListener = new TextInputOnFocusChangedListener();

        for (InputTypeSV type :
                InputTypeSV.values()) {
            if (getTextInputLayout(type) != null) {
                getTextInputLayout(type).getEditText().setOnFocusChangeListener(focusChangedListener);
            }
        }
    }

    public class TextInputOnFocusChangedListener implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {

                ((SignInActivity) getActivity()).scrollBy(100);
            }
        }
    }

    @OnClick(R.id.resetPasswordBtn)
    public void onResetButtonClick() {


        mPresenter.sendCode(mUsername, mCodeTI.getEditText().getText().toString(),
                mPasswordTI.getEditText().getText().toString(), getContext());

    }

    @Override
    public void showToast(int messageId) {
        Toast.makeText(getContext(), getString(messageId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }



    private void setCodeTextChangedListener() {
        mCodeTI.getEditText().addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                mPresenter.validateInput(charSequence.toString(), InputTypeSV.CODE, getContext());

            }
        });
    }

    @Override
    public void backToLogin() {
        FragmentHelper.clearBackStack(getActivity().getSupportFragmentManager());
    }

    @Override
    protected TextInputLayout getTextInputLayout(InputTypeSV inputType) {
        TextInputLayout textInputLayout = null;
        switch (inputType) {
            case CODE:
                textInputLayout = mCodeTI;
                break;
            case PASSWORD:
                textInputLayout = mPasswordTI;
                break;
            case CONFIRM_PASSWORD:
                textInputLayout = mConfirmPasswordTI;
                break;
            default:
                break;
        }
        return textInputLayout;
    }
}
