package com.scorevine.ui.signing;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.adamstyrc.cookiecutter.CookieCutterImageView;
import com.iceteck.silicompressorr.SiliCompressor;
import com.scorevine.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CropPhotoActivity extends AppCompatActivity {

    public static final int CAMERA_REQUEST_CODE = 123;
    public static final int GALLERY_REQUEST_CODE = 124;
    public static final String PHOTO_EXTRA = "photoRequestCode";
    public static final String BUNDLE_EXTRA_PHOTO_URI = "photoUri";
    public static final String APP_FILES_PATH = Environment
            .getExternalStorageDirectory().toString() + "/ScoreVine";
    public static final String IMAGES_PATH = "/Images";
    public static final String IMAGE_NAME_PREFIX = "sv_photo";
    private static final String IMAGE_NAME_SUFFIX = ".png";
    private String mPhotoUri;
    @BindView(R.id.ivCropPhoto)
    CookieCutterImageView mCookieCutterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_photo);
        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            mPhotoUri = savedInstanceState.getString(BUNDLE_EXTRA_PHOTO_URI, null);
        }
        if (mPhotoUri != null) {
            setPhoto();
        } else {
            int photoRequestCode = getIntent().getExtras().getInt(PHOTO_EXTRA, 0);
            if (photoRequestCode != 0) {
                if (photoRequestCode == CAMERA_REQUEST_CODE) openCamera();
                if (photoRequestCode == GALLERY_REQUEST_CODE) openGallery();
            }
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mPhotoUri != null) {
            outState.putString(BUNDLE_EXTRA_PHOTO_URI, mPhotoUri);
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mPhotoUri = savedInstanceState.getString(BUNDLE_EXTRA_PHOTO_URI, null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                setPhoto();
            }
            if (requestCode == GALLERY_REQUEST_CODE) {
                Uri photoUri = data.getData();
                mPhotoUri = photoUri.toString();
                setPhoto();
            }
        } else {
            onBackPressed();
        }
    }

    private void setPhoto() {
        try {
            if (mPhotoUri != null) {
                Log.i("authority", Uri.parse(mPhotoUri).getAuthority());
                if ("com.google.android.apps.docs.storage".equals(Uri.parse(mPhotoUri).getAuthority())
                        || "com.android.providers.media.documents".equals(Uri.parse(mPhotoUri).getAuthority()) ||
                        "com.android.providers.downloads.documents".equals(Uri.parse(mPhotoUri).getAuthority())) {
                    mCookieCutterImageView.setImageURI(Uri.parse(mPhotoUri));

                } else {
                    Bitmap photoBitmap = SiliCompressor.with(this).getCompressBitmap(mPhotoUri);
                    mCookieCutterImageView.setImageBitmap(photoBitmap);
                }

            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void openGallery() {

        Intent imageIntent = new Intent(Intent.ACTION_GET_CONTENT);
        imageIntent.setType("image/*");
        if (imageIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(imageIntent, GALLERY_REQUEST_CODE);
        }
    }

    private void openCamera() {
        File picture = createImageFile();

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(picture));
        startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
    }


    @OnClick(R.id.saveButton)
    public void onSaveButtonClick() {
        createImageFileFromBitmap();
        Intent data = new Intent();
        data.putExtra(BUNDLE_EXTRA_PHOTO_URI, mPhotoUri);
        setResult(RESULT_OK, data);
        finish();
    }

    @OnClick(R.id.backArrowButton)
    public void onBackArrowClick() {
        setResult(RESULT_CANCELED);
        onBackPressed();
    }

    public File createImageFile() {
        createImagesDirectory();
        File f = new File(APP_FILES_PATH + IMAGES_PATH,
                IMAGE_NAME_PREFIX + Calendar.getInstance().getTimeInMillis() + IMAGE_NAME_SUFFIX);
        try {
            f.createNewFile();

            Intent intent =
                    new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(f));
            sendBroadcast(intent);
            mPhotoUri = Uri.fromFile(f).toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }


    private void createImageFileFromBitmap() {
        createImagesDirectory();

        //create a file to write bitmap data
        File f = new File(APP_FILES_PATH + IMAGES_PATH,
                IMAGE_NAME_PREFIX + Calendar.getInstance().getTimeInMillis() + IMAGE_NAME_SUFFIX);
        try {
            f.createNewFile();

            //Convert bitmap to byte array
            Bitmap bitmap = mCookieCutterImageView.getCroppedBitmap();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] bitmapdata = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            Intent intent =
                    new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            intent.setData(Uri.fromFile(f));
            sendBroadcast(intent);

            mPhotoUri = Uri.fromFile(f).toString();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void createImagesDirectory() {
        //create directory if it does't exist
        String[] paths = new String[]{APP_FILES_PATH, APP_FILES_PATH + IMAGES_PATH};

        for (String path : paths) {
            File dir = new File(path);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Log.v("test", "ERROR: Creation of directory " + path + " on sdcard failed");
                    return;
                } else {
                    Log.v("test", "Created directory " + path + " on sdcard");
                }
            }
        }
    }


    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
