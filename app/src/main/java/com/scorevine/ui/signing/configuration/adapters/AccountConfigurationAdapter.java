package com.scorevine.ui.signing.configuration.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.scorevine.ui.home.HomeMainActivity;
import com.scorevine.R;
import com.thoughtbot.expandablerecyclerview.MultiTypeExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NIKAK
 * Ready4S
 * on 28.11.2016 18:25
 */
public class AccountConfigurationAdapter extends MultiTypeExpandableRecyclerViewAdapter<GroupViewHolder, AccountConfigurationAdapter.SubCategoryViewHolder> {
    private static final int CATEGORY_VIEW_TYPE = 4;
    private static final int SKIP_VIEW_TYPE = 5;
    private final Context mContext;
    private LayoutInflater mInflater;


    public AccountConfigurationAdapter(Context context, @NonNull List<Category> categories) {
        super(categories);
        Log.i("categories", categories.toString());
        Log.i("categories", categories.get(0).toString());
        Category o = (Category) categories.get(0);
        Log.i("categories", o.getItems().get(0).toString());

        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public GroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        GroupViewHolder groupViewHolder = null;
        View view;
        switch (viewType) {
            case CATEGORY_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_parent_configuration, parent, false);
                groupViewHolder = new CategoryViewHolder(view);
                break;
            case SKIP_VIEW_TYPE:
                view = mInflater.inflate(R.layout.list_item_skip_configuration, parent, false);
                groupViewHolder = new SkipViewHolder(view);
                break;
            default:
        }

        return groupViewHolder;
    }

    @NonNull
    @Override
    public SubCategoryViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_child_configuration, childViewGroup, false);
        return new SubCategoryViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(SubCategoryViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        Log.i("categories", group.getItems().toString());
        holder.bind((SubCategory) group.getItems().get(childIndex));
    }

    @Override
    public void onBindGroupViewHolder(GroupViewHolder holder, final int flatPosition, ExpandableGroup group) {

        int viewType = getItemViewType(flatPosition);
        switch (viewType) {
            case CATEGORY_VIEW_TYPE:
                ((CategoryViewHolder) holder).bind(group.getTitle());
                break;
            case SKIP_VIEW_TYPE:
                break;
            default:
        }

    }


    public int getFlattenedGroupPosition(int groupIndex) {
        int runningTotal = 0;
        for (int i = 0; i < groupIndex; i++) {
            runningTotal += numberOfVisibleItemsInGroup(i);
        }
        return runningTotal;
    }

    private int numberOfVisibleItemsInGroup(int group) {
        if (expandableList.expandedGroupIndexes.get(group)) {
            return expandableList.groups.get(group).getItemCount() + 1;
        } else {
            return 1;
        }
    }

    @Override
    public boolean isGroup(int viewType) {
        return viewType == CATEGORY_VIEW_TYPE || viewType == SKIP_VIEW_TYPE;
    }

    @Override
    public int getGroupViewType(int position, ExpandableGroup group) {
        if (position == getItemCount() - 1) {
            return SKIP_VIEW_TYPE;
        } else {
            return CATEGORY_VIEW_TYPE;
        }
    }

    public ArrayList<Integer> getCheckedSubcategories() {
        ArrayList<Integer> subCategories = new ArrayList<>();
        for (ExpandableGroup<SubCategory> group :
                getGroups()) {
            Category category = (Category) group;
            for (SubCategory subcategory :
                    category.getItems()) {
                if (subcategory.isChecked()) {
                    subCategories.add(subcategory.getId());
                }
            }

        }
        return subCategories;
    }

    class CategoryViewHolder extends GroupViewHolder {
        @BindView(R.id.parentNameTV)
        TextView mParentNameTV;
        @BindView(R.id.rightArrow)
        ImageView mRightArrow;

        @Override
        public void onClick(View v) {
            if (isGroupExpanded(getAdapterPosition())) return;
            super.onClick(v);
        }

        @Override
        public void expand() {
            animateArrow(0, 180);

            List<? extends ExpandableGroup> groups = getGroups();
            for (int i = 0; i < groups.size(); i++) {
                int flatPos = getFlattenedGroupPosition(i);
                if (i != getAdapterPosition() && isGroupExpanded(flatPos)) {
                    toggleGroup(flatPos);
                    notifyItemChanged(flatPos);
                }
            }

        }

        @Override
        public void collapse() {
            animateArrow(180, 0);
        }


        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(String name) {
            mParentNameTV.setText(name);
        }


        private void animateArrow(int fromDegrees, int toDegrees) {
            RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setInterpolator(new LinearInterpolator());
            rotate.setFillAfter(true);
            mRightArrow.startAnimation(rotate);
        }


    }

    class SkipViewHolder extends GroupViewHolder {
        public SkipViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {
        }

        @OnClick(R.id.skipConfigurationTV)
        public void onClick() {
            mContext.startActivity(new Intent(mContext, HomeMainActivity.class));
        }
    }

    class SubCategoryViewHolder extends ChildViewHolder implements RecyclerView.OnClickListener {
        @BindView(R.id.childCategoryNameCTV)
        CheckedTextView mCategoryName;
        @BindView(R.id.childCategoryCB)
        ImageView mCategoryImageView;
        SubCategory childItem;

        public SubCategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(SubCategory child) {
            childItem = child;
            mCategoryName.setText(child.getCategory());
            boolean isChecked = child.isChecked();
            mCategoryName.setChecked(isChecked);
            mCategoryImageView.setSelected(isChecked);
        }

        @OnClick({R.id.childCategoryCB, R.id.childCategoryNameCTV})
        public void onClick(View view) {
            boolean isChecked = !mCategoryName.isChecked();
            mCategoryName.setChecked(isChecked);
            mCategoryImageView.setSelected(isChecked);
            childItem.setChecked(isChecked);
        }
    }

}
