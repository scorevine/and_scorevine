package com.scorevine.ui.signing.resetpassword.step1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Spannable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.resetpassword.step2.ResetPasswordStep2Fragment;
import com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment;
import com.scorevine.ui.validation.InputTypeSV;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Agnieszka Duleba on 2016-12-02.
 */
public class ResetPasswordStep1Fragment extends Fragment implements ResetPasswordStep1Contract.View {

    public static final String TAG_RESETPASSWORD = ResetPasswordStep1Fragment.class.getSimpleName();

    @BindView(R.id.reset_email_et)
    TextInputEditText mResetUniqueNameET;
    @BindView(R.id.reset_email_ti)
    TextInputLayout mResetUniqueNameTL;

    @BindView(R.id.reset_password_btn)
    TextView mResetPasswordBtn;

    ResetPasswordStep1Contract.Presenter mPresenter;

    private String mUsername;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new ResetPasswordStep1Presenter(this);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe((SignInActivity) getActivity());
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @OnClick(R.id.reset_password_btn)
    public void onResetPasswordBtnClick() {
        KeyboardHelper.hideKeyboard(getView(), getActivity());
        if (!mResetUniqueNameET.getText().toString().isEmpty() && mResetUniqueNameET.getError() == null) {
            mUsername = mResetUniqueNameET.getText().toString().substring(1);
            mPresenter.sendResetPasswordRequest(mUsername, getContext());
            Log.i("resetpassword", "onnextclick");
        } else {
            Log.i("resetpassword", "onnextclick");
            showMessage(R.string.error_invalid_fields, false);
        }
    }


    @Override
    public void showInputError(int errorMessageId) {
        mResetUniqueNameTL.setErrorEnabled(true);
        mResetUniqueNameTL.setError(getString(errorMessageId));
        mResetPasswordBtn.setEnabled(false);

    }

    @Override
    public void hideInputError() {
        mResetUniqueNameTL.setErrorEnabled(false);
        mResetUniqueNameTL.setError(null);
        mResetPasswordBtn.setEnabled(true);
    }

    @Override
    public void setListeners() {
        mResetUniqueNameET.addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }

                if (charSequence.length() == 1 && i1 >= 1) {
                    mResetUniqueNameET.setText("");
                } else if (charSequence.length() == 1) {

                    Spannable text = getSpannable(String.format("@%s", charSequence), getContext());

                    mResetUniqueNameET.setText(text);
                    mResetUniqueNameET.setSelection(2);
                }
                if (charSequence.length() < 5) {
                    showInputError(R.string.error_min_characters_count_4);
                }
                if (charSequence.length() == 16) {
                    showInputError(R.string.error_max_characters_count_15);
                } else if (charSequence.length() > 4) {
                    mPresenter.validateInput(charSequence.toString().substring(1), InputTypeSV.UNIQUE_NAME);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (mResetUniqueNameET.getText().length() > 0 && mResetUniqueNameET.getText().toString().charAt(0) != '@') {
                    Spannable text = getSpannable(String.format("@%s", editable.toString()), getContext());
                    mResetUniqueNameET.setText(text);
                    mResetUniqueNameET.setSelection(mResetUniqueNameET.getText().length());

                }
            }
        });
    }

    @Override
    public void goToStep3(int resetType) {

        FragmentHelper.replaceFragment(getActivity().getSupportFragmentManager(),
                ResetPasswordStep3Fragment.getInstance(resetType, ResetPasswordStep3Fragment.RESET_PASSWORD, mUsername),
                ResetPasswordStep3Fragment.RESET_PASSWORD_STEP3_TAG);

    }

    @Override
    public void goToStep2() {
        FragmentHelper.replaceFragment(getActivity().getSupportFragmentManager(), ResetPasswordStep2Fragment.getInstance(mUsername), "step2");
    }

    @Override
    public void showMessage(int messageId, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(message, isLong);
    }

    @Override
    public void setUpView(SignInActivity activity) {
        activity.setBottomButtonVisibility(false);
        activity.setToolbarVisibility(true);
    }
}
