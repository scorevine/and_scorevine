package com.scorevine.ui.signing.resetpassword.step2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment.RESET_PASSWORD;
import static com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment.RESET_PASSWORD_STEP3_TAG;
import static com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment.RESET_WITH_EMAIL;
import static com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment.RESET_WITH_MOBILE;

/**
 * Created by weronikapapkouskaya on 21.12.2016.
 */

public class ResetPasswordStep2Fragment extends Fragment {


    private String mUsername;
    public static final String USERNAME_ARG = "username";


    public static ResetPasswordStep2Fragment getInstance(String username) {
        Bundle bundle = new Bundle();
        bundle.putString(USERNAME_ARG, username);
        ResetPasswordStep2Fragment fragment = new ResetPasswordStep2Fragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password_step2, container, false);
        ButterKnife.bind(this, view);

        mUsername = getArguments().getString(USERNAME_ARG);

        ((SignInActivity) getActivity()).setBottomButtonVisibility(false);


        return view;
    }


    @OnClick(R.id.resetWithMobileBtn)
    public void onResetWithMobileClick() {
        FragmentHelper.replaceFragment(getActivity().getSupportFragmentManager(),
                ResetPasswordStep3Fragment.getInstance(RESET_WITH_MOBILE, RESET_PASSWORD, mUsername),
                RESET_PASSWORD_STEP3_TAG);

    }

    @OnClick(R.id.resetWithMobileBtn)
    public void onResetWithEmailClick() {
        FragmentHelper.replaceFragment(getActivity().getSupportFragmentManager(),
                ResetPasswordStep3Fragment.getInstance(RESET_WITH_EMAIL,
                        RESET_PASSWORD, mUsername), RESET_PASSWORD_STEP3_TAG);

    }
}
