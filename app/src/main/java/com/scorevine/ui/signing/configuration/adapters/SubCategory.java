package com.scorevine.ui.signing.configuration.adapters;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by NIKAK
 * Ready4S
 * on 29.11.2016 16:19
 */
public class SubCategory implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("category")
    @Expose
    private String category;
    @Nullable
    private boolean isChecked = false;

    public SubCategory(String category, int id) {
        this.category = category;
    }

    protected SubCategory(Parcel in) {
        id = in.readInt();
        category = in.readString();
        isChecked = in.readByte() != 0;
    }


    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChecked() {
        return isChecked;
    }




    @Override
    public String toString() {
        return "SubCategory{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(category);
        parcel.writeByte((byte) (isChecked ? 1 : 0));
    }
}
