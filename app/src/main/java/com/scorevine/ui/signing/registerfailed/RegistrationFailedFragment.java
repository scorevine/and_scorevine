package com.scorevine.ui.signing.registerfailed;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.signing.LoadingFragment;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.configuration.ConfigurationActivity;
import com.scorevine.ui.signing.login.LoginFragment;
import com.scorevine.ui.signing.signup1.SignUpStep1Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NIKAK
 * Ready4S
 * on 30.11.2016 17:15
 */
public class RegistrationFailedFragment extends Fragment implements RegistrationFailedContract.View{
    public static final String TAG_FRAGMENT_REGISTER_FAILED = RegistrationFailedFragment.class.getSimpleName();

    RegistrationFailedContract.Presenter mPresenter;

    @BindView(R.id.saveAndTryLaterButton)
    TextView mSaveBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_failed, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new RegistrationFailedPresenter(this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe((SignInActivity) getActivity());
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @OnClick(R.id.saveAndTryLaterButton)
    public void onSaveAndTryLaterButtonClick() {
        mPresenter.saveData(getActivity());
    }

    @OnClick(R.id.retryRightNowButton)
    public void onRetryRightNowButtonClick() {

        mPresenter.registerAgain(getActivity());
    }

    @OnClick(R.id.cancelButton)
    public void onCancelButtonClick() {
        mPresenter.clearData(getActivity());
    }


    @Override
    public void goToLogin() {

        FragmentHelper.clearBackStack(getFragmentManager());
        FragmentHelper.addFirstFragment(getFragmentManager(), new LoginFragment(), LoginFragment.TAG_FRAGMENT_LOGIN);

    }

    @Override
    public void goBack() {
        FragmentHelper.clearBackStack(getFragmentManager());
        FragmentHelper.replaceFragment(getFragmentManager(), new SignUpStep1Fragment(), SignUpStep1Fragment.TAG_FRAGMENT_REGISTER1);

    }

    @Override
    public void goNext() {
        new SharedPrefHelper().clearSharedPref(getActivity());

        Intent intent = new Intent(this.getActivity(), ConfigurationActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToLoading() {
        FragmentHelper.replaceFragment(getFragmentManager(), new LoadingFragment(), LoadingFragment.TAG_FRAGMENT_LOADING);
    }

    @Override
    public void goToRegisterFailed() {
        FragmentHelper.replaceFragment(getFragmentManager(), new RegistrationFailedFragment(), RegistrationFailedFragment.TAG_FRAGMENT_REGISTER_FAILED);
    }

    @Override
    public void showMessage(int messageId, boolean isLong) {
        ((BaseActivity)getActivity()).showToast(getString(messageId), isLong);
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        ((BaseActivity)getActivity()).showToast(message, isLong);
    }

    @Override
    public void setUpView(SignInActivity activity) {
        activity.setBottomButtonVisibility(false);
        activity.setToolbarVisibility(false);
        if (!new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_FB_ID, "").equals("")) {
            mSaveBtn.setVisibility(View.GONE);
        } else {
            mSaveBtn.setVisibility(View.VISIBLE);
        }
    }

}
