package com.scorevine.ui.signing.signup1;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.ListPopupWindow;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Spannable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.scorevine.R;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.helpers.TooltipHelper;
import com.scorevine.model.CountryCodes;
import com.scorevine.model.signing.SignUpInfoPart1;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.signing.CustomTextWatcher;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.signup2.SignUpStep2Fragment;
import com.scorevine.ui.validation.InputTypeSV;
import com.tooltip.Tooltip;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by Agnieszka Duleba on 2016-11-29.
 */

public class SignUpStep1Fragment extends Fragment implements SignUpStep1Contract.View {

    public static final String TAG_FRAGMENT_REGISTER1 = SignUpStep1Fragment.class.getSimpleName();
    public static final String TAG_FRAGMENT_REGISTER1_BUNDLE = "part1 data";

    public static final String EXTRA_FACEBOOK_FIRST_NAME = "extra_facebook_first_name";
    public static final String EXTRA_FACEBOOK_LAST_NAME = "extra_facebook_last_name";
    public static final String EXTRA_FACEBOOK_ID = "extra_facebook_id";
    public static final String EXTRA_FACEBOOK_EMAIL = "extra_facebook_email";


    @BindView(R.id.email_ti)
    TextInputLayout mEmailTI;
    @BindView(R.id.phone_code_ti)
    TextInputLayout mPhoneCodeTI;
    @BindView(R.id.phoneNumber_ti)
    TextInputLayout mPhoneNumberTI;
    @BindView(R.id.firstNameTI)
    TextInputLayout mFirstNameTI;
    @BindView(R.id.lastNameTI)
    TextInputLayout mLastNameTI;
    @BindView(R.id.uniqueNameTI)
    TextInputLayout mUniqueNameTI;
    @BindView(R.id.passwordTI)
    TextInputLayout mPasswordTI;
    @BindView(R.id.confirmPasswordTI)
    TextInputLayout mConfirmPasswordTI;

    ListPopupWindow listPopupWindow;
    SignUpStep1Presenter mPresenter;
    SignUpInfoPart1 signUpInfo;

    private Tooltip mEmailTooltip;
    private Tooltip mPhoneNumberTooltip;


    int countryCodePosition;
    String fbFirstName = "";
    String fbLastName = "";
    String fbFacebookId = "";
    String fbEmail = "";

    public static SignUpStep1Fragment getInstance(String id, String lastName, String firstName, String email) {
        SignUpStep1Fragment signUpStep1Fragment = new SignUpStep1Fragment();

        Bundle args = new Bundle();
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_ID, id);
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_LAST_NAME, lastName);
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_FIRST_NAME, firstName);
        args.putString(SignUpStep1Fragment.EXTRA_FACEBOOK_EMAIL, email);
        signUpStep1Fragment.setArguments(args);
        return signUpStep1Fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register_s1, container, false);
        ButterKnife.bind(this, view);

        signUpInfo = new SignUpInfoPart1();
        mPresenter = new SignUpStep1Presenter(this);
        mPresenter.initSPData();

        if (!fbFirstName.isEmpty()) {
            mFirstNameTI.getEditText().setText(fbFirstName);
            signUpInfo.firstName = fbFirstName;
        }
        if (!fbLastName.isEmpty()) {
            mLastNameTI.getEditText().setText(fbLastName);
            signUpInfo.surname = fbLastName;
        }
        if (!fbFacebookId.isEmpty()) {
            mPasswordTI.getEditText().setFocusable(false);
            mConfirmPasswordTI.getEditText().setFocusable(false);
            mPasswordTI.setVisibility(View.INVISIBLE);
            mConfirmPasswordTI.setVisibility(View.INVISIBLE);
        }
        if (!fbEmail.isEmpty()) {
            mEmailTI.getEditText().setText(fbEmail);
            signUpInfo.email = fbEmail;
            suggestUniqueName();
            mUniqueNameTI.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        }

        KeyboardHelper.hideKeyboard(view, getActivity());

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            fbFirstName = bundle.getString(EXTRA_FACEBOOK_FIRST_NAME, "");
            fbLastName = bundle.getString(EXTRA_FACEBOOK_LAST_NAME, "");
            fbFacebookId = bundle.getString(EXTRA_FACEBOOK_ID, "");
            fbEmail = bundle.getString(EXTRA_FACEBOOK_EMAIL, "");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe((SignInActivity) getActivity());
    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            signUpInfo = (SignUpInfoPart1) savedInstanceState.getSerializable(TAG_FRAGMENT_REGISTER1_BUNDLE);
            if (signUpInfo != null) {
                mEmailTI.getEditText().setText(signUpInfo.email);
                mPhoneCodeTI.getEditText().setText(signUpInfo.code);
                mPhoneNumberTI.getEditText().setText(signUpInfo.mobile);
                mFirstNameTI.getEditText().setText(signUpInfo.firstName);
                mLastNameTI.getEditText().setText(signUpInfo.surname);
                mUniqueNameTI.getEditText().setText(signUpInfo.uniqueName);
                mPasswordTI.getEditText().setText(signUpInfo.password);
                mConfirmPasswordTI.getEditText().setText(signUpInfo.confirmPassword);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TAG_FRAGMENT_REGISTER1_BUNDLE, signUpInfo);
        //Save the fragment's state here
    }


    @OnClick(R.id.phone_code_et)
    public void onPhoneCodeClick() {
        listPopupWindow.show();
        KeyboardHelper.hideKeyboard(getView(), getActivity());
    }

    @OnClick(R.id.email_help_iv)
    public void onEmailHelpClick() {
        mEmailTooltip.show();
    }

    @OnClick(R.id.phone_help_iv)
    public void onPhoneHelpClick() {
        mPhoneNumberTooltip.show();
    }

    public void setCodeSpinner(final List<CountryCodes.CountryCode> countryCodesList) {

        final List<String> countriesList = new ArrayList<>();

        TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(TELEPHONY_SERVICE);
        String countryCodeValue = tm.getSimCountryIso();
        int index = -1;
        for (CountryCodes.CountryCode element : countryCodesList) {
            if (countryCodeValue.equals(element.getCountryCode().toLowerCase())) {
                index = countryCodesList.indexOf(element);
            }
            countriesList.add(element.getCountryName());
        }


        listPopupWindow = new ListPopupWindow(getActivity());
        listPopupWindow.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.dropdown_item, countriesList));
        listPopupWindow.setAnchorView(mPhoneCodeTI.getEditText());
        listPopupWindow.setModal(true);
        listPopupWindow.setWidth(getResources().getDimensionPixelSize(R.dimen.code_spinner_width));

        if (index > 0) {
            listPopupWindow.setSelection(index);
            mPhoneCodeTI.getEditText().setText(countryCodesList.get(index).getDialCode());
            signUpInfo.code = countryCodesList.get(index).getDialCode();
            countryCodePosition = index;
        }

        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    signUpInfo.code = countryCodesList.get(position).getDialCode();
                    mPhoneCodeTI.getEditText().setText(countryCodesList.get(position).getDialCode());
                    listPopupWindow.dismiss();
                    countryCodePosition = position;
                }
            }
        });
    }

    @Override
    public void setTextWatchers() {

        setEmailTextChangedListener();

        setPhoneCodeTextChangedListener();

        setPhoneNumberTextChangedListener();

        setFirstNameTextChangedListener();

        setLastNameTextChangedListener();

        setUniqueNameTextChangedListener();

        setPasswordTextChangedListener();

        setConfirmPasswordTextChangedListener();
    }

    @Override
    public void suggestUniqueName() {
        String[] split = mEmailTI.getEditText().getText().toString().split("@");
        String suggestedName = split[0].replaceAll("[^A-Za-z0-9]", "");
        if (split[0].length() >= 15) {
            suggestedName = suggestedName.substring(0, 14);
        }
        mUniqueNameTI.getEditText().setText(suggestedName);
    }

    private void setConfirmPasswordTextChangedListener() {
        mConfirmPasswordTI.getEditText().addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }

                if (charSequence.length() >= 8) {
                    if (mPresenter.validateInput(getString(R.string.regex_password), charSequence, R.string.error_characters_password, InputTypeSV.CONFIRM_PASSWORD)) {
                        if (mPasswordTI.getEditText().getText().length() > 0) {
                            if (mPasswordTI.getEditText().getText().toString().equals(charSequence.toString())) {
                                hideInputError(InputTypeSV.CONFIRM_PASSWORD);
                                hideInputError(InputTypeSV.PASSWORD);
                            } else {
                                showInputError(R.string.error_passwords_dont_match, InputTypeSV.CONFIRM_PASSWORD);
                            }
                        }
                    }
                } else {
                    showInputError(R.string.error_min_characters_count_8, InputTypeSV.CONFIRM_PASSWORD);
                }
                if (!mPasswordTI.getEditText().isFocusable()) {
                    hideInputError(InputTypeSV.CONFIRM_PASSWORD);
                    hideInputError(InputTypeSV.PASSWORD);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.confirmPassword = editable.toString();

            }
        });
    }

    private void setPasswordTextChangedListener() {
        mPasswordTI.getEditText().addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() >= 8) {
                    if (mPresenter.validateInput(getString(R.string.regex_password), charSequence, R.string.error_characters_password, InputTypeSV.PASSWORD)) {
                        if (mConfirmPasswordTI.getEditText().getText().length() > 0) {
                            if (mConfirmPasswordTI.getEditText().getText().toString().equals(charSequence.toString())) {
                                hideInputError(InputTypeSV.CONFIRM_PASSWORD);
                                hideInputError(InputTypeSV.PASSWORD);
                            } else {
                                showInputError(R.string.error_passwords_dont_match, InputTypeSV.PASSWORD);
                            }
                        }
                    }
                } else {
                    showInputError(R.string.error_min_characters_count_8, InputTypeSV.PASSWORD);
                }
                if (!mConfirmPasswordTI.getEditText().isFocusable()) {
                    hideInputError(InputTypeSV.CONFIRM_PASSWORD);
                    hideInputError(InputTypeSV.PASSWORD);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.password = editable.toString();

            }
        });
    }

    private void setUniqueNameTextChangedListener() {
        mUniqueNameTI.getEditText().addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }

                if (charSequence.length() == 1 && i1 >= 1) {
                    mUniqueNameTI.getEditText().setText("");
                } else if (charSequence.length() == 1) {

                    Spannable text = getSpannable(String.format("@%s", charSequence), getContext());

                    mUniqueNameTI.getEditText().setText(text);
                    mUniqueNameTI.getEditText().setSelection(2);
                }
                if (charSequence.length() < 5) {
                    showInputError(R.string.error_min_characters_count_4, InputTypeSV.UNIQUE_NAME);
                }
                if (charSequence.length() == 16) {
                    showInputError(R.string.error_max_characters_count_15, InputTypeSV.UNIQUE_NAME);
                } else if (charSequence.length() > 4) {
                    mPresenter.validateInput(getString(R.string.regex_unique_name), charSequence.toString().substring(1), R.string.error_unique_name_symbols, InputTypeSV.UNIQUE_NAME);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (mUniqueNameTI.getEditText().getText().length() > 0 && mUniqueNameTI.getEditText().getText().toString().charAt(0) != '@') {
                    Spannable text = getSpannable(String.format("@%s", editable.toString()), getContext());
                    mUniqueNameTI.getEditText().setText(text);
                    mUniqueNameTI.getEditText().setSelection(mUniqueNameTI.getEditText().getText().length());

                }
                if (editable.length() > 1)
                    signUpInfo.uniqueName = editable.toString().substring(1, editable.length());


            }
        });
    }

    private void setLastNameTextChangedListener() {
        mLastNameTI.getEditText().addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() < 2) {
                    showInputError(R.string.error_min_characters_count_2, InputTypeSV.SURNAME);
                } else if (charSequence.length() == 15) {
                    showInputError(R.string.error_max_characters_count_15, InputTypeSV.SURNAME);
                } else {
                    hideInputError(InputTypeSV.SURNAME);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.surname = editable.toString();

            }
        });
    }

    private void setFirstNameTextChangedListener() {
        mFirstNameTI.getEditText().addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() < 2) {
                    showInputError(R.string.error_min_characters_count_2, InputTypeSV.FIRST_NAME);
                } else if (charSequence.length() == 15) {
                    showInputError(R.string.error_max_characters_count_15, InputTypeSV.FIRST_NAME);
                } else {
                    hideInputError(InputTypeSV.FIRST_NAME);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.firstName = editable.toString();

            }
        });
    }

    private void setPhoneNumberTextChangedListener() {
        mPhoneNumberTI.getEditText().addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                mPresenter.validateInput(getString(R.string.regex_phone_number), charSequence, R.string.error_invalid_phone_number, InputTypeSV.PHONE_NUMBER);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.mobile = editable.toString().replaceFirst("^0+(?!$)", "");
                Log.i("register", signUpInfo.mobile);

            }
        });
    }

    private void setPhoneNumberOnFocusChangedListener() {
        mPhoneNumberTooltip = TooltipHelper.createToolTipDialogBox(mPhoneNumberTI.getEditText(),
                R.string.signup_help_phone, Gravity.BOTTOM, getContext());

        mPhoneNumberTI.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ((SignInActivity) getActivity()).scrollBy(100);
                    if (mPhoneNumberTI.getEditText().getText().toString().isEmpty()) {
                        mPhoneNumberTooltip.show();
                    }
                } else {
                    mPhoneNumberTooltip.dismiss();
                }
            }
        });
    }

    private void setPhoneCodeTextChangedListener() {
        mPhoneCodeTI.getEditText().addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.code = editable.toString();

            }
        });
    }

    private void setEmailTextChangedListener() {

        mEmailTI.getEditText().addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                mPresenter.validateInput(getString(R.string.email_pattern), charSequence, R.string.error_invalid_email, InputTypeSV.EMAIL);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.email = editable.toString();
            }
        });
    }

    private void setEmailOnFocusChangedListener() {

        mEmailTooltip = TooltipHelper.createToolTipDialogBox(mEmailTI.getEditText(),
                R.string.signup_email_help, Gravity.BOTTOM, getContext());
        mEmailTI.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ((SignInActivity) getActivity()).scrollBy(100);
                    if (mEmailTI.getEditText().getText().toString().isEmpty()) {
                        mEmailTooltip.show();
                    }
                } else {
                    if (!(mUniqueNameTI.getEditText().length() > 0)
                            && mEmailTI.getError() == null
                            && !mEmailTI.getEditText().getText().toString().isEmpty()) {
                        suggestUniqueName();
                    }
                    mEmailTooltip.dismiss();
                }
            }
        });
    }


    @Override
    public void showMessage(int messageId, boolean isLong) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
        }
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(message, isLong);
    }

    @Override
    public void showSavedData() {
        if (mPresenter.savedToSP(getActivity())) {
            mEmailTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_EMAIL, null));
            mPhoneNumberTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_MOBILE, null));
            mPhoneCodeTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_CODE, null));
            mFirstNameTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_FIRST_NAME, null));
            mLastNameTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_SURNAME, null));
            mUniqueNameTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_UNIQUENAME, null));
            mPasswordTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_PASSWORD, null));
            mConfirmPasswordTI.getEditText().setText(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_CONFIRMPASSWORD, null));
        }
        if (signUpInfo != null) {
            signUpInfo.email = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_EMAIL, null);
            signUpInfo.mobile = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_MOBILE, null);
            signUpInfo.code = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_CODE, null);
            signUpInfo.firstName = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_FIRST_NAME, null);
            signUpInfo.surname = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_SURNAME, null);
            signUpInfo.uniqueName = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_UNIQUENAME, null);
            signUpInfo.password = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_PASSWORD, null);
            signUpInfo.confirmPassword = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_CONFIRMPASSWORD, null);
        }
    }

    @Override
    public boolean checkNotNull() {
        if (mEmailTI.getEditText().getText().length() == 0 ||
                mPhoneCodeTI.getEditText().getText().length() == 0 ||
                mPhoneNumberTI.getEditText().getText().length() == 0 ||
                mFirstNameTI.getEditText().getText().length() == 0 ||
                mLastNameTI.getEditText().getText().length() == 0 ||
                mUniqueNameTI.getEditText().getText().length() == 0) {
            return false;
        } else if (
                (mPasswordTI.getEditText().getText().length() == 0 ||
                        mConfirmPasswordTI.getEditText().getText().length() == 0) && fbFacebookId.isEmpty()) {

            return false;
        } else if (mEmailTI.isErrorEnabled() ||
                mPhoneCodeTI.isErrorEnabled() ||
                mPhoneNumberTI.isErrorEnabled() ||
                mFirstNameTI.isErrorEnabled() ||
                mLastNameTI.isErrorEnabled() ||
                mUniqueNameTI.isErrorEnabled() ||
                mPasswordTI.isErrorEnabled() ||
                mConfirmPasswordTI.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void showInputError(int errorMessageId, InputTypeSV inputType) {
        TextInputLayout textInputLayout = getTextInputLayout(inputType);
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(getString(errorMessageId));
        }

    }

    @Override
    public void hideInputError(InputTypeSV inputType) {
        TextInputLayout textInputLayout = getTextInputLayout(inputType);
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(false);
        }
    }

    private TextInputLayout getTextInputLayout(InputTypeSV inputType) {
        TextInputLayout textInputLayout = null;
        switch (inputType) {
            case EMAIL:
                textInputLayout = mEmailTI;
                break;
            case PHONE_NUMBER:
                textInputLayout = mPhoneNumberTI;
                break;
            case FIRST_NAME:
                textInputLayout = mFirstNameTI;
                break;
            case SURNAME:
                textInputLayout = mLastNameTI;
                break;
            case UNIQUE_NAME:
                textInputLayout = mUniqueNameTI;
                break;
            case PASSWORD:
                textInputLayout = mPasswordTI;
                break;
            case CONFIRM_PASSWORD:
                textInputLayout = mConfirmPasswordTI;
                break;
            default:
                break;
        }
        return textInputLayout;
    }

    @Override
    public void setNewUsername(String suggestedUsername) {
        if (!suggestedUsername.isEmpty())
            mUniqueNameTI.getEditText().setText(suggestedUsername);
    }


    @Override
    public void sendVerification() {
        saveToSharePref();
        if (!fbFacebookId.isEmpty())
            new SharedPrefHelper().addSharedPref(getActivity(), SharedPrefHelper.SP_FB_ID, fbFacebookId);
        String uniquename = mUniqueNameTI.getEditText().getText().toString();
        uniquename = uniquename.substring(1, uniquename.length());

        String phoneNumber = mPhoneNumberTI.getEditText().getText().toString().replaceFirst("^0+(?!$)", "");
        mPresenter.sendVerificationRequest(uniquename,
                mEmailTI.getEditText().getText().toString(),
                mPhoneCodeTI.getEditText().getText().toString(),
                phoneNumber);
    }


    @Override
    public void goNext() {

        SignUpStep2Fragment signUpStep2Fragment = new SignUpStep2Fragment();
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.country_code_bundle_data), countryCodePosition);
        signUpStep2Fragment.setArguments(bundle);
        FragmentHelper.replaceFragment(getFragmentManager(), signUpStep2Fragment, SignUpStep2Fragment.TAG_FRAGMENT_REGISTER2);
    }

    @Override
    public void setUpView(SignInActivity activity) {
        activity.setBottomButtonVisibility(true);
        activity.setBottomButtonText(getString(R.string.signup_next_button));
        activity.setToolbarVisibility(true);

        setOnFocusedChangedListeners();
    }

    private void setOnFocusedChangedListeners() {
        TextInputOnFocusChangedListener focusChangedListener = new TextInputOnFocusChangedListener();

        for (InputTypeSV type :
                InputTypeSV.values()) {
            if (getTextInputLayout(type) != null) {
                switch (type) {
                    case EMAIL:
                        setEmailOnFocusChangedListener();
                        break;
                    case PHONE_NUMBER:
                        setPhoneNumberOnFocusChangedListener();
                        break;
                    default:
                        getTextInputLayout(type).getEditText().setOnFocusChangeListener(focusChangedListener);
                        break;
                }
            }
        }
    }

    public class TextInputOnFocusChangedListener implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {

                ((SignInActivity) getActivity()).scrollBy(100);
            }
        }
    }

    @Override
    public void saveToSharePref() {
        String uniquename = mUniqueNameTI.getEditText().getText().toString();
        uniquename = uniquename.substring(1, uniquename.length());

        Activity activity = getActivity();
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_EMAIL, mEmailTI.getEditText().getText().toString());
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_FIRST_NAME, mFirstNameTI.getEditText().getText().toString());
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_SURNAME, mLastNameTI.getEditText().getText().toString());
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_UNIQUENAME, uniquename);
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_MOBILE, mPhoneNumberTI.getEditText().getText().toString());
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_CODE, mPhoneCodeTI.getEditText().getText().toString());
        if (!mPasswordTI.getEditText().getText().toString().isEmpty())
            new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_PASSWORD, mPasswordTI.getEditText().getText().toString());
        if (!mConfirmPasswordTI.getEditText().getText().toString().isEmpty())
            new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_CONFIRMPASSWORD, mConfirmPasswordTI.getEditText().getText().toString());

        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_SAVE_DATA_P1, "true");
    }
}
