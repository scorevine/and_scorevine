package com.scorevine.ui.signing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.scorevine.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NIKAK
 * Ready4S
 * on 30.11.2016 16:42
 */
public class LoadingFragment extends Fragment {
    public static final String TAG_FRAGMENT_LOADING = LoadingFragment.class.getSimpleName();
    @BindView(R.id.spinner)
    ImageView mSpinner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loading, container, false);
        ButterKnife.bind(this, view);
        setupView((SignInActivity) getActivity());


        RotateAnimation anim = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(1500);

        mSpinner.startAnimation(anim);

        return view;
    }


    public void setupView(SignInActivity activity) {
        activity.setBottomButtonVisibility(false);
        activity.setToolbarVisibility(false);
    }
}
