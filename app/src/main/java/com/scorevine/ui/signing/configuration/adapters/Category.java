package com.scorevine.ui.signing.configuration.adapters;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NIKAK
 * Ready4S
 * on 29.11.2016 14:43
 */
public class Category extends ExpandableGroup<SubCategory> {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("subCategories")
    @Expose
    private
    ArrayList<SubCategory> subCategories;
    @Nullable
    private Boolean isChecked = false;

    public Category(String title, List<SubCategory> items) {
        super(title, items);
        if (id == null) {
            id = 800;
        }
        this.subCategories = (ArrayList<SubCategory>) items;
        category = title;
    }

    public String getCategoryName() {
        return category;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", subCategories=" + subCategories +
                '}';
    }

    @Override
    public List<SubCategory> getItems() {
        return getSubCategories();
    }

    @Override
    public String getTitle() {
        return getCategoryName();
    }

    @Nullable
    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(@Nullable Boolean checked) {
        isChecked = checked;
    }
}
