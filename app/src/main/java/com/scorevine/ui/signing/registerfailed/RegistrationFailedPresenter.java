package com.scorevine.ui.signing.registerfailed;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.LoginResponse;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.signup3.SignUpStep3Contract;
import com.scorevine.ui.signing.signup3.SignUpStep3Interactor;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class RegistrationFailedPresenter implements RegistrationFailedContract.Presenter, SignUpStep3Contract.Interactor.OnFinishedListener {

    private final RegistrationFailedContract.View mView;
    private final SignUpStep3Contract.Interactor mInteractor;

    public RegistrationFailedPresenter(@NonNull RegistrationFailedContract.View view) {
        mView = view;
        mInteractor = new SignUpStep3Interactor();

    }

    @Override
    public void saveData(FragmentActivity fragmentActivity) {
        new SharedPrefHelper().addSharedPref(fragmentActivity, SharedPrefHelper.SP_SAVE_DATA, "true");
        mView.goToLogin();
    }

    @Override
    public void clearData(FragmentActivity fragmentActivity) {
        new SharedPrefHelper().clearSharedPref(fragmentActivity);
        mView.goToLogin();
    }

    @Override
    public void subscribe(SignInActivity activity) {
        mView.setUpView(activity);
    }

    @Override
    public void unsubscribe() {

    }
//    @Override
//    public void registerAgain(Activity activity){
//        mView.goToLoading();
//
//        mInteractor.sendRegister(mView.getRegisterData(), this);
//    }
    @Override
    public void registerAgain(Activity activity) {

        String mobileStr = (new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_CODE, "") + "-" +
                new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_MOBILE, ""));
        String birthDate = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_BIRTHDATE, "");
        String birthDateFormattedStr = DateFormatHelper.changeDateFormat(DateFormatHelper.APP_PROFILE_DATE_FORMAT, DateFormatHelper.API_PROFILE_DATE_FORMAT, birthDate);

        String passwordStr = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_PASSWORD, "");
        String facebookIdStr = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_FB_ID, "");

        String referal = "";
        String uuidStr = "";
        String notificationTokenStr = "";

        MultipartBody.Part filePart = null;
        String imagePath = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_PHOTO, "");
        if(!imagePath.equals("")) {
            Uri imageUri = Uri.parse(imagePath.substring(7, imagePath.length()));
            File file = new File(imageUri.getPath());
            if(file!=null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                filePart = MultipartBody.Part.createFormData("profile_image", file.getName(), requestFile);
            }
        }

        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), mobileStr);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_EMAIL, ""));
        RequestBody uniqueName = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_UNIQUENAME, ""));
        RequestBody password = RequestBody.create(MediaType.parse("text/plain"), passwordStr);
        RequestBody facebookId = RequestBody.create(MediaType.parse("text/plain"), facebookIdStr);
        RequestBody birthDateFormatted = RequestBody.create(MediaType.parse("text/plain"), birthDateFormattedStr);
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_GENDER, ""));
        RequestBody country = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_COUNTRY, ""));
        RequestBody firstName = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_FIRST_NAME, ""));
        RequestBody surname = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SURNAME, ""));
        RequestBody referral = RequestBody.create(MediaType.parse("text/plain"),  referal );
        RequestBody smscode = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SMSCODE, ""));
        RequestBody uuid = RequestBody.create(MediaType.parse("text/plain"), uuidStr);
        RequestBody notificationToken = RequestBody.create(MediaType.parse("text/plain"), notificationTokenStr);

        mInteractor.sendRegister(email, mobile, uniqueName, password, facebookId,
                birthDateFormatted, gender, country, firstName, surname,
                referral, filePart, smscode, uuid, notificationToken, this);

        mView.goToLoading();
    }


    @Override
    public void onResponse(LoginResponse response) {
        mView.goNext();
    }

    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, false);
        mView.goToRegisterFailed();
    }

    @Override
    public void onError(String error) {
        mView.goBack();
        mView.showTextMessage(error, false);
    }

    @Override
    public void onError(APIErrorList errorsList) {
        mView.goBack();

        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);
    }
}
