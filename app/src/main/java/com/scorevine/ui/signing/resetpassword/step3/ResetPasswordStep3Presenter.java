package com.scorevine.ui.signing.resetpassword.step3;

import android.content.Context;
import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.ResetPasswordGetCodeRequest;
import com.scorevine.api.signing.ResetPasswordSendCodeRequest;
import com.scorevine.helpers.InternetConnectionHelper;
import com.scorevine.ui.validation.ValidateInputPresenter;

/**
 * Created by weronikapapkouskaya on 21.12.2016.
 */

public class ResetPasswordStep3Presenter extends ValidateInputPresenter implements ResetPasswordStep3Contract.Presenter, ResetPasswordStep3Contract.Interactor.OnFinishedListener {

    ResetPasswordStep3Contract.Interactor mInteractor = new ResetPasswordStep3Interactor();
    private int mResetType = 0;
    private int mMethodType = 0;
    private ResetPasswordStep3Contract.View mView;


    public ResetPasswordStep3Presenter(ResetPasswordStep3Contract.View view, int resetType, int methodType) {
        super(view);
        mView = view;
        this.mResetType = resetType;
        this.mMethodType = methodType;
    }

    @Override
    public void getCode(String username, String method, Context context) {
        if (InternetConnectionHelper.isConnected(context)) {

            ResetPasswordGetCodeRequest request = new ResetPasswordGetCodeRequest(username, method);
            Log.i("resetpassword", String.format("%d %s %s", mResetType, username, method));
            if (mResetType == ResetPasswordStep3Fragment.RESET_PASSWORD) {
                mInteractor.getResetCode(request, this);
            } else if (mResetType == ResetPasswordStep3Fragment.UNLOCK) {
                mInteractor.unlockGetCode(request, this);
            } else mView.showToast(R.string.connection_error);


        }
    }

    @Override
    public void sendCode(String username, String code, String password, Context context) {

        if (InternetConnectionHelper.isConnected(context)) {

            if (!username.isEmpty() && !password.isEmpty() && !code.isEmpty()) {
                ResetPasswordSendCodeRequest request = new ResetPasswordSendCodeRequest(username, password, code);
                if (mResetType == ResetPasswordStep3Fragment.RESET_PASSWORD) {
                    mInteractor.resetPassword(request, this);
                } else if (mResetType == ResetPasswordStep3Fragment.UNLOCK) {
                    mInteractor.unlockSendCode(request, this);
                }

            } else mView.showToast(R.string.error_invalid_fields);

        } else mView.showToast(R.string.connection_error);

    }

    @Override
    public void onError(APIErrorList error) {
        if (error.getError().get(0) != null)
            mView.showToast(error.getError().get(0).getMessage());
    }

    @Override
    public void onResponseGetResetCode() {
        int messageId = R.string.toast_smscode_send_to_mobile;
        if (mMethodType == ResetPasswordStep3Fragment.RESET_WITH_EMAIL) {
            messageId = R.string.toast_smscode_send_to_email;
        }

        mView.showToast(messageId);
    }

    @Override
    public void onFailureGetResetCode() {
        mView.showToast(R.string.check_your_internet_connection);

    }

    @Override
    public void onResponseResetPassword() {

        mView.showToast(R.string.account_password_reset_success);
        mView.backToLogin();

    }

    @Override
    public void onFailureResetPassword() {

        mView.showToast(R.string.check_your_internet_connection);


    }
}
