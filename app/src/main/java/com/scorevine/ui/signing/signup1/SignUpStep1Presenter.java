package com.scorevine.ui.signing.signup1;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.VerificationRequest;
import com.scorevine.api.signing.VerificationResponse;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.LoadFileHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.validation.InputTypeSV;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.regex.Pattern;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class SignUpStep1Presenter implements SignUpStep1Contract.Presenter, SignUpStep1Contract.Interactor.OnFinishedListener {

    private final SignUpStep1Contract.View mView;
    private final SignUpStep1Contract.Interactor mInteractor;

    public SignUpStep1Presenter(@NonNull SignUpStep1Contract.View view) {
        mView = view;
        mInteractor = new SignUpStep1Interactor();

    }

    @Override
    public void sendVerificationRequest(String username, String email, String code, String mobile) {
        String phoneNumber = code + "-" + mobile;
        VerificationRequest verificationRequest = new VerificationRequest(username, email, phoneNumber);

        mInteractor.sendVerification(verificationRequest, this);

    }

    @Override
    public void subscribe(SignInActivity activity) {
        mView.setUpView(activity);
        mView.setCodeSpinner(LoadFileHelper.getCountryCodesList(activity));
        mView.suggestUniqueName();
        EventBus.getDefault().register(this);

    }

    @Override
    public boolean savedToSP(FragmentActivity activity) {
        return new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SAVE_DATA_P1, "false").equals("true")
                || new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SAVE_DATA, "false").equals("true");
    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void initSPData() {
        mView.setTextWatchers();

        mView.showSavedData();
    }


    @Subscribe
    public void onEvent(NextButtonEvent event) {
       // mView.goNext();

        if (mView.checkNotNull())
            mView.sendVerification();
        else
            mView.showMessage(R.string.error_invalid_fields, true);

    }


    @Override
    public void onResponse(VerificationResponse verificationResponse) {
        if (verificationResponse.getStatus().equals("true")) {
            mView.goNext();
        }
    }


    @Override
    public void onResponseCodeSend(VerificationResponse verificationResponse) {
        if (verificationResponse.getStatus().equals("true")) {
            mView.showMessage(R.string.toast_smscode_send_to_mobile, true);
            mView.goNext();
        }
    }


    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onError(APIErrorList errorsList) {
        if (errorsList.getError() != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

        if (errorsList.getSuggestedUsername() != null)
            mView.setNewUsername(errorsList.getSuggestedUsername());
    }

    @Override
    public void onError(String error) {
        mView.showTextMessage(error, true);
    }

    @Override
    public boolean validateInput(String patternStr, CharSequence editable, int errorTextId, InputTypeSV inputType) {
        Pattern pattern = Pattern.compile(patternStr);

        if (!pattern.matcher(editable.toString()).matches()) {
            mView.showInputError(errorTextId, inputType);

        } else {
            mView.hideInputError(inputType);
        }

        return pattern.matcher(editable).matches();

    }


}
