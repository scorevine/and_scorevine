package com.scorevine.ui.signing.login;

import com.facebook.CallbackManager;
import com.scorevine.api.APIErrorList;
import com.scorevine.ui.signing.BaseViewSigning;
import com.scorevine.ui.signing.SignInActivity;

import java.util.List;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */

public interface LoginContract {

    interface View extends BaseViewSigning {

        void showInputError(int errorMessageId, int inputType);

        void hideInputError(int inputType);

        void showMessage(int messageId, boolean isLong);

        void showTextMessage(String message, boolean isLong);
        void goToMyFeed(String token);
        void goNext();
        void putFacebookData(String id, String lastName, String firstName, String email);

        void showResetPasswordMethodsDialog(String username, List<String> retreivalMethods);
    }

    interface Presenter {
        boolean validateInput(String patternStr, CharSequence editable, int errorTextId, int inputType);
        void sendLoginRequest(String username, String password);
        void subscribe(SignInActivity activity);
        void unsubscribe();

        void registerCallback(CallbackManager callbackManager, LoginFragment fragment);
    }

    interface Interactor{

        interface OnFinishedListener {
            void onResponse(String token);

            void onFailure(int responseId);
            void onError(APIErrorList errorsList);
        }

        interface OnFinishedFbListener {
            void onResponseFb(String token);

            void onFailureFb(int responseId);

            void onErrorFb(APIErrorList errorsList);
            void onUserNotFound();
        }

        void fbLogin(String token, final OnFinishedFbListener listener);
        void login(String username, String password, final OnFinishedListener listener);

    }
}
