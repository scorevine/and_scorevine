package com.scorevine.ui.signing.resetpassword.step1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.RetreivalMethods;
import com.scorevine.helpers.InternetConnectionHelper;
import com.scorevine.helpers.ValidationHelper;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment;
import com.scorevine.ui.validation.InputTypeSV;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class ResetPasswordStep1Presenter implements ResetPasswordStep1Contract.Presenter, ResetPasswordStep1Contract.Interactor.OnFinishedListener {

    private final ResetPasswordStep1Contract.View mView;
    private final ResetPasswordStep1Contract.Interactor mInteractor;

    private ValidationHelper mValidator;

    public ResetPasswordStep1Presenter(@NonNull ResetPasswordStep1Contract.View view) {
        mView = view;
        mInteractor = new ResetPasswordStep1Interactor();
        mValidator = new ValidationHelper();
    }

    @Override
    public void sendResetPasswordRequest(String username, Context context) {
        if (InternetConnectionHelper.isConnected(context)) {
            mInteractor.getResetPasswordMethods(username, this);
        } else {
            mView.showMessage(R.string.connection_error, false);
        }
    }

    @Override
    public void subscribe(SignInActivity activity) {
        mView.setListeners();
        mView.setUpView(activity);
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void validateInput(String value, InputTypeSV inputType) {
        if (!mValidator.validateMinLength(value, inputType)) {
            mView.showInputError(R.string.error_min_characters_count_4);
            return;
        }
        if (!mValidator.validateMaxLength(value, inputType)) {
            mView.showInputError(R.string.error_max_characters_count_15);
            return;
        }
        if (!mValidator.validateCharacters(value, inputType)) {
            mView.showInputError(R.string.error_unique_name_symbols);
            return;
        }
        mView.hideInputError();
    }

    @Override

    public void onResponse(RetreivalMethods response) {
        Log.e(ResetPasswordStep1Fragment.TAG_RESETPASSWORD, "onGetResponse: " + response.getRetreivalMethods().size());
        Log.e(ResetPasswordStep1Fragment.TAG_RESETPASSWORD, "onGetResponse: " + response.toString());
        Log.e(ResetPasswordStep1Fragment.TAG_RESETPASSWORD, "onGetResponse: " + response.getRetreivalMethods().get(0));


        if (response.getRetreivalMethods().size() == 1) {

            if (response.getRetreivalMethods().get(0).equals("mobile")) {
                mView.goToStep3(ResetPasswordStep3Fragment.RESET_WITH_MOBILE);
            }
            if (response.getRetreivalMethods().get(0).equals("email")) {
                mView.goToStep3(ResetPasswordStep3Fragment.RESET_WITH_EMAIL);
            }

        }
        if (response.getRetreivalMethods().size() == 2) {

            mView.goToStep2();
        }

    }

    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, true);

    }

    @Override
    public void onError(APIErrorList errorList) {
        if (errorList != null && errorList.getError() != null) {
            mView.showTextMessage(errorList.getError().get(0).getMessage(), true);
        } else {
            mView.showTextMessage("Unknown error", true);
        }

    }

}
