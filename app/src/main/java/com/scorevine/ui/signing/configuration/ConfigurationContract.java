package com.scorevine.ui.signing.configuration;

import android.content.Context;

import com.scorevine.api.APIErrorList;
import com.scorevine.ui.signing.BaseViewSigning;
import com.scorevine.ui.signing.configuration.adapters.Category;

import java.util.List;

/**
 * Created by weronikapapkouskaya on 12.12.2016.
 */

public interface ConfigurationContract {


    interface View extends BaseViewSigning {


        void showMessage(int messageId, boolean isLong);

        void setCategories(List<Category> categories);

        void goNext();


    }

    interface Presenter {

        void getCategories(Context context);

        void followCategories(List<Integer> categoriesIdList, Context context);
    }

    interface Interactor {

        interface OnFinishedListener {
            void onGetCategoriesResponse(List<Category> categories);

            void onGetCategoriesFailure(int responseId);

            void onFollowCategoriesResponse();

            void onFollowCategoriesFailure();

            void onError(APIErrorList errorsList);
        }

        void getCategories(String encodedToken, final OnFinishedListener listener);

        void followCategories(String encodedToken, String subcategoriesIdList, final OnFinishedListener listener);
    }

}
