package com.scorevine.ui.signing;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;

import com.scorevine.R;

/**
 * Created by NIKAK
 * Ready4S
 * on 09.12.2016 15:07
 */
public class CustomTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @NonNull
    public Spannable getSpannable(String format, Context context) {
        Spannable text = new SpannableString(format);
        text.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorTextAccent)), 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return text;
    }
}
