package com.scorevine.ui.signing.signup3;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.scorevine.R;
import com.scorevine.api.APIErrorList;
import com.scorevine.api.signing.LoginResponse;
import com.scorevine.eventbus.NextButtonEvent;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.ui.signing.SignInActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Agnieszka Duleba on 2016-12-06.
 */

public class SignUpStep3Presenter implements SignUpStep3Contract.Presenter, SignUpStep3Contract.Interactor.OnFinishedListener {

    private final SignUpStep3Contract.View mView;
    private final SignUpStep3Contract.Interactor mInteractor;
    private final FragmentActivity mActivity;

    public SignUpStep3Presenter(@NonNull SignUpStep3Contract.View view, FragmentActivity activity) {
        mView = view;
        mInteractor = new SignUpStep3Interactor();
        mActivity = activity;
    }

    @Override
    public void subscribe(SignInActivity activity) {
        mView.setUpView(activity);
        mView.setUpWebview();
        EventBus.getDefault().register(this);

    }

    @Override
    public void unsubscribe() {
        EventBus.getDefault().unregister(this);

    }

    @Subscribe
    public void onEvent(NextButtonEvent event) {
        if (mView.isBtnChecked()) {
            register(mActivity);
            new SharedPrefHelper().addSharedPref(mActivity, SharedPrefHelper.SP_TERMS_OF_SERVICE, "true");
            mView.goToLoading();


        } else {
            mView.showMessage(R.string.sigup_error_accept, true);
        }
    }

    @Override
    public void onResponse(LoginResponse response) {
        if (response.getToken().length() > 0) {

            SharedPrefHelper sharedPrefHelper = new SharedPrefHelper();
            sharedPrefHelper.clearSharedPref(mActivity);
            sharedPrefHelper.addSharedPref(mActivity, SharedPrefHelper.SP_TOKEN, response.getToken());

            mView.goNext(response.getToken());

            //   mView.showTextMessage(response.getToken().toString(), true);
        } else{
            mView.goToRegisterFailed();
        }

    }

    @Override
    public void onFailure(int responseId) {
        mView.showMessage(responseId, true);
        mView.goToRegisterFailed();

    }

    @Override
    public void onError(APIErrorList errorsList) {

        if (errorsList.getError().get(0) != null)
            mView.showTextMessage(errorsList.getError().get(0).getMessage(), false);

        mView.goBack();

    }

    @Override
    public void onError(String error) {
        mView.showTextMessage(error, true);
        mView.goBack();

    }

    @Override
    public void register(Activity activity) {

        String mobileStr = (new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_CODE, "") + "-" +
                new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_MOBILE, ""));
        String birthDate = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_BIRTHDATE, "");
        String birthDateFormattedStr = DateFormatHelper.changeDateFormat(DateFormatHelper.APP_PROFILE_DATE_FORMAT, DateFormatHelper.API_PROFILE_DATE_FORMAT, birthDate);

        String passwordStr = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_PASSWORD, "");
        String facebookIdStr = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_FB_ID, "");

        String referal = "";
        String uuidStr = "";
        String notificationTokenStr = "";

        MultipartBody.Part filePart = null;
        String imagePath = new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_PHOTO, "");
        if(!imagePath.equals("")) {
            Uri imageUri = Uri.parse(imagePath.substring(7, imagePath.length()));
            File file = new File(imageUri.getPath());
            if(file!=null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                filePart = MultipartBody.Part.createFormData("profile_image", file.getName(), requestFile);
            }
        }

        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), mobileStr);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_EMAIL, ""));
        RequestBody uniqueName = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_UNIQUENAME, ""));
        RequestBody password = RequestBody.create(MediaType.parse("text/plain"), passwordStr);
        RequestBody facebookId = RequestBody.create(MediaType.parse("text/plain"), facebookIdStr);
        RequestBody birthDateFormatted = RequestBody.create(MediaType.parse("text/plain"), birthDateFormattedStr);
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_GENDER, ""));
        RequestBody country = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_COUNTRY, ""));
        RequestBody firstName = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_FIRST_NAME, ""));
        RequestBody surname = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SURNAME, ""));
        RequestBody referral = RequestBody.create(MediaType.parse("text/plain"),  referal );
        RequestBody smscode = RequestBody.create(MediaType.parse("text/plain"), new SharedPrefHelper().getSharedPref(activity, SharedPrefHelper.SP_SMSCODE, ""));
        RequestBody uuid = RequestBody.create(MediaType.parse("text/plain"), uuidStr);
        RequestBody notificationToken = RequestBody.create(MediaType.parse("text/plain"), notificationTokenStr);

        mInteractor.sendRegister(email, mobile, uniqueName, password, facebookId,
                birthDateFormatted, gender, country, firstName, surname,
                referral, filePart, smscode, uuid, notificationToken, this);
    }
}