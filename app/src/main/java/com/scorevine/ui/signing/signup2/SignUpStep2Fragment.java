package com.scorevine.ui.signing.signup2;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.ListPopupWindow;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.scorevine.R;
import com.scorevine.ui.dialogs.GalleryDialogFragment;
import com.scorevine.ui.dialogs.ResendDialogFragment;
import com.scorevine.eventbus.SetHelpDialogsEvent;
import com.scorevine.helpers.DateFormatHelper;
import com.scorevine.helpers.FragmentHelper;
import com.scorevine.helpers.KeyboardHelper;
import com.scorevine.helpers.SharedPrefHelper;
import com.scorevine.helpers.TooltipHelper;
import com.scorevine.model.CountryCodes;
import com.scorevine.model.signing.SignUpInfoPart2;
import com.scorevine.ui.BaseActivity;
import com.scorevine.ui.signing.CropPhotoActivity;
import com.scorevine.ui.signing.SignInActivity;
import com.scorevine.ui.signing.signup3.SignUpStep3Fragment;
import com.tooltip.Tooltip;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static com.scorevine.ui.signing.CropPhotoActivity.CAMERA_REQUEST_CODE;
import static com.scorevine.ui.signing.CropPhotoActivity.GALLERY_REQUEST_CODE;

/**
 * Created by Agnieszka Duleba on 2016-11-29.
 */

public class SignUpStep2Fragment extends Fragment implements SignUpStep2Contract.View {

    public static final String TAG_FRAGMENT_REGISTER2 = SignUpStep2Fragment.class.getSimpleName();
    public static final String TAG_FRAGMENT_REGISTER2_BUNDLE = "part2 data";

    @Nullable
    @BindView(R.id.date_of_birth_ti)
    TextInputLayout mBirthDateTI;

    @Nullable
    @BindView(R.id.gender_ti)
    TextInputLayout mGenderTI;

    @Nullable
    @BindView(R.id.country_ti)
    TextInputLayout mCountryTI;

    @Nullable
    @BindView(R.id.sms_verification_ti)
    TextInputLayout mSmsVerificationTI;

    @Nullable
    @BindView(R.id.referral_codde_ti)
    TextInputLayout mRefferalCodeTI;

    String mBirthdate = "";
    String mGender = "";
    String mCountryName = "";
    String mSmsVerificationCode = "";
    String mReferralCode = "";

    @BindView(R.id.avatar_iv)
    ImageView mAvatarIV;

    @BindArray(R.array.genders)
    String[] mGenders;
    String[] mCountries;

    ListPopupWindow listPopupWindow;
    ListPopupWindow listPopupWindow2;

    Calendar myCalendar = Calendar.getInstance();
    SignUpStep2Contract.Presenter mPresenter;
    SignUpInfoPart2 signUpInfo;
    int countryCodePosition = -1;
    private Tooltip mReferralTooltip;
    private Tooltip mSmsCodeTooltip;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register_s2, container, false);
        ButterKnife.bind(this, view);
        mPresenter = new SignUpStep2Presenter(this);
        signUpInfo = new SignUpInfoPart2();

        mPresenter.initSPData();
        myCalendar.add(Calendar.YEAR, -21);
        myCalendar.set(Calendar.MONTH, 0);
        myCalendar.set(Calendar.DAY_OF_MONTH, 1);

        KeyboardHelper.hideKeyboard(getView(), getActivity());
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (getArguments() != null) {
            countryCodePosition = getArguments().getInt(getString(R.string.country_code_bundle_data), -1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe((SignInActivity) getActivity());

    }

    @Override
    public void onPause() {
        mPresenter.unsubscribe();
        super.onPause();
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            signUpInfo = (SignUpInfoPart2) savedInstanceState.getSerializable(TAG_FRAGMENT_REGISTER2_BUNDLE);
            if (signUpInfo != null) {
                mGenderTI.getEditText().setText(signUpInfo.gender);
                mSmsVerificationTI.getEditText().setText(signUpInfo.smsCode);
                mCountryTI.getEditText().setText(signUpInfo.country);
                mRefferalCodeTI.getEditText().setText(signUpInfo.refferal);
                mBirthDateTI.getEditText().setText(signUpInfo.birthdate);
                if (signUpInfo.photoUri != null)
                    mAvatarIV.setImageURI(Uri.parse(signUpInfo.photoUri));

            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TAG_FRAGMENT_REGISTER2_BUNDLE, signUpInfo);
        //Save the fragment's state here
    }

    @OnClick(R.id.country_et)
    public void onCountryClick() {
        KeyboardHelper.hideKeyboard(getView(), getActivity());
        listPopupWindow.show();
    }

    @OnClick(R.id.gender_et)
    public void onGenderClick() {
        KeyboardHelper.hideKeyboard(getView(), getActivity());
        listPopupWindow2.show();
    }

    @OnClick(R.id.date_of_birth_et)
    public void onBirthDateClick() {
        KeyboardHelper.hideKeyboard(getView(), getActivity());
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                mBirthDateTI.getEditText().setText(DateFormatHelper.getDateWithFormat(DateFormatHelper.APP_PROFILE_DATE_FORMAT, myCalendar.getTime()));
                mBirthdate = mBirthDateTI.getEditText().getText().toString();

            }
        };

        Date date1 = new Date();
        Calendar calendar21 = Calendar.getInstance();
        calendar21.setTime(date1);
        calendar21.add(Calendar.YEAR, -21);

        long time = calendar21.getTime().getTime();
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(time);
        datePickerDialog.show();
    }

    protected ResendDialogFragment resendDialog;

    @OnClick(R.id.resend_sms_iv)
    public void onResendClick() {
        String mobile = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_CODE, "")
                + new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_MOBILE, "");
        String formattedString = String.format(getString(R.string.signup_help_resend), mobile);
        resendDialog = ResendDialogFragment.newInstance(new Bundle(), formattedString, getString(R.string.ok_btn), null, false);

        resendDialog.show(getFragmentManager(), "dialog_referral");

    }

    public void onResendOkClicked() {
        mPresenter.resendSmsCode(new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_UNIQUENAME, ""),
                new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_EMAIL, ""),
                new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_CODE, ""),
                new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_MOBILE, ""));

        if (resendDialog != null)
            resendDialog.dismiss();
    }

    public void onResendCancelClicked() {
        if (resendDialog != null)
            resendDialog.dismiss();
    }

    @Subscribe
    public void onEvent(SetHelpDialogsEvent event) {
        showTextMessage(event.message, true);
    }


    @OnClick(R.id.referral_help_iv)
    public void onReferralHelpClick() {
        mReferralTooltip.show();
    }

    @Override
    public void setGenderSpinner() {
        listPopupWindow2 = new ListPopupWindow(getActivity());
        listPopupWindow2.setAdapter(new ArrayAdapter(getActivity(), R.layout.dropdown_item, mGenders));
        listPopupWindow2.setAnchorView(mGenderTI.getEditText());
        listPopupWindow2.setModal(true);
        listPopupWindow2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    mGender = mGenders[position];
                    mGenderTI.getEditText().setText(mGender);
                    listPopupWindow2.dismiss();
                } else
                    mGender = null;
            }
        });
    }

    @Override
    public void setCountrySpinner(List<CountryCodes.CountryCode> countryCodesList) {

        final List<String> countriesList = new ArrayList<>();

        for (CountryCodes.CountryCode element : countryCodesList) {
            countriesList.add(element.getCountryName());
        }

        listPopupWindow = new ListPopupWindow(getActivity());
        listPopupWindow.setAdapter(new ArrayAdapter(getContext(), R.layout.dropdown_item, countriesList));
        listPopupWindow.setAnchorView(mCountryTI.getEditText());
        listPopupWindow.setModal(true);
        if (countryCodePosition != -1) {
            mCountryTI.getEditText().setText(countriesList.get(countryCodePosition));
        }
        listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != -1) {
                    mCountryName = countriesList.get(position);
                    mCountryTI.getEditText().setText(mCountryName);
                    listPopupWindow.dismiss();
                } else
                    mCountryName = null;
            }
        });
    }

    @Override
    public void goNext() {
        saveToSharePref();
        FragmentHelper.replaceFragment(getFragmentManager(), new SignUpStep3Fragment(), SignUpStep3Fragment.TAG_FRAGMENT_REGISTER3);

    }

    @Override
    public void showSavedData() {
        if (mPresenter.savedToSP(getActivity())) {

            signUpInfo.birthdate = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_BIRTHDATE, null);
            signUpInfo.smsCode = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_SMSCODE, null);
            signUpInfo.gender = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_GENDER, null);
            signUpInfo.country = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_COUNTRY, null);
            signUpInfo.photoUri = new SharedPrefHelper().getSharedPref(getActivity(), SharedPrefHelper.SP_PHOTO, null);

            mBirthDateTI.getEditText().setText(signUpInfo.birthdate);
            mCountryTI.getEditText().setText(signUpInfo.country);
            mSmsVerificationTI.getEditText().setText(signUpInfo.smsCode);
            mGenderTI.getEditText().setText(signUpInfo.gender);
            mAvatarIV.setImageURI(Uri.parse(signUpInfo.photoUri));

        }
    }

    @Override
    public void saveToSharePref() {
        Activity activity = getActivity();
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_BIRTHDATE, mBirthDateTI.getEditText().getText().toString());
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_GENDER, mGenderTI.getEditText().getText().toString());
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_COUNTRY, mCountryTI.getEditText().getText().toString());
        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_SMSCODE, mSmsVerificationTI.getEditText().getText().toString());

        if (signUpInfo.photoUri != null)
            new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_PHOTO, signUpInfo.photoUri);

        new SharedPrefHelper().addSharedPref(activity, SharedPrefHelper.SP_SAVE_DATA_P2, "true");

    }

    @Override
    public boolean checkNotNull() {
        if (mBirthDateTI.getEditText().getText().length() == 0 ||
                mGenderTI.getEditText().getText().length() == 0 ||
                mCountryTI.getEditText().getText().length() == 0 ||
                mSmsVerificationTI.getEditText().getText().length() == 0) {
            return false;
        } else if (mBirthDateTI.isErrorEnabled() ||
                mGenderTI.isErrorEnabled() ||
                mCountryTI.isErrorEnabled() ||
                mSmsVerificationTI.isErrorEnabled() ||
                mRefferalCodeTI.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void setTextWatchers() {

        mBirthDateTI.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.birthdate = editable.toString();
            }
        });

        mGenderTI.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.gender = editable.toString();

            }
        });

        mCountryTI.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.country = editable.toString();

            }
        });

        mSmsVerificationTI.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() != 6) {
                    mSmsVerificationTI.setErrorEnabled(true);
                    mSmsVerificationTI.setError(getString(R.string.sms_code_6_numbers));
                } else {
                    mSmsVerificationTI.setErrorEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.smsCode = editable.toString();
            }
        });

        mRefferalCodeTI.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                signUpInfo.refferal = editable.toString();

            }
        });

    }

    @Override
    public void showMessage(int messageId, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(getString(messageId), isLong);
    }

    @Override
    public void showTextMessage(String message, boolean isLong) {
        ((BaseActivity) getActivity()).showToast(message, isLong);
    }

    @OnClick(R.id.upload_btn)
    public void onUploadPhotoButtonClick() {
       GalleryDialogFragment galleryDialogFragment = GalleryDialogFragment.newInstance(
                   new Bundle(),
                   getString(R.string.choose_image_label),
                   getString(R.string.camera_btn),
                   getString(R.string.gallery_btn), false);
        galleryDialogFragment.show(getChildFragmentManager(), "gallery_dialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) && resultCode == RESULT_OK) {
            signUpInfo.photoUri = data.getExtras().getString(CropPhotoActivity.BUNDLE_EXTRA_PHOTO_URI);
            mAvatarIV.setImageURI(Uri.parse(signUpInfo.photoUri));
        }
    }

    @Override
    public void setUpView(SignInActivity activity) {

        activity.setBottomButtonVisibility(true);
        activity.setBottomButtonText(getString(R.string.signup_next_button));
        activity.setToolbarVisibility(true);
        setOnReferralOnFocusedChangedListener();
        setOnSmsCodeOnFocusedChangedListener();
    }

    public void setOnReferralOnFocusedChangedListener() {
        mReferralTooltip = TooltipHelper.createToolTipDialogBox(mRefferalCodeTI.getEditText(),
                R.string.signup_referral_help, Gravity.BOTTOM, getContext());

        mRefferalCodeTI.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ((SignInActivity) getActivity()).scrollBy(100);
                    if (mRefferalCodeTI.getEditText().getText().toString().isEmpty()) {
                        mReferralTooltip.show();
                    }
                } else {
                    mReferralTooltip.dismiss();
                }
            }
        });
    }

    public void setOnSmsCodeOnFocusedChangedListener() {
        mSmsCodeTooltip = TooltipHelper.createToolTipDialogBox(mSmsVerificationTI.getEditText(),
                R.string.signup_sms_code_help, Gravity.BOTTOM, getContext());

        mSmsVerificationTI.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ((SignInActivity) getActivity()).scrollBy(100);
                    if (mSmsVerificationTI.getEditText().getText().toString().isEmpty()) {
                        mSmsCodeTooltip.show();
                    }
                } else {
                    mSmsCodeTooltip.dismiss();
                }
            }
        });
    }
}
