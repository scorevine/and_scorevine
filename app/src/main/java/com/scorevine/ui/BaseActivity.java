package com.scorevine.ui;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.prashantsolanki.secureprefmanager.SecurePrefManagerInit;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Agnieszka Duleba on 2016-12-07.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        //Fabric.with(this, new Crashlytics());
    }


    public void showToast(String message, boolean isLong){
        if(isLong)
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public static Activity getActivity(){
        return getActivity();
    }
}
