package com.scorevine.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.ui.my_profile.profile_settings.account_settings.mobile.ChangeMobileFragment;

/**
 * Created by Agnieszka Duleba on 2016-12-21.
 */

public class SmsCodeDialogFragment  extends CustomDialogFragment{

    ChangeMobileFragment changeMobileFragment;

    public static SmsCodeDialogFragment newInstance(Bundle bundle, String message, String leftBtnText, String rightBtnText, boolean isInputLabel) {
        SmsCodeDialogFragment f = new SmsCodeDialogFragment();
        bundle.putString(CustomDialogFragment.DIALOG_MESSAGE_BUNDLE, message);
        bundle.putString(CustomDialogFragment.DIALOG_LEFT_BUNDLE, leftBtnText);
        bundle.putString(CustomDialogFragment.DIALOG_RIGHT_BUNDLE, rightBtnText);
        bundle.putBoolean(CustomDialogFragment.DIALOG_INPUT_BUNDLE, isInputLabel);
        f.setArguments(bundle);
        return f;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        changeMobileFragment = ((ChangeMobileFragment) getActivity().getSupportFragmentManager().findFragmentByTag(ChangeMobileFragment.TAG_FRAGMENT_CHANGE_MOBILE));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onLeftClick(){
        changeMobileFragment.onResendClicked();
    }

    @Override
    public void onRightClick(){
        if(!mInputLayout.getEditText().getText().toString().isEmpty())
        changeMobileFragment.onConfirmClicked(mInputLayout.getEditText().getText().toString());

    }
}
