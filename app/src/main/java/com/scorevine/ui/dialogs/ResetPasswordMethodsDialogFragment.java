package com.scorevine.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.helpers.FragmentHelper;
import com.scorevine.ui.signing.resetpassword.step3.ResetPasswordStep3Fragment;

/**
 * Created by NIKAK
 * Ready4S
 * on 23.12.2016 12:28
 */
public class ResetPasswordMethodsDialogFragment extends CustomDialogFragment {

    private String username;

    public static ResetPasswordMethodsDialogFragment newInstance(String message, String leftBtnText, String rightBtnText, String username) {
        ResetPasswordMethodsDialogFragment f = new ResetPasswordMethodsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CustomDialogFragment.DIALOG_MESSAGE_BUNDLE, message);
        bundle.putString(CustomDialogFragment.DIALOG_LEFT_BUNDLE, leftBtnText);
        bundle.putString(CustomDialogFragment.DIALOG_RIGHT_BUNDLE, rightBtnText);
        bundle.putString(ResetPasswordStep3Fragment.USERNAME_ARG, username);
        f.setArguments(bundle);
        return f;
    }

    public static ResetPasswordMethodsDialogFragment newInstance(String message, String leftBtnText, String username) {
        ResetPasswordMethodsDialogFragment f = new ResetPasswordMethodsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CustomDialogFragment.DIALOG_MESSAGE_BUNDLE, message);
        bundle.putString(CustomDialogFragment.DIALOG_LEFT_BUNDLE, leftBtnText);
        bundle.putString(ResetPasswordStep3Fragment.USERNAME_ARG, username);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if (getArguments() != null) {
            username = getArguments().getString(ResetPasswordStep3Fragment.USERNAME_ARG);
        }
        return view;
    }

    @Override
    public void onLeftClick() {
        if (mLeftButton.getText().toString().equals("mobile")) {
            resetWith(ResetPasswordStep3Fragment.RESET_WITH_MOBILE);
        } else if (mLeftButton.getText().toString().equals("email")) {
            resetWith(ResetPasswordStep3Fragment.RESET_WITH_EMAIL);
        }
    }

    private void resetWith(int resetType) {
        FragmentHelper.replaceFragment(getParentFragment().getActivity().getSupportFragmentManager(),
                ResetPasswordStep3Fragment.getInstance(resetType, ResetPasswordStep3Fragment.UNLOCK, username),
                ResetPasswordStep3Fragment.RESET_PASSWORD_STEP3_TAG);
        dismiss();
    }

    @Override
    public void onRightClick() {
        if (mRightButton.getText().toString().equals("mobile")) {
            resetWith(ResetPasswordStep3Fragment.RESET_WITH_MOBILE);
        } else if (mRightButton.getText().toString().equals("email")) {
            resetWith(ResetPasswordStep3Fragment.RESET_WITH_EMAIL);
        }
    }
}
