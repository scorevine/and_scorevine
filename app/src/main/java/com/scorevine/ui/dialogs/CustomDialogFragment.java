package com.scorevine.ui.dialogs;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.scorevine.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by NIKAK
 * Ready4S
 * on 30.11.2016 13:20
 */
public class CustomDialogFragment extends AppCompatDialogFragment {
    public static final String DIALOG_MESSAGE_BUNDLE = "message";
    public static final String DIALOG_LEFT_BUNDLE = "right btn";
    public static final String DIALOG_RIGHT_BUNDLE = "left btn";
    public static final String DIALOG_INPUT_BUNDLE = "input hint";

    @BindView(R.id.messageTextView)
    TextView mMessageTextView;
    @BindView(R.id.buttonLeft)
    TextView mLeftButton;
    @BindView(R.id.buttonRight)
    TextView mRightButton;
    @BindView(R.id.buttonsLinearLayout)
    LinearLayout mButtonsLinearLayout;
    @BindView(R.id.inputLayout)
    TextInputLayout mInputLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment, container, false);
        ButterKnife.bind(this, view);

        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        hideButtons();
        hideInput();

        if (getArguments() != null) {
            String message = getArguments().getString(DIALOG_MESSAGE_BUNDLE);
            setMessage(message);
            final String rightBtn = getArguments().getString(DIALOG_RIGHT_BUNDLE);
            final String leftBtn = getArguments().getString(DIALOG_LEFT_BUNDLE);
            final boolean isInput = getArguments().getBoolean(DIALOG_INPUT_BUNDLE);
            if (rightBtn != null && !rightBtn.isEmpty()) {
                setRightButton(rightBtn);
            }
            if (leftBtn != null && !leftBtn.isEmpty()) {
                setLeftButton(leftBtn);
            }
            if (isInput) {
                showInput();
            }
        }
        return view;
    }

    public void setMessage(String message) {
        mMessageTextView.setText(message);
    }

    public void setMessage(SpannableString message) {
        mMessageTextView.setText(message);
    }

    public void setRightButton(String name) {
        mButtonsLinearLayout.setVisibility(View.VISIBLE);
        mRightButton.setVisibility(View.VISIBLE);
        mRightButton.setText(name);
    }

    public void setLeftButton(String name) {
        mButtonsLinearLayout.setVisibility(View.VISIBLE);
        mLeftButton.setVisibility(View.VISIBLE);
        mLeftButton.setText(name);
    }

    public void hideButtons() {
        mButtonsLinearLayout.setVisibility(View.GONE);
        mLeftButton.setVisibility(View.GONE);
        mRightButton.setVisibility(View.GONE);


    }

    public void hideInput() {
        mInputLayout.setVisibility(View.GONE);
    }

    public void showInput() {
        mInputLayout.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.buttonLeft)
    public void onLeftClick() {

    }


    @OnClick(R.id.buttonRight)
    public void onRightClick() {
    }


}
