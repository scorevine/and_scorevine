package com.scorevine.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.ui.my_vine.tag_suggest.SubmitEvidenceFragment;

/**
 * Created by Agnieszka Duleba on 2016-12-14.
 */

public class ConfirmDialogFragment extends CustomDialogFragment{

    SubmitEvidenceFragment mSubmitEvidenceFragment;

    public static ConfirmDialogFragment newInstance(Bundle bundle, String message, String leftBtnText, String rightBtnText, boolean inputLabel) {
        ConfirmDialogFragment f = new ConfirmDialogFragment();
        bundle.putString(CustomDialogFragment.DIALOG_MESSAGE_BUNDLE, message);
        bundle.putString(CustomDialogFragment.DIALOG_LEFT_BUNDLE, leftBtnText);
        bundle.putString(CustomDialogFragment.DIALOG_RIGHT_BUNDLE, rightBtnText);
        bundle.putBoolean(CustomDialogFragment.DIALOG_INPUT_BUNDLE, inputLabel);
        f.setArguments(bundle);
        return f;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mSubmitEvidenceFragment = ((SubmitEvidenceFragment) getActivity().getSupportFragmentManager().findFragmentByTag(SubmitEvidenceFragment.SUBMIT_EVIDENCE_FRAGMENT));

        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onLeftClick(){
        mSubmitEvidenceFragment.onConfirmClicked();
    }

    @Override
    public void onRightClick(){
    }
}
