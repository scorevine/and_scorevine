package com.scorevine.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.ui.signing.signup2.SignUpStep2Fragment;

/**
 * Created by Agnieszka Duleba on 2016-12-14.
 */

public class ResendDialogFragment extends CustomDialogFragment{

    SignUpStep2Fragment mSignUpStep2Fragment;

    public static ResendDialogFragment newInstance(Bundle bundle, String message, String leftBtnText, String rightBtnText, boolean inputLabel) {
        ResendDialogFragment f = new ResendDialogFragment();
        bundle.putString(CustomDialogFragment.DIALOG_MESSAGE_BUNDLE, message);
        bundle.putString(CustomDialogFragment.DIALOG_LEFT_BUNDLE, leftBtnText);
        bundle.putString(CustomDialogFragment.DIALOG_RIGHT_BUNDLE, rightBtnText);
        bundle.putBoolean(CustomDialogFragment.DIALOG_INPUT_BUNDLE, inputLabel);
        f.setArguments(bundle);
        return f;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mSignUpStep2Fragment = ((SignUpStep2Fragment) getActivity().getSupportFragmentManager().findFragmentByTag(SignUpStep2Fragment.TAG_FRAGMENT_REGISTER2));

        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onLeftClick(){
        mSignUpStep2Fragment.onResendOkClicked();
    }

    @Override
    public void onRightClick(){
    }
}
