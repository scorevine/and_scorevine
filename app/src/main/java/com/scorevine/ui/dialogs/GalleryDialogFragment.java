package com.scorevine.ui.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scorevine.helpers.FragmentHelper;
import com.scorevine.ui.my_vine.tag_suggest.SuggestTagFragment;
import com.scorevine.ui.signing.CropPhotoActivity;
import com.scorevine.ui.signing.signup2.SignUpStep2Fragment;

import static com.scorevine.ui.signing.CropPhotoActivity.CAMERA_REQUEST_CODE;
import static com.scorevine.ui.signing.CropPhotoActivity.GALLERY_REQUEST_CODE;
import static com.scorevine.ui.signing.CropPhotoActivity.PHOTO_EXTRA;
import static com.scorevine.ui.signing.CropPhotoActivity.verifyStoragePermissions;

/**
 * Created by Agnieszka Duleba on 2016-12-14.
 */

public class GalleryDialogFragment extends CustomDialogFragment {

    public static GalleryDialogFragment newInstance(Bundle bundle, String message, String leftBtnText, String rightBtnText, boolean inputLabel) {
        GalleryDialogFragment f = new GalleryDialogFragment();
        bundle.putString(CustomDialogFragment.DIALOG_MESSAGE_BUNDLE, message);
        bundle.putString(CustomDialogFragment.DIALOG_LEFT_BUNDLE, leftBtnText);
        bundle.putString(CustomDialogFragment.DIALOG_RIGHT_BUNDLE, rightBtnText);
        bundle.putBoolean(CustomDialogFragment.DIALOG_INPUT_BUNDLE, inputLabel);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        verifyStoragePermissions(getActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void startCropPhotoActivity(int requestCode) {
        Intent intent = new Intent(getActivity(), CropPhotoActivity.class);
        intent.putExtra(PHOTO_EXTRA, requestCode);
        getActivity().startActivityForResult(intent, requestCode);
        dismiss();
    }

    @Override
    public void onLeftClick() {
        startCropPhotoActivity(CAMERA_REQUEST_CODE);
    }

    @Override
    public void onRightClick() {
        startCropPhotoActivity(GALLERY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(FragmentHelper.getVisibleFragment(getFragmentManager()) instanceof SignUpStep2Fragment ||
                FragmentHelper.getVisibleFragment(getFragmentManager()) instanceof SuggestTagFragment) {

            Fragment fragment = FragmentHelper.getVisibleFragment(getFragmentManager());

            if (requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) {
                fragment.onActivityResult(requestCode, resultCode, data);
                dismissAllowingStateLoss();
            }
        }
    }
}
