package com.scorevine.ui.validation;

import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.TextView;

import com.scorevine.R;
import com.scorevine.ui.signing.CustomTextWatcher;

/**
 * Created by NIKAK
 * Ready4S
 * on 27.12.2016 14:46
 */
public abstract class ValidateInputFragment extends Fragment implements ValidateInputView {

    ValidateInputPresenterInterface mPresenter = new ValidateInputPresenter(this);

    @Override
    public void hideInputError(InputTypeSV inputType) {
        TextInputLayout textInputLayout = getTextInputLayout(inputType);
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(false);
        }
    }

    protected void setEmailTextChangedListener() {

        EditText mEmailET = getTextInputLayout(InputTypeSV.EMAIL).getEditText();
        mEmailET.addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                mPresenter.validateInput(charSequence.toString(), InputTypeSV.EMAIL, getContext());
            }

        });
    }

    public void showInputError(String errorMessage, InputTypeSV inputType) {
        TextInputLayout textInputLayout = getTextInputLayout(inputType);
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorMessage);
        }

    }

    public void setPasswordTextChangedListener(final InputTypeSV inputType) {
        EditText passwordET = getTextInputLayout(inputType).getEditText();
        passwordET.addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() >= 8) {
                    mPresenter.validateInput(charSequence.toString(), inputType, getContext());
                } else {
                    showInputError(getString(R.string.error_min_characters_count_8), inputType);
                }

            }
        });
    }

    protected void setConfirmPasswordTextChangedListener() {
        EditText confirmPasswordET = getTextInputLayout(InputTypeSV.CONFIRM_PASSWORD).getEditText();

        confirmPasswordET.addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() >= 8) {
                    if (mPresenter.validateInput(charSequence.toString(), InputTypeSV.CONFIRM_PASSWORD, getContext())) {
                        EditText passwordET = getTextInputLayout(InputTypeSV.PASSWORD).getEditText();
                        if (passwordET.length() > 0) {
                            if (passwordET.getText().toString().equals(charSequence.toString())) {
                                hideInputError(InputTypeSV.CONFIRM_PASSWORD);
                                hideInputError(InputTypeSV.PASSWORD);
                            } else {
                                showInputError(getString(R.string.error_passwords_dont_match), InputTypeSV.CONFIRM_PASSWORD);
                            }
                        }
                    }
                } else {
                    showInputError(getString(R.string.error_min_characters_count_8), InputTypeSV.CONFIRM_PASSWORD);
                }
            }
        });
    }

    protected void setPasswordTextChangedListener() {
        EditText passwordET = getTextInputLayout(InputTypeSV.PASSWORD).getEditText();
        passwordET.addTextChangedListener(new CustomTextWatcher() {

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                if (charSequence.length() >= 8) {
                    if (mPresenter.validateInput(charSequence.toString(), InputTypeSV.PASSWORD, getContext())) {
                        EditText confirmPasswordET = getTextInputLayout(InputTypeSV.CONFIRM_PASSWORD).getEditText();
                        if (confirmPasswordET.getText().length() > 0) {
                            if (confirmPasswordET.getText().toString().equals(charSequence.toString())) {
                                hideInputError(InputTypeSV.CONFIRM_PASSWORD);
                                hideInputError(InputTypeSV.PASSWORD);
                            } else {
                                showInputError(getString(R.string.error_passwords_dont_match), InputTypeSV.PASSWORD);
                            }
                        }
                    }
                } else {
                    showInputError(getString(R.string.error_min_characters_count_8), InputTypeSV.PASSWORD);
                }
            }
        });
    }

    protected void setPhoneNumberTextChangedListener() {
        TextView editText = getTextInputLayout(InputTypeSV.PHONE_NUMBER).getEditText();
        editText.addTextChangedListener(new CustomTextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    return;
                }
                mPresenter.validateInput(charSequence.toString(), InputTypeSV.PHONE_NUMBER, getContext());
            }
        });
    }

    protected abstract TextInputLayout getTextInputLayout(InputTypeSV inputType);
}
