package com.scorevine.ui.validation;

/**
 * Created by weronikapapkouskaya on 22.12.2016.
 */
public enum InputTypeSV {
    EMAIL,
    PHONE_CODE,
    PHONE_NUMBER,
    FIRST_NAME(2, 15),
    SURNAME(2, 15),
    UNIQUE_NAME(4, 15),
    CURRENT_PASSWORD(8, 50),
    PASSWORD(8, 50),
    CONFIRM_PASSWORD(8, 50),
    CODE(6, 6),
    URI(1,100),
    TAG_NAME;

    private int minLength;
    private int maxLength;

    InputTypeSV() {
        minLength = 0;
        maxLength = Integer.MAX_VALUE;
    }

    InputTypeSV(int minLength, int maxLength) {
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    public int minLength() {
        return minLength;
    }

    public int maxLength() {
        return maxLength;
    }
}
