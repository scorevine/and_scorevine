package com.scorevine.ui.validation;

import android.content.Context;

/**
 * Created by NIKAK
 * Ready4S
 * on 27.12.2016 15:47
 */
public interface ValidateInputPresenterInterface {
    boolean validateInput(String value, InputTypeSV inputType, Context context);
}
