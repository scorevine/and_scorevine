package com.scorevine.ui.validation;

/**
 * Created by NIKAK
 * Ready4S
 * on 27.12.2016 14:51
 */
public interface ValidateInputView {

    void hideInputError(InputTypeSV inputType);

    void showInputError(String errorMessage, InputTypeSV inputType);
}
