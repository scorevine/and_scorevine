package com.scorevine.ui.validation;

import android.content.Context;

import com.scorevine.R;
import com.scorevine.helpers.ValidationHelper;

/**
 * Created by NIKAK
 * Ready4S
 * on 27.12.2016 14:57
 */
public class ValidateInputPresenter implements ValidateInputPresenterInterface {
    public ValidateInputView mValidateInputView;
    private ValidationHelper mValidator = new ValidationHelper();

    public ValidateInputPresenter(ValidateInputView mView) {
        this.mValidateInputView = mView;
    }

    @Override
    public boolean validateInput(String value, InputTypeSV inputType, Context context) {
        boolean isValid = true;
        if (!mValidator.validateMinLength(value, inputType)) {
            mValidateInputView.showInputError(String.format(context.getString(R.string.error_min_characters_count), inputType.minLength()), inputType);
            return !isValid;
        }
        if (!mValidator.validateMaxLength(value, inputType)) {
            mValidateInputView.showInputError(String.format(context.getString(R.string.error_max_characters_count), inputType.maxLength()), inputType);
            return !isValid;
        }
        if (!mValidator.validateCharacters(value, inputType)) {
            mValidateInputView.showInputError(context.getString(mValidator.getError(inputType)), inputType);
            return !isValid;
        }
        mValidateInputView.hideInputError(inputType);
        return isValid;

    }
}
